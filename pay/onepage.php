<?php
/* -------Start Order Number Generate --------------- */
$length = 10;
$randomString = substr(str_shuffle(md5(time())),0,$length);
/* -------End Order Number Generate --------------- */

	//- Customer/Order Details - //
	$UserTitle = "Mr";
	$UserFirstname = "Stephen";
	$UserSurname = "Ward";
	$BillHouseNumber = "49";
	$Ad1 = "Lullingstone Lane";
	$Ad2 = "Central Areas";
	$BillTown = "Hither Green";
	$BillCountry = "UNITED KINGDOM";
	$Pcde = "SE13 6UH";
	$ContactTel = "07747429755";
	$ShopperEmail = "stephen.ward@my-pain.co.uk";
	$ShopperLocale = "en_GB";
	$CurrencyCode = "GBP";
	
	$Addressline1n2 = $BillHouseNumber . " " .$Ad1 . ", ". $Ad2; 
	$CustomerName = $UserTitle . " " . $UserFirstname . " " . $UserSurname; 
	
	$PaymentAmount = "10000"; 			// this is 1 pound (100p)
	$OrderDataRaw = "HDTV - ".$randomString; 	// order description
	$OrderID = $randomString;			// Order Id - needs to be unique
	
	//- integration user details - //
	$PW = "mypain@#1122rahul";
	$PSPID = "myp6uh"; 

	//- payment design options - //
	$TXTCOLOR = "#005588";
	$TBLTXTCOLOR = "#005588";
	$FONTTYPE = "Helvetica, Arial";
	$BUTTONTXTCOLOR	= "#005588";
	$BGCOLOR = "#d1ecf3";
	$TBLBGCOLOR = "#ffffff"; 
	$BUTTONBGCOLOR = "#cccccc";
	$TITLE = "Merchant Shop - Secure Payment";
	$LOGO = "http://54.72.176.89/assets/img/logo.gif";
	$PMLISTTYPE = 1;
	
	//= create string to hash (digest) using values of options/details above
	$DigestivePlain =
	"AMOUNT=" . $PaymentAmount . $PW .
	"BGCOLOR=" . $BGCOLOR . $PW .
	"BUTTONBGCOLOR=" . $BUTTONBGCOLOR . $PW .
	"BUTTONTXTCOLOR=" . $BUTTONTXTCOLOR . $PW .
	"CN=" . $CustomerName  . $PW . 
	"COM=" . $OrderDataRaw  . $PW . 
	"CURRENCY=" . $CurrencyCode . $PW .
	"EMAIL=" . $ShopperEmail . $PW .
	"FONTTYPE=" . $FONTTYPE . $PW .
	"LANGUAGE=" . $ShopperLocale . $PW .
	"LOGO=" .$LOGO . $PW .
	"ORDERID=" . $OrderID . $PW .
	"OWNERADDRESS=" . $Addressline1n2 . $PW .
	"OWNERCTY=" . $BillCountry . $PW .
	"OWNERTELNO=" . $ContactTel . $PW . 
	"OWNERTOWN=" . $BillTown . $PW .
	"OWNERZIP=" . $Pcde . $PW .
	"PMLISTTYPE=". $PMLISTTYPE . $PW .
	"PSPID=" . $PSPID . $PW .
	"TBLBGCOLOR=" . $TBLBGCOLOR . $PW .
	"TBLTXTCOLOR=" . $TBLTXTCOLOR . $PW .
	"TITLE=" . $TITLE . $PW .
	"TXTCOLOR=" . $TXTCOLOR . $PW .
	"";
	
	//=SHA encrypt the string=//
	$strHashedString_plain = strtoupper(sha1($DigestivePlain));
	//-Form to submit order details along with ecrypted string of order details
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Barclaycard Payment</title>
</head>
<body>
<center>
<p>Press submit to continue to payment.</p>

<form name="OrderForm" id="OrderForm" action="https://mdepayments.epdq.co.uk/ncol/test/orderstandard.asp" method="POST">
	<input type="hidden" name="AMOUNT" id="AMOUNT" value="<?PHP print $PaymentAmount; ?>"/> 
	<input type="hidden" name="CN" value="<?PHP print $CustomerName; ?>"> 
	<input type="hidden" name="COM" value="<?PHP print $OrderDataRaw; ?>">
	<input type="hidden" name="CURRENCY" id="CURRENCY" value="<?PHP print $CurrencyCode; ?>"/>
	<input type="hidden" name="EMAIL" id="EMAIL" value="<?PHP print $ShopperEmail; ?>">
	<input type="hidden" name="FONTTYPE" id="FONTTYPE" value="<?PHP print $FONTTYPE; ?>">
	<input type="hidden" name="LANGUAGE" id="LANGUAGE" value="<?PHP print $ShopperLocale; ?>">
	<input type="hidden" name="LOGO" value="<?PHP print $LOGO; ?>">
	<input type="hidden" name="ORDERID" id="ORDERID" value="<?PHP print $OrderID ?>"/> 
	<input type="hidden" name="OWNERADDRESS" id="OWNERADDRESS" value="<?PHP print $Addressline1n2; ?>">
	<input type="hidden" name="OWNERCTY" id="OWNERCTY" value="<?PHP print $BillCountry; ?>">
	<input type="hidden" name="OWNERTELNO" value="<?PHP print $ContactTel; ?>"> 
	<input type="hidden" name="OWNERTOWN" id="OWNERTOWN" value="<?PHP print $BillTown; ?>">
	<input type="hidden" name="OWNERZIP" id="OWNERZIP" value="<?PHP print $Pcde; ?>">
	<input type="hidden" name="PMLISTTYPE" id="PMLISTTYPE" value="<?PHP print $PMLISTTYPE ?>"/>												
	<input type="hidden" name="PSPID" id="PSPID" value="<?PHP print $PSPID ?>"/>
	<input type="hidden" name="BGCOLOR" id="BGCOLOR" value="<?PHP print $BGCOLOR; ?>"/>
	<input type="hidden" name="BUTTONBGCOLOR" id="BUTTONBGCOLOR" value="<?PHP print $BUTTONBGCOLOR; ?>"/>
	<input type="hidden" name="BUTTONTXTCOLOR" id="BUTTONTXTCOLOR" value="<?PHP print $BUTTONTXTCOLOR; ?>"/>
	<input type="hidden" name="TBLBGCOLOR" id="TBLBGCOLOR" value="<?PHP print $TBLBGCOLOR; ?>"/>
	<input type="hidden" name="TBLTXTCOLOR" id="TBLTXTCOLOR" value="<?PHP print $TBLTXTCOLOR; ?>">
	<input type="hidden" name="TITLE" id="TITLE" value="<?PHP print $TITLE; ?>"/>
	<input type="hidden" name="TXTCOLOR" id="TXTCOLOR" value="<?PHP print $TXTCOLOR; ?>">
	
	<input type="hidden" name="SHASign" value="<?PHP print $strHashedString_plain; ?>">

	<input onClick="document.OrderForm.submit(); return false;" type="image" src="submit.png" id="tstbtn" />

</form>
</center>
</body>
</html>