------------------------  Virendra (12/09/2014) ------------------------------
ALTER TABLE `mp_users` CHANGE `type` `type` ENUM('S','H','D','P','T') CHARSET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'S:Superadmin, H:Hospital, D:Doctor, P:Patient, T:Ticket'; 


INSERT INTO `mypain`.`mp_users` (`parentId`, `type`, `firstName`, `lastName`, `loginEmail`, `username`,`password`,`stdCd1`,`phone1`,`dob`,`status`,`isActivated`,`created`,`updated`) 
    VALUES ('0', 'T', 'Ticket', 'Admin', 'virendra@itglobalconsulting.com', 'ticket','09fcc2729c2852b3b61f5b456b37f5900db85cf3319a82afe4481196485c4050','+91','8800124098','1999-06-22 13:57:12' ,'1','1','2014-09-12 13:57:37','2014-09-12 13:57:42'); 

ALTER TABLE `mp_faq` 
    ADD COLUMN `userId` BIGINT(11) NULL AFTER `id`, CHANGE `ques` `question` VARCHAR(300) CHARSET utf8 COLLATE utf8_general_ci NULL, 
    CHANGE `ans` `answer` TEXT CHARSET utf8 COLLATE utf8_general_ci NULL, 
    ADD COLUMN `keyword` VARCHAR(255) NULL AFTER `answer`, 
    CHANGE `type` `category` ENUM('H','D','P','A') CHARSET utf8 COLLATE utf8_general_ci NULL COMMENT '\'A\'=>\'All\', \'H\'=>\'Hospital\', \'D\'=>\'Clinician\', \'P\'=>\'Patient\'', 
    ADD COLUMN `created` DATETIME NULL AFTER `showDefault`, 
    ADD COLUMN `updated` DATETIME NULL AFTER `created`, 
    ADD COLUMN `visible` ENUM('0','1') DEFAULT '1' NULL COMMENT '0 => hide, 1 => show' AFTER `updated`;
    
CREATE TABLE `mp_help_category`( `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `name` VARCHAR(255), `created` DATETIME, `updated` DATETIME, PRIMARY KEY (`id`) );

CREATE TABLE `mp_tickets`( `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT, 
    `name` VARCHAR(255) NOT NULL, 
    `email` VARCHAR(255) NOT NULL, 
    `categoryId` INT(11) NOT NULL, 
    `query` TEXT NOT NULL, `created` DATETIME, 
    `updated` DATETIME, 
    `status` ENUM('0','1') DEFAULT '1', PRIMARY KEY (`id`) );   
    
CREATE TABLE `mp_ticket_reply`( `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT, 
    `ticketId` INT(11) NOT NULL, 
    `reply` TEXT, 
    `name` VARCHAR(255), 
    `email` VARCHAR(255), 
    `created` DATETIME, 
    `updated` DATETIME, PRIMARY KEY (`id`) ); 
    
ALTER TABLE `mp_tickets` ADD COLUMN `ticketNo` VARCHAR(10) NOT NULL AFTER `id`; 
-----------------------------------------------------------------------------