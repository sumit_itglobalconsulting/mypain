URL: http://54.72.176.89/api/

----------------------------------------

Codes:

WC: Wrong Code
NF: Not Found
OK: Request is ok


----------------------------------------

API's
----------------------------------------

1. userLogin (http://54.72.176.89/api/userLogin)
   
   Input:   email, password, deviceToken
   Output:  Payload[userId, email, firstName, lastName, gender(M,F), dob, zipcode, profilePic, stdCode, phone, hospitalLogo, hospitalName, branchName, 	
   			deviceToken], Status, Message, Code


2. updateProfile (http://54.72.176.89/api/updateProfile)
   
   Input:   userId, stdCode, phone (Becouse user can update only contact no.)
   Output:  Same as userLogin
   

3. changePassword (http://54.72.176.89/api/changePassword)
   
   Input:   userId, oldpassword, password
   Output:  Same as userLogin
   
   
4. forgotPass (http://54.72.176.89/api/forgotPass)
   
   Input:   email
   Output:  Same as userLogin
   
   
5. userGuide (http://54.72.176.89/api/userGuide)
   
   Input:   
   Output: Payload[array(tab, pdf), array(tab, pdf)...], Status, Message, Code
   
   
6. disclaimer (http://54.72.176.89/api/disclaimer)
   
   Input:   
   Output: Payload[disclaimer], Status, Message, Code


7. responseForms (http://54.72.176.89/api/responseForms)
   
   Input:  userId
   Output: Payload[responseForms=>formId, formName, nextResDueDate], Status, Message, Code
   

8. formQuestions(http://54.72.176.89/api/formQuestions)
   
   Input:  formId
   Output: Payload[questions=>questionId, question, type(Radio, Slider), options=>(optionId,optionName,caption)], Status, Message, Code
   