<style type="text/css">
/** Default popup **/
.bodyCover_		{opacity:0.4; filter:alpha(opacity = 40); background:#000; width:100%; position:absolute; margin:0px; left:0; top:0; z-index:6000; overflow:auto}
.popupParent_			{overflow-x:auto; overflow-y:hidden; position:absolute; top:0; left:0; margin:0; background:none; z-index:6010}
.popupTopBtmMargin_		{margin:0px 0px}
.popupBx_				{border:0px solid #999; background:#fff; z-index:10001; padding:0px;
-moz-box-shadow: 3px 3px 10px #363636; 
-webkit-box-shadow: 3px 3px 10px #363636;	
box-shadow: 3px 3px 10px #363636;
display:none;
width:95%;
z-index:6020;
}
.popupPad_		{padding:0px; overflow:auto}
.popupTitle_	{font-weight:normal; font-size:18px; font-family:Arial, Helvetica, sans-serif; color:#fff; padding:7px 6px 7px 10px; line-height:normal; text-align:left; background:#617AAC; background:#F9F9F9; color:#333; border-bottom:1px solid #E5E5E5}

.popupClose_		{width:16px; height:21px; background:url(<?php echo base_url('assets/popupAjax/cross.png');?>) 100% 1px no-repeat; float:right; cursor:pointer}
.popupClose_:hover	{background-position:100% -25px}

.loaderPopup_	{background:url(<?php echo base_url('assets/popupAjax/loading.gif');?>) 50% 50% no-repeat #FFF; width:180px; height:60px; border-radius:4px; -moz-box-shadow: 3px 3px 10px #363636; 
-webkit-box-shadow: 3px 3px 10px #363636;	
box-shadow: 3px 3px 10px #363636; 
z-index:6030;
}

#popupTitle_	{padding:0px 10px 0px 0px}
.AJXPContent	{overflow:auto}

html.noscroll	{position:fixed; overflow-y:scroll; width: 100%}

</style>
<!--Default PopupAjax Box-->
<script type="text/javascript">
var popupContentBx_='<div class="bodyCover_" id="bodyCover_">&nbsp;</div>'+
					'<div class="popupParent_">'+
						'<div class="popupTopBtmMargin_">'+
						'<div class="loaderPopup_">&nbsp;</div>'+
						'<div class="popupBx_" id="popupBx_">'+
							'<div class="popupTitle_">'+
								'<div class="popupClose_" onclick="closeAJXP()" title="Close">&nbsp;</div>'+
								'<span id="popupTitle_">&nbsp;</span>'+
							'</div>'+
							'<div class="popupPad_">'+
								'<div class="AJXPContent" id="AJXPContent">'+
								'</div>'+
							'</div>'+
						'</div>'+
						'</div>'+
					'</div>';


jQuery.fn.setPPos_ = function () {
    this.css("position","relative");
	$top=Math.max(0, ((jQuery(".popupParent_").height() - this.outerHeight()) / 2));
	$top=$top-Math.round($top*15/100);
		
	$left=Math.max(0, ((jQuery(".popupParent_").width() - this.outerWidth()) / 2));
	
	this.css("top", $top+"px");
	
	this.css("left", $left+"px");
		
    return this;
}

var $popupajax_onclose_=false,$scrollTop_=0, $isAJXP=false,AJXPW_=0;
function popupAjax(page,title,bxwidth,onclose,form_id,onlyCross, staticContentEl)
{
	/** Closing previous popup **/
	jQuery("#bodyCover_").remove();
	jQuery("#popupBx_").remove();
	jQuery(".popupParent_").remove();
	jQuery(".loaderPopup_").remove();
	/** Closing previous popup end **/
	
	$isAJXP=true;
	AJXPW_=bxwidth?bxwidth:0;
	jQuery(window).scrollLeft(0);
	$popupajax_onclose_=onclose;
	$scrollTop_=jQuery(window).scrollTop();
	
	jQuery('body').prepend(popupContentBx_);
	
	jQuery(".popupParent_").css('width',jQuery(window).width()+'px');
	jQuery(".popupParent_").css('height',jQuery(window).height()+'px');
	
	//z=maxZIndex_()+1;
	//jQuery("#bodyCover_").css("zIndex",z);
	//jQuery(".popupParent_").css("zIndex",z+1);
	//jQuery("#popupBx_").css("zIndex",z+2);
	jQuery('.loaderPopup_').setPPos_();
	//jQuery(".loaderPopup_").css("zIndex",z+2);
	jQuery('#bodyCover_').disableSelection();
	
	if(jQuery(document).height()>jQuery(window).height())
	{
		jQuery('html').addClass('noscroll');
		jQuery('html').css('top','-'+$scrollTop_+"px");
		jQuery('.popupParent_').css('top',$scrollTop_+"px");
	}
	
	if(!onlyCross)
	{
		jQuery("#popupBx_").click(function(e){
			e.stopPropagation();
		});
		
		jQuery("#bodyCover_, .popupParent_").click(function(){
			closeAJXP(); 
		});
	}
	
	jQuery("#bodyCover_").height(jQuery(document).height());
	jQuery("#bodyCover_").width(jQuery(document).width());
	
	if(bxwidth)
		jQuery("#popupBx_").css({'width':bxwidth+'px'});
	
	if(title)
		jQuery("#popupTitle_").html(title);
	
	if(page)
	{
		if(form_id)
			ajax(page,"AJXPContent",form_id,'jQuery("#popupBx_").setPPos_(); showAJXP()','post');
		else
			ajax(page,"AJXPContent",false,'jQuery("#popupBx_").setPPos_(); showAJXP()');
	}
	else if(staticContentEl){
		$("#AJXPContent").html($("#"+staticContentEl).html());
		jQuery("#popupBx_").setPPos_();
		showAJXP();
	}
}

function showAJXP()
{
	jQuery('.loaderPopup_').hide();
	jQuery(".popupPad_").css({'max-height':jQuery(window).height()-80});
	jQuery('#popupBx_').show();
	if(jQuery("#AJXPContent").height()>jQuery("#popupBx_").height())
	{
		if(AJXPW_)
		{
			jQuery("#popupBx_").css({'width':(AJXPW_+17)+'px'});
			jQuery("#popupBx_").setPPos_();
		}
	}
}

function closeAJXP(delayTime)
{
	$isAJXP=false;
	if(delayTime)
		setTimeout('closeAJXPOPUP()',delayTime);
	else
		closeAJXPOPUP();
}

function closeAJXPOPUP()
{
	jQuery("#bodyCover_").remove();
	jQuery("#popupBx_").remove();
	jQuery(".popupParent_").remove();
	jQuery(".loaderPopup_").remove();
	
	jQuery('html').removeClass('noscroll');
	
	jQuery(window).scrollTop($scrollTop_);
	
	if($popupajax_onclose_)
		eval($popupajax_onclose_);
		
	$popupajax_onclose_=false;
}

function setAJXPTitle(title)
{
	jQuery("#popupTitle_").html(title);
}

function maxZIndex_()
{
	var z=0;
	jQuery('div,p,span,dd,a').each(function(){
		if(jQuery(this).css("zIndex") && !isNaN(jQuery(this).css("zIndex")))
		{
			if(jQuery(this).css("zIndex")>z)
				z=jQuery(this).css("zIndex");
		}
	});
	
	return z;
}

jQuery(window).resize(function(){
	if($isAJXP)
	{
		jQuery("#bodyCover_").height(jQuery(document).height());
		jQuery("#bodyCover_").width(jQuery(document).width());
		jQuery(".popupParent_").css('width',jQuery(window).width()+'px');
		jQuery(".popupParent_").css('height',jQuery(window).height()+'px');
		jQuery(".popupPad_").css({'max-height':jQuery(window).height()-80});
		jQuery("#popupBx_").setPPos_();
	}
});
</script>
<!--/PopupAjax Box-->