<?php 
/** For Admin **/
class User_model extends CI_Model {
	function updateProfile($data) {
		unset($data['hospitalName']);
		unset($data['regNo']);
		
		if($userId=$data['id']){
			$data['updated']=currentDate();
			
			$data['phone1']=$data['stdCode1'].$data['phone1'];
			$data['phone2']=$data['stdCode2'].$data['phone2'];
			
			$this->pdb->update("mp_users",$data,"id=".$data['id']);
			unset($data['id']);
			$this->pdb->update("mp_hospitals",$data,"userId=".$userId);
			return true;
		}
	}
	
	function changePassword($data) {
		if($data['id'] && $data['password']){
			$data['password']=encryptText($data['password']);
			$this->pdb->update("mp_users",$data,"id=".$data['id']);
			return true;
		}
	}
	
	function allClinicians() {
		$q="select A.id, CONCAT(A.firstName, ' ', A.lastName) name from mp_users A where A.parentId=".USER_ID."  AND A.type='D' order by name";
		return $this->pdb->query($q);
	}
	
	function miscInfo() {
		$data['totalDepts']=$this->pdb->singleVal("mp_departments", "userId=".USER_ID, "COUNT(id)");
		$data['totalDocts']=$this->pdb->singleVal("mp_users", "parentId=".USER_ID." AND type='D'", "COUNT(id)");
		
		$q="select COUNT(DISTINCT A.userId) n from mp_patient_forms A JOIN mp_users B ON (A.userId=B.id) JOIN mp_patients C ON (C.userId=B.id AND C.discharged=0)
		where B.parentId=".USER_ID." AND A.type=1 AND A.nextResDueDate<NOW()";
		$rs=$this->pdb->query($q);
		$data['noOfResDue']=$rs[0]['n'];
		
		return $data;
	}
	
	
	function resWisePatients($resType='P', $doctorId=0) {
		$cond="A.parentId=".USER_ID." AND A.type='P' AND B.discharged=0 AND B.archived=0";
		
		if($doctorId)
			$cond.=" AND B.doctorId='$doctorId'";
		
		$havCond="1";
		
		if($resType=='P')
			$havCond="overallRes<30 OR overallRes IS NULL";
		else if($resType=='A')
			$havCond="overallRes>=30 AND overallRes<50";
		else if($resType=='G')
			$havCond="overallRes>50";
		
		$q="SELECT 
				DISTINCT A.id ,
				
				ROUND( 
					AVG( IF( F.realTotalScore=E.baselineScore, ((E.lastScore-E.baselineScore)*10), (((E.lastScore-E.baselineScore)*100)/(F.realTotalScore-E.baselineScore)) ) ) 
				) overallRes
			FROM 
				mp_users A 
			JOIN 
				mp_patients B ON (A.id=B.userId) 
			LEFT JOIN 
				mp_patient_forms C ON (C.userId=A.id AND C.type=1) 
			LEFT JOIN 
				mp_forms D ON (C.formId=D.id AND D.setGoal=1 AND D.showReport=1) 
				
			LEFT JOIN 
				mp_patient_form_scales E ON (D.id=E.formId) 
			LEFT JOIN
				mp_form_scales F ON (F.id=E.scaleId)
				
			WHERE 
				$cond 
			GROUP BY 
				A.id 
			HAVING 
				$havCond";
			
		$rs= $this->pdb->query($q);
		
		return $rs?count($rs):0;
	}
	
	function patientsInvitaion() { 
		$qs=addSlash(arrayTrim($_GET));
		$y=$qs['year']?$qs['year']:date('Y');
		
		$cond="A.parentId=".USER_ID." AND YEAR(B.created)='$y'";
		if($qs['doctorId'])
			$cond.=" AND B.doctorId='{$qs['doctorId']}'";
		
		$q="select COUNT(A.id) noOfInv, DATE_FORMAT(B.created, '%b') monthN, MONTH(B.created) mno from mp_users A JOIN mp_patients B ON (A.id=B.userId) 
		where $cond GROUP BY monthN ORDER BY mno";
		return $this->pdb->query($q);
	}
	
	function noOfPatientTreatOptWise() {
		$qs=addSlash(arrayTrim($_GET));
		$parentId=USER_ID;
		$cond="B.parentId=$parentId AND !A.discharged"; 
		if($qs['doctorId'])
			$cond.=" AND A.doctorId='{$qs['doctorId']}'";
			
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A JOIN mp_users B ON (A.userId=B.id) where $cond AND A.treatment1=1");
		$data[1]=$rs[0]['n'];
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A JOIN mp_users B ON (A.userId=B.id) where $cond AND A.treatment2=1");
		$data[2]=$rs[0]['n'];
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A JOIN mp_users B ON (A.userId=B.id) where $cond AND A.treatment3=1");
		$data[3]=$rs[0]['n'];
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A JOIN mp_users B ON (A.userId=B.id) where $cond AND A.treatment4=1");
		$data[4]=$rs[0]['n'];
		
		return $data;
	}
        
        
}

