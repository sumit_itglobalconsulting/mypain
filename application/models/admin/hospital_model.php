<?php 
class Hospital_model extends CI_Model {
	function save($data, $tbl=""){
		if(!$tbl)
			return;
			
		if($id=$data['id']){
			$data['updated']=currentDate();
			$success=$this->pdb->update($tbl, $data, "id=$id");
			if(!$success)
				return 0;
		}
		else{
			$data['created']=$data['updated']=currentDate();
			$id=$this->pdb->insert($tbl, $data);
		}
		
		return $id;
	}
	
	/** Forms **/
	function hospitalForms() {
            $qs=arrayUrlDecode(addSlash(arrayTrim($_GET)));
            $cond="A.userId=".USER_ID;
            if($qs['catId'])
                    $cond.=" AND C.catId='{$qs['catId']}'";

            $q="SELECT DISTINCT A.id, A.formId, A.created addedDate, B.formName, B.image FROM mp_hospital_forms A 
                    JOIN mp_forms B ON (A.formId=B.id) LEFT JOIN mp_form_categories C ON (A.formId=C.formId) WHERE $cond order by B.formName,B.formTitle";
            return $this->pdb->query($q);
	}
	
	
	/** Departments **/
	function saveDept($data) {
            $data['userId']=USER_ID;
            $deptId=$this->save($data, "mp_departments");
            if($deptId){
                $this->pdb->delete("mp_department_forms", "deptId=".$deptId);
                if($data['formId']){
                    $inf['deptId']=$deptId;
                    $inf['created']=currentDate();
                    foreach($data['formId'] as $formId){
                            $inf['formId']=$formId;
                            $this->pdb->insert("mp_department_forms", $inf);
                    }
                }
            }
            return $deptId;
	}
	
	function listDepts() {
		$q="select A.*, (select count(id) from mp_department_forms where deptId=A.id) noFForms from mp_departments A where A.userId=".USER_ID;
		return $this->pdb->query($q);
	}
	
	function deptDetail($id=0) {
		return $this->pdb->singleRow("mp_departments", "id=:id AND userId=".USER_ID, "*", array(':id'=>$id));
	}
	
	function deptForms($deptId=0){
		$q="SELECT A.id, A.formId, A.created addedDate, B.formName, B.image FROM mp_department_forms A JOIN mp_forms B ON (A.formId=B.id) WHERE A.deptId=:deptId";
		return $this->pdb->query($q, array(':deptId'=>$deptId));
	}
	
	
	/** Clinicians / Doctors **/
	function saveClinician($data) {
            $data['parentId']=USER_ID;
            $data['type']='D';
            $data['phone1']=$data['stdCode1'].$data['phone1'];
            $data['isActivated']=$data['status']=1;
            $userId=$this->save($data, "mp_users");
            if($userId){
                    $inf=$data;
                    unset($inf['id']);
                    $inf['userId']=$userId;

                    if($this->pdb->singleVal("mp_doctors", "userId=$userId", "id"))
                            $this->pdb->update("mp_doctors", $inf, "userId=$userId");
                    else
                            $this->pdb->insert("mp_doctors", $inf);

                    $this->pdb->delete("mp_doctor_forms", "userId=".$userId);
                    if($data['formId']){
                            $info['userId']=$userId;
                            $info['created']=currentDate();
                            foreach($data['formId'] as $formId){
                                    $info['formId']=$formId;
                                    $this->pdb->insert("mp_doctor_forms", $info);
                            }
                    }
            }
            return $userId;
	}
	
	function listClinicians($p, $ps) {
		$q="select A.*, C.name deptName, (select count(id) from mp_doctor_forms MF where userId=A.id) noFForms from mp_users A JOIN mp_doctors B ON (A.id=B.userId) JOIN 
		mp_departments C ON (B.deptId=C.id) where A.parentId=".USER_ID." AND A.type='D'";
		return $this->pdb->pagedQuery($q, $p, $ps);
	}
	function allClinicians() {
		$q="select A.id, CONCAT(A.firstName, ' ', A.lastName) name from mp_users A where A.parentId=".USER_ID."  AND A.type='D' order by name";
		return $this->pdb->query($q);
	}
	
	function clinicianForms($userId=0){
		$q="SELECT A.id, A.formId, A.created addedDate, B.formName, B.image FROM mp_doctor_forms A JOIN mp_forms B ON (A.formId=B.id) WHERE A.userId=:userId";
		return $this->pdb->query($q, array(':userId'=>$userId));
	}
	
	function deleteClinician($data){
		$this->pdb->delete("mp_doctors", "userId=:userId", array(':userId'=>$data['id']));
		$this->pdb->delete("mp_doctor_forms", "userId=:userId", array(':userId'=>$data['id']));
		$success=$this->pdb->delete("mp_users", "id=:userId", array(':userId'=>$data['id']));
		if($success){
			$status['success']=true;
			if($img=$data['image'])
				delFile("assets/uploads/user_images/".$img);
		}
		else{
			$status['success']=false;
			$status['msg']="Could not be deleted due to some errors. Try again.";
		}
		
		return $status;
	}
	
	/** Patients **/
	
}

