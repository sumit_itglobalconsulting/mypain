<?php 
/** For Doctor **/
class User_model extends CI_Model {
	function updateProfile($data) {
		if($userId=$data['id']){
			$data['updated']=currentDate();
			
			$data['phone1']=$data['stdCode1'].$data['phone1'];
			
			$this->pdb->update("mp_users",$data,"id=".$data['id']);
			unset($data['id']);
			$this->pdb->update("mp_doctors",$data,"userId=".$userId);
			return true;
		}
	}
	
	function changePassword($data) {
		if($data['id'] && $data['password']){
			$data['password']=encryptText($data['password']);
			$this->pdb->update("mp_users",$data,"id=".$data['id']);
			return true;
		}
	}
	
	
	/** Dashboard **/
	function resWisePatients($resType='P', $doctorId=0) {
		$doctId=USER_ID;
		if($doctorId){
			$doctId=$doctorId;
		}
		
		if(USER_TYPE=='H')
			$cond="B.doctorId=$doctId AND A.type='P'";
		else
			$cond="B.doctorId=$doctId AND A.type='P' AND B.archived=0 AND B.discharged=0";
		
		if($arc)
			$cond="B.doctorId=$doctId AND A.type='P' AND B.archived=1 AND B.discharged=0";
		if($dis)
			$cond="B.doctorId=$doctId AND A.type='P' AND B.discharged=1";
		
		$havCond="1";
		if($resType=='P')
			$havCond="overallRes<=30 OR overallRes IS NULL";
		else if($resType=='A')
			$havCond="overallRes>30 AND overallRes<50";
		else if($resType=='G')
			$havCond="overallRes>=50";
	
		$q="SELECT 
				DISTINCT A.id, A.parentId, A.type, A.hospitalId, CONCAT(A.firstName, ' ', A.lastName) name, 
				B.userId, B.needFollowUp, B.followUpType, B.followUpDate, B.followUpType, 
				B.onOpioidTherapy, B.needAssistance, B.treatment1, B.treatment2, B.treatment3, B.treatment4, B.archived, B.discharged ,
				
				IF(C.weightage=0,
					ROUND(
						AVG( IF(F.realTotalScore = IFNULL(E.baselineScore, 0), ((IFNULL(E.lastScore, 0) - IFNULL(E.baselineScore, 0)) * 10), 
						  (((IFNULL(E.lastScore,0) - IFNULL(E.baselineScore,0)) * 100 ) / (F.realTotalScore - IFNULL(E.baselineScore,0)))) )
					),
					
					ROUND(
						( IF(F.realTotalScore = IFNULL(E.baselineScore, 0), ((IFNULL(E.lastScore, 0) - IFNULL(E.baselineScore, 0)) * 10), 
						  (((IFNULL(E.lastScore,0) - IFNULL(E.baselineScore,0)) * 100 ) / (F.realTotalScore - IFNULL(E.baselineScore,0)))) )*C.weightage/100
					)
			    ) overallRes 
				
			FROM 
				mp_users A 
			JOIN 
				mp_patients B ON (A.id=B.userId) 
			LEFT JOIN 
				mp_patient_forms C ON (C.userId=A.id AND C.type=1 AND C.doctorId=B.doctorId) 
			LEFT JOIN 
				mp_forms D ON (C.formId=D.id AND D.setGoal=1 AND D.showReport=1) 
			LEFT JOIN 
				mp_patient_form_scales E ON (D.id=E.formId AND E.userId=A.id AND E.doctorId=B.doctorId) 
			LEFT JOIN
				mp_form_scales F ON (F.id=E.scaleId)
				
			WHERE 
				$cond 
			GROUP BY 
				A.id 
			HAVING 
				$havCond  
			";
			
		$rs= $this->pdb->query($q);
		
		return $rs?count($rs):0;
	}
	
	function noOfPatientTreatOptWise() {
		$doctId=USER_ID;
		$cond="A.doctorId=$doctId AND !A.discharged";
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond");
		$total=$rs[0]['n'];
		
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.treatment1=1");
		$data[1]=$rs[0]['n'];
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.treatment1=1 AND A.treatment2=1");
		$data[12]=$rs[0]['n'];
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.treatment1=1 AND A.treatment3=1");
		$data[13]=$rs[0]['n'];
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.treatment1=1 AND A.treatment4=1");
		$data[14]=$rs[0]['n'];
		
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.treatment2=1");
		$data[2]=$rs[0]['n'];
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.treatment2=1 AND A.treatment3=1");
		$data[23]=$rs[0]['n'];
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.treatment2=1 AND A.treatment4=1");
		$data[24]=$rs[0]['n'];
		
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.treatment3=1");
		$data[3]=$rs[0]['n'];
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.treatment3=1 AND A.treatment4=1");
		$data[34]=$rs[0]['n'];
		
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.treatment4=1");
		$data[4]=$rs[0]['n'];
		
		
		
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND A.onOpioidTherapy='Y'");
		$data[5]=$rs[0]['n'];
		
		$data['opiodPer']=($data[5]*100)/$total;
		
		return $data;
	}
	
	function analysisInfo() {
		$doctId=USER_ID;
		$cond="A.doctorId=$doctId AND !A.discharged";
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond");
		$data['noOfInv']=$rs[0]['n'];
		
		$d=date('j');
		$m=date('n');
		$y=date('Y');
		$rs=$this->pdb->query("select COUNT(A.id) n from mp_patients A where $cond AND MONTH(A.created)='$m' AND YEAR(A.created)='$y'");
		$data['noOfInvCurMonth']=$rs[0]['n'];
		
		$q="select COUNT(DISTINCT A.userId) n from mp_patient_forms A JOIN mp_patients B ON (A.userId=B.userId AND A.doctorId=B.doctorId AND B.discharged=0) 
		LEFT JOIN mp_response C ON (C.userId=A.userId AND C.doctorId=B.doctorId AND C.formId=A.formId) 
		where A.doctorId=$doctId AND A.type=1 AND A.nextResDueDate<=NOW() AND (A.resFreq!='O' OR C.resDate IS NULL)";
		$rs=$this->pdb->query($q);
		$data['noOfResDue']=$rs[0]['n'];
		
		$q="select COUNT(DISTINCT A.userId) n from mp_response A where A.doctorId=$doctId AND DAY(A.resDate)='$d' AND MONTH(A.resDate)='$m' AND YEAR(A.resDate)='$y'";
		$rs=$this->pdb->query($q);
		$data['resToday']=$rs[0]['n'];
		
		$q="select COUNT(DISTINCT A.userId) n from mp_response A JOIN mp_patients B ON (A.userId=B.userId AND A.doctorId=B.doctorId AND B.discharged=0) 
			where A.doctorId=$doctId AND WEEKOFYEAR(A.resDate)=WEEKOFYEAR(NOW())";
		$rs=$this->pdb->query($q);
		$data['resWeek']=$rs[0]['n'];
		
		$q="select COUNT(DISTINCT A.userId) n from mp_response A JOIN mp_patients B ON (A.userId=B.userId AND A.doctorId=B.doctorId AND B.discharged=0) 
			where A.doctorId=$doctId AND MONTH(A.resDate)='$m'";
		$rs=$this->pdb->query($q);
		$data['resMonth']=$rs[0]['n'];
                
                $q = "select count(id) as n from mp_patients where doctorId='$doctId' AND invitation = 1";
                $rs = $this->pdb->query($q);
                $data['inviSend']=$rs[0]['n'];
                
                
                $q = "select count(id) as n from mp_patients where doctorId='$doctId' AND invitation = 2";
                $rs = $this->pdb->query($q);
                $data['inviAppr']=$rs[0]['n'];
		
		return $data;
	}
        
        
        
        
}