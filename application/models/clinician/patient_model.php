<?php

class Patient_model extends CI_Model {

    function save($data, $tbl = "") {
        if (!$tbl)
            return;

        if ($id = $data['id']) {
            $data['updated'] = currentDate();
            $success = $this->pdb->update($tbl, $data, "id=$id");
            if (!$success)
                return 0;
        }
        else {
            $data['created'] = $data['updated'] = currentDate();
            $id = $this->pdb->insert($tbl, $data);
        }

        return $id;
    }

    function savePatient($data) {
        $data['type'] = 'P';
        $data['treatment1'] = $data['treatment1'] ? 1 : 0;
        $data['treatment2'] = $data['treatment2'] ? 1 : 0;
        $data['treatment3'] = $data['treatment3'] ? 1 : 0;
        $data['treatment4'] = $data['treatment4'] ? 1 : 0;

        $data['isActivated'] = $data['status'] = 1;

        $data['phone1'] = $data['stdCode1'] . $data['phone1'];
        $data['phone2'] = $data['stdCode2'] . $data['phone2'];

        $userId = $this->save($data, "mp_users");
        if ($userId) {
            $inf = $data;
            unset($inf['id']);
            $inf['userId'] = $userId;
            $inf['doctorId'] = USER_ID;

            if (!$inf['followUpFreq'])
                $inf['followUpFreq'] = '';

            $cond = "userId=$userId AND doctorId=" . USER_ID;
            
            $inf['invitation'] = 3;
            if ($this->pdb->singleVal("mp_patients", $cond, "id"))
                $this->pdb->update("mp_patients", $inf, $cond);
            else {
                $inf['created'] = currentDate();
                $this->pdb->insert("mp_patients", $inf);
            }

            /** Patient's form * */
            $this->pdb->delete("mp_patient_forms", $cond); //Commented on 9-feb
           // $this->pdb->update("mp_patient_forms",array("editable"=>0), $cond); //Added on 9-feb

            if ($data['assignForm']) {
                $info = array('userId' => $userId, 'doctorId' => USER_ID, 'created' => currentDate());
                foreach ($data['assignForm'] as $formId => $v) {
                    if ($v && $formId) {
                        $info['formId'] = $formId;
                        $info['nextResDueDate'] = timeStamp($data['nextResDueDate'][$formId]);
                        $info['resFreq'] = $data['resFreq'][$formId];
                        $info['symptom'] = $data['symptom'][$formId] ? $data['symptom'][$formId] : '';
                        $info['weightage'] = $data['weightage'][$formId] ? $data['weightage'][$formId] : 0;

                        if ($data['formStatus'][$formId] || $data['formStatus'][$formId] === 0 || $data['formStatus'][$formId] === '0')
                            $info['status'] = $data['formStatus'][$formId];

                        if ($data['formCreated'][$formId])
                            $info['created'] = $data['formCreated'][$formId];

                        //$this->pdb->singleVal("mp_patient_forms", $cond, "id");
                        $this->pdb->insert("mp_patient_forms", $info);

                        /** Scales * */
                        $scaleIds = $this->pdb->query("SELECT DISTINCT A.id, B.baselineScore, B.lastScore FROM mp_form_scales A LEFT JOIN 
						mp_patient_form_scales B 
						ON (A.id=B.scaleId AND B.userId='$userId' AND B.doctorId=" . USER_ID . ") WHERE A.formId='$formId'", 0, 1);

                        $this->pdb->delete("mp_patient_form_scales", $cond . " AND formId='$formId'");

                        if ($scaleIds) {
                            $sinf['formId'] = $formId;
                            $sinf['userId'] = $userId;
                            $sinf['doctorId'] = USER_ID;

                            foreach ($scaleIds as $s) {
                                $sinf['scaleId'] = $s['id'];
                                $sinf['goodRes'] = $data['goodRes'][$s['id']];
                                $sinf['avgRes'] = $data['avgRes'][$s['id']];
                                $sinf['baselineScore'] = $s['baselineScore'];
                                $sinf['lastScore'] = $s['lastScore'];
                                $this->pdb->insert("mp_patient_form_scales", $sinf);
                            }
                        }
                    }
                }
            }



            /**             * */
            $cond = "patientId=$userId AND doctorId=" . USER_ID;
            $inf = array();
            $inf['patientId'] = $userId;
            $inf['doctorId'] = USER_ID;

            if ($this->pdb->singleVal("mp_patients_doctors", $cond, "id")) {
                $this->pdb->update("mp_patients_doctors", $inf, $cond);
            } else {
                $inf['created'] = currentDate();
                $this->pdb->insert("mp_patients_doctors", $inf);
            }
        }
        return $userId;
    }
    
    function invitePatients($p, $ps){
         $qs = addSlash(arrayTrim(arrayUrlDecode($_GET)));
         $doctId = USER_ID;
         $orderBy = " name ASC";
         if ($qs['orderBy']) {
            $order = explode(":", $qs['orderBy']);
            $orderBy = " " . $order[0] . " " . $order[1];
         }
         
         $cond   = "B.doctorId='$doctId' AND B.invitation=1";
         if ($qs['k'])
            $cond.=" AND ( CONCAT(A.firstName, ' ', A.lastName) LIKE '%{$qs['k']}%' OR A.loginEmail LIKE '%{$qs['k']}%' )";
                                        
         $q = "Select CONCAT(A.firstname, ' ' , A.lastname) name, A.loginEmail from mp_users A INNER JOIN mp_patients B ON B.userId=A.id where $cond ORDER BY $orderBy;";
         $rs = $this->pdb->pagedQuery($q, $p, $ps);
         return $rs;
        
    }
    

    function listPatients($p, $ps, $followUp = 0, $doctorId = 0, $arc = 0, $dis = 0) {
        
        $d = date('d');
        $m = date('m');
        $y = date('Y');

        $qs = addSlash(arrayTrim(arrayUrlDecode($_GET)));
        $invAna = $qs['invAna'];
        
        if($invAna == 'IA'){
            $iuids = $this->pdb->query("select DISTINCT A.userId from mp_patients A where A.doctorId='".USER_ID."' AND A.invitation='2'");
            if ($iuids) {
                $iuids = multiArrToKeyValue($iuids, "", "userId");
                $iuids = implode(",", $iuids);
            } else
                $iuids = 0;
            
            $dids = $this->pdb->query("select DISTINCT A.doctorId from mp_patients A where A.userId IN ($iuids) ");
            if ($dids) {
                $dids = multiArrToKeyValue($dids, "", "doctorId");
                $dids = implode(",", $dids);
            } else
                $dids = 0;
        }

        $doctId = USER_ID;
        if ($doctorId) {
            $doctId = $doctorId;
        }
        if(true){  //added for remove notification in patient header
            $today=date('Y-m-d 23:59:00');
            
            $q="update mp_patient_forms A  set A.readDoctor='1' where 
				A.doctorId='$doctId' AND 
				A.status=1 AND A.type=1 AND ( (A.nextResDueDate<='$today' AND A.resFreq!='O') OR  (A.nextResDueDate<=NOW() AND A.resFreq='O' ) )";
            $this->pdb->query($q);
        }

        $havCond = "1";

        if (USER_TYPE == 'H')
            $cond = "B.doctorId=$doctId AND A.type='P'";
        else{
            if(!$dids)
                $cond = "B.doctorId=$doctId AND A.type='P' AND B.archived=0 AND B.discharged=0";
            else 
                $cond = "B.doctorId IN ($dids) AND A.type='P' AND B.archived=0 AND B.discharged=0";
        }    

        if ($arc)
            $cond = "B.doctorId=$doctId AND A.type='P' AND B.archived=1 AND B.discharged=0";
        if ($dis)
            $cond = "B.doctorId=$doctId AND A.type='P' AND B.discharged=1";

        if ($qs['treatOpt']) {
            if ($qs['treatOpt'] == 'OT')
                $cond.=" AND onOpioidTherapy='Y'";
            else
                $cond.=" AND treatment{$qs['treatOpt']}=1";
        }

        if ($qs['k'])
            $cond.=" AND ( CONCAT(A.firstName, ' ', A.lastName) LIKE '%{$qs['k']}%' OR A.hospitalId LIKE '%{$qs['k']}%' OR 
					DATE_FORMAT(A.dob, '%d %M %Y') LIKE '%{$qs['k']}%' )";


        if ($followUp) {
            $cond.=" AND B.needFollowUp='Y'";
            //$orderBy=" B.followUpDate DESC";
        }

        $orderBy = " name ASC";
        if ($qs['orderBy']) {
            $order = explode(":", $qs['orderBy']);
            $orderBy = " " . $order[0] . " " . $order[1];
        }


        switch ($invAna) {
            case 'TI':
                break;

            case 'IM':
                $cond.=" AND B.created LIKE '$y-$m%'";
                break;

            case 'RD':
                $cond.=" AND ( (C.nextResDueDate<=NOW() AND C.resFreq!='O') OR (C.nextResDueDate<=NOW() AND C.resFreq='O' AND G.baselineScore IS NULL) )";
                break;

            case 'RT':
                $uids = $this->pdb->query("select DISTINCT A.userId from mp_response A where A.doctorId=$doctId AND A.resDate LIKE '$y-$m-$d%'");
                if ($uids) {
                    $uids = multiArrToKeyValue($uids, "", "userId");
                    $uids = implode(",", $uids);
                } else
                    $uids = 0;

                $cond.=" AND A.id IN($uids)";
                break;

            case 'RW':
                $cond.=" AND ( (C.nextResDueDate<=NOW() AND C.resFreq!='O') OR (C.nextResDueDate<=NOW() AND C.resFreq='O' AND G.baselineScore IS NULL) )";
                break;

            case 'RM':
                $cond.=" AND ( (C.nextResDueDate<=NOW() AND C.resFreq!='O') OR (C.nextResDueDate<=NOW() AND V.resFreq='O' AND G.baselineScore IS NULL) )";
                break;
            case 'IA':
                $cond.=" AND A.id IN($iuids)";
                break;
        }


        $resType = $qs['resType'];
        if ($resType == 'P')
            $havCond.=" AND overallRes<=30 OR overallRes IS NULL";
        else if ($resType == 'A')
            $havCond.=" AND overallRes>30 AND overallRes<50";
        else if ($resType == 'G')
            $havCond.=" AND overallRes>=50";
        
        $cond .=" AND (B.invitation='0' OR B.invitation='3')  ";

        $q = "SELECT 
				DISTINCT A.id, A.parentId, A.type, A.hospitalId, CONCAT(A.firstName, ' ', A.lastName) name, A.dob, 
				B.userId, B.needFollowUp, B.followUpType, B.followUpDate, B.followUpType, B.followUpDone, B.followedDates, 
				B.onOpioidTherapy, B.needAssistance, B.treatment1, B.treatment2, B.treatment3, B.treatment4, B.archived, B.discharged,
				IF(C.weightage=0,
					
					AVG( IF(F.realTotalScore = IFNULL(E.baselineScore, 0), ((IFNULL(E.lastScore, 0) - IFNULL(E.baselineScore, 0)) * 10), 
					  (((IFNULL(E.lastScore,0) - IFNULL(E.baselineScore,0)) * 100 ) / (F.realTotalScore - IFNULL(E.baselineScore,0)))) )
					,
					
					( IF(F.realTotalScore = IFNULL(E.baselineScore, 0), ((IFNULL(E.lastScore, 0) - IFNULL(E.baselineScore, 0)) * 10), 
					  (((IFNULL(E.lastScore,0) - IFNULL(E.baselineScore,0)) * 100 ) / (F.realTotalScore - IFNULL(E.baselineScore,0)))) )*C.weightage/100
			    ) overallRes 
				
			FROM 
				mp_users A 
			JOIN 
				mp_patients B ON (A.id=B.userId) 
			LEFT JOIN 
				mp_patient_forms C ON (C.userId=A.id AND C.type=1 AND C.doctorId=B.doctorId) 
			LEFT JOIN 
				mp_forms D ON (C.formId=D.id AND D.setGoal=1 AND D.showReport=1) 
			LEFT JOIN 
				mp_patient_form_scales E ON (D.id=E.formId AND E.userId=A.id AND E.doctorId=B.doctorId) 
			LEFT JOIN
				mp_form_scales F ON (F.id=E.scaleId) 
			
			WHERE 
				$cond 
			GROUP BY 
				A.id 
			HAVING 
				$havCond  
			ORDER BY 
				$orderBy;"; 

        $rs = $this->pdb->pagedQuery($q, $p, $ps);
        return $rs;
        /* before where
         * LEFT JOIN 
				mp_patient_form_scales G ON (G.formId=C.formId AND G.userId=A.id AND G.doctorId=B.doctorId) 
         * 
         */
    }

    function assignedForms($userId, $doctorId = 0) {
        $cond = "type=1 AND userId=$userId AND status=1";
        if ($doctorId)
            $cond = "type=1 AND userId=$userId AND doctorId=$doctorId";

        return $this->pdb->select("mp_patient_forms", $cond);
    }

    function checkPatientExists($data) {
        $data = addSlash(arrayTrim($data));
        $doctorId = USER_ID;

        $q = "SELECT A.*, CONCAT(A.firstName, ' ', A.lastName) fullName, B.userId, B.caseSummary, B.needFollowUp, B.followUpType, 
			B.followUpDate, B.followUpFreq, B.onOpioidTherapy, B.needAssistance, B.treatment1, 
			B.treatment2, B.treatment3, B.treatment4, B.archived, B.discharged  
			FROM mp_users A LEFT JOIN mp_patients B ON (A.id=B.userId AND B.doctorId=$doctorId) 
			WHERE A.hospitalId='{$data['hospitalId']}' AND A.loginEmail='{$data['loginEmail']}' AND A.parentId=" . PARENT_ID;

        $rs = $this->pdb->query($q);
        return $rs[0];
    }

    function otherDoctorsOfPatient($patientId) {
        $q = "select A.caseSummary, A.created, B.firstName doctName, B.loginEmail doctEmail from mp_patients A JOIN mp_users B ON (A.doctorId=B.id) 
			where A.userId=$patientId AND A.discharged=0 AND A.doctorId!=" . USER_ID . " order by A.id DESC";
        return $this->pdb->query($q);
    }

    /** Archive / Discharge* */
    function makeArchive($pid, $v) {
        $inf['archived'] = $v;
        $inf['archiveDate'] = BLANK_DATE;
        if ($v == 1)
            $inf['archiveDate'] = currentDate();
        return $this->pdb->update("mp_patients", $inf, "userId=$pid AND doctorId=" . USER_ID);
    }

    function makeDischarge($pid, $v) {
        $inf['discharged'] = $v;
        $inf['archived'] = 0;

        $inf['dischargeDate'] = BLANK_DATE;
        if ($v == 1)
            $inf['dischargeDate'] = currentDate();

        $info['status'] = !$v;
        $cond = "";

        if ($v == 0) {
            $rs = $this->pdb->query("select formId from mp_patient_forms where userId=$pid AND status=1");
            $rs = multiArrToKeyValue($rs, "formId", "formId");
            if ($rs) {
                $rs = implode(",", $rs);
                $cond = " AND formId NOT IN ($rs)";
            }
        }

        $this->pdb->update("mp_patient_forms", $info, "userId=$pid AND doctorId=" . USER_ID . $cond);

        return $this->pdb->update("mp_patients", $inf, "userId=$pid AND doctorId=" . USER_ID);
    }

    /** Deletion * */
    function delete($id = 0) {
        $cond = "userId='$id' AND doctorId=" . USER_ID;
        $this->pdb->delete("mp_patients", $cond);
        $this->pdb->delete("mp_patient_forms", $cond);
        $this->pdb->delete("mp_patient_form_scales", $cond);

        $resIds = $this->pdb->select("mp_response", $cond, "id");
        if ($resIds) {
            $this->pdb->delete("mp_response", $cond);
            $resIds = implode(",", $resIds);
            $this->pdb->query("delete from mp_response_details where resId IN ($resIds)");
        }

        if (!$this->pdb->singleRow("mp_patients", $cond)) {
            $this->pdb->delete("mp_users", "id='$id'");
        }

        return true;
    }

}
