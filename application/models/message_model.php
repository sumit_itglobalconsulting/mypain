<?php 
class Message_model extends CI_Model {
	function sendMsg($data) { 
                  if(USER_TYPE==='P')
                        $data['readPatient'] = 1;
                else 
                        $data['readDoctor'] = 1;
		return $this->pdb->insert("mp_messages", $data);
                
	}
	
	function recievedMsg($p, $ps) {
            $qs=addSlash(arrayUrlDecode(arrayTrim($_GET)));
            if(USER_TYPE==='P')
                $readType = 'A.readPatient as readT';
            else 
                $readType= 'A.readDoctor as readT';
            $cond="A.toId=".USER_ID." AND A.replyId='0' AND (A.parent_id='0' OR isNew='1') AND recievedDeleted=0";

            if($qs['k']){
                    $cond.=" AND (A.subject LIKE '%{$qs['k']}%' OR A.message LIKE '%{$qs['k']}%' OR CONCAT(B.firstName, ' ', B.lastName) LIKE '%{$qs['k']}%' 
                                     OR CONCAT(C.firstName, ' ', C.lastName) LIKE '%{$qs['k']}%')";
            }

           $q="SELECT A.*, $readType, CONCAT(B.firstName, ' ', B.lastName) fromName, CONCAT(C.firstName, ' ', C.lastName) toName FROM mp_messages A JOIN mp_users B ON (A.fromId=B.id) JOIN 
                    mp_users C ON (A.toId=C.id) WHERE $cond ORDER BY A.created DESC";
            /*$q="select A.*, CONCAT(B.firstName, ' ', B.lastName) fromName, CONCAT(C.firstName, ' ', C.lastName) toName FROM mp_messages A JOIN mp_users B ON (A.fromId=B.id) JOIN 
                    mp_users C ON (A.toId=C.id)
 where A.parent_id = 0
   and A.toId=".USER_ID."
   and not exists (select 1
          from mp_messages Y
         where Y.parent_id = A.id
           and Y.toId = A.toId)
union all
A.*, CONCAT(B.firstName, ' ', B.lastName) fromName, CONCAT(C.firstName, ' ', C.lastName) toName FROM mp_messages A JOIN mp_users B ON (A.fromId=B.id) JOIN 
                    mp_users C ON (A.toId=C.id)
 where A.parent_id <> 0
   and A.toId=".USER_ID."
   and A.created = (select max(Y.created)
                       from mp_messages Y
                      where Y.parent_id = A.parent_id)
 order by Y.id, A.created";*/
            return $this->pdb->pagedQuery($q, $p, $ps);
	}
	
	function sentMsg($p, $ps) {
            $qs=addSlash(arrayUrlDecode(arrayTrim($_GET)));
            if(USER_TYPE==='P')
                $readType = 'A.readPatient as readT';
            else 
                $readType= 'A.readDoctor as readT';
            
            $cond="A.fromId=".USER_ID." AND replyId='0' AND fwdId='0' AND parent_id='0' AND  sentDeleted=0";
            if($qs['k']){
                    $cond.=" AND (A.subject LIKE '%{$qs['k']}%' OR A.message LIKE '%{$qs['k']}%' OR CONCAT(B.firstName, ' ', B.lastName) LIKE '%{$qs['k']}%' 
                                     OR CONCAT(C.firstName, ' ', C.lastName) LIKE '%{$qs['k']}%')";
            }

            $q="SELECT A.*, $readType, CONCAT(B.firstName, ' ', B.lastName) toName, CONCAT(C.firstName, ' ', C.lastName) fromName FROM mp_messages A JOIN mp_users B ON (A.toId=B.id) JOIN 
                    mp_users C ON (A.fromId=C.id) WHERE $cond ORDER BY A.created DESC";
            return $this->pdb->pagedQuery($q, $p, $ps);
	}
	
	function detail($id=0){
             $readCond = "toId=".USER_ID." AND (id='$id' OR replyId='$id' OR parent_id='$id')";
            if(USER_TYPE==='P')
                $read['readPatient'] = 1;
            else 
                $read['readDoctor'] = 1;
               
                
                $this->pdb->update("mp_messages", $read, $readCond);
            $cond="A.id=:id OR (A.replyId=:id AND A.fromId=".USER_ID.") OR (A.replyId=:id AND A.toId=".USER_ID.") OR (A.fwdId=:id AND A.fromId=".USER_ID.") OR A.parent_id=:id";
            $q="SELECT A.*, CONCAT(B.firstName, ' ', B.lastName) toName, CONCAT(C.firstName, ' ', C.lastName) fromName FROM mp_messages A JOIN mp_users B ON (A.toId=B.id) JOIN 
                    mp_users C ON (A.fromId=C.id) WHERE $cond order by id ASC";
            $dtl=$this->pdb->query($q, array(':id'=>$id));
            return $dtl;
	}
	
//	function countUnreadMsg() {
//            $q="select COUNT(A.id) n from mp_messages A JOIN mp_users B ON (A.toId=B.id) JOIN mp_users C ON (A.fromId=C.id) where A.toId=".USER_ID." AND 
//            A.recievedDeleted=0 AND A.isNew=1";
//            $rs=$this->pdb->query($q);
//            return $rs[0]['n'];
//	}
        
        function countUnreadMsg() {
             if(USER_TYPE==='P')
                $read = 'A.readPatient';
            else 
                $read = 'A.readDoctor';
            $q="select COUNT(A.id) n from mp_messages A JOIN mp_users B ON (A.toId=B.id) JOIN mp_users C ON (A.fromId=C.id) where A.toId=".USER_ID." AND 
            A.recievedDeleted=0 AND $read=0";
            $rs=$this->pdb->query($q);
            return $rs[0]['n'];
	}
}

