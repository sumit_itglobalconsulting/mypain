<?php 
class Comm extends CI_Model { 
        
	function patientDueForms($userId=0) { //Added group by A.formId on 10-jan-2015
		$today=date('Y-m-d 23:59:00');
		$q="select 
				DISTINCT A.doctorId, A.formId, B.formName, A.nextResDueDate, A.resFreq, C.id
			from 
				mp_patient_forms A 
			join 
				mp_forms B ON (A.formId=B.id) 
			left join 
				mp_response C ON (A.userId=C.userId AND A.formId=C.formId) 
			left join 
				mp_patient_form_scales G ON (G.formId=A.formId AND G.userId=A.userId)
			where 
				A.userId=$userId AND A.readPatient='0' AND
				A.status=1 AND A.type=1 AND ( (A.nextResDueDate<='$today' AND A.resFreq!='O') OR  (A.nextResDueDate<=NOW() AND A.resFreq='O' AND G.baselineScore IS NULL ) )  group by A.formId
			";
			
			
		return $this->pdb->query($q);
	}
	
	function followUpDue($doctorId=0) {
		$today=date('Y-m-d 00:00:00');
		$cond="A.archived=0 AND A.discharged=0 AND A.doctorId=$doctorId AND A.followUpDone=0 AND A.followUpDate<='$today' AND A.needFollowUp='Y'";
		
		$q="select CONCAT (B.firstName, ' ', B.lastName) name from mp_patients A JOIN mp_users B ON (A.userId=B.id) where $cond";
		return $this->pdb->query($q);
	}
	
	function resDue($doctId=0) {
		$cond="B.doctorId=$doctId AND A.type='P' AND B.archived=0 AND B.discharged=0 AND C.readDoctor='0'";
		$cond.=" AND ( (C.nextResDueDate<=NOW() AND C.resFreq!='O') OR (C.nextResDueDate<=NOW() AND C.resFreq='O' AND G.baselineScore IS NULL) )";
		
		$q="SELECT 
				DISTINCT A.id, A.parentId, A.type, A.hospitalId, CONCAT(A.firstName, ' ', A.lastName) name
			FROM 
				mp_users A 
			JOIN 
				mp_patients B ON (A.id=B.userId) 
			LEFT JOIN 
				mp_patient_forms C ON (C.userId=A.id AND C.type=1 AND C.doctorId=B.doctorId) 
			LEFT JOIN 
				mp_patient_form_scales G ON (G.formId=C.formId AND G.userId=A.id AND G.doctorId=B.doctorId)
			WHERE 
				$cond 
			ORDER BY 
				name ASC;";
				
		return $this->pdb->query($q);
	}
	
	
	/** CronJobs **/
	function resDuePatients() {
		$today=date('Y-m-d 23:59:00');
		$q="select 
				U.notificationEmail, U.notificationMsg, U.loginEmail, U.deviceToken, CONCAT(U.firstName, ' ', U.lastName) name, GROUP_CONCAT(DISTINCT B.formName) forms, 
				CONCAT(U.stdCd1, phone1) mob 
			from 
				mp_patient_forms A 
			join 
				mp_forms B ON (A.formId=B.id) 
			left join 
				mp_response C ON (A.userId=C.userId AND A.formId=C.formId) 
			join 
				mp_users U ON (A.userId=U.id) 
			left join 
				mp_patient_form_scales G ON (G.formId=A.formId AND G.userId=U.id)
			where 
				A.status=1 AND A.type=1 AND ( (A.nextResDueDate<='$today' AND A.resFreq!='O') OR  (A.nextResDueDate<=NOW() AND A.resFreq='O' AND G.baselineScore IS NULL ) ) 
			group by 
				U.loginEmail ";
			
		$rs= $this->pdb->query($q);
		
		return $rs;
	}
	
	function followUpDueDoctors() {
		$today=date('Y-m-d 00:00:00');
		$cond="A.archived=0 AND A.discharged=0 AND A.followUpDone=0 AND A.followUpDate<='$today' AND A.needFollowUp='Y'";
		
		$q="select U.loginEmail doctorEmail, GROUP_CONCAT( CONCAT (B.firstName, ' ', B.lastName) ) patientName, 
		CONCAT(U.firstName, ' ', U.lastName) doctorName from mp_patients A 
		JOIN mp_users B ON (A.userId=B.id) JOIN mp_users U ON (A.doctorId=U.id) where $cond GROUP BY doctorEmail";
		return $this->pdb->query($q);
	}
        
        function doctorInvitationDue($userId=0){
            $q="select A.id, CONCAT(A.firstName, ' ',A.lastName) doctorName, A.loginEmail from mp_users A INNER JOIN mp_patients B ON B.doctorId=A.id where B.userId=$userId AND B.invitation=1";
            return $this->pdb->query($q);
        }
        
        function doctorInvitationApprove($userId=0){
            $q="select CONCAT(A.firstName, ' ',A.lastName) patientName from mp_users A INNER JOIN mp_patients B ON B.userId=A.id where B.doctorId=$userId AND B.invitation=2";
            return $this->pdb->query($q);
        }
}

