<?php 
class Form_model extends CI_Model {
	/** Form **/
	function save($data, $tbl="mp_forms"){
		if($id=$data['id']){
			$data['updated']=currentDate();
			$success=$this->pdb->update($tbl, $data, "id=$id");
			if(!$success)
				return 0;
		}
		else{
			$data['created']=$data['updated']=currentDate();
			$id=$this->pdb->insert($tbl, $data);
		}
		
		return $id;
	}
	
	function detail($id) {
		$q="select A.*, GROUP_CONCAT(B.catId) catIds from mp_forms A left join mp_form_categories B 
		    ON (A.id=B.formId) where A.id=$id group by A.id";
		$rs=$this->pdb->query($q);
		return $rs[0];
	}
	
	function listForms() {
		$qs=arrayUrlDecode(addSlash(arrayTrim($_GET)));
		$cond="1";
		
		if($qs['catId'])
			$cond.=" AND B.catId='{$qs['catId']}'";
			
		$q="select DISTINCT A.id, A.formName, A.image from mp_forms A left join mp_form_categories B ON (A.id=B.formId) where $cond order by A.formName,A.formTitle;";
		return $this->pdb->query($q);
	}
	
	
	/** Form Questions **/
	function listQues($formId) {
		$q="SELECT A.* FROM mp_form_question A WHERE A.formId=$formId";
		return $this->pdb->query($q);
	}
	
	function listQuesGroup() {
		$q="SELECT A.id, A.title FROM mp_ques_group A";
		$list=$this->pdb->query($q);
		if($list){
			return multiArrToKeyValue($list, 'id', 'title');
		}
		
		return array();
	}
	
	function saveQues($data) {
		if(!$data['isParent'])
			$data['isParent']=0;
		return $this->save($data, "mp_form_question");
	}
	
	function quesDetail($id) {
		return $this->pdb->singleRow("mp_form_question", "id=$id");
	}
	
	function saveQuesOption($data) {
		return $this->save($data, "mp_form_ques_options");
	}
	
	function listQuesOptions($quesId) {
		return $this->pdb->select("mp_form_ques_options", "quesId=$quesId");
	}
	
	/** Plans and Pricing **/
	function listPlans() {
		return $this->pdb->select("mp_plans");
	}
	
	function savePlan($data) {
		return $this->save($data, "mp_plans");
	}
	
	function planDetail($id) {
		return $this->pdb->singleRow("mp_plans", "id=$id");
	}
	
	/** Form Categories **/
	function cats() {
		return $this->pdb->select("mp_form_cats");
	}
}
