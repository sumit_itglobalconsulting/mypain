<?php 
class Report_model extends CI_Model {
	function listForms() {
		return $this->pdb->select("mp_forms", "1", "id, formName");
	}
	
	function getNoOfFormUsers($formId) {
		$q="select COUNT(DISTINCT A.userId) n from mp_patient_forms A JOIN mp_patients B ON (A.userId=B.userId) where A.formId=$formId AND B.discharged=0";
		$rs=$this->pdb->query($q);
		return $rs[0]['n'];
	}
	
	function firstLastDates() {
		$q="select MIN(created) sdate, MAX(created) edate from mp_patient_forms";
		$rs=$this->pdb->query($q);
		return $rs[0];
	}
	
	function formInvAnalysis($data) {
		$from=strtotime($data['fromDate']);
		$to=strtotime($data['toDate']);
		
		$n=floor(($to-$from)/3600/24)+1;
		if($n<=7)
			$rs=$this->invResInDays($data['formId'], $from, $to);
		else if($n>7)
			$rs=$this->invResInMonths($data['formId'], $from, $to);
			
		return $rs;
	}
	
	function invResInDays($formId, $from, $to){
		$from=timeStamp($from);
		$to=date('Y-m-d 23:59:00', $to);
		
		$q="select DATE_FORMAT(A.created, '%d %M') time, COUNT(DISTINCT A.userId) n from mp_patient_forms A where A.created>='$from' AND A.created<='$to' 
			GROUP BY time ORDER BY A.created ASC";
		$rs= $this->pdb->query($q);
		return $rs;
	}
	
	function invResInMonths($formId, $from, $to){
		$from=timeStamp($from);
		$to=date('Y-m-d 23:59:00', $to);
		
		$q="select DATE_FORMAT(A.created, '%b %Y') time, COUNT(DISTINCT A.userId) n from mp_patient_forms A where A.formId=$formId AND A.created>='$from' AND A.created<='$to' 
			GROUP BY time ORDER BY A.created ASC";
		$rs=$this->pdb->query($q);
		return $rs;
	}
	
	function resWisePatients($resType='P') {
		$cond="A.type='P' AND B.discharged=0 AND B.archived=0";
		
		$havCond="1";
		
		if($resType=='P')
			$havCond="overallRes<=30 OR overallRes IS NULL";
		else if($resType=='A')
			$havCond="overallRes>30 AND overallRes<=60";
		else if($resType=='G')
			$havCond="overallRes>60";
		
		$q="SELECT 
				DISTINCT A.id ,
				
				ROUND( 
					AVG( IF( F.realTotalScore=E.baselineScore, ((E.lastScore-E.baselineScore)*10), (((E.lastScore-E.baselineScore)*100)/(F.realTotalScore-E.baselineScore)) ) ) 
				) overallRes
			FROM 
				mp_users A 
			JOIN 
				mp_patients B ON (A.id=B.userId) 
			LEFT JOIN 
				mp_patient_forms C ON (C.userId=A.id AND C.type=1) 
			LEFT JOIN 
				mp_forms D ON (C.formId=D.id AND D.setGoal=1 AND D.showReport=1) 
				
			LEFT JOIN 
				mp_patient_form_scales E ON (D.id=E.formId) 
			LEFT JOIN
				mp_form_scales F ON (F.id=E.scaleId)
				
			WHERE 
				$cond 
			GROUP BY 
				A.id 
			HAVING 
				$havCond";
			
		$rs= $this->pdb->query($q);
		
		return $rs?count($rs):0;
	}
	
	function goalAnalys($formId) {
		$g=$this->pdb->singleVal("mp_forms", "id=$formId", "defaultGoodRes");
		$data['defaultGoodRes']=$g;
		$data['withUs']=$this->pdb->singleVal("mp_patient_forms", "formId=$formId AND goodRes=$g", "COUNT(DISTINCT userId)");
		$data['settingOwn']=$this->pdb->singleVal("mp_patient_forms", "formId=$formId AND goodRes!=$g", "COUNT(DISTINCT userId)");
		return $data;
	}
}

