<?php 
class User_model extends CI_Model {
	function updateProfile($data) {
		if($userId=$data['id']){
			$data['updated']=currentDate();
			
			$data['phone1']=$data['stdCode1'].$data['phone1'];
			$data['phone2']=$data['stdCode2'].$data['phone2'];
			
			$this->pdb->update("mp_users",$data,"id=".$data['id']);
			unset($data['id']);
			$this->pdb->update("mp_superadmin",$data,"userId=".$userId);
			return true;
		}
	}
	
	function changePassword($data) {
		if($data['id'] && $data['password']){
			$data['password']=encryptText($data['password']);
			$this->pdb->update("mp_users",$data,"id=".$data['id']);
			return true;
		}
	}
	
	function countUsers() {
		$m= date('n', time());
		$y= date('Y', time());
		/*****************Start Nitin Code*******************/
		//////////////Total Current Month User///////////////
		$current_month_users = "SELECT count(*) as current_month_user FROM mp_users  WHERE YEAR(created) = $y AND MONTH(created) = $m AND type='H'";
		$re = $this->pdb->query($current_month_users);
		$data['totalCurMonth'] = $re[0]['current_month_user'];
		
		//////////////User Expire In Current Week///////////////
		
		$expir_userDetails = $this->userExpireInWeek();
		$data['expireCurWeek'] = $expir_userDetails['total'];
		
		////////////count user group by plan /////////////
		$data['countUserByGroup'] = $this->countUserGroupBy();
		
		/*****************End Nitin Code*******************/
		$data['total']=$this->pdb->singleVal("mp_users", "type='H'", "COUNT(id)");
		//$data['totalCurMonth']=$this->pdb->singleVal("mp_users", "type='H' AND 1", "COUNT(id)");
		return $data;
	}
	
	/*****************Start Nitin Code*******************/
	
	function userExpireInWeek(){
		$expire_Cur_Week = "SELECT id,duration, durationType FROM mp_plans";
		$re = $this->pdb->query($expire_Cur_Week);
		foreach($re as $plan)
		{
			switch($plan['durationType'])
			{
				Case 'M' : $Duration = $plan['duration']*30;
							$expir_date = strtotime(date('Y-m-d H:i:s') . " -$Duration day");
							$expir_date = date('Y-m-d H:i:s', $expir_date);
						break;
						
				Case 'D' : $Duration = $plan['duration'];
							$expir_date = strtotime(date('Y-m-d H:i:s') . " -$Duration day");
							$expir_date = date('Y-m-d H:i:s', $expir_date);
						break;
						
				Case 'Y' : $Duration = $plan['duration']*365;
							$expir_date = strtotime(date('Y-m-d H:i:s') . " -$Duration day");
							$expir_date = date('Y-m-d H:i:s', $expir_date);
							
						break;
			}
			$plan_id = $plan['id'];
			
			$ts = strtotime($expir_date);
			$startDate = (date('w', $ts) == 0) ? $ts : strtotime('last monday', $ts);
			$endDate = (date('w', $ts) == 0) ? $ts : strtotime('next sunday', $ts);
			$startDate = date('Y-m-d H:i:s', $startDate);
			$endDate = date('Y-m-d H:i:s', $endDate);
			
			$query = "SELECT count(MHP.userId) as expireinweek FROM mp_users MPU LEFT JOIN mp_hospital_plans MHP ON MHP.userId = MPU.id  WHERE MPU.type='H' AND MHP.planId=$plan_id AND MHP.created BETWEEN '$startDate' AND '$endDate'";
			$qre = $this->pdb->query($query);
			$result[$plan['id']] = $qre[0]['expireinweek'];
			
		}
		$result['total'] = array_sum($result);
		return $result;
	}
	
	function countUserGroupBy()
	{
		$where =" WHERE 1";
		if($_POST['fdate'])
		{
			$fdate=timeStamp($_POST['fdate']);
			$where .=" AND MHP.created >= '$fdate'";
		}
		if($_POST['tdate'])
		{
			$fdate=timeStamp($_POST['tdate']);
			$where .=" AND MHP.created <= '$tdate'";
		}
		$query = "SELECT MP.id planId, MP.planName,count(DISTINCT MHP.userId) as users FROM mp_plans MP LEFT JOIN mp_hospital_plans MHP ON MHP.planId = MP.id $where 
		GROUP BY MP.id order by MP.planName";
		$qre = $this->pdb->query($query);
		return $qre;
	}
	
	function getUserByPlan($plan="", $p, $ps)
	{
		$where =" WHERE A.type='H'";
		if($plan && is_numeric($plan))
		{
			$where .= " AND C.planId=$plan";
		}
		if(isset($_GET['searchVal']))
		{
			$searchval = $_GET['searchVal'];
			$where .=" AND (A.firstName like '%$searchval%' OR A.lastName like '%$searchval%' OR A.loginEmail like '%$searchval%' OR B.hospitalName like '%$searchval%')";
		}
		
		if($_GET['searchDate'])
		{
			$searchDate = $_GET['searchDate'];
			$where .=" AND MPU.created =".date('Y-m-d H:i:s', strtotime($searchDate));
		}
		
		$query ="SELECT 
					A.*, D.planName, D.duration, D.durationType, B.hospitalName 
				FROM 
					mp_users A 
				JOIN 
					mp_hospitals B ON B.userId = A.id 
				JOIN 
					mp_hospital_plans C ON C.userId = A.id 
				JOIN 
					mp_plans D ON D.id = C.planId 
				$where ";
		$qre = $this->pdb->pagedQuery($query, $p, $ps);
		
		return $qre;
	}
	
	function userDetail($userId)
	{
		$query =  "SELECT MPU.*, MP.planName, MP.duration, MP.durationType, MPH.hospitalName, MPH.regNo, MPH.branchName, MPH.punchLine FROM mp_users MPU 
				LEFT JOIN mp_hospital_plans MHP ON MHP.userId = MPU.id
				LEFT JOIN mp_plans MP ON MP.id = MHP.planId
				LEFT JOIN mp_hospitals MPH ON MPH.userId = MPU.id
				where MPU.id=$userId";
		$qre = $this->pdb->query($query);
		//pr($qre); die();
		return $qre[0];
	}
	
	function updateUser($data) {
		$data['updated']=currentDate();
		
		$data['phone1']=$data['stdCode1'].$data['phone1'];
		$data['phone2']=$data['stdCode2'].$data['phone2'];
		$hdata = array(
				'branchName' => $data['branchName'],
				'punchLine'=> $data['punchLine']
				);
		$this->pdb->update("mp_users",$data,"id=".$data['id']);
		
		$this->pdb->update("mp_hospitals",$hdata,"userId=".$data['id']);
		return true;
	}
	/*****************End Nitin Code*******************/
	
	
	/** Misc **/
	
}

