<?php 
class User_model extends CI_Model {
        function __construct() {
            parent::__construct();
            define("TBL_ATTEMPTS", "mp_login_attempts");
            define("ATTEMPTS_NUMBER", 2);
            define("TIME_PERIOD", 240);
        }
        
	function login($data) {
		$username=$data['username'];
		$pass=encryptText($data['password']);
		$cond="(username=:uname AND password=:pass) OR (loginEmail=:uname AND password=:pass)";
		return $this->pdb->singleRow("mp_users", $cond, "*", array(':uname'=>$username, ':pass'=>$pass));
	}
	
	/** Hospital Register **/
	function registerHospital($data) {
		$data['type']='H';
		$data['created']=$data['updated']=currentDate();
		
		$data['phone1']=$data['stdCode1'].$data['phone1'];
		$data['phone2']=$data['stdCode2'].$data['phone2'];
		
		$data['userId']=$this->pdb->insert("mp_users", $data);
		unset($data['id']);
		$id=$this->pdb->insert("mp_hospitals", $data);
		
		$inf['userId']=$data['userId'];
		$inf['planId']=$data['planId'];
		$inf['created']=currentDate();
                $inf['status']=1;
                $inf['expiryDate']=date('Y-m-d', strtotime("+30 days"));
		$this->pdb->insert("mp_hospital_plans", $inf);
		
		return $data['userId'];
	}
        
         /** Last Modified Date: 20 DEC 2014 7:09 PM 
           *  Modified By: Php Developer 2 
          */
        
        /** Add login attempts if wrong **/
        function addLoginAttempt($value) {
            //Increase number of attempts. Set last login attempt if required.
            $q = "SELECT * FROM ".TBL_ATTEMPTS." WHERE ip = '$value'";
            $data = $this->pdb->query($q);
            $data=$data[0];
            if(!empty($data))
            {
              $attempts = $data["attempts"]+1;        

              if($attempts==3) {
                $q = "UPDATE ".TBL_ATTEMPTS." SET attempts=".$attempts.", lastlogin=NOW() WHERE ip = '$value'";
                $this->pdb->query($q);
              }
              else {
                $q = "UPDATE ".TBL_ATTEMPTS." SET attempts=".$attempts." WHERE ip = '$value'";
                $this->pdb->query($q);
              }
            }
            else {
              $q = "INSERT INTO ".TBL_ATTEMPTS." (attempts,IP,lastlogin) values (1, '$value', NOW())";
              $this->pdb->query($q);
              return 1;
            }
            return $attempts;
         }
         
         
         function confirmIPAddress($value) {
            $q = "SELECT attempts, (CASE when lastlogin is not NULL and DATE_ADD(LastLogin, INTERVAL ".TIME_PERIOD.
            " MINUTE)>NOW() then 1 else 0 end) as Denied FROM ".TBL_ATTEMPTS." WHERE ip = '$value'";

            $data =  $this->pdb->query($q);
            $data = $data[0]; 
            //Verify that at least one login attempt is in database

            if (empty($data)) {
              return 0;
            }
            if ($data["attempts"] >= ATTEMPTS_NUMBER)
            {
              if($data["Denied"] == 1)
              {
                return 1;
              }
              else
              { 
                $this->clearLoginAttempts($value);
                return 0; 
              }
            }
            return 0;
          }
          
          /** Clear login attempts if trying after 30 minutes **/
          function clearLoginAttempts($value) {
            $q = "UPDATE ".TBL_ATTEMPTS." SET attempts = 0 WHERE ip = '$value'";
            $this->pdb->query($q);
          }
          
          /**
           * End Modified
           */
          
         /** Hospital Payment **/
//	function paymentHospital($data) {
//		$data['type']='H';
//		$data['created']=$data['updated']=currentDate();
//		$data['userId']=$this->pdb->insert("mp_users", $data);
//		unset($data['id']);
//		$id=$this->pdb->insert("mp_hospitals", $data);
//		
//		$inf['userId']=$data['userId'];
//		$inf['planId']=$data['planId'];
//		$inf['created']=currentDate();
//		$this->pdb->insert("mp_hospital_plans", $inf);
//		
//		return $data['userId'];
//	}
        
        /** Customer Register **/
	function registerCustomer($data){ 
            $data['type']='H';
            $data['created']=$data['updated']=currentDate();
            
            $id = $this->checkExisting($data['loginEmail']);
            if(empty($id))
                $data['userId']=$this->pdb->insert("mp_users", $data);
            else{
                $this->pdb->update("mp_users", $data, "id=:userId", array(':userId'=>$id));
                $data['userId'] = $id;
            }

            $inf['transactionId'] = $data['vendorTxCode'];
            $inf['userId']=$data['userId'];
            $inf['planId']=$data['planId'];
            $inf['noOfPatient']=$data['noOfPatient'];
            $inf['created']=currentDate();
            $inf['updated']=currentDate();
            $this->pdb->insert("mp_hospital_plans", $inf);
	}
        
        function checkExisting($loginEmail){
            $cond = "loginEmail='".$loginEmail."'";
            $id=$this->pdb->singleVal("mp_users", $cond, "id");
            return $id;
        }
        
        function paymentHosp($data){
            $cond="vendorTxCode='".$data['VendorTxCode']."'";
            $userId=$this->pdb->singleVal("mp_users",$cond,"id");
            
            
            $inf['paymentStatus']=1;
            $inf['payment']=$data['Amount'];
            $inf['payment_date']=  currentDate();
            $inf['expiryDate']= date('Y-m-d', strtotime('+1 years'));
            $inf['status']=1;
            $inf['updated'] = currentDate();
            
            $this->pdb->update("mp_hospital_plans", $inf, "transactionId=:transactionId", array(':transactionId'=>$data['VendorTxCode']));
            
            $cond="id='".$userId."'";
            $userDtl=array();
           
            $password = time();
            $tmp['password']=encryptText($password);
            $this->pdb->update("mp_users", $tmp, $cond);
            
            $userDtl = $this->pdb->singleRow("mp_users", $cond, "id, firstName, loginEmail");
           
            $userDtl['realPass'] = $password;
            return $userDtl;
        }
        
        function autoRegister($userId){
            $cond="id='".$userId."'";
            $userDtl=$this->pdb->singleRow("mp_users",$cond);
            return !empty($userDtl)?$userDtl:'';
        }
        
        function updateAutoRegister($data){
            $cond="id='".$data['userId']."'";
            $data['updated'] = currentDate(); 
            $this->pdb->update("mp_users", $data, $cond);
            $id=$this->pdb->insert("mp_hospitals", $data);
            !empty($id)?$id:'';
        }
        function checkExpire($userDtl){ 
               switch($userDtl['type']){
                    case 'H':
                            $userId= $userDtl['id'];
                    break;

                    case 'D':
                            $userId= $userDtl['parentId'];
                    break;

                    case 'P':
                            $userId=$this->patientHospId($userDtl['parentId']);
                    break;

                    
               }
              $cond = "userId='".$userId."' and status='1'";
              $planDtl=$this->pdb->singleRow("mp_hospital_plans",$cond,"planId, expiryDate");
              $planDtl['type']=$this->pdb->singleVal("mp_plans","id='".$planDtl['planId']."'","type");
              return !empty($planDtl)?$planDtl:'';
        }
        
        function patientHospId($docId){
            $cond="id='".$docId."'";
            return $this->pdb->singleVal("mp_users",$cond,"parentId");
        }
        
        function showCustomerDetail($loginEmail){
            $cond="loginEmail='".$loginEmail."'";
            $rs = $this->pdb->singleRow("mp_users",$cond,"firstName,lastName,address,city,state,country,zipcode");
            return !empty($rs)?$rs:'';
        }
       
        function checkHospAlreadyRegi($userId){
            $cond = "userId='".$userId."'";
            $id = $this->pdb->singleval("mp_hospitals",$cond,"id");
            return $id;
        }
}

