<?php 
class Help_category_model extends CI_Model {
	function save($data, $tbl=""){
		if(!$tbl)
			return;
			
		if($id=$data['id']){
			$data['updated']=currentDate();
			$success=$this->pdb->update($tbl, $data, "id=$id");
			if(!$success)
				return 0;
		}
		else{
			$data['created']=$data['updated']=currentDate();
			$id=$this->pdb->insert($tbl, $data);
		}
		
		return $id;
	}
    
    function lists($p, $ps, $withOutLimit = false) {	    
		$q="select * from mp_help_category ORDER BY name asc";
        
        if($withOutLimit){
              return $this->pdb->query($q);
        } else {
		      return $this->pdb->pagedQuery($q, $p, $ps);
        }
	}
    
    function helpCategoryDetails($id){
		$q="SELECT * FROM mp_help_category  WHERE id=:helpId";
		$result = $this->pdb->query($q, array(':helpId'=>$id));
        
        return ($result)? $result['0'] : '';
	}
    
    function deleteHelpCategory($id){		
		return $this->pdb->delete("mp_help_category", "id=:helpId", array(':helpId'=>$id)); 
	}
}

