<?php

class Common_model extends CI_Model {

    function singleRowQuery($q) {
        $rs = $this->pdb->query($q);
        return $rs[0];
    }

    
    function userDetail($id, $doctorId = 0) {
        $type = $this->pdb->singleVal("mp_users", "id=:id", "type", array(':id' => $id));
        switch ($type) {
            case 'S':
                $q = "SELECT A.*, CONCAT(A.firstName, ' ', A.lastName) fullName, B.userId FROM mp_users A JOIN mp_superadmin B ON (A.id=B.userId) 
				WHERE A.id=$id";
                $dtl = $this->singleRowQuery($q);
                break;

            case 'H':
                $q = "SELECT A.*, CONCAT(A.firstName, ' ', A.lastName) fullName, B.userId, B.hospitalName, B.regNo, B.branchName, B.punchLine FROM mp_users A 
				    JOIN mp_hospitals B ON (A.id=B.userId) WHERE A.id=$id";
                $dtl = $this->singleRowQuery($q);
                break;

            case 'D':
                $q = "SELECT CONCAT('MP', A.id) uniqueId, A.*, CONCAT(A.firstName, ' ', A.lastName) fullName, B.userId, B.deptId, B.patientInvitationLimit, C.hospitalName, 
					C.regNo, C.branchName, 
				    C.punchLine, D.name deptName, D.deptHead, 
					E.image hospitalLogo, E.firstName hospitalAdminName FROM mp_users A JOIN mp_doctors B ON (A.id=B.userId) JOIN mp_hospitals C 
					ON (A.parentId=C.userId) 
					JOIN mp_departments D ON (B.deptId=D.id) JOIN mp_users E ON (A.parentId=E.id) 
					WHERE A.id=$id";
                $dtl = $this->singleRowQuery($q);
                break;

            case 'P':
                $q = "SELECT DISTINCT CONCAT('MP', A.id) uniqueId, A.*, CONCAT(A.firstName, ' ', A.lastName) fullName, B.userId, B.caseSummary, B.needFollowUp, B.followUpType, 
				    B.followUpDate, B.followUpFreq, B.followUpDone, B.followedDates, B.onOpioidTherapy, B.needAssistance, B.treatment1, 
					B.treatment2, B.treatment3, B.treatment4, B.archived, B.discharged, 
					F.image hospitalLogo, E.hospitalName, E.regNo, E.branchName, E.punchLine 
					FROM mp_users A LEFT JOIN mp_patients B ON (A.id=B.userId AND B.doctorId=$doctorId) 
					JOIN mp_hospitals E ON (A.parentId=E.userId) JOIN mp_users F ON (E.userId=F.id) WHERE A.id=$id";
                $dtl = $this->singleRowQuery($q);
                break;

            case 'T':
                $q = "SELECT A.*, CONCAT(A.firstName, ' ', A.lastName) fullName FROM mp_users A  WHERE A.id=$id";
                $dtl = $this->singleRowQuery($q);
                break;
        }

        return $dtl;
    }

    function isEmailExists($email, $userId = 0) {
        $email = addslashes($email);
        if ($userId)
            $cond = "(loginEmail='$email' AND id!=$userId)";
        else
            $cond = "loginEmail='$email'";

        return $this->pdb->singleVal("mp_users", $cond, "id");
    }

    function listPlans() {
        $cond = "custom=0";
        return $this->pdb->select("mp_plans");
    }

    function listAllForms() {
        $qs = arrayUrlDecode(addSlash(arrayTrim($_GET)));
        $cond = "1";

        if ($qs['catId'])
            $cond.=" AND B.catId='{$qs['catId']}'";

        $q = "select DISTINCT A.id, A.formName, A.image from mp_forms A left join mp_form_categories B ON (A.id=B.formId) where $cond order by A.formName,A.formTitle;";
        return $this->pdb->query($q);
    }

    function formDetail($formId) {
        $q = "select B.id formId, B.formName, B.formTitle, B.pdf, B.userGuide, B.toolkit, B.supportingStatement, B.infoForClinician, B.functionsOfForm, B.infoForPatient, 
			B.createdBy, B.formInfo, B.image, B.defaultGoodRes, B.defaultAvgRes from mp_forms B where B.id=$formId";
        $rs = $this->pdb->query($q);
        return $rs[0];
    }

    function doctorForms($userId = 0) {
        $qs = arrayUrlDecode(addSlash(arrayTrim($_GET)));

        $cond = "A.userId=$userId";
        if ($qs['catId'])
            $cond.=" AND C.catId='{$qs['catId']}'";
        $q = "select A.formId, B.formName, B.formTitle, GROUP_CONCAT(C.catId) catIds, B.image, B.setGoal from mp_doctor_forms A JOIN mp_forms B 
			ON (A.formId=B.id) left join mp_form_categories C ON (A.formId=C.formId) where $cond group by A.formId order by B.formName,B.formTitle";

        return $this->pdb->query($q);
    }

    function hospitalForms($userId = 0) {
        $qs = arrayUrlDecode(addSlash(arrayTrim($_GET)));
        $cond = "A.userId=$userId";
        if ($qs['catId'])
            $cond.=" AND C.catId='{$qs['catId']}'";

        $q = "SELECT DISTINCT A.id, A.formId, A.created addedDate, B.formName, B.image FROM mp_hospital_forms A 
			JOIN mp_forms B ON (A.formId=B.id) LEFT JOIN mp_form_categories C ON (A.formId=C.formId) WHERE $cond order by B.formName,B.formTitle";
        return $this->pdb->query($q);
    }

    function patientForms($userId = USER_ID, $doctorId = 0, $onlyScore = 0, $euroQolPgic = 0) {
        if(true){  //added for remove notification in patient header
            $today=date('Y-m-d 23:59:00');
            
            $q="update mp_patient_forms A  set A.readPatient='1' where 
				A.userId=$userId AND 
				A.status=1 AND A.type=1 AND ( (A.nextResDueDate<='$today' AND A.resFreq!='O') OR  (A.nextResDueDate<=NOW() AND A.resFreq='O' ) )";
            $this->pdb->query($q);
        }
        $qs = arrayUrlDecode(addSlash(arrayTrim($_GET)));

        $cond = "A.userId=$userId AND A.type=1 AND A.status=1";
        if ($doctorId)
            $cond = "A.userId=$userId AND A.type=1 AND A.doctorId=$doctorId";

        if ($onlyScore) {
            if ($euroQolPgic)
                $cond.=" AND (B.showReport=1 OR B.id=2 OR B.id=19 OR B.id=7)";
            else
                $cond.=" AND B.showReport=1";
        }

        if ($qs['catId'])
            $cond.=" AND D.catId='{$qs['catId']}'";

        $q = "select DISTINCT A.formId, DATE_FORMAT(A.nextResDueDate, '%Y-%m-%d') nextResDueDate, A.symptom, A.weightage, A.resFreq, B.setGoal, 
			B.formName, B.formTitle, B.image, C.id doctorId, CONCAT(C.firstName, ' ', C.lastName) doctorName 
			from mp_patient_forms A JOIN mp_forms B ON (A.formId=B.id) JOIN mp_users C ON (A.doctorId=C.id) 
			LEFT JOIN mp_form_categories D ON (A.formId=D.formId) where $cond order by B.formName,B.formTitle;";
        return $this->pdb->query($q);
    }

    function patientFormDetail($formId, $userId = USER_ID) {
        $q = "select A.formId, A.nextResDueDate, A.symptom, B.formName, B.formTitle, B.pdf, B.userGuide,  B.toolkit, B.supportingStatement, 
			B.infoForClinician, B.functionsOfForm, B.infoForPatient, B.createdBy, B.formInfo, B.image, 
			CONCAT(C.firstName, ' ', C.lastName) doctorName 
			from mp_patient_forms A JOIN mp_forms B ON (A.formId=B.id) JOIN mp_users C ON (A.doctorId=C.id) where A.formId=$formId AND 
			A.userId=$userId AND A.type=1;";
        $rs = $this->pdb->query($q);
        return $rs[0];
    }

    function superAdminDetail() {
        return $this->pdb->singleRow("mp_users", "type='S'");
    }

    function getUserLastPlanDtl($userId = 0) {
        /*         * Added on 14-jan-2015* */
        $cond = "A.userId='" . $userId . "' and A.expiryDate>now()";
        /*         * End Adding * */
        /* $rs=$this->pdb->query("select A.planId, A.created buyDate, B.* from mp_hospital_plans A JOIN mp_plans B ON (A.planId=B.id) 
          order by A.id desc limit 0,1;"); */

        /*         * Added on 14-jan-2015* */
        $rs = $this->pdb->query("select A.planId, A.created buyDate, B.*,C.formId from mp_hospital_plans A JOIN mp_plans B ON (A.planId=B.id) JOIN mp_plan_form C ON (A.planId=C.planId) 
		order by A.id desc limit 0,1;");
        /*         * End Adding * */
        if ($rs) {
            unset($rs[0]['id']);
            return $rs[0];
        }
    }

    function doctorsOfPatient($userId = USER_ID, $online = false) {
        $cond = "B.userId=$userId";
        if ($online) {
            $c1hr = date('Y-m-d H:i:s', strtotime('-1 hours', time()));
            $cond.=" AND (A.loggedIn=1 AND A.lastActTime>'$c1hr')";
            $orderby = " order by fullName";
        }

        $q = "SELECT CONCAT('MP', A.id) uniqueId, A.id, A.id doctorId, A.onlineStatus, B.userId patientId, CONCAT(A.firstName, ' ', A.lastName) fullName, A.loggedIn, 
			DATE_FORMAT(B.followUpDate, '%Y-%m-%d') followUpDate, B.followedDates 
			FROM mp_users A JOIN mp_patients B ON (A.id=B.doctorId) WHERE $cond $orderby;";
        return $this->pdb->query($q);
    }

    function listPatients($userId = USER_ID, $online = false) {
        $cond = "B.doctorId=$userId AND A.type='P'";
        if ($online) {
            $c1hr = date('Y-m-d H:i:s', strtotime('-1 hours', time()));
            $cond.=" AND (A.loggedIn=1 AND A.lastActTime>'$c1hr')";
        }

        $q = "SELECT CONCAT('MP', A.id) uniqueId, A.id, A.id patientId, A.onlineStatus, B.doctorId, CONCAT(A.firstName, ' ', A.lastName) fullName FROM mp_users A 
			JOIN mp_patients B ON (A.id=B.userId)  
			WHERE $cond ORDER BY A.id DESC;";

        return $this->pdb->query($q);
    }

    function ques($formId) {
        $q = "SELECT DISTINCT A.*, GROUP_CONCAT(B.id,'|',B.optionName,'|',B.score, '|',B.caption ORDER BY B.id  SEPARATOR '~') optionDtl FROM mp_form_question A LEFT 
			JOIN mp_form_ques_options B ON (A.id=B.quesId) WHERE A.formId=$formId GROUP BY A.id";

        return $this->pdb->query($q);
    }

    function gppaq_trend($formId, $doctorId, $userId) {
        $formId = addslashes($formId);
        $userId = addslashes($userId);
        $resForm = $this->pdb->select("mp_response a1", "formId='$formId'  AND userId='$userId' AND 
										doctorId='$doctorId' GROUP BY DATE(a1.resDate) order by a1.id desc limit 5", "MAX(a1.id) id");
        foreach ($resForm as $res) {
            foreach ($res as $id) {
                $ids[] = $id;
            }
        }

        sort($ids);
        foreach ($ids as $id) {
            $fIds .= $id . ",";
        }
        $ids = substr($fIds, 0, -1);
        $q = "SELECT A.quesId, B.score score,DATE(D.resDate) resDate  FROM mp_response_details A LEFT JOIN mp_form_ques_options B 
ON (B.id=A.optionId) LEFT JOIN mp_form_question C ON (C.id=A.quesId) LEFT JOIN mp_response D ON (D.id=A.resId) WHERE A.resId 
IN ($ids) ORDER BY A.resId,A.quesId";
        $qd = $this->pdb->query($q);
        if (!empty($qd)) {
            $date1 = $qd[0]['resDate'];
            foreach ($qd as $row) {
                if ($date1 != $row['resDate']) {
                    $ts = 0;
                    $date1 = $row['resDate'];
                }
                $ts += $row['score'];
                $result[$row['resDate']]['ts'] = $ts;
            }
            $i = 1;
            $score[0][] = 'Genre';
            $score[0][] = 'Inactive';
            $score[0][] = 'Moderately Inactive';
            $score[0][] = 'Moderately Active';
            $score[0][] = 'Active';
            $score[0][] = array('role' => 'annotation');
            foreach ($result as $date => $value) {
                $score[$i][0] = date('d M', strtotime($date));
                $score[$i][4] = $score[$i][3] = $score[$i][2] = $score[$i][1] = 0;
                foreach ($value as $lastScore) {
                    if ($lastScore >= 3)
                        $score[$i][4] = 4;
                    elseif ($lastScore >= 2 and $lastScore < 3)
                        $score[$i][3] = 3;
                    elseif ($lastScore >= 1 and $lastScore < 2)
                        $score[$i][2] = 2;
                    else
                        $score[$i][1] = 1;
                }
                $score[$i][5] = '';
                $i++;
            }
            return $score;
        }
    }

    function star_back_risk($formId, $doctorId, $userId, $trend) {
        $formId = addslashes($formId);
        $userId = addslashes($userId);
        if (!$trend) {
            $resForm = $this->pdb->singleRow("mp_response", "formId='$formId'  AND userId='$userId' AND 
										doctorId='$doctorId' order by resDate DESC", "id, dueDate, resDate");
        } else {
            $resForm = $this->pdb->select("mp_response a1", "formId='$formId'  AND userId='$userId' AND 
										doctorId='$doctorId' GROUP BY DATE(a1.resDate) order by a1.id desc limit 5", "MAX(a1.id) id");
            foreach ($resForm as $res) {
                foreach ($res as $id) {
                    $ids[] = $id;
                }
            }

            sort($ids);
            foreach ($ids as $id) {
                $fIds .= $id . ",";
            }
            $ids = substr($fIds, 0, -1);
        }
        if (!empty($resForm) || !empty($ids)) {
            if (isset($ids) AND ! empty($ids)) {
                $q = "SELECT A.quesId, B.score score,DATE(D.resDate) resDate  FROM mp_response_details A LEFT JOIN mp_form_ques_options B 
ON (B.id=A.optionId) LEFT JOIN mp_form_question C ON (C.id=A.quesId) LEFT JOIN mp_response D ON (D.id=A.resId) WHERE A.resId 
IN ($ids) ORDER BY A.resId,A.quesId";
                $qd = $this->pdb->query($q);
                if (!empty($qd)) {
                    $sc_qid = array(67, 68, 69, 70, 71);
                    $fs = 63;
                    foreach ($qd as $row) {
                        if ($fs == $row['quesId']) {
                            $ts = 0;
                            $sc = 0;
                        }
                        $ts += $row['score'];
                        if (in_array($row['quesId'], $sc_qid)) {
                            $sc += $row['score'];
                        }
                        $result[$row['resDate']]['sc'] = $sc;
                        $result[$row['resDate']]['ts'] = $ts;
                    }
                    $i = 1;
                    $score[0][] = 'Genre';
                    $score[0][] = 'Sub Score';
                    $score[0][] = 'Total Score';
                    $score[0][] = array('role' => 'annotation');
                    foreach ($result as $date => $value) {
                        $score[$i][] = date('d M', strtotime($date));
                        foreach ($value as $data) {
                            $score[$i][] = $data;
                        }
                        $score[$i][] = '';
                        $i++;
                    }
                    //echo '<pre>';print_r($score);
                    return $score;
                }
            } else {
                $q = "select A.quesId, C.catName, C.ques, GROUP_CONCAT( A.ans SEPARATOR '|' ) ans, GROUP_CONCAT( A.file SEPARATOR '|' ) file, GROUP_CONCAT(A.optionId) optionId, 
				GROUP_CONCAT(B.optionName SEPARATOR '|') optionName, 
				GROUP_CONCAT(B.score) score 
				from mp_response_details A LEFT JOIN mp_form_ques_options B ON (B.id=A.optionId) LEFT JOIN mp_form_question C ON (C.id=A.quesId)
				where A.resId='{$resForm['id']}' GROUP BY A.quesId";
                $qd = $this->pdb->query($q);
                if (!empty($qd)) {
                    $ts = 0;
                    $sc = 0;
                    $sc_qid = array(67, 68, 69, 70, 71);
                    foreach ($qd as $row) {
                        $ts += $row['score'];
                        if (in_array($row['quesId'], $sc_qid)) {
                            $sc += $row['score'];
                        }
                    }
                    $score['ts'] = $ts;
                    $score['sc'] = $sc;
                    return $score;
                }
            }
        }
    }

    function responseDetail($formId, $resDate, $doctorId, $userId = USER_ID, $baseLine = false, $export = false) {
        $formId = addslashes($formId);
        $resDate = addslashes($resDate);
        $userId = addslashes($userId);

        if (!$resDate && (USER_TYPE == 'D' || $export)) {
            $order = $baseLine ? 'ASC' : 'DESC';
            $resDate = $this->pdb->singleVal("mp_response", "formId='$formId' AND userId='$userId' AND doctorId='$doctorId' 
											order by resDate $order", "resDate");
        }

        $resForm = $this->pdb->singleRow("mp_response", "formId='$formId' AND resDate='$resDate' AND userId='$userId' AND 
										doctorId='$doctorId'", "id, dueDate, resDate");


        if ($formId == 1) {
            $medTake = $this->pdb->singleVal("mp_bpi_med", "formId='$formId' AND resDate='$resDate' AND userId='$userId' AND 
										doctorId='$doctorId'", "medTake");
        }



        if ($resForm) {
            /* $q="select A.quesId, A.optionId, B.optionName, B.score from mp_response_details A JOIN mp_form_ques_options B ON (B.id=A.optionId) 
              where A.resId={$resForm['id']}"; */

            /** New * */
            $q = "select A.quesId, C.catName, C.ques, GROUP_CONCAT( A.ans SEPARATOR '|' ) ans, GROUP_CONCAT( A.file SEPARATOR '|' ) file, GROUP_CONCAT(A.optionId) optionId, 
				GROUP_CONCAT(B.optionName SEPARATOR '|') optionName, 
				GROUP_CONCAT(B.score) score 
				from mp_response_details A LEFT JOIN mp_form_ques_options B ON (B.id=A.optionId) LEFT JOIN mp_form_question C ON (C.id=A.quesId)
				where A.resId='{$resForm['id']}' GROUP BY A.quesId";

            $qd = $this->pdb->query($q);
            $quesDtl = array();
            if ($qd) {
                foreach ($qd as $k => $v) {
                    $quesDtl[$v['quesId']] = $v;
                }
            }
        }
        if ($formId != 1)
            return array('resForm' => $resForm, 'quesDtl' => $quesDtl);
        else
            return array('resForm' => $resForm, 'quesDtl' => $quesDtl, 'medTake' => $medTake);
    }

    function responseDates($formId, $doctorId, $userId = USER_ID) {
        $formId = addslashes($formId);
        $userId = addslashes($userId);

        $dates = $this->pdb->select("mp_response", "formId='$formId' AND userId='$userId' AND doctorId='$doctorId' order by resDate DESC", "resDate");
        $dates = array_map(function($a) {
            return $a['resDate'];
        }, $dates);
        return $dates;
    }

    function firstLastResDates($userId = USER_ID) {
        return $this->pdb->singleRow("mp_response", "userId='$userId'", "MIN(resDate) firstRes, MAX(resDate) lastRes");
    }

    function isFormSubmited($userId = USER_ID, $formId = 0) {
        $q = "select A.* from mp_response A JOIN mp_patient_forms B ON (A.formId=B.formId AND A.userId=B.userId AND A.doctorId=B.doctorId) 
			where A.userId=$userId AND A.formId=$formId AND B.status=1 order by id DESC";
        $rs = $this->pdb->query($q);
        return $rs[0];
        //return $this->pdb->singleRow("mp_response", "userId=$userId AND formId=$formId order by id DESC", "*");
    }

    function isDischarged($userId, $doctorId) {
        return $this->pdb->singleVal("mp_patients", "userId=$userId AND doctorId=$doctorId", "discharged");
    }

    function getFeelScore($userId = USER_ID) {
        $q = "select A.lastScore from mp_patient_form_scales A join mp_patient_forms B ON (A.formId=B.formId) where A.userId='$userId' AND A.formId=2 AND B.type=1";
        $rs = $this->pdb->query($q);
        $feel = $rs[0]['lastScore'];
        return $feel ? $feel : 0;
    }

    function baselineScore($formId = 0, $userId = USER_ID) { //doubt
        $doctorId = USER_TYPE == 'D' ? USER_ID : 0;
        $cond = "userId='$userId' AND formId=$formId AND status=1 AND type=1";
        if ($doctorId)
            $cond = "userId='$userId' AND formId=$formId AND doctorId=$doctorId AND type=1";

        $bs = $this->pdb->singleVal("mp_patient_forms", $cond, "baselineScore");
        return $bs ? $bs : 0;
    }

    function scalesReport($formId, $doctorId, $userId = 0) {
        $normal= "IF( 
						A.realTotalScore=B.baselineScore, ((B.lastScore-B.baselineScore)*10), 
						(((B.lastScore-B.baselineScore)*100)/(A.realTotalScore-B.baselineScore)) 
					) curRes";
        if($formId==5){
            $normal  = "(B.lastScore-B.baselineScore)/(B.baselineScore/100) curRes";
        }
        $cond = "A.formId='$formId' AND B.doctorId='$doctorId' AND B.userId='$userId'";
        $q = "select 
				A.id, A.groupName scale, A.totalScore, A.defaultGoodRes, B.baselineScore, B.lastScore, B.goodRes, B.avgRes, 
				 
					$normal
				
			from 
				mp_form_scales A 
			join
				mp_patient_form_scales B ON(B.scaleId=A.id) 
			where
				$cond";

        $cond = "formId='$formId' AND doctorId='$doctorId' AND userId='$userId'";
        $lastResTime = $this->pdb->singleVal("mp_response", $cond, "MAX(resDate)");
        
        $rs = $this->pdb->query($q);
        //pr($rs);
        $dtl['scales'] = $rs;
        if ($formId == 1) {
            $condMed = "formId='$formId' AND doctorId='$doctorId' AND userId='$userId' AND resDate='$lastResTime'";
            $medTake = $this->pdb->singleVal('mp_bpi_med', $condMed, 'medTake');
            $dtl['scales'][1]['medTake'] = $medTake;
        }

        $dtl['lastResTime'] = $lastResTime;

        $scaleIds = array_map(function($a) {
            return $a['id'];
        }, $rs);
        $scaleIds = implode(",", $scaleIds);
        if (!$scaleIds)
            $scaleIds = 0;

        $FLDates = $this->firstLastResDates($userId);
        $fromD = showDate($FLDates['firstRes']);
        $toD = showDate($FLDates['lastRes']);

        $reports = $this->scaleOverTime($formId, $scaleIds, $fromD, $toD, $userId, $doctorId);
        //pr($reports);

        $scalesAr = array();

        if ($reports) {
            foreach ($reports as $r) {
                if ($r['formId'] == 19) {
                    $scalesAr[$r['scaleId']][$r['responseDate']] = $r['euroVal' . $r['scaleId']];
                } else
                    $scalesAr[$r['scaleId']][$r['responseDate']] = $r['overallRes'];
            }
        }

        $dtl['scalesReport'] = $scalesAr;
        //pr($dtl);
        return $dtl;
    }

    function scaleOverTime($formId, $scaleIds, $from, $to, $userId, $doctorId) {
        $from = strtotime($from);
        $to = strtotime($to);
        $days = array();
        for ($a = $from; $a <= $to; $a = strtotime("+1 days", $a)) {
            $days[] = "'" . date('Y-m-d', $a) . "'";
        }

        $days = implode(",", $days);

        $cond = "A.formId='$formId' AND A.id IN(select MAX(id) from mp_response where 
			   DATE_FORMAT(resDate, '%Y-%m-%d') IN($days) AND userId='$userId' AND doctorId='$doctorId' group by formId, DATE_FORMAT(resDate, '%Y-%m-%d'))";
        
        
        $normal= "IF( 
					G.realTotalScore=F.baselineScore, (( SUM(D.score)-F.baselineScore)*10), 
					((( SUM(D.score)-F.baselineScore)*100)/(G.realTotalScore-F.baselineScore)) 
				) overallRes";
        if($formId==5){
            $normal  = "( SUM(D.score)-F.baselineScore)/(F.baselineScore/100) overallRes";
        }

        $q = "select
				A.id, A.formId, A.resDate, DATE_FORMAT(A.resDate, '%d %b') responseDate,
				E.scaleId, F.baselineScore, A.euroVal36, A.euroVal37, 
				$normal
			from
				mp_response A
			join
				mp_forms B ON(A.formId=B.id AND (B.showReport=1 OR B.id=19))
			join
				mp_response_details C ON(A.id=C.resId)
			join
				mp_form_ques_options D ON (C.optionId=D.id)
			join
				mp_form_question E ON(D.quesId=E.id AND E.scaleId IN ($scaleIds))
			join
				mp_patient_form_scales F ON(E.scaleId=F.scaleId AND A.doctorId=F.doctorId AND F.userId='$userId') 
			join
				mp_form_scales G ON(F.scaleId=G.id)
			join
				mp_patient_forms I ON (F.userId=I.userId AND F.formId=I.formId AND F.doctorId=I.doctorId)
			where
				$cond
			group by 
				responseDate, E.scaleId 
			order by 
				A.resDate ASC";
        //echo $q;
        $rs = $this->pdb->query($q);
        return $rs;
    }

    function formScales($formId = 0, $userId = 0, $doctId = 0) {
        return $this->pdb->query("select A.*, B.baselineScore, B.lastScore, B.goodRes, B.avgRes from mp_form_scales A  
							      LEFT JOIN mp_patient_form_scales B ON (A.id=B.scaleId AND B.userId='$userId' AND B.doctorId='$doctId') where A.formId='$formId'");
    }

    function getOverallScore($userId = USER_ID, $doctId = 0, $share=0) {
        if ($doctId)
            $doctorId = $doctId;
        else
            $doctorId = USER_TYPE == 'D' ? USER_ID : 0;
        
        
        if($share==1)
            $doctorId = 0;

        $cond = "A.userId='$userId' AND B.type=1 AND D.setGoal=1";
        if ($doctorId) {
            $cond = "A.userId='$userId' AND A.doctorId='$doctorId' 
				   AND A.formId NOT IN (select formId from mp_patient_forms where userId=$userId AND doctorId!=$doctorId)
				   AND B.type=1 AND D.setGoal=1";
        }

       $q = "select 
				IF(B.weightage=0,
					
					AVG( IF(C.realTotalScore = IFNULL(A.baselineScore, 0), ((IFNULL(A.lastScore, 0) - IFNULL(A.baselineScore, 0)) * 10), 
					  (((IFNULL(A.lastScore,0) - IFNULL(A.baselineScore,0)) * 100 ) / (C.realTotalScore - IFNULL(A.baselineScore,0)))) )
					,
					
					( IF(C.realTotalScore = IFNULL(A.baselineScore, 0), ((IFNULL(A.lastScore, 0) - IFNULL(A.baselineScore, 0)) * 10), 
					  (((IFNULL(A.lastScore,0) - IFNULL(A.baselineScore,0)) * 100 ) / (C.realTotalScore - IFNULL(A.baselineScore,0)))) )*B.weightage/100
			  ) overallRes 
			from
				mp_patient_form_scales A
			join
				mp_patient_forms B ON (A.formId=B.formId AND A.userId=B.userId AND A.doctorId=B.doctorId)
			join
				mp_form_scales C ON (A.scaleId=C.id)
			join 
				mp_forms D ON (A.formId=D.id)
			where
				$cond 
			"; 

        $rs = $this->pdb->query($q);

        return round($rs[0]['overallRes']);
    }

    function chartReports($forms, $from, $to, $userId, $doctorId) {
        $n = floor(($to - $from) / 3600 / 24) + 1;

        if ($n <= 7)
            $rs = $this->chartReportsDays($forms, $from, $to, $userId, $doctorId);
        else if ($n > 7)
            $rs = $this->chartReportsMonths($forms, $from, $to, $userId, $doctorId);

        return $rs;
    }

    function chartReportsDays($forms, $from, $to, $userId, $doctorId) {
        $days = array();
        for ($a = $from; $a <= $to; $a = strtotime("+1 days", $a)) {
            $days[] = "'" . date('Y-m-d', $a) . "'";
        }
        $formIds = implode(",", $forms);
        $days = implode(",", $days);

        $cond = "A.formId IN($formIds) AND A.id IN(select MAX(id) from mp_response where 
			   DATE_FORMAT(resDate, '%Y-%m-%d') IN($days) AND userId='$userId' group by formId, DATE_FORMAT(resDate, '%Y-%m-%d') ORDER BY resDate)";

        if ($doctorId) {
            $cond = "A.formId IN($formIds) AND A.id IN(select MAX(id) from mp_response where 
			   DATE_FORMAT(resDate, '%Y-%m-%d') IN($days) AND userId='$userId' AND doctorId='$doctorId' group by formId, DATE_FORMAT(resDate, '%Y-%m-%d')
			    ORDER BY resDate)";
        }

        $q = "select 
				
				formId, 
				IF(formId=19, ( (totalEuroScore-SUM(baselineScore))*100/(SUM(realTotalScore)-SUM(baselineScore)) ), AVG(overallRes)) overallRes,
				responseDate 
				
			from (
			select
				A.id, A.formId, A.resDate, DATE_FORMAT(A.resDate, '%d %b') responseDate,
				E.scaleId, (SUM(D.score)), F.baselineScore, 
				(A.euroVal36 + A.euroVal37) totalEuroScore, 
    			G.realTotalScore, 
				 
				IF( 
					G.realTotalScore=F.baselineScore, (( SUM(D.score)-F.baselineScore)*10), 
					((( SUM(D.score)-F.baselineScore)*100)/(G.realTotalScore-F.baselineScore)) 
				) overallRes
				 
			from
				mp_response A
			join
				mp_forms B ON(A.formId=B.id AND (B.showReport=1 OR B.id=2 OR B.id=19))
			join
				mp_response_details C ON(A.id=C.resId)
			join
				mp_form_ques_options D ON (C.optionId=D.id)
			join
				mp_form_question E ON(D.quesId=E.id)
			join
				mp_patient_form_scales F ON(E.scaleId=F.scaleId AND A.doctorId=F.doctorId AND F.userId='$userId') 
			join
				mp_form_scales G ON(F.scaleId=G.id)
			join
				mp_patient_forms I ON (F.userId=I.userId AND F.formId=I.formId AND F.doctorId=I.doctorId)
			where
				$cond
			group by 
				responseDate, E.scaleId ) tmpp group by formId, responseDate order by resDate";

        $rs = $this->pdb->query($q);
        return $rs;
    }

    function chartReportsMonths($forms, $from, $to, $userId, $doctorId) {
        $n = cal_days_in_month(CAL_GREGORIAN, date('n', $to), date('Y', $to));
        $to = strtotime($n . ' ' . date('M', $to) . ' ' . date('Y', $to));
        $months = array();
        for ($a = $from; $a <= $to; $a = strtotime("+1 months", $a)) {
            $months[] = "'" . date('Y-m', $a) . "'";
        }
        $formIds = implode(",", $forms);
        $months = implode(",", $months);

        $cond = "A.formId IN($formIds) AND A.id IN(select MAX(id) from mp_response where 
			   DATE_FORMAT(resDate, '%Y-%m') IN($months) AND userId='$userId' group by formId, DATE_FORMAT(resDate, '%b %Y'))";

        if ($doctorId) {
            $cond = "A.formId IN($formIds) AND A.id IN(select MAX(id) from mp_response where 
                               DATE_FORMAT(resDate, '%Y-%m') IN($months) AND userId='$userId' AND doctorId='$doctorId' group by formId, DATE_FORMAT(resDate, '%b %Y'))";
        }
        //echo '<pre>';
       $q = "select 
				formId, 
				IF(formId=19, ( (totalEuroScore-SUM(baselineScore))*100/(SUM(realTotalScore)-SUM(baselineScore)) ), AVG(overallRes)) overallRes,
				responseDate 
			
			from 
			
			(
			select
				A.id, A.formId, A.resDate, DATE_FORMAT(A.resDate, '%b %Y') responseDate,
				E.scaleId, SUM(D.score), F.baselineScore, 
				(A.euroVal36 + A.euroVal37) totalEuroScore, 
    			G.realTotalScore, 
				 
				IF( 
					G.realTotalScore=F.baselineScore, (( SUM(D.score)-F.baselineScore)*10), 
					((( SUM(D.score)-F.baselineScore)*100)/(G.realTotalScore-F.baselineScore)) 
				) overallRes
				 
			from
				mp_response A
			join
				mp_forms B ON(A.formId=B.id AND (B.showReport=1 OR B.id=2 OR B.id=19))
			join
				mp_response_details C ON(A.id=C.resId)
			join
				mp_form_ques_options D ON (C.optionId=D.id)
			join
				mp_form_question E ON(D.quesId=E.id)
			join
				mp_patient_form_scales F ON(E.scaleId=F.scaleId AND A.doctorId=F.doctorId AND F.userId='$userId') 
			join
				mp_form_scales G ON(F.scaleId=G.id)
			join
				mp_patient_forms I ON (F.userId=I.userId AND F.formId=I.formId AND F.doctorId=I.doctorId)
			where
				$cond
			group by 
				responseDate, E.scaleId ) tmpp group by formId, responseDate order by resDate";

        $rs = $this->pdb->query($q);
        //echo '<pre>';print_r($rs);
        return $rs;
    }

    function formGoals($userId = USER_ID) {
        $q = "select formId, goodRes from mp_patient_forms where userId=$userId";
        return $this->pdb->query($q);
    }

    function misc() {
        return $this->pdb->singleRow("mp_misc", "id=1");
    }

    /*     * ***********Start Nitin Code******** */

    function getAllPlan() {
        $planQuery = "SELECT id, planName FROM mp_plans";
        $re = $this->pdb->query($planQuery);
        return $re;
    }

    /*     * ***********End Nitin Code******** */

    function listTicketUser() {
        $cond = "A.type='T'";

        $q = "SELECT * FROM mp_users A WHERE $cond ORDER BY A.id DESC;";

        return $this->pdb->query($q);
    }

    function licenceCost($planid) {
        $cond = "id='" . $planid . "'";
        $price = $this->pdb->singleVal("mp_plans", $cond, "price");
        return !empty($price) ? $price : 0;
    }

    function noOfAddedPat() {
        $cond = "A.parentId='" . PARENT_ID . "' and A.type='P'";
        $q = "SELECT count('id') addedPat FROM mp_users A WHERE $cond";
        $ap = $this->pdb->query($q);
        return $ap[0]['addedPat'];
    }

    function allowPatAdd() { // means total no. of patientused or not
        $cond = "userId='" . PARENT_ID . "'";
        $npa = $this->pdb->singleVal("mp_hospital_plans", $cond, "noOfPatient");
        $ap = $this->noOfAddedPat();
        if ($ap >= $npa)
            return false;
        else
            return true;
    }

    function getPlanDetail($planId) {
        $cond = "id='" . $planId . "'";
        $pd = $this->pdb->singleRow("mp_plans", $cond);
        return !empty($pd) ? $pd : '';
    }

    function getFormAvail($planid) {
        $cond = "id='" . $planid . "'";
        $formIds = $this->pdb->singleVal("mp_plan_form", $cond, 'formId');
        $fn = $this->pdb->query("select `formName`  from mp_forms where `id` IN (" . $formIds . ")");
        if (!empty($fn)) {
            foreach ($fn as $row) {
                //pr($row['formName']);
                $fv .= $row['formName'] . ", ";
            }
            $fv = substr($fv, 0, -2);
        }
        return $fv;
    }

    function planDetail($userId = 0) {
        $cond = "userId='" . $userId . "' and status='1'";
        $planDtl = $this->pdb->singleRow("mp_hospital_plans", $cond, "planId, expiryDate");
        $planDtl['type'] = $this->pdb->singleVal("mp_plans", "id='" . $planDtl['planId'] . "'", "type");
        return !empty($planDtl) ? $planDtl : '';
    }

    function getMsgOtherReply($id) {
        $cond = "(A.replyId=$id AND A.fromId=" . USER_ID . ") OR (A.fwdId= $id AND  A.fromId=" . USER_ID . ") OR (A.replyId=$id AND A.toId=" . USER_ID . ") OR A.parent_id=$id";
        
         if(USER_TYPE==='P')
                $readType = 'A.readPatient';
            else 
                $readType= 'A.readDoctor';
        $q = "SELECT CONCAT(B.firstName,' ',B.lastName) name, $readType AS readT from mp_messages A JOIN mp_users B ON B.id=A.fromId WHERE $cond";
        $or = $this->pdb->query($q);
        //echo count($or);
        
        $read = 1;
        if(count($or)>0){
            foreach ($or as $row) {
                if($row['readT']==0){
                    $read = 0;
                    break;
                }
                
               
            }
        }
        $other['read'] = $read;
        $other['name'] = '';
        if (count($or) < 1){
            return 0;
            //echo '1';
        }    
        elseif (count($or) > 2) {
            $last = end($or);
            $name = ', ..,&nbsp;' . $last['name'];
            $name .= "(" . (count($or)+ 1) . ")";
            //echo '2';
        } elseif (count($or) > 1) {
            foreach ($or as $row) {
                $name .= ",&nbsp&nbsp" . $row['name'];
            }
            $name = substr($name, 0, -1);
            $name .= "(" . (count($or) + 1) . ")";
            //echo '3';
        }elseif (count($or) == 1) {
            $name = ",&nbsp&nbsp" . $or[0]['name'];
            //echo '4';
            
        }
        $other['name'] = $name; 
       
        return $other;
        
    }
    
    function getTotalDoctor($userId){
        $q = "select count(id) as totDoctor from mp_patients where userId='$userId' AND (invitation=0  OR invitation=3)";
        $rs  = $this->pdb->query($q);
        return $rs[0]['totDoctor'];
    }
    
    function doctorName($id=0){ 
        $q = "select CONCAT(A.firstName, ' ', A.lastName) fullName from mp_users A where id='$id'";
        $rs = $this->pdb->query($q);
        return $rs[0]['fullName'];
    }
    
    function csqFirstLastResDates($userId = USER_ID) {
        return $this->pdb->singleRow("mp_response", "doctorId='$userId' AND formId='8'  ", "MIN(resDate) firstRes, MAX(resDate) lastRes");
    }
    
    function csqChartReports($forms, $from, $to, $userId, $doctorId) {
        $n = floor(($to - $from) / 3600 / 24) + 1;

        if ($n <= 7)
            $rs = $this->csqChartReportsDays($forms, $from, $to, $userId, $doctorId);
        else if ($n > 7)
            $rs = $this->csqChartReportsMonths($forms, $from, $to, $userId, $doctorId);

        return $rs;
    }

    function csqChartReportsDays($forms, $from, $to, $userId, $doctorId) {
        $days = array();
        for ($a = $from; $a <= $to; $a = strtotime("+1 days", $a)) {
            $days[] = "'" . date('Y-m-d', $a) . "'";
        }
        $formIds = implode(",", $forms);
        $days = implode(",", $days);

        $cond = "A.formId IN($formIds) AND A.id IN(select MAX(id) from mp_response where 
			   DATE_FORMAT(resDate, '%Y-%m-%d') IN($days) AND userId='$userId' group by formId, DATE_FORMAT(resDate, '%Y-%m-%d') ORDER BY resDate)";

        if ($doctorId) {
            $cond = "A.formId IN($formIds) AND A.id IN(select MAX(id) from mp_response where 
			   DATE_FORMAT(resDate, '%Y-%m-%d') IN($days) AND userId='$userId' AND doctorId='$doctorId' group by formId, DATE_FORMAT(resDate, '%Y-%m-%d')
			    ORDER BY resDate)";
        }

        $q = "select 
				
				formId, 
				IF(formId=19, ( (totalEuroScore-SUM(baselineScore))*100/(SUM(realTotalScore)-SUM(baselineScore)) ), AVG(overallRes)) overallRes,
				responseDate 
				
			from (
			select
				A.id, A.formId, A.resDate, DATE_FORMAT(A.resDate, '%d %b') responseDate,
				E.scaleId, (SUM(D.score)), F.baselineScore, 
				(A.euroVal36 + A.euroVal37) totalEuroScore, 
    			G.realTotalScore, 
				 
				IF( 
					G.realTotalScore=F.baselineScore, (( SUM(D.score)-F.baselineScore)*10), 
					((( SUM(D.score)-F.baselineScore)*100)/(G.realTotalScore-F.baselineScore)) 
				) overallRes
				 
			from
				mp_response A
			join
				mp_forms B ON(A.formId=B.id AND (B.showReport=1 OR B.id=2 OR B.id=19))
			join
				mp_response_details C ON(A.id=C.resId)
			join
				mp_form_ques_options D ON (C.optionId=D.id)
			join
				mp_form_question E ON(D.quesId=E.id)
			join
				mp_patient_form_scales F ON(E.scaleId=F.scaleId AND A.doctorId=F.doctorId AND F.userId='$userId') 
			join
				mp_form_scales G ON(F.scaleId=G.id)
			join
				mp_patient_forms I ON (F.userId=I.userId AND F.formId=I.formId AND F.doctorId=I.doctorId)
			where
				$cond
			group by 
				responseDate, E.scaleId ) tmpp group by formId, responseDate order by resDate";

        $rs = $this->pdb->query($q);
        return $rs;
    }

    function csqChartReportsMonths($forms, $from, $to, $userId, $doctorId) {
        $n = cal_days_in_month(CAL_GREGORIAN, date('n', $to), date('Y', $to));
        $to = strtotime($n . ' ' . date('M', $to) . ' ' . date('Y', $to));
        $months = array();
        for ($a = $from; $a <= $to; $a = strtotime("+1 months", $a)) {
            $months[] = "'" . date('Y-m', $a) . "'";
        }
        $formIds = implode(",", $forms);
        $months = implode(",", $months);

        $cond = "A.formId IN($formIds) AND A.id IN(select MAX(id) from mp_response where 
			   DATE_FORMAT(resDate, '%Y-%m') IN($months) AND userId='$userId' group by formId, DATE_FORMAT(resDate, '%b %Y'))";

        if ($doctorId) {
            $cond = "A.formId IN($formIds) AND A.id IN(select MAX(id) from mp_response where 
                               DATE_FORMAT(resDate, '%Y-%m') IN($months) AND userId='$userId' AND doctorId='$doctorId' group by formId, DATE_FORMAT(resDate, '%b %Y'))";
        }
        
        //echo '<pre>';
        $q = "select 
				formId, 
				IF(formId=19, ( (totalEuroScore-SUM(baselineScore))*100/(SUM(realTotalScore)-SUM(baselineScore)) ), AVG(overallRes)) overallRes,
				responseDate 
			
			from 
			
			(
			select
				A.id, A.formId, A.resDate, DATE_FORMAT(A.resDate, '%b %Y') responseDate,
				E.scaleId, SUM(D.score), F.baselineScore, 
				(A.euroVal36 + A.euroVal37) totalEuroScore, 
    			G.realTotalScore, 
				 
				IF( 
					G.realTotalScore=F.baselineScore, (( SUM(D.score)-F.baselineScore)*10), 
					((( SUM(D.score)-F.baselineScore)*100)/(G.realTotalScore-F.baselineScore)) 
				) overallRes
				 
			from
				mp_response A
			join
				mp_forms B ON(A.formId=B.id AND (B.showReport=1 OR B.id=2 OR B.id=19))
			join
				mp_response_details C ON(A.id=C.resId)
			join
				mp_form_ques_options D ON (C.optionId=D.id)
			join
				mp_form_question E ON(D.quesId=E.id)
			join
				mp_patient_form_scales F ON(E.scaleId=F.scaleId AND A.doctorId=F.doctorId AND F.userId='$userId') 
			join
				mp_form_scales G ON(F.scaleId=G.id)
			join
				mp_patient_forms I ON (F.userId=I.userId AND F.formId=I.formId AND F.doctorId=I.doctorId)
			where
				$cond
			group by 
				responseDate, E.scaleId ) tmpp group by formId, responseDate order by resDate";
        $q1 = "SELECT AVG(score)*3.125 score, DATE_FORMAT(resDate, '%b %Y') resDate FROM(
        SELECT  SUM(QO.score) score, resDate, EXTRACT(MONTH  FROM resDate ) m , EXTRACT(YEAR  FROM resDate ) yr    FROM(
		SELECT * FROM mp_response WHERE id IN( 
		SELECT MAX(id)   FROM mp_response WHERE doctorId='".USER_ID."' AND  formId=8  GROUP BY userId)
	) AS res2 INNER JOIN mp_response_details  RD ON res2.id = RD.resId  INNER JOIN mp_form_ques_options QO ON RD.optionId = QO.Id GROUP BY userId
	) AS res3 GROUP BY yr,m";

        $rs = $this->pdb->query($q1);
        //echo '<pre>';print_r($rs);
        return $rs;
    }

    /** Weemo chat notification * */
}
