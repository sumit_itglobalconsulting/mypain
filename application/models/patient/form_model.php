<?php

/** For Patient * */
class Form_model extends CI_Model {

    function save($data, $tbl = "") {
        if (!$tbl)
            return;

        if ($id = $data['id']) {
            $data['updated'] = currentDate();
            $success = $this->pdb->update($tbl, $data, "id=$id");
            if (!$success)
                return 0;
        }
        else {
            $data['created'] = $data['updated'] = currentDate();
            $id = $this->pdb->insert($tbl, $data);
        }

        return $id;
    }

    function response($data) {
        //echo '<pre>';print_r($this->input->post());die;
        if ($data['formId'] == 2 && !empty($data['quesOpt'])) {
            if ($data['quesOpt'][32] == 196)
                $data['quesOpt'][32] = 202;
        }
        $userId = $data['userId'] = !empty($data['userId'])?$data['userId']:USER_ID;
        $data['resDate'] = currentDate();

        $data['formId'] = addslashes($data['formId']);

        if ($data['combination']) {
            $data['euroVal36'] = euroVals($data['combination']);
            $data['euroVal37'] = $this->pdb->singleVal("mp_form_ques_options", "id='{$data['quesOpt']['314']}'", "optionNameEuro");
            if (!$data['euroVal37'] && $data['euroVal37'] != 0)
                $data['euroVal37'] = '';
         
        }
        
        $optIds = array();

        if (!empty($data['resDT'])) {
            $cond = "userId='$userId' AND doctorId='{$data['doctorId']}' AND formId='{$data['formId']}' AND resDate='{$data['resDT']}'";
            $resId = $this->pdb->singleVal("mp_response", $cond, "id");
            $medId = $this->pdb->singleVal("mp_bpi_med", $cond, "id");
            if ($data['combination']) {
                $inf['euroVal36'] = $data['euroVal36'];
                $inf['euroVal37'] = $data['euroVal37'];
            }
            $this->pdb->update("mp_response", $inf, $cond);

            if ($data['formId'] == 1) {
                $medData['medTake'] = $data['medTake'];
                $medId = $this->pdb->update("mp_bpi_med", $medData, $cond);
            }
        } else {
           
            $resId = $this->pdb->insert("mp_response", $data);

            if ($data['formId'] == 1) {
                $medData['medTake'] = $data['medTake'];
                $medId = $this->pdb->insert("mp_bpi_med", $data);
            }
        }

        if (!empty($data['resDT']) && $resId) {
            $this->pdb->delete("mp_response_details", "resId=$resId");
        }

        if ($resId && $data['quesOpt']) {
            foreach ($data['quesOpt'] as $k => $v) {
                $inf['resId'] = $resId;
                $inf['quesId'] = $k;

                if (is_array($v)) {
                    foreach ($v as $v1) {
                        $optIds[] = $inf['optionId'] = $v1;
                        $this->pdb->insert("mp_response_details", $inf);
                    }
                } else {
                    if($data['formId'] == 1 && $k==9 && $data['medTake']==0)
                       $v=51;
                    $optIds[] = $inf['optionId'] = $v;
                    $this->pdb->insert("mp_response_details", $inf);
                }
            }
        }


        if ($resId && $data['quesFreeAns']) {
            foreach ($data['quesFreeAns'] as $k => $v) {
                $inf['resId'] = $resId;
                $inf['quesId'] = $k;
                $inf['optionId'] = 0;
                $inf['ans'] = $v;
                $this->pdb->insert("mp_response_details", $inf);
            }
        }

        if ($_FILES['image']['name'][89] && checkImageExt($_FILES['image']['name'][89])) {
            $this->load->library("Image");
            $inf['resId'] = $resId;
            $inf['quesId'] = 89;
            $inf['optionId'] = 0;
            $inf['file'] = time() . USER_ID . "." . getExt($_FILES['image']['name'][89]);
            $this->image->resize($_FILES['image']['tmp_name'][89], "assets/uploads/scans/" . $inf['file'], 800);
            $this->pdb->insert("mp_response_details", $inf);
        }

        /**         * */
        if ($optIds) {
            $optIds = implode(",", $optIds);
            $q = "SELECT B.scaleId, SUM(A.score) score FROM mp_form_ques_options A JOIN mp_form_question B ON (A.quesId=B.id) WHERE A.id IN ($optIds) 
			GROUP BY B.scaleId";
            $scales = $this->pdb->query($q);
            if ($scales) {
                foreach ($scales as $a) {
                    $cond = "userId='$userId' AND formId='{$data['formId']}' AND doctorId='{$data['doctorId']}' AND scaleId='{$a['scaleId']}'";
                    $sdtl = $this->pdb->singleRow("mp_patient_form_scales", $cond);
                    $sinf = array();
                    if ($sdtl['baselineScore'] === null || ($data['resDT'] && $data['firstResDate'] == $data['resDT'])) {
                        $sinf['baselineScore'] = $a['score'];

                        if ($data['formId'] == 19 && $a['scaleId'] == 36) {
                            $sinf['baselineScore'] = euroVals($data['combination']);
                        }
                    }

                    if (empty($data['resDT']) || $data['resDT'] == $data['lastResDate']) {
                        $sinf['lastScore'] = $a['score'];

                        if ($data['formId'] == 19 && $a['scaleId'] == 36) {
                            $sinf['lastScore'] = euroVals($data['combination']);
                        }
                    }

                    $this->pdb->update("mp_patient_form_scales", $sinf, $cond);
                }
            }
        }
        /**         * */
        $formDtl = $this->pdb->query("select id, nextResDueDate, resFreq from mp_patient_forms where userId=$userId AND formId='{$data['formId']}'");
        $formDtl = $formDtl[0];

        if (strtotime(date('Y-m-d')) >= strtotime(date('Y-m-d', strtotime($formDtl['nextResDueDate'])))) {
            $inf['nextResDueDate'] = getNextDate($formDtl['nextResDueDate'], $formDtl['resFreq']);
            $this->pdb->update("mp_patient_forms", $inf, "id=" . $formDtl['id']);
        }
    }

}
