<?php 
class Faq_model extends CI_Model {
	function save($data, $tbl=""){
		if(!$tbl)
			return;
			
		if($id=$data['id']){
			$data['updated']=currentDate();
			$success=$this->pdb->update($tbl, $data, "id=$id");
			if(!$success)
				return 0;
		}
		else{
			$data['created']=$data['updated']=currentDate();
			$id=$this->pdb->insert($tbl, $data);
		}
		
		return $id;
	}
    
    function lists($p, $ps, $userType = '', $email = null, $ticketNo = null,$withoutLogin = false) {
	    $cond = '';
        $qs=addSlash(arrayTrim(arrayUrlDecode($_GET)));
		
        
        if($qs['search']){
			$cond.=" and  ( VT.question LIKE '%{$qs['search']}%' OR VT.answer LIKE '%{$qs['search']}%' OR VT.keyword LIKE '%{$qs['search']}%' )  ";
        }
        
        if($withoutLogin){
			if($email){
			     $cond.=" and  VT.email = '".$email."' ";
            }
            
            if($ticketNo){
			     $cond.=" and  VT.ticketNo = '".$ticketNo."' ";
            }   
        }
        
        
        $q="select * from ( 
            Select id , userId, question, answer, keyword, category, created, updated, visible, null as ticketNo, null as name, null as email , null as status, null as helpCategory , 'faq' as type from mp_faq where 1=1 ";
          
            if(isset($qs['category'])  && $qs['category'] != 'A'){
			     $q.=" and  category = '{$qs['category']}' ";
            }  
            if($userType){
                $q.=" and (category = 'A' OR category = '".$userType."' )";
            }
            
         $q .="  UNION
            
            Select T.id , null as userId, T.query as question, null as answer, null as keyword, null as category, T.created, T.updated, null as visible ,T.ticketNo , T.name, T.email , T.status , HC.name as helpCategory , 'ticket' as type from mp_tickets T 
             INNER JOIN mp_help_category as HC on HC.id =  T.categoryId where 1=1 ";
             
            if($email){
			     $q.=" and  email = '".$email."' ";
            }
            
            if($ticketNo){
			     $q.=" and  ticketNo = '".$ticketNo."' ";
            }   
        
       $q .= " ) AS VT  WHERE 1=1 $cond ORDER BY updated desc";
        
	//	$q="select * from mp_faq  WHERE 1=1 $cond ORDER BY updated desc";
		return $this->pdb->pagedQuery($q, $p, $ps,'',false);
	}
    
    function faqDetails($id){
		$q="SELECT * FROM mp_faq  WHERE id=:faqId";
		$result = $this->pdb->query($q, array(':faqId'=>$id));
        
        return ($result)? $result['0'] : '';
	}
    
    function deleteFaq($id){		
		return $this->pdb->delete("mp_faq", "id=:faqId", array(':faqId'=>$id)); 
	}
}

