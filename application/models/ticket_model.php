<?php 
class Ticket_model extends CI_Model {
	function save($data, $tbl=""){
		if(!$tbl)
			return;
			
		if($id=$data['id']){
			$data['updated']=currentDate();
			$success=$this->pdb->update($tbl, $data, "id=$id");
			if(!$success)
				return 0;
		}
		else{
		    $data['status'] = '1';  
            $data['ticketNo'] = $this->getUniqueTicketNo();
			$data['created']=$data['updated']=currentDate();
			$id=$this->pdb->insert($tbl, $data);
		}
		
		return $id;
	}
    
    function Ticketlists($p, $ps, $withOutLimit = false) {	    
		$q="select * from mp_tickets ORDER BY name asc";
        
        if($withOutLimit){
              return $this->pdb->query($q);
        } else {
		      return $this->pdb->pagedQuery($q, $p, $ps);
        }
	}
    
    function ticketDetails($id){
		$q="SELECT * FROM mp_tickets  WHERE id=:helpId";
		$result = $this->pdb->query($q, array(':helpId'=>$id));
        
        return ($result)? $result['0'] : '';
	}
    
    
    function getUniqueTicketNo() {
        
        $ticketNo = mt_rand();
        do {
            $ticketNo = mt_rand();
        } while (!$this->checkUniqueTicketNo($ticketNo));
        
		return $ticketNo;
	}
    
    function checkUniqueTicketNo($ticketNo) {
		 $result = $this->pdb->query("select * from mp_tickets where ticketNo = '".$ticketNo."' ");
         
         return (count($result))? false: true;
	}
    function getTicketReply($ticketId) {
		 $result = $this->pdb->query("select * from mp_ticket_reply where ticketId = '".$ticketId."' ");
         
         return (count($result))? $result: array();
	}
    
    function ticketCheck($ticketNo, $email){
        $result = $this->pdb->query("select * from mp_tickets where ticketNo = '".$ticketNo."' and email = '".$email."'");
         
         return (count($result))? $result: array();
    }
   
}

