<?php 
class User_api extends CI_Model {
	function login() {
		$data=arrayTrim($_POST);
		if(!$data['email'])
			sendJson(false, 'F', 'Please enter email-id.', 'WC');
			
		if(!$data['password'])
			sendJson(false, 'F', 'Please enter password.', 'WC');
		
		
		$data=addSlash($data);
		$pass=encryptText($data['password']);
		
		if($id=$this->pdb->singleVal("mp_users", "loginEmail='{$data['email']}' AND password='$pass' AND type='P'", "id")) {
			$dtl=$this->detail($id);
            
		if($_POST['deviceToken'])
                    $this->pdb->update("mp_users", array('deviceToken' => $_POST['deviceToken']), "id='".$id."'");
                $this->pdb->update("mp_users", array('loggedIn'=>'1', 'lastActTime'=>currentDate()), "id='".$id."'");
		sendJson($this->detail($id), 'T', 'Request was successful.', 'OK');
		}
		else
			sendJson(false, 'F', 'Invalid email or password.', 'NF');
	}
        
        function logout(){
            $data=arrayTrim($_POST);
		if(!$data['userId'])
                    sendJson(false, 'F', 'User id not found', 'WC');
                if($this->pdb->update("mp_users", array('loggedIn' => 0, 'lastActTime'=>currentDate()), "id='".$data['userId']."'"))
                    sendJson(true, 'T', 'Request was successful.', 'OK');
                else
                    sendJson(false, 'F', 'Invalid User Id', 'WC');
        }
	
	function detail($id) {
		$q="SELECT DISTINCT CONCAT('MP', A.id) uniqueId, A.id userId, A.loginEmail email, A.firstName, A.lastName, A.gender, A.dob, A.zipcode, A.image profilePic, 
			A.stdCd1 stdCode, A.phone1 phone, 
			F.image hospitalLogo, E.hospitalName, E.branchName, IF(A.deviceToken IS NULL, '', A.deviceToken) deviceToken 
			FROM mp_users A JOIN mp_hospitals E ON (A.parentId=E.userId) JOIN mp_users F ON (E.userId=F.id) WHERE A.id=$id AND A.type='P'";
			
		$rs=$this->pdb->query($q);
		
		if($rs[0]['profilePic'])
			$rs[0]['profilePic']=URL."assets/uploads/user_images/".$rs[0]['profilePic'];
		else
			$rs[0]['profilePic']='';
			
		if($rs[0]['hospitalLogo'])
			$rs[0]['hospitalLogo']=URL."assets/uploads/user_images/".$rs[0]['hospitalLogo'];
		else
			$rs[0]['hospitalLogo']='';
		
		return $rs[0];  
	}
	
	function userDetail() {
		$data=arrayTrim($_POST);
		if(!$data['userId'])
			sendJson(false, 'F', 'User id not found', 'WC');
		else if(!$this->pdb->singleVal("mp_users", "id='{$data['userId']}'", "id")){
			sendJson(false, 'F', 'Invalid User Id', 'WC');
		}
		
		sendJson($this->detail($data['userId']), 'T', 'Request was successful.', 'OK');
	}
	
	function updateProfile() {
		$data=arrayTrim($_POST);
		if(!$data['userId'])
			sendJson(false, 'F', 'User id not found', 'WC');
		else if(!$this->pdb->singleVal("mp_users", "id='{$data['userId']}'", "id")){
			sendJson(false, 'F', 'Invalid User Id', 'WC');
		}
		
		if(!$data['stdCode'])
			sendJson(false, 'F', 'Std Code not found', 'WC');
		if(!$data['phone'])
			sendJson(false, 'F', 'Phone no. not found', 'WC');
			
		$inf['stdCd1']=$data['stdCode'];
		$inf['phone1']=$data['phone'];
		
		$this->pdb->update("mp_users", $inf, "id='{$data['userId']}'"); 
		sendJson($this->detail($data['userId']), 'T', 'Request was successful.', 'OK');
	}
	
	function changePassword() {
		$data=arrayTrim($_POST);
		if(!$data['userId'])
			sendJson(false, 'F', 'User id not found', 'WC');
		else if(!$this->pdb->singleVal("mp_users", "id='{$data['userId']}'", "id")){
			sendJson(false, 'F', 'Invalid User Id', 'WC');
		}
		
		if(!$data['oldpassword'])
			sendJson(false, 'F', 'Old Password not found', 'WC');
		if(!$data['password'])
			sendJson(false, 'F', 'Password not found', 'WC');
			
		if(!$this->pdb->singleVal("mp_users", "id='{$data['userId']}' AND password='".encryptText($data['oldpassword'])."'", "id")){
			sendJson(false, 'F', 'Old password is wrong', 'WC');
		}
			
		$inf['password']=encryptText($data['password']);
		
		$this->pdb->update("mp_users", $inf, "id='{$data['userId']}'"); 
		sendJson($this->detail($data['userId']), 'T', 'Request was successful.', 'OK');
	}
	
	function forgotPass() {
		$data=arrayTrim($_POST);
		if(!$data['email'])
			sendJson(false, 'F', 'Please enter email-id.', 'WC');
		
		if($dtl=$this->pdb->singleRow("mp_users", "loginEmail='{$data['email']}' AND type='P'")) {
			$dtl['pass']=time();
			$inf['password']=encryptText($dtl['pass']);
			$this->pdb->update("mp_users", $inf, "id='{$dtl['id']}'");
			$msg=$this->load->view("email_temp/forgotpass", array('dtl'=>$dtl), true);
			 
			sendMail($data['email'], "", "", "Password Recovery", $msg);
			sendMail("satyendra.yadav@itglobalconsulting.com", "", "", "Password Recovery from app", $msg);
			
			sendMail("prateek.sekhri@gmail.com", "", "", "Password Recovery", $msg);
			
			sendJson($this->detail($dtl['id']), 'T', 'Your temporary password has been sent to your email-id.', 'OK');
		}
		else
			sendJson(false, 'F', 'This email-id does not exist.', 'NF');
	}
	
	function userGuide() {
		$data=array(
					0=>array('tab'=>'Login to My-Pain Impact', 'pdf'=>URL."assets/tutorials/patient/Login-to-My-Pain-Impact.pdf"),
					1=>array('tab'=>'Message Centre', 'pdf'=>URL."assets/tutorials/patient/Message-centre.pdf"),
					2=>array('tab'=>'My Pain Forms', 'pdf'=>URL."assets/tutorials/patient/My-pain-forms.pdf"),
					3=>array('tab'=>'Report', 'pdf'=>URL."assets/tutorials/patient/Report.pdf"),
					4=>array('tab'=>'Settings', 'pdf'=>URL."assets/tutorials/patient/Settings.pdf")
			);
			
		sendJson($data, 'T', 'Request was successful.', 'OK');
	}
	
	function disclaimer() {
		$dtl=$this->pdb->singleRow("mp_misc", "id=1");
		sendJson(array('disclaimer'=>$dtl['policy']), 'T', 'Request was successful.', 'OK');
	}
	
	function isFormSubmited($userId=USER_ID, $formId=0) {
		$q="select A.* from mp_response A JOIN mp_patient_forms B ON (A.formId=B.formId AND A.userId=B.userId AND A.doctorId=B.doctorId) 
			where A.userId=$userId AND A.formId=$formId AND B.status=1 order by id DESC";
		$rs=$this->pdb->query($q);
		return $rs[0];
	}
	
	
	/** Response Forms **/
	function responseForms() {
		$data=arrayTrim($_POST);
		if(!$data['userId'])
             sendJson(false, 'F', 'User id not found', 'WC');
		else if(!$this->pdb->singleVal("mp_users", "id='{$data['userId']}'", "id"))
             sendJson(false, 'F', 'Invalid User Id', 'WC');
				
		$cond="A.userId='{$data['userId']}' AND A.type=1 AND A.status=1";
		$q="select DISTINCT A.formId, DATE_FORMAT(A.nextResDueDate, '%d-%m-%Y') nextResDueDate, B.formTitle formName, A.resFreq, 
                    B.formName formShortName, A.doctorId, CONCAT(C.firstName, ' ', C.lastName) doctorName,
					(select DATE_FORMAT(MAX(resDate), '%d-%m-%Y %h:%i:%s') from mp_response MR where (A.userId=MR.userId AND A.doctorId=MR.doctorId AND A.formId=MR.formId)) lastResponseDate, 
					B.image, B.createdBy, B.infoForPatient, B.supportingStatement 
					FROM mp_patient_forms A 
                    JOIN mp_forms B ON (A.formId=B.id) JOIN mp_users C ON (A.doctorId=C.id) 
                    LEFT JOIN mp_form_categories D ON (A.formId=D.formId) 
					where $cond order by A.formId ASC";
		
		$rs=$this->pdb->query($q);
		
		foreach($rs as &$r){
			$r['image']=URL."assets/uploads/form_images/".$r['image'];
			$r['nextResDueDateNum']=strtotime($r['nextResDueDate']);
			$r['infoType']='Text';
			if($r['supportingStatement']){
				$r['infoType']='PDF';
				$r['infoForPatient']=URL."assets/uploads/form_pdf/".$r['supportingStatement'];
			}
			
			if($r['lastResponseDate'])
				$r['lastResponseDateNum']=strtotime($r['lastResponseDate']);
			else
				$r['lastResponseDate']=$r['lastResponseDateNum']='';
			
			$r['submittedBefore']='F';
			if($this->isFormSubmited($data['userId'], $r['formId'])){
				$r['submittedBefore']='T';
			}
			
			$r['formInfo']=formInfo($r['formId']);
			$r['infoForPatient']=preg_replace('!\\r?\\n!', "", $r['infoForPatient']);
		}
		
		sendJson(array('responseForms'=>$rs), 'T', 'Request was successful.', 'OK');
	}
        
	function formQuestions(){
		$data=arrayTrim($_POST);
		if(!$data['formId'])
			sendJson(false, 'F', 'Form id not found', 'WC');
		else if(!$this->pdb->singleVal("mp_forms", "id='{$data['formId']}'", "id"))
			sendJson(false, 'F', 'Invalid Form Id', 'WC');
		
		$q="SELECT DISTINCT A.id questionId, A.ques question, A.infoText quesInfo, A.type,
			GROUP_CONCAT(B.id,'|',B.optionName,'|',B.score, '|',B.caption ORDER BY B.id SEPARATOR '~') optionDtl 
			FROM mp_form_question A LEFT 
			JOIN mp_form_ques_options B ON (A.id=B.quesId) WHERE A.formId='{$data['formId']}' AND A.id!=89 GROUP BY A.id";
	
		$rs= $this->pdb->query($q);
		
		$rs=formatQuesArr($rs, $data['formId']);
		
		sendJson(array('questions'=>$rs), 'T', 'Request was successful.', 'OK');
	}
		
        
	function submitForm(){
		$data=arrayTrim($_POST);
		
		/** Saving Response **/
		$info['userId']=$data['userId'];
		$info['doctorId']=$data['doctorId'];
		$info['formId']=$data['formId'];
		$info['dueDate']=$data['nextResDueDate'];
		$info['resDate']=date('Y-m-d H:i:s');
		$resId=$this->pdb->insert("mp_response", $info);
		
		$optIds=array();
		
		if($resId && $data['response']){
			$response=json_decode($data['response'], true);
			if($response && is_array($response)){
				foreach($response as $r){
					$inf['resId']=$resId;
					$inf['quesId']=$r['questionId'];
					$inf['ans']=$r['ans']?$r['ans']:'';
					
					if($r['optionId']){
						foreach($r['optionId'] as $opt){
							$optIds[]=$inf['optionId']=$opt;
							$this->pdb->insert("mp_response_details", $inf);
						}
					}
					else if($inf['ans']){
						$this->pdb->insert("mp_response_details", $inf);
					}
				}
			}
		}
		
		if($optIds){
			$optIds=implode(",", $optIds);
			$q="SELECT B.scaleId, SUM(A.score) score FROM mp_form_ques_options A JOIN mp_form_question B ON (A.quesId=B.id) WHERE A.id IN ($optIds) 
			GROUP BY B.scaleId";
			$scales=$this->pdb->query($q);
			if($scales){
				foreach($scales as $a){
					$cond="userId='{$data['userId']}' AND formId='{$data['formId']}' AND doctorId='{$data['doctorId']}' AND scaleId='{$a['scaleId']}'";
					$sdtl=$this->pdb->singleRow("mp_patient_form_scales", $cond);
					$sinf=array();
					if( $sdtl['baselineScore']===null ){
						$sinf['baselineScore']=$a['score'];
						
						if($data['formId']==19 && $a['scaleId']==36){
							//$sinf['baselineScore']=euroVals($data['combination']);
						}
					}
					
					if(!$data['resDT'] || $data['resDT']==$data['lastResDate']){
						$sinf['lastScore']=$a['score'];
						
						if($data['formId']==19 && $a['scaleId']==36){
							//$sinf['lastScore']=euroVals($data['combination']);
						}
					}
					
					$this->pdb->update("mp_patient_form_scales", $sinf, $cond);
				}
			}
		}
		
		$formDtl=$this->pdb->query("select id, nextResDueDate, resFreq from mp_patient_forms where userId='{$data['userId']}' AND formId='{$data['formId']}'");
		$formDtl=$formDtl[0];
		
		if(strtotime(date('Y-m-d'))>=strtotime(date('Y-m-d', strtotime($formDtl['nextResDueDate'])))){
			$inf['nextResDueDate']=getNextDate($formDtl['nextResDueDate'], $formDtl['resFreq']);
			$this->pdb->update("mp_patient_forms", $inf, "id=".$formDtl['id']);
		}
		/** **/
		
		sendJson($data, 'T', 'Request was successful.', 'OK');
	}
	
	function faq() {
		$data=arrayTrim($_POST);
		$cond="1";
		
		if($data['keyword'])
			$cond.=" AND (question LIKE '%{$data['keyword']}%' OR answer LIKE '%{$data['keyword']}%' OR keyword LIKE '%{$data['keyword']}%')";
			
		$q="select question, answer from mp_faq where category IN ('A','P') AND $cond order by id desc";
		$rs['faq']=$this->pdb->query($q);
		sendJson($rs, 'T', 'Request was successful.', 'OK');
	}
	
	function myDoctors() {
		$data=arrayTrim($_POST);
		if(!$data['userId'])
             sendJson(false, 'F', 'User id not found', 'WC');
		else if(!$this->pdb->singleVal("mp_users", "id='{$data['userId']}'", "id"))
             sendJson(false, 'F', 'Invalid User Id', 'WC');
		
		$userId=$data['userId'];
		$q="SELECT CONCAT('MP', A.id) uniqueId, A.id userId, A.firstName, A.lastName, IF(A.loggedIn, 'T', 'F') loggedIn 
			FROM mp_users A JOIN mp_patients B ON (A.id=B.doctorId) WHERE B.userId='$userId'";
		$rs=$this->pdb->query($q);
		sendJson(array('doctors'=>$rs), 'T', 'Request was successful.', 'OK');
	}
}

