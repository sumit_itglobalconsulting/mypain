<?php 
class User_model extends CI_Model {
	function login($data) {
		$username=$data['username'];
		$pass=encryptText($data['password']);
		$cond="(username=:uname AND password=:pass) OR (loginEmail=:uname AND password=:pass)";
		return $this->pdb->singleRow("mp_users", $cond, "*", array(':uname'=>$username, ':pass'=>$pass));
	}
	
	/** Hospital Register **/
	function registerHospital($data) {
		$data['type']='H';
		$data['created']=$data['updated']=currentDate();
		
		$data['phone1']=$data['stdCode1'].$data['phone1'];
		$data['phone2']=$data['stdCode2'].$data['phone2'];
		
		$data['userId']=$this->pdb->insert("mp_users", $data);
		unset($data['id']);
		$id=$this->pdb->insert("mp_hospitals", $data);
		
		$inf['userId']=$data['userId'];
		$inf['planId']=$data['planId'];
		$inf['created']=currentDate();
		$this->pdb->insert("mp_hospital_plans", $inf);
		
		return $data['userId'];
	}
}

