<div class="pageheader">
	<h2><i class="fa fa-envelope-o"></i> Contact Us</h2>
	<?php if(!loggedUserData()){?>
	<div class="breadcrumb-wrapper">
		<a href="<?php echo URL;?>">&laquo; back to login</a>
	</div>
	<?php }?>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">Get in touch with us</h4>
		</div> 
        
		<form action="" method="post">
    <input type='hidden' name='contact' value='contact'>	
		<div class="panel-body" style="min-height:800px">
      <div class="row ">
        <div class="col-md-8">
         <div class="row mb20">
            <div class="col-md-6">
                <label>Name* </label>
                <input type="text" name="firstName" class="form-control">
                <label class="error"><?php echo error_msg($errors,'firstName');?></label>
            </div>
            <div class="col-md-6">
              <label>Last Name </label>
              <input type="text" name="lastName" class="form-control">
              <label class="error"><?php echo error_msg($errors,'lastName');?></label>
            </div>
          </div>
          
          <div class="row mb20">
            <div class="col-md-6">
                <label>Email* </label>
                <input type="text" name="email" class="form-control">
                <label class="error"><?php echo error_msg($errors,'email');?></label>
            </div>
            <div class="col-md-6">
              <label>Confirm Email* </label>
              <input type="text" name="confirmemail" class="form-control">
               <label class="error"><?php echo error_msg($errors,'confirmemail');?></label>
               <label class="error"><?php echo error_msg($errors,'mismatch');?></label>
            </div>
          </div>
          
          <div class="row mb20">
            <div class="col-md-6">
                <label>Subject* </label>
                <input type="text" name="subject" class="form-control">
                 <label class="error"><?php echo error_msg($errors,'subject');?></label>
            </div>
          </div>
          
           <div class="row mb20">
             <div class="col-md-6">
              <label>Your Message</label>
              <textarea name="message" class="form-control">  
              </textarea>
               <label class="error"><?php echo error_msg($errors,'message');?></label>
              </div>
         </div>
          
          <div class="row">
            <div class="col-md-6">
               <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </div>
        
         
        <div class="col-md-3 pull-right">
             <h4>Please contact Susan Young for appointments</h4>
             49 Lullingstone Lane, Hither Green, London, SE13 6UH.  
             <div class="clear">&nbsp;</div>
              enquiries@my-pain.co.uk
        </div>
        <div class="clearfix"></div>
      </div>
			
		</div>
    </form>
	</div>
</div>