<div class="pageheader">
	<h2><i class="fa fa-book"></i> Acceptable Use Policy</h2>
	<?php if(!loggedUserData()){?>
	<div class="breadcrumb-wrapper">
		<a href="<?php echo URL;?>">&laquo; back to login</a>
	</div>
	<?php }?>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
		<div class="panel-body cms" style="min-height:800px">
			<p>This acceptable use policy sets out terms between you and us under which you may access our website 
            <a href="https://mypainimpact.com" target="_blank">mypainimpact.com</a> 
            Your use of our site means that you accept, and agree to abide by, all the policies in this acceptable use policy 
            (which supplement our <a href="<?php echo URL."page/terms";?>">terms of use</a>).</p>
            <p>&nbsp;</p>
            
            <h4>1. Prohibited Uses</h4>
            <div>
            	<ul>
                	<li>
                    	1.1. You may use our site only for lawful purposes.  You may not (non-exclusively) use our site:
                        <ol type="A">
                        	<li>
                            	<p>In any way that:</p>
                                <ol type="i">
                                	<li><p>Breaches any applicable local, national or international law or regulation;</p></li>
                                    <li><p>Is unlawful or fraudulent, or has any unlawful or fraudulent purpose or effect;</p></li>
                                </ol>
                            </li>
                            <li>
                            	<p>For the purpose of harming or attempting to harm minors in any way;</p>
                            </li>
                            <li>
                            	To:
                                <ol type="I">
                                	<li><p>Send, knowingly receive, upload, download, use or re-use any material which does not comply with our content standards below;</p></li>
                                    <li><p>Transmit, or procure the sending of, any unsolicited or unauthorised advertising or promotional material or any other 
                                    form of similar solicitation ('spam');</p></li>
                                    <li><p>Knowingly transmit any data, send or upload any material that contains viruses, Trojan horses, worms, time-bombs, keystroke 
                                    loggers, spyware, adware or any other harmful programs or similar computer code designed to adversely affect the operation of 
                                    any computer software or hardware.</p></li>
                                </ol>
                            </li>
                        </ol>
                    </li>
                    
                    <li>
                    	1.2. You also agree not to:
                        <ol type="A">
                        	<li>
                            	<p>Reproduce, duplicate, copy or re-sell any part of our site (or material available on it) in contravention of the provisions of 
                                our <a href="<?php echo URL."page/terms";?>">terms of use</a>; and</p>
                            </li>
                            
                            <li>
                            	<p>Access without authority, interfere with, damage or disrupt:</p>
                                <ol type="i">
                                	<li><p>Any part of our site;</p></li>
                                    <li><p>Any equipment or network on which our site is stored;</p></li>
                                    <li><p>Any software used in the provision of our site; or</p></li>
                                    <li><p>Any equipment or network or software owned or used by any third party.</p></li>
                                </ol>
                            </li>
                        </ol>
                    </li>
                </ul>
                
                
            </div>
            <h4>2.  Interactive services</h4>
            <div>
                  <ul>
                    <li>
                      2.1. We may from time to time provide interactive services on our site, including (without limitation):
                            <ol type="A">
                              	<li>
                                  <p>chat rooms;</p>
                                </li>
                                
                                <li>
                                 <p>bulletin boards;</p>
                                </li>
                                
                                <li>
                                 <p>the facility to:</p>
                                   <ol type="i">
                                     <li><p>create 'modules', featuring text and audio-visual content uploaded by you;</p></li>
                                     <li><p>comment on 'modules' by uploading text,</p></li>
                                   </ol>
                                </li>
                            </ol>
                    </li>
                    
                    <li>
                      2.2. We do not moderate interactive services, except where we have reason to believe that a breach of this acceptable use policy 
                      and/or our terms of use has taken (or may take) place.
                    </li>
                    
                    <li>
                      2.3. We are under no obligation to oversee, monitor or moderate any interactive service we provide on our site and we expressly 
                      exclude our liability for any loss or damage arising from the use of any interactive service  (whether moderated or not)
                       by a user in contravention of our content standards.
                    </li>
                    
                    <li>
                     2.4. Should you have any concern about our interactive services, please contact us via enquiries@my-pain.co.uk
                    </li>
                  </ul>
            </div>
            
            <h4>3. Content standards</h4>
            <div>
               <ul>
                 <li>
                  3.1. These content standards apply to any and all material which you 
                  contribute to our site (contributions) and to any interactive services associated with it.
                 </li>
                 
                 <li>
                  3.2. You must comply with the spirit of the following standards as well as the letter.
                   The standards apply to each part of any contribution as well as to its whole.
                 </li>
                 
                 <li>
                 3.3. Contributions must:
                   <ol type="a">
                              	<li>
                                  <p>be accurate (where they state facts); </p>
                                </li>
                                <li>
                                  <p>be genuinely held (where they state opinions); and</p>
                                </li>
                                <li>
                                 <p>comply with applicable law in any country/territory from which they are posted.</p>
                                </li>
                    </ol>
                 </li>
                 
                 <li>
                 3.4. Contributions must not:
                   <ol type="a">
                       <li>
                          <p>contain any material which is:</p>
                          
                          <ol type="i">
                            <li>
                             <p>defamatory of any person; or</p>
                            </li>
                            <li>
                              <p>obscene, offensive, hateful or inflammatory;</p>
                            </li>
                          </ol>
                       </li>
                       
                       <li>
                        <p>promote:</p>
                         
                          <ol type="i">
                            <li>
                               <p>sexually explicit material;</p>
                            </li>
                            <li>
                              <p>violence;</p>
                            </li>
                            <li>
                             <p>discrimination based on race, sex, religion, nationality, disability, sexual orientation or age;</p>
                            </li>
                            <li>
                              <p>any other illegal activity; or</p>
                            </li>
                            
                         </ol> 
                       </li>
                       
                    
                       
                       <li>
                         <p>infringe any intellectual property rights (including, without limitation, copyright, database rights and trade marks) 
                            of any other person;</p>
                       </li>
                       
                       <li>
                         <p>be:</p>
                         
                         <ol type="i">
                           <li>
                               <p>made in breach of any legal duty owed to a third party, such as a contractual duty or a duty of confidence; </p>
                           </li>
                           
                           <li>
                               <p>threatening, abuse or invade another's privacy, or cause annoyance, inconvenience or needless anxiety;</p>
                           </li>
                           
                           <li>
                               <p>likely to:</p>
                                
                              <ol type="A">
                                <li>
                                  <p>deceive any person; or</p>
                                </li>
                                <li>
                                  <p>harass, upset, embarrass, alarm or annoy any other person; or</p>
                                </li>
                                
                             </ol> 
                           </li>
                           
                            <li>
                               <p>(iv) used to impersonate any person, or to misrepresent your identity or affiliation with any person;</p>
                            </li>
                              
                        </ol>
                       </li>
                      
                      <li>
                        <p>give the impression that they emanate from us, if this is not the case; or</p>
                      </li>
                      
                      <li>
                       <p>advocate, promote or assist any unlawful act such as (by way of example only) copyright infringement or computer misuse.</p>
                      </li> 
                      
                   </ol>
                 </li>
               </ul>
            </div>
          
         <h4>4. Suspension and termination</h4>  
         <div>
            <ul>
               <li>
                4.1. We will determine, in our sole and absolute discretion, whether there has been a breach of this acceptable use policy 
                through your use of our site. When we determinate that a breach of this policy has occurred, we may take such action as we deem appropriate.
               </li>
              
              <li>
               4.2. Failure to comply with this acceptable use policy constitutes a material breach of the <a href="<?php echo URL."page/terms";?>">terms of use</a> 
               and may result in our taking all or any of the following actions:
               
               <ol type="a">
                   <li>
                     <p>immediate, temporary and/or permanent:</p>
                       <ol type="i">
                          <li>
                            <p> withdrawal of your right to use our site;</p>
                          </li>
                          <li>
                            <p>(ii) removal of any posting or material uploaded by you to our site;</p>
                          </li>
                       </ol>
                   </li>
                   
                   <li>
                      <p>issue of a warning to you; </p>
                   </li>
                   
                   <li>
                     <p>legal proceedings against you for reimbursement of all costs 
                         (including, but not limited to, reasonable administrative and legal costs) resulting from the breach;</p>
                   </li>
                   
                   <li>
                    <p>further legal action against you;</p>
                   </li>
                   
                   <li>
                     <p>disclosure of such information to law enforcement authorities as we reasonably feel is necessary.</p>
                   </li>
               </ol>
              </li> 
             
             
             <li>
                4.3. We exclude liability for actions taken in response to breaches of this acceptable use policy.  
                The responses described in this policy are not limited, and we may take any other action we reasonably deem appropriate.
             </li>  
            </ul>
         
         </div>
         
      <h4>5. Changes to the acceptable use policy</h4>
        
        <p>We may revise this acceptable use policy at any time by amending this page.  You are expected to check this page from time to time 
        to take notice of any changes we make, as they are legally binding on you. Some of the provisions contained in this acceptable
         use policy may also be superseded by provisions or notices published elsewhere on our site.</p> 
            
		</div>
	</div>
</div>