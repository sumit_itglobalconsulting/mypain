<div class="pageheader">
	<h2><i class="fa fa-pencil-square-o"></i> Privacy Policy</h2>
	<?php if(!loggedUserData()){?>
	<div class="breadcrumb-wrapper">
		<a href="<?php echo URL;?>">&laquo; back to login</a>
	</div>
	<?php }?>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
		<div class="panel-body cms" style="min-height:800px">
			<p>My Pain Limited ("we", "us", "our" and trading as "My Pain") are committed to protecting and respecting your privacy.</p>
         <p>&nbsp;</p>
          This policy (together with My Pain's <a href="<?php echo URL."page/terms";?>">terms of use</a> and any other documents referred to on it) 
          sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. 
          Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it.
          <p>&nbsp;</p>
            
      <p>For the purpose of the Data Protection Act 1998 (the Act), 
         My Pain Limited (My Pain) is a data controller and its address is 49 Lullingstone Lane, Hither Green, London, SE13 6UH.
      </p>
            
      <h5>My Pain's nominated representative for the purpose of the Act is Dr Rahul Seewal.  </h5>
        
        <h4>1. INFORMATION WE MAY COLLECT FROM YOU</h4> 
           <p>We may collect and process the following data about you:</p>
           <ol type="a">
             <li>
               <p>information that you provide by filling in forms on <a href="http://my-pain.co.uk" target="_blank">my-pain.co.uk.</a> .  
               This includes information provided at the time of registering to use the site, subscribing to our service, 
               posting material or requesting further services; </p>
             </li>
             
             <li>
              <p>if you contact us, we may keep a record of that correspondence;</p>
             </li>
             
             <li>
             <p>details of your visits to the site (including, but not limited to, traffic data, location data and other communication data,
                 whether this is required for our own billing purposes or otherwise) and the resources that you access on the site.</p>
             </li>
           </ol>
         
            <h4>2. COOKIES</h4>
            <div>
                  <ul>
                    <li>
                     <p> 2.1 We may also obtain information about your general Internet usage by using a cookie file 
                         which is stored on your browser or the hard drive of your computer.  Cookies contain information that is transferred to your computer's hard drive.  
                        They help us to improve the site and to deliver a better and more personalised service.  
                        Some of the cookies we use are essential for the site to operate. </p>
                    </li>
                    
                    <li>
                     <p> 2.2 You may block cookies by activating the setting on your browser which allows you to refuse the setting of all or some cookies.
                        However, if you use your browser settings to block all cookies (including essential cookies) 
                        you may not be to access all or parts of the site.  Unless you have adjusted your browser setting so that it will refuse cookies,
                         the system will issue cookies as soon you visit the site. </p>
                    </li>
                    
                    <li>
                      <p>2.3 Details of the cookies used on the site are set out below.</p>
                       <table border="1px solid" >
                          <tr>
                            <th>Cookie</th> <th>Name</th>  <th>Purpose</th>
                          </tr>
                          <tr>
                             <td>PHPSESSID</td>
                             <td>PHPSESSID</td>
                             <td>To identify a session, a series of related message exchanges, in order that we can identify users visiting our site.</td>
                          </tr>
                          <tr>
                            <td>ci_session</td>
                            <td>ci_session</td>
                            <td>To allow us to store session information in our database and track user activity while using our site.</td>
                          </tr>
                          <tr>
                            <td>adminer_version</td>
                            <td>adminer_version</td>
                            <td>To allow us to access the database in a graphical interface displaying information on user activity.</td>
                          </tr>
                       </table>
                      
                    </li>
                    
                    <li>
                    <p>2.4 Except for essential cookies, all cookies will expire after [INSERT EXPIRY PERIOD].</p>
                    </li>
                  
            </div>
            
            <h4>3. IP ADDRESSES</h4>
            
             <p>We may also collect information about your computer (including your IP address, operating system and browser type) for system administration.</p>   
                    
          
         <h4>4. WHERE WE STORE YOUR PERSONAL DATA</h4>  
         <div>
           <ul>
             <li>
               <p>4.1 The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area (EEA). 
                   It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers.  
                   Such staff may be engaged  in, among other things, hosting and maintaining the site. 
                    By submitting your personal data, you agree to this transfer, storing or processing. 
                     We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</p>
             </li>
             <li>
              <p>4.2 All information you provide to us is stored on secure servers.   
                 You have been given a password which enables you to access certain parts of the site - you are responsible for keeping 
                 this password confidential.  You must not share your password with anyone.</p>
             </li> 
             
             <li>
              <p>4.3 Unfortunately, the transmission of information via the internet is not completely secure.  
              Although we will do our best to protect your personal data, 
              we cannot guarantee the security of your data transmitted to the site; any transmission is at your own risk.  
              Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.
              </p>
             </li>                                                                                      
           </ul> 
         </div>
         
      <h4>5. USES MADE OF THE INFORMATION</h4>
        
        <div>
          <ul>
            <li>
             <p>5.1 We use information held about you in the following ways:</p>
              <ol type="a">
                 <li> 
                    <p>to ensure that content from the site is presented in the most effective manner for you and for your computer;</p>
                 </li>
                 <li>
                    <p>to understand and review which resources you are using on the site;</p>
                 </li>
                 <li>
                   <p>to ensure that you are an authorised user of the site;</p>
                 </li>
                 <li>
                  <p>to notify you about changes to the service.</p>
                 </li>
              </ol>
            </li>
            <li>
              <p>5.2 Except for our service providers (e.g. website hosts), we do not disclose your data to third parties.</p>
            </li>
          </ul>
        </div>
           
      <h4>6. DISCLOSURE OF YOUR INFORMATION</h4>  
      <div>
         <ul>
           <li>
              <p>6.1 We may disclose your personal information to any member of our respective groups
               (which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in section 1159 
               of the UK Companies Act 2006).</p>
            </li>
           
           <li>
             <p>6.2 We may disclose your personal information to third parties:</p>
             <ol type="a">
               <li>
                 <p>in the event that we sell or buy any business or assets
                     (in which case we may disclose your personal data to the prospective seller or buyer of such business or assets);</p>
               </li>
               <li>
                <p> if substantially all of our assets are acquired by a third party 
                    (in which case personal data held about users of the site will be one of the transferred assets);</p>
               </li>
               
               <li>
                <p> 
                 if we are under a duty to disclose or share your personal data in order to comply with any legal obligation,
                  or in order to enforce or apply the <a href="<?php echo URL."page/terms";?>">terms of use</a> and other agreements, 
                  or to protect the rights, property, or safety of My Pain or others.
                </p>
               </li>
             </ol>
           </li>
           
        </ul>
      </div> 
      
      <h4>7. YOUR RIGHTS</h4>
       <div>
         <ul>
           <li>
             <p>7.1 We will not process your personal data for marketing purposes. </p>
           </li>
           <li>
            <p>7.2 The site may, from time to time, contain links to and from the websites of our partner networks and affiliates.  
            If you follow a link to any of these websites, please note that these websites have their own privacy policies and that 
            we do not accept any responsibility or liability for these policies.  
            Please check these policies before you submit any personal data to these websites.</p>
           </li>
         </ul>
       </div>
      
      
      <h4>8. ACCESS TO INFORMATION</h4>
        <p>The Act gives you the right to access information held about you.  
        Your right of access can be exercised in accordance with the Act.  
        Any access request may be subject to a fee of �10 to meet our costs in providing you with details of the information we hold about you.</p>
    
      
      <h4>9. CHANGES TO OUR PRIVACY POLICY</h4>
       <p>Any changes we may make to our privacy policy in the future will be posted on <a href="#" >LINK </a>  and, where appropriate, notified to you by e-mail.</p>
      
      
      <h4>10. CONTACT</h4>
    
            <p> Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to 
              enquires@my-pain.co.uk or write to: Privacy Compliance Co-ordinator, My Pain Limited, 49 Lullingstone Lane,
              Hither Green, London, SE13 6UH..</p>
          
    </div>
	</div>
</div>