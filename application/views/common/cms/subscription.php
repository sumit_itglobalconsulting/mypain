<div class="pageheader">
	<h2><i class="fa fa-book"></i> Subscription Licence Agreement</h2>
	<?php if(!loggedUserData()){?>
	<div class="breadcrumb-wrapper">
		<a href="<?php echo URL;?>">&laquo; back to login</a>
	</div>
	<?php }?>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
		<div class="panel-body" style="min-height:800px">
			
			<strong>My Pain Limited</strong><br />

and<br />

[FULL COMPANY NAME OF THE OTHER PARTY]<br /><br />

My Pain Impact <br />
Subscription Licence Agreement<br />


<strong>Briffa<br />
Business Design Centre<br />
52 Upper Street<br />
London<br />
N1 0QH, UK<br />
Tel:   44 (0)20 7288 6003<br />
Fax:  44 (0)20 7288 6004<br />
www.briffa.com</strong><br /><br />


<strong>THIS AGREEMENT</strong> is dated [DATE].<br /><br />

<strong>Parties</strong><br />
<strong>(1)</strong> My Pain Limited, incorporated and registered in England and Wales with company number 08691506, whose registered office is at 49 Lullingstone Lane, Hither Green, London, SE13 6UH (My Pain).<br />
<strong>(2)</strong> [FULL COMPANY NAME], incorporated and registered in [JURISDICTION] with company number [NUMBER], whose registered office is at [REGISTERED OFFICE ADDRESS] (the Customer).<br />

<strong>Background</strong><br />
<strong>(A)</strong> My Pain has developed certain software applications and platforms which it makes available to subscribers via the Internet on a pay-per-use basis for the purpose of managing pain.<br />
<strong>(B)</strong> The Customer wishes to use My Pain's service in its operations.<br />
<strong>(C)</strong> My Pain has agreed to provide and the Customer has agreed to take and pay for My Pain's service, from the Effective Date (as defined below), subject to the terms and conditions of this agreement.<br />

<strong>Agreed terms</strong><br /><br />


<strong>1. Interpretation</strong><br />

<strong>1.1</strong> The definitions and rules of interpretation in this clause 1 apply in this agreement.<br />
<strong>AU Personal Data:</strong>	has the meaning ascribed to it in clause 6.1.<br />
<strong>Authorised Users:</strong> 	those third parties and/or Representatives who are authorised to use the Services and the Documentation by completing the registration process described in clause 2.2.<br />
<strong>Business Day:</strong> 	any day which is not a Saturday, Sunday or public holiday in in the United Kingdom.<br />
<strong>Business Hours:</strong>	8.00 am to 6.00 pm UK local time, each Business Day.<br />
<strong>Commencement Date:</strong>	the date on which this agreement becomes legally binding on the parties.<br />
<strong>Confidential Information:</strong> 	all confidential information (however recorded, preserved or disclosed) disclosed by a party or its Representatives to the other party and/or that party's Representatives, including the terms of this agreement but not including any information that is trivial, obvious or useless.<br />
<strong>Customer Data:</strong> 	any data inputted/stored (whether on the Customer's systems or those of My Pain) by the Customer and/or Authorised Users (and/or My Pain on the Customer's and/or Authorised Users' behalf) for the purpose of using the Services or facilitating the Customer's and/or Authorised Users' use of the Services.<br /><br />
<strong>DPA:</strong>	Data Protection Act 1998.<br />
<strong>Data Controller:</strong>	has the meaning ascribed to it in the DPA.<br />
<strong>Data Processor:</strong>	has the meaning ascribed to it in the DPA.<br />
<strong>Documentation:</strong> 	the document(s) made available to the Customer and Authorised Users by My Pain online via <my-pain.co.uk> (or such other web address notified by My Pain to the Customer from time to time) which sets out a description of the Services and the user instructions for the Services.<br />
<strong>Effective Date:</strong> 	[1st January [YEAR]].  [This is the date on which the parties' obligations commence.]<br />
<strong>Initial Subscription Term:</strong> 	one Year from the Effective Date.<br />
<strong>Intellectual Property Rights:</strong>	patents, rights to inventions, copyright and related rights, trade marks and service marks, trade names and domain names, rights in get-up, rights to goodwill and to sue for passing off and unfair competition, rights in designs, rights in computer software, database rights, rights in confidential information (including know-how and trade secrets) and any other intellectual property rights, in each case whether registered or unregistered and including all applications (and rights to apply) for, and renewals or extensions of, such rights and all similar or equivalent rights or forms of protection which subsist or will subsist, now or in the future, in any part of the world.<br />
<strong>Local Regulations:</strong> 	laws and regulations applicable to the provision of the Services in the Customer's jurisdiction(s).<br />
<strong>My Pain Data:</strong>	any data provided by My Pain to the Customer, excluding any Customer Data.<br />
<strong>Personal Data:</strong>	has the meaning ascribed to it in the DPA.<br />
<strong>Privacy Policy:</strong>	the privacy policy (as may be amended by My Pain from time to time) to which Authorised Users must agree before they can use the Software, the current version of which is annexed at Schedule 2.<br />
<strong>Processing:</strong>	has the meaning ascribed to it in the DPA; Process, Processed etc. shall have corresponding meanings.<br />
<strong>Renewal Period:</strong> 	has the meaning ascribed to it in clause 17.1.<br />
<strong>Representative:</strong>	employees, agents and other representatives of a party.<br />
<strong>Services:</strong> 	the subscription services provided by My Pain to Authorised Users for the benefit of the Customer under this agreement via <my-pain.co.uk> 
<p>(or any other website notified to the Customer by My Pain from time to time), as more particularly described in the Documentation.<br />
  <strong>Software:</strong> 	the online software applications provided by My Pain as part of the Services.<br />
  <strong>Subscription Fees:</strong> 	the subscription fees payable by the Customer to My Pain for the User Subscriptions, as set out in paragraph 1 of Schedule 1.<br />
  <strong>Subscription Term:</strong> 	has the meaning ascribed to it in clause 17.1.<br />
  <strong>Support Services Policy:</strong> 	My Pain's policy (as may be amended by My Pain from time to time, acting reasonably) for providing support in relation to the Services, the current version of which is annexed at Schedule 4.<br />
  <strong>T's & C's of Use:</strong>	the terms and conditions (as may be amended by My Pain from time to time) to which Authorised Users must agree before they can use the Software, the current version of which are annexed at Schedule 2.<br />
Trial Period	the period of trial use of the Services in which all the provisions of this Agreement shall apply, save as the Customer shall have no obligation to pay any Subscription Fees in respect of the trial period and either party may terminate the Agreement immediately by giving written notice to the other party at any time before the end of the trial period.<br />
<strong>User Subscriptions:</strong> 	the user subscriptions purchased by the Customer pursuant to clause 10.1 which entitle Authorised Users to access and use the Services and the Documentation in accordance with this agreement.<br />
<strong>VAT:</strong>	value added tax chargeable under the Value Added Tax Act 1994 and any similar additional tax and any similar additional tax or any other similar turnover, sales or purchase tax or duty levied in any other jurisdiction.<br />
<strong>Year:</strong>	the period of 12 months from the Effective Date and each consecutive period of 12 months thereafter during the period of this agreement.<br /><br />

<strong>1.2</strong> Clause, schedule and paragraph headings shall not affect the interpretation of this agreement.<br />
<strong>1.3</strong> A reference to:<br />
<strong>(a)</strong> a person includes an individual, corporate or unincorporated body (whether or not having separate legal personality) and that person's legal and personal representatives, successors or permitted assigns;<br />
<strong>(b)</strong> a company shall include any company, corporation or other body corporate, wherever and however incorporated or established;<br />
<strong>(c)</strong> one gender shall include a reference to the other genders;<br />
<strong>(d)</strong> writing or written does not include e-mail or fax;<br />
<strong>(e)</strong> including shall be deemed to be followed by the words without limitation;<br />
<strong>(f)</strong> a statute or statutory provision:<br />
<strong>(i)</strong> is a reference to it as amended, extended or re-enacted from time to time;<br />
<strong>(ii)</strong> shall include all subordinate legislation made from time to time under that statute or statutory provision;<br />
<strong>(g)</strong> a clause or a schedule is to a clause or schedule of this agreement; <br />
<strong>(h)</strong> a paragraph is to a paragraph of the relevant schedule to this agreement.<br />
<strong>1.4 </strong>Words in the singular shall include the plural and vice versa.<br /><br />
<strong>2. User subscriptions</strong><br />
<strong>2.1</strong> Subject to the Customer purchasing the User Subscriptions in accordance with clause 3.3 and clause 10.1, the  restrictions set out in this clause 2 and the other terms and conditions of this agreement, My Pain hereby grants to the Customer a non-exclusive, non-transferable right to permit the Authorised Users to use the Services and the Documentation (from the Effective Date and during the Subscription Term) solely for the Customer and the Authorised Users' management of pain purposes.<br />
<strong>2.2</strong> Those third parties and/or Representatives which the Customer wishes to use the Services must register (at a link to be provided to the Customer by My Pain) by:<br />
<strong>(a)</strong> providing their name, email and such other information as My Pain may reasonably require); <br />
<strong>(b)</strong> choosing a password; and<br />
<strong>(c)</strong> confirming their acceptance of the T's & C's of Use and the Privacy Policy. <br />
<strong>2.3</strong> Once the number of third parties and/or Representatives who have completed such registration reaches the number of the User Subscriptions purchased by the Customer, subject to clause 3, My Pain shall not be obliged to permit any further registrations.<br />
<strong>2.4</strong> In relation to the Authorised Users, the Customer undertakes that:<br />
<strong>(a)</strong> it will not knowingly allow or suffer any User Subscription to be used by more than one individual;<br />
<strong>(b)</strong> each Authorised User shall:<br />
<strong>(i)</strong> change his password for his use of the Services and Documentation no less frequently than [MONTHLY]; and<br />
<strong>(ii)</strong> keep his password confidential.<br />
<strong>2.5</strong> [The parties agree that My Pain shall only provide the Services to those Authorised Users who access them from the following IP addresses:<br />
<strong>(a)</strong> [IP ADDRESS];<br />
<strong>(b)</strong> [IP ADDRESS].]<br />
<strong>2.6</strong> The Customer agrees that My Pain shall not be obliged to provide the Services to any individual who does not confirm their acceptance of the T's & C's of Use and the Privacy Policy.<br />
<strong>2.7</strong> The Customer shall procure that the Authorised Users use the Services and the Documentation in accordance with the T's & C's of Use.<br />
<strong>2.8</strong> In the event that My Pain discovers (or reasonably believes that there has been) a breach of clause 2.7, My Pain shall have the right, without liability to the Customer and without prejudice to its other rights, to disable the Authorised Users' passwords, accounts and access to all or part of the Services and My Pain shall be under no obligation to provide any or all of the Services until the Authorised User(s) who caused such breach have been identified.  The Customer shall provide My Pain with such assistance and information as may be required by My Pain in order to identify such Authorised User(s).  Following identification of such Authorised User(s), My Pain shall:<br />
<strong>(a)</strong> (without prejudice to its other rights and remedies) be entitled to keep such Authorised Users' passwords, accounts and access to all or part of the Services disabled and My Pain shall be under no obligation to provide any of the Services to them; and<br />
<strong>(b)</strong> (if applicable) promptly re-enable the remaining Authorised Users' passwords, accounts and access to all of the Services.<br />
<strong>2.9</strong> The Customer shall not:<br />
<strong>(a)</strong> except as may be allowed by any applicable law (which is incapable of exclusion by agreement between the parties):<br />
<strong>(i)</strong> and except to the extent expressly permitted under this agreement, attempt to copy, modify, duplicate, create derivative works from, frame, mirror, republish, download, display, transmit, or distribute all or any portion of the Software and/or Documentation (as applicable) in any form or media or by any means; or<br />
<strong>(ii)</strong> attempt to reverse compile, disassemble, reverse engineer or otherwise reduce to human-perceivable form all or any part of the Software; or<br />
<strong>(b)</strong> access all or any part of the Services and Documentation in order to build a product or service which competes with the Services and/or the Documentation; or<br />
<strong>(c)</strong> except for Authorised Users:<br />
<strong>(i)</strong> use the Services and/or Documentation to provide services to third parties; or<br />
<strong>(ii)</strong> subject to clause 24.1, license, sell, rent, lease, transfer, assign, distribute, display, disclose, or otherwise commercially exploit, or otherwise make the Services and/or Documentation available to any third party; or<br />
<strong>(iii)</strong> attempt to obtain, or assist third parties in obtaining, access to the Services and/or Documentation.<br />
<strong>2.10</strong> The Customer shall use all reasonable endeavours to prevent any unauthorised access to, or use of, the Services and/or the Documentation and, in the event of any such unauthorised access or use, promptly notify My Pain.<br />
<strong>2.11</strong> The rights provided under this clause 2 are granted to the Customer only and not to any third parties.<br /><br />
<strong>3. Additional user subscriptions</strong><br />
<strong>3.1</strong> Subject to clause 3.2 and clause 3.3, the Customer may, from time to time during the Subscription Term, purchase additional User Subscriptions in excess of the number set out in paragraph 1 of Schedule 1 and My Pain shall grant access to the Services and the Documentation to such additional Authorised Users in accordance with the provisions of this agreement.<br />
<strong>3.2</strong> If the Customer wishes to purchase additional User Subscriptions, the Customer shall notify My Pain in writing.  My Pain shall evaluate such request for additional User Subscriptions and respond to the Customer with approval or rejection of the request (such approval not to be unreasonably withheld, conditioned or delayed).<br />
<strong>3.3</strong> If My Pain approves the Customer's request to purchase additional User Subscriptions, the Customer shall, on the Effective Date, pay to My Pain the relevant fees for such additional User Subscriptions as set out in paragraph 2 of Schedule 1.  If such additional User Subscriptions are purchased by the Customer part way through a Year, such fees shall not be pro-rated (in other words, User Subscriptions for part Years are charged at the same price as for whole Years).<br /><br />
<strong>4. Services</strong><br />
<strong>4.1</strong> My Pain shall, after the Commencement Date and during the Subscription Term, provide the Services and make available the Documentation to the Customer and Authorised Users on, and subject to, the terms of this agreement.<br />
<strong>4.2</strong> My Pain shall use its reasonable endeavours to make the Services available 24 hours a day, seven days a week, except for:<br />
<strong>(a)</strong> planned maintenance carried out during the maintenance window of 10.00 pm to 2.00 am UK local time; and<br />
<strong>(b)</strong> unscheduled maintenance performed outside Business Hours, provided that My Pain has used reasonable endeavours to give the Customer at least two whole Business Days' notice in advance.<br />
<strong>4.3</strong> My Pain will, as part of the Services and at no additional cost to the Customer, provide the Customer with customer support services in accordance with the Support Services Policy.<br /><br />
<strong>5. Customer data</strong><br />
<strong>5.1</strong> Save as provided in this agreement, My Pain shall not have any rights in respect of Customer Data.<br />
<strong>5.2</strong> My Pain shall use reasonable commercial endeavours to back-up Customer Data (whether stored on My Pain's systems or those of a third party on My Pain's behalf).  In the event of any loss or damage to Customer Data, the Customer's sole and exclusive remedy shall be for My Pain to use reasonable commercial endeavours to restore the lost or damaged Customer Data from the latest back-up of such Customer Data. My Pain shall not be responsible for any loss, destruction, alteration or disclosure of Customer Data caused by any third party (except those third parties sub-contracted by My Pain to perform services related to Customer Data maintenance and back-up).<br /><br />
<strong>6. Data protection</strong><br />
<strong>6.1</strong> The parties acknowledge and agree that:<br />
<strong>(a)</strong> they are both Data Controllers of the Personal Data (acquired by My Pain in relation to My Pain's provision of the Services) pertaining to the Authorised Users (AU Personal Data);<br />
<strong>(b)</strong> My Pain and/or its sub-contractor(s) will also be Data Processors of the AU Personal Data; and<br />
<strong>(c)</strong> the Customer may be a Data Processor (e.g. should it hold or store AU Personal Data).<br />
<strong>6.2</strong> In relation to AU Personal Data which either party holds and/or stores (or which is held or stored on their behalf by a third party), each party:<br />
<strong>(a)</strong> warrants that it (or its relevant sub-contractors) has in place appropriate technical and organisational security measures against:<br />
<strong>(i)</strong> unauthorised or unlawful Processing of the AU Personal Data; and<br />
<strong>(ii)</strong> accidental loss or destruction of, or damage to, the AU Personal Data;<br />
<strong>(b)</strong> shall be entitled to refuse to any request from the other to Process the AU Personal Data to the extent that it reasonably believes that complying with such request would render it (or both parties) in breach of its/their obligations under the DPA and shall have no liability whatsoever for so refusing;<br />
<strong>(c)</strong> agrees that the Personal Data may be transferred or stored outside the EEA or the country where the Customer and/or the Authorised Users are located.<br />
<strong>6.3</strong> The Customer warrants that My Pain's compliance with the DPA will be sufficient to comply with any equivalent or similar local laws and/or regulations in any jurisdiction to which the Customer is subject.<br />
<strong>6.4</strong> My Pain warrants that it shall comply with its obligations under the DPA.<br />
<strong>6.5</strong> The Customer warrants that:<br />
<strong>(a)</strong> where it is subject to the DPA, it shall comply with the DPA; and<br />
<strong>(b)</strong> it shall comply with any equivalent or similar local laws and/or regulations in any other jurisdiction to which it is subject. <br /><br />
<strong>7. Third party providers </strong><br />
The Customer acknowledges that the Services may enable or assist Authorised Users to access the website content of, correspond with, and purchase products and services from, third parties via third-party websites and that they do so solely at their own risk.<br />
<strong>8. My Pain's obligations</strong><br />
<strong>8.1</strong> My Pain undertakes that the Services will be performed:<br />
<strong>(a)</strong> substantially in accordance with the Documentation; and<br />
<strong>(b)</strong> with reasonable skill and care.<br />
This undertaking shall not apply to the extent of any non-conformance which is caused by:<br />
<strong>(c)</strong> use of the Services contrary to My Pain's instructions; <br />
<strong>(d)</strong> the Customer Data; and/or<br />
<strong>(e)</strong> modification or alteration of the My Pain Data by any party other than My Pain.<br />
<strong>8.2</strong> If the Services do not conform with the foregoing undertaking, My Pain will, at its expense, use all reasonable commercial endeavours to correct any such non-conformance promptly.  Such correction or substitution constitutes the Customer's sole and exclusive remedy for any breach of the undertaking set out in clause 8.1.  Notwithstanding the foregoing, My Pain:<br />
<strong>(a)</strong> does not warrant that:<br />
<strong>(i)</strong> the Customer's use of the Services will be uninterrupted or error-free; <br />
<strong>(ii)</strong> the Services, Documentation and/or the information obtained by the Authorised Users through the Services will meet the Customer and/or the Authorised Users' requirements; and<br />
<strong>(b)</strong> is not responsible for any delays, delivery failures, or any other loss or damage resulting from the transfer of data over communications networks and facilities, including the Internet, and the Customer acknowledges that the Services and Documentation may be subject to limitations, delays and other problems inherent in the use of such communications facilities.<br />

<strong>8.3</strong> This agreement shall not prevent My Pain from entering into similar agreements with third parties, or from independently developing, using, selling or licensing documentation, products and/or services which are similar to those provided under this agreement.<br />

<strong>8.4</strong> My Pain warrants that it has and will maintain all necessary (under the laws of England and Wales) licences, consents, and permissions necessary for the performance of its obligations under this agreement.<br />
<strong>9. Customer's obligations</strong><br />
<strong>The Customer shall:</strong><br />
<strong>(a)</strong> provide My Pain with all necessary:<br />
<strong>(i)</strong> co-operation in relation to this agreement; and<br />
<strong>(ii)</strong> access to such information as may be required by My Pain;
in order to provide the Services;<br />
<strong>(b)</strong> comply with all applicable laws and regulations with respect to its activities under this agreement;<br />
<strong>(c)</strong> carry out all other Customer responsibilities set out in this agreement in a timely and efficient manner;<br />
<strong>(d)</strong> obtain and shall maintain all necessary licences, consents, and permissions necessary (in any jurisdiction(s) to which it is subject, save for such as are required in England and Wales) for My Pain, its contractors and agents to perform their obligations under this agreement, including the provision of the Services;<br />
<strong>(e)</strong> ensure that its network and systems comply with the relevant specifications reasonably provided by My Pain from time to time; and<br />
<strong>(f)</strong> be solely responsible for:<br />
<strong>(i)</strong> procuring and maintaining its network connections and telecommunications links from its systems to My Pain's data centres; and<br />
<strong>(ii)</strong> all problems, conditions, delays, delivery failures and all other loss or damage arising from or relating to the Customer's network connections or telecommunications links or caused by the Internet.<br /><br />

<strong>10. Charges and payment</strong><br />
<strong>10.1</strong> With the exception of the Trial Period, the Customer shall pay the Subscription Fees to My Pain for the User Subscriptions in accordance with this clause 10 and Schedule 1.<br />
<strong>10.2</strong> All Subscription Fees are payable in advance of Services.<br />
<strong>10.3</strong> My Pain shall charge the Customer:<br />
<strong>(a)</strong> during the Initial Subscription Term:<br />
<strong>(i)</strong> on the Effective Date for the Subscription Fees payable in respect of the following Year; <br />
<strong>(ii)</strong> at least 30 days prior to each anniversary of the Effective Date for the Subscription Fees payable in respect of the Year following such anniversary; and<br />
<strong>(b)</strong> subject to clause 17.1, at least 30 days prior to each anniversary of the Effective Date for the Subscription Fees payable in respect of the next Renewal Period,<br />
and the Customer shall pay by PayPal, BACS, credit card, standing order or electronic bank transfer upon the Effective Date.<br />
<strong>10.4</strong>  If My Pain has not received payment on the Effective Date, and without prejudice to any other rights and remedies of My Pain:<br />
<strong>(c)</strong> My Pain may, without liability to the Customer, disable the Authorised Users' passwords, accounts and access to all or part of the Services and My Pain shall be under no obligation to provide any or all of the Services while payment remains unpaid; and<br />
<strong>(d)</strong> interest shall accrue on such due amounts at an annual rate equal to 3% over the then current base lending rate of the Bank of England at the date the relevant payment was due, commencing on the due date and continuing until fully paid, whether before or after judgment.<br />
<strong>10.5</strong> All amounts and fees stated or referred to in this agreement:<br />
<strong>(e)</strong> shall be payable in pounds sterling;<br />
<strong>(f)</strong> are non-cancellable and non-refundable;<br />
<strong>(g)</strong> are inclusive of VAT, which shall be added on payment at the appropriate rate and shall also be paid by the Customer;<br />
<strong>10.4</strong> My Pain shall be entitled to increase the Subscription Fees (i.e. the fee per Year per User Subscription at the start of each Renewal Period upon 90 days' prior notice to the Customer and Schedule 1 shall be deemed to have been amended accordingly.<br /><br />
<strong>11. Proprietary rights</strong><br />
<strong>11.1</strong> The Customer acknowledges and agrees that My Pain and/or its licensors own all Intellectual Property Rights in the Software, the Services and the Documentation. Except as expressly stated herein, this agreement does not grant the Customer any rights to, or in, patents, copyrights, database rights, trade secrets, trade names, trade marks (whether registered or unregistered), or any other rights or licences in respect of the Software, the Services or the Documentation.<br />
<strong>11.2</strong> My Pain warrants that it has all the rights in relation to the Software, the Services and the Documentation that are necessary to grant all the rights it purports to grant under, and in accordance with, the terms of this agreement.<br /><br />
<strong>12. Confidentiality</strong>
<strong>12.1</strong> Each party may be given access to Confidential Information from the other party in order to perform its obligations under this agreement.  A party's Confidential Information shall not be deemed to include information that:<br />
<strong>(a)</strong> is or becomes publicly known other than through any act or omission of the receiving party;<br />
<strong>(b)</strong> was in the other party's lawful possession before the disclosure;<br />
<strong>(c)</strong> is lawfully disclosed to the receiving party by a third party without restriction on disclosure;<br />
<strong>(d)</strong> is independently developed by the receiving party, which independent development can be shown by written evidence; or<br />
<strong>(e)</strong> is required to be disclosed by law, by any court of competent jurisdiction or by any regulatory or administrative body.<br />
<strong>12.2</strong> Each party shall hold the other's Confidential Information in confidence and, unless required by law, not:<br />
<strong>(a)</strong> make the other's Confidential Information available to any third party; or<br />
<strong>(b)</strong> use the other's Confidential Information for any purpose other than the implemen<br />tation of this agreement.
<strong>12.3</strong> Each party shall take all reasonable steps to ensure that the other's Confidential Information to which it has access is not disclosed or distributed by its Representatives in violation of the terms of this agreement.<br />
<strong>12.4</strong> The Customer acknowledges that details of the Services, and the results of any performance tests of the Services, constitute My Pain's Confidential Information.<br />
<strong>12.5</strong> This clause 12 shall survive termination (howsoever arising) of this agreement.<br /><br />
<strong>13. My Pain Indemnity</strong><br />
<strong>13.1</strong> My Pain hereby indemnifies the Customer (and shall keep the Customer indemnified) against all liabilities, costs, expenses, damages and losses (including, without limitation, any direct, indirect or consequential losses, loss of profit, loss of reputation and all interest, penalties and legal costs (calculated on a full indemnity basis) and all other reasonable professional costs and expenses) suffered or incurred by the Customer arising out of, or in connection (in any way whatsoever) with a claim that the Services, the Software and/or the Documentation infringe the Intellectual Property Rights of any third party.<br />
<strong>13.2</strong> This indemnity shall not cover the Customer to the extent that a claim under it results from the Customer and/or any Authorised User's:<br />
<strong>(a)</strong> negligence or wilful misconduct; or<br />
<strong>(a)</strong> use of the Services and/or Documentation in a manner contrary to any instructions given by My Pain.<br />
<strong>13.3</strong> The principles of remoteness shall not apply to any claim under this clause 13.<br />
<strong>13.4 </strong>Nothing in this clause 13 shall restrict or limit the Customer's general obligation at law to mitigate a loss it may suffer or incur as a result of an event that may give rise to a claim under this indemnity.<br />
<strong>13.5</strong> If any third party makes a claim, or notifies an intention to make a claim, against the Customer which may reasonably be considered likely to give rise to a liability under this indemnity, the Customer shall:<br />
<strong>(a)</strong> as soon as reasonably practicable, give written notice of the claim to My Pain, specifying the nature of the claim in reasonable detail;<br />
<strong>(b)</strong> not make any admission of liability, agreement or compromise in relation to the claim without the prior, written consent of My Pain;<br />
<strong>(c)</strong> give My Pain and its professional advisers access at reasonable times (on reasonable prior notice) to its premises and its officers, directors, employees, agents, representatives or advisers, and to any relevant assets, accounts, documents and records within the power or control of the Customer, so as to enable My Pain and its professional advisers to examine them and to take copies (at My Pain's expense) for the purpose of assessing the claim; and<br />
<strong>(d)</strong> subject to My Pain providing security to the Customer to the Customer's reasonable satisfaction against any claim, liability, costs, expenses, damages or losses which may be incurred, take such action as My Pain may reasonably request to avoid, dispute, compromise or defend the claim.<br />
<strong>13.6</strong> If a payment due from My Pain under this clause 13 is subject to tax (whether by way of direct assessment or withholding at its source), the Customer shall be entitled to receive from My Pain such amounts as shall ensure that the net receipt, after tax, to the Customer in respect of the payment is the same as it would have been were the payment not subject to tax.<br />
<strong>13.7</strong> In the defence or settlement of any claim, My Pain may procure the right for the Customer to continue using the Software, the Services and the Documentation (or replace or modify the foregoing so that they become non-infringing) or, if such remedies are not reasonably available, terminate this agreement on two Business Days' notice to the Customer.<br /><br />
<strong>14. Customer Indemnity</strong><br />
<strong>14.1</strong> The Customer hereby indemnifies My Pain (and shall keep My Pain indemnified) against all liabilities, costs, expenses, damages and losses (including, without limitation, any direct, indirect or consequential losses, loss of profit, loss of reputation and all interest, penalties and legal costs (calculated on a full indemnity basis) and all other reasonable professional costs and expenses) suffered or incurred by My Pain arising out of, or in connection (in any way whatsoever) with:<br />
<strong>(a)</strong> a claim that Customer Data infringe the Intellectual Property Rights of any third party; <br />
<strong>(b)</strong> any Authorised User's breach of the T's & C's of Use; and/or<br />
<strong>(c)</strong> a claim/allegation that My Pain has breached any non-UK laws and/or regulations similar to the DPA, provided that the Customer shall not be obliged to indemnify My Pain to the extent that any such claim/allegation arises from circumstances in which My Pain failed to comply with the DPA.<br />
<strong>14.2</strong> This indemnity shall not cover My Pain to the extent that a claim under it results from My Pain's negligence or wilful misconduct.<br />
<strong>14.3</strong> The principles of remoteness shall not apply to any claim under this clause 14.<br />
<strong>14.4</strong> Nothing in this clause 14 shall restrict or limit My Pain's general obligation at law to mitigate a loss it may suffer or incur as a result of an event that may give rise to a claim under this indemnity.<br />
<strong>14.5</strong> If any third party makes a claim, or notifies an intention to make a claim, against My Pain which may reasonably be considered likely to give rise to a liability under this indemnity, My Pain shall:<br />
<strong>(a)</strong> as soon as reasonably practicable, give written notice of the claim to the Customer, specifying the nature of the claim in reasonable detail;<br />
<strong>(b)</strong> not make any admission of liability, agreement or compromise in relation to the claim without the prior, written consent of the Customer;<br />
<strong>(c)</strong> give the Customer and its professional advisers access at reasonable times (on reasonable prior notice) to its premises and its officers, directors, employees, agents, representatives or advisers, and to any relevant assets, accounts, documents and records within the power or control of My Pain, so as to enable the Customer and its professional advisers to examine them and to take copies (at the Customer's expense) for the purpose of assessing the claim; and<br />
<strong>(d)</strong> subject to the Customer providing security to My Pain to My Pain's reasonable satisfaction against any claim, liability, costs, expenses, damages or losses which may be incurred, take such action as the Customer may reasonably request to avoid, dispute, compromise or defend the claim.<br />
<strong>14.6</strong> If a payment due from the Customer under this clause 14 is subject to tax (whether by way of direct assessment or withholding at its source), My Pain shall be entitled to receive from the Customer such amounts as shall ensure that the net receipt, after tax, to My Pain in respect of the payment is the same as it would have been were the payment not subject to tax.<br /><br />
<strong>15. Limitation of liability</strong><br />
<strong>15.1</strong> This clause 15 sets out the entire financial liability of each party (including any liability for the acts or omissions of their employees, agents and sub-contractors) to the other:<br />
<strong>(a)</strong> arising under, or in connection with, this agreement; and<br />
<strong>(b</strong>) in respect of:<br />
<strong>(i)</strong> the Services and Documentation or any part of them; and<br />
<strong>(ii)</strong> any representation, statement or tortious act or omission (including negligence) arising under or in connection with this agreement.<br />
<strong>15.2</strong> Nothing in this agreement limits or excludes the liability of either party:<br />
<strong>(a)</strong> for death or personal injury caused by negligence; or<br />
<strong>(b)</strong> for fraud or fraudulent misrepresentation; or<br />
<strong>(c)</strong> for any other liability which may not be limited or excluded by applicable law.<br />
<strong>15.3</strong> Subject to clause 15.2:<br />
<strong>(a)</strong> except as expressly and specifically provided in this agreement:<br />
<strong>(i)</strong> the Customer assumes sole responsibility for results obtained from the use of the Services and the Documentation by the Authorised Users, and for conclusions drawn from such use.  My Pain shall have no liability for:<br />
<strong>(A)</strong> any damage caused by errors or omissions in any information or  instructions provided to My Pain by the Customer and/or any Authorised Users in connection with the Services; or
<strong>(B)</strong> any actions taken by My Pain at the Customer's direction; and<br />
<strong>(ii)</strong> all warranties, representations, conditions and all other terms of any kind whatsoever implied by statute or common law are, to the fullest extent permitted by applicable law, excluded from this agreement;<br />
<strong>(b)</strong> clause 13 states the Customer's sole and exclusive rights and remedies, and My Pain's (including My Pain's Representatives') entire obligations and liability, for infringement of any Intellectual Property Rights;<br />
<strong>(c)</strong> clause 14 states My Pain's sole and exclusive rights and remedies, and the Customer's (including the Customer's Representatives') entire obligations and liability, for infringement of any Intellectual Property Rights;<br />
<strong>(d)</strong> My Pain shall have no liability to the Customer should the Customer Process the AU Personal Data in breach of the DPA and/or any equivalent and/or similar local laws and/or regulations elsewhere in the world;<br />
<strong>(e)</strong> in the event of termination pursuant to clause 13.7, My Pain shall refund (on a pro-rata basis) paid Subscription Fees but shall have no additional liability or obligation to pay liquidated damages or other additional costs to the Customer; and<br />
<strong>(f)</strong> each party's total aggregate liability in contract, tort (including negligence or breach of statutory duty), misrepresentation, restitution or otherwise, arising in connection with the performance or contemplated performance of this agreement shall be limited to &pound;[AMOUNT].<br /><br />
<strong>16. Insurance</strong><br />
On signature of this agreement, each party shall provide to the other written confirmation from its insurers that it has in force an insurance policy sufficient to provide cover as required by law or in respect of any foreseeable liability which may arise in connection with this agreement of not less than &pound;[AMOUNT] per claim or series of related claims per year.  Each party shall, at its own expense, maintain such policy (or an equivalent policy) in force for the term of this agreement and for seven years thereafter and shall provide a certificate of insurance from its insurers of such policy to the other as reasonably requested from time to time.
<strong>17. Term and termination</strong><br />
<strong>17.1</strong> This agreement shall, unless otherwise terminated as provided in clause 13.7 or this clause 17, commence on the Commencement Date and shall continue to the end of the Initial Subscription Term and, thereafter, this agreement shall be automatically renewed for successive periods of one Year (each a Renewal Period), unless:<br />
<strong>(a)</strong> either party notifies the other party of termination, in writing, at least 60 days before the end of the Initial Subscription Term or any Renewal Period, in which case this agreement shall terminate upon the expiry of (as applicable) the applicable Initial Subscription Term or Renewal Period; or<br />
<strong>(b)</strong> otherwise terminated in accordance with the provisions of this agreement,<br />
and the Initial Subscription Term together with any subsequent Renewal Periods shall constitute the Subscription Term.<br />
<strong>17.2</strong> Without prejudice to any other rights or remedies to which the parties may be entitled, either party may terminate this agreement without liability to the other if: <br />
<strong>(a)</strong> the other party:<br />
<strong>(i)</strong> fails to pay any amount due under this agreement on the due date for payment and remains in default not less than 30 days after being notified in writing to make such payment; or<br />
<strong>(ii)</strong> commits a material breach of any of the terms of this agreement and (if such a breach is remediable) fails to remedy that breach within 30 days of that party being notified in writing of the breach; or<br />
<strong>(iii)</strong> suspends, or threatens to suspend, payment of its debts, or is unable to pay its debts as they fall due or admits inability to pay its debts, or (being a company) is deemed unable to pay its debts within
 the meaning of section 123 of the Insolvency Act 1986, or (being an individual) is deemed either unable to pay its debts or as having no reasonable prospect of so doing, in either case, within the meaning of section 268 of the Insolvency Act 1986, or (being a partnership) has any partner to whom any of the foregoing apply; or<br />
 <strong>(iv)</strong> commences negotiations with all or any class of its creditors with a view to rescheduling any of its debts, or makes a proposal for or enters into any compromise or arrangement with its creditors other than (in the case of a company) for the sole purpose of a scheme for a solvent amalgamation of that other party with one or more other companies or the solvent reconstruction of that other party; or<br />
 <strong>(b)</strong> a petition is filed, a notice is given, a resolution is passed, or an order is made, for or in connection with the winding up of that other party (being a company) other than for the sole purpose of a scheme for a solvent amalgamation of that other party with one or more other companies or the solvent reconstruction of that other party; or <br />
 <strong>(c)</strong> an application is made to court, or an order is made, for the appointment of an administrator, or if a notice of intention to appoint an administrator is given or if an administrator is appointed, over the other party (being a company); or<br />
 <strong>(d)</strong> the holder of a qualifying floating charge over the assets of that other party (being a company) has become entitled to appoint or has appointed an administrative receiver; or<br />
 <strong>(e)</strong> a person becomes entitled to appoint a receiver over the assets of the other party or a receiver is appointed over the assets of the other party; or<br />
 <strong>(f)</strong> a creditor or encumbrancer of the other party attaches or takes possession of, or a distress, execution, sequestration or other such process is levied or enforced on or sued against, the whole or any part of the other party's assets and such attachment or process is not discharged within 14 days; or<br />
 <strong>(g)</strong> any event occurs, or proceeding is taken, with respect to the other party in any jurisdiction to which it is subject that has an effect equivalent or similar to any of the events mentioned in clause 1<strong>7.2(a)(iii)</strong> to clause 17.2<strong>(f)</strong> (inclusive); or<br />
 <strong>(h)</strong> the other party suspends or ceases, or threatens to suspend or cease, carrying on all or a substantial part of its business.
 <strong>17.3</strong> On termination of this agreement for any reason:<br />
 <strong>(a)</strong> all licences granted under this agreement shall immediately terminate;<br />
 <strong>(b)</strong> each party shall:<br />
 <strong>(i)</strong> return and make no further use of any equipment, property, documentation and other items (and all copies of them) belonging to the other party;<br />
 <strong>(ii)</strong> destroy or otherwise dispose of any and all:<br />
 <strong>(A)</strong> AU Personal Data in its possession, custody or control;<br />
 <strong>(B)</strong> data belonging to the other (including the My Pain Data and Customer Data) in its possession, custody or control; and<br />
 <strong>(c)</strong> the accrued rights of the parties as at termination, or the continuation after termination of any provision expressly stated to survive or implicitly surviving termination, shall not be affected or prejudiced.<br /><br />
 <strong>18. Set-off</strong>
All amounts due under this agreement shall be paid in full without any deduction or withholding (other than any deduction or withholding of tax as required by law).  Neither shall be entitled to claim set-off or to counterclaim against the other in relation to the payment of the whole or part of any such amount.<br /><br />
<strong>19. Force majeure</strong><br />
My Pain shall have no liability to the Customer under this agreement if it is prevented from or delayed in performing its obligations under this agreement, or from carrying on its business, by acts, events, omissions or accidents beyond its reasonable control, including strikes, lock-outs or other industrial disputes (whether involving the workforce of My Pain or any other party), failure of a utility service (e.g. power) or transport or telecommunications network, act of God, war, riot, civil commotion, malicious damage, compliance with any law or governmental order, rule, regulation or direction, accident, breakdown of plant or machinery, fire, flood, storm or default  (including by reason of such an event) of suppliers or sub-contractors, provided that the Customer is (where such an event is expected to, or actually does, continue for more than five Business Days) notified of such an event and its expected duration.<br /><br />
<strong>20. Waiver</strong><br />
<strong>20.1</strong> A waiver of any right under this agreement is only effective if it is in writing and it applies only to the party to whom the waiver is addressed and to the circumstances for which it is given.<br />
<strong>20.2</strong> Unless specifically provided otherwise, rights arising under this agreement are cumulative and do not exclude rights provided by law.<br /><br />
<strong>21. Severance</strong><br />
<strong>21.1</strong> If any provision (or part of a provision) of this agreement is found by any court or administrative body of competent jurisdiction to be invalid, unenforceable or illegal, the other provisions shall remain in force.<br /><br />

<strong>22.</strong> If any invalid, unenforceable or illegal provision of this agreement would be valid, enforceable and legal if some part of it were deleted, the provision shall apply with the minimum modification necessary to make it legal, valid and enforceable.<br /><br />
<strong>23. Entire agreement</strong><br />
<strong>23.1</strong> This agreement, and any documents referred to in it, constitute the whole agreement between the parties and supersede any previous arrangement, understanding or agreement between them relating to the subject matter they cover.<br />
<strong>23.2</strong> Each of the parties acknowledges and agrees that in entering into this agreement it does not rely on any undertaking, promise, assurance, statement, representation, warranty or understanding (whether in writing or not) of any person (whether party to this agreement or not) relating to the subject matter of this agreement, other than as expressly set out in this agreement.<br /><br />
<strong>24.</strong> Assignment<br />
<strong>24.1</strong> The Customer shall not, without the prior written consent of My Pain, assign, transfer, charge, sub-contract or deal in any other manner with all or any of its rights or obligations under this agreement.<br />
<strong>24.2</strong> My Pain may at any time assign, transfer, charge, sub-contract or deal in any other manner with all or any of its rights or obligations under this agreement.<br /><br />
<strong>25. No partnership or agency</strong><br />
Nothing in this agreement is intended to or shall operate to create a partnership between the parties, or authorise either party to act as agent for the other, and neither party shall have the authority to act in the name or on behalf of or otherwise to bind the other in any way (including the making of any representation or warranty, the assumption of any obligation or liability and the exercise of any right or power).<br /><br />
<strong>26. Third party rights</strong><br />

This agreement does not confer any rights on any person or party (other than the parties to this agreement and, where applicable, their successors and permitted assigns) pursuant to the Contracts (Rights of Third Parties) Act 1999.<br />
<strong>27. Notices</strong><br />
<strong>27.1</strong> Any notice required to be given under this agreement shall be in writing and shall be delivered:<br />
<strong>(a)</strong> by hand; or<br />
<strong>(b)</strong> sent by recorded delivery post to the other party at its address set out in this agreement (or such other address as may have been notified by that party for such purposes).<br />
<strong>27.2</strong> Any notice or communication shall be deemed to have been received:<br />
<strong>(a)</strong> if delivered by hand, on signature of a delivery receipt or at the time the notice is left at the proper address; or<br />
<strong>(b)</strong> if delivered by post, at the time recorded by the delivery service.<br />
<strong>27.3</strong> This clause 27 does not apply to the service of any proceedings or other documents in any legal action.<br /><br />
<strong>28. Governing law and jurisdiction</strong>
<strong>28.1</strong> This agreement and any disputes or claims arising out of or in connection with it or its subject matter or formation (including non-contractual disputes or claims) are governed by, and construed in accordance with, the laws of England and Wales.<br />
<strong>28.2</strong> The parties irrevocably agree that the courts of England and Wales have exclusive jurisdiction to settle any dispute or claim that arises out of or in connection with this agreement or its subject matter or formation (including non-contractual disputes or claims).<br />
<br />

<strong>This agreement has been entered into on the date stated at the beginning of it.</strong></p>

<table width="797" cellpadding="7" cellspacing="0">
  <col width="506">
  <col width="263">
  <tr valign="top">
    <td width="506"><p" align="justify"> Signed by [NAME OF SIGNATORY]  			(a director) for, and on behalf of, My Pain Limited</p>
      <p" align="justify"> <br/>
      </p>
      <p" align="justify"> <br/>
      </p>
      <p" align="justify"> <br/>
      </p>
      <p" align="justify"> <br/>
      </p>
      <p" align="justify"> <br/>
      </p>
      <p" align="justify"><br/>
      </p></td>
    <td width="263"><p" align="left"> .......................................</p>
      <p" align="left">[NAME  			OF SIGNATORY]</p></td>
  </tr>
  <tr valign="top">
    <td width="506"><p" align="justify">Signed  			by [NAME OF SIGNATORY] (a  			director) for, and on behalf of, [FULL  			NAME OF THE CUSTOMER]</p></td>
    <td width="263"><p" align="left"> .......................................</p>
      <p" align="left">[NAME  			OF SIGNATORY]</p></td>
  </tr>
</table>



<p><strong>Schedule 1  Subscription Fees</strong><br>
  <strong>1. Subscription Fees</strong><br>
The Subscription Fees shall amount to a total of &pound;[TBC] per Year, based on [TBC] User Subscriptions at &pound;[TBC] per Year per User Subscription.<br><br>
<strong>2. Additional User Subscription Fees</strong><br>
Additional User Subscriptions may be purchased by the Customer in accordance with clause 3 at &pound;[TBC] per Year per User Subscription.  <br>
<strong>Schedule 2  T's & C's of Use</strong><br>
[Then current T's & C's to be annexed.]<br>
<strong>Schedule 3  Privacy Policy</strong><br>
[Then current privacy policy to be annexed.]<br>
<strong>Schedule 4  Support Services Policy</strong><br>
<strong>1.</strong> Any errors/faults in the Software must be notified to [EMAIL ADDRESS].<br>
<strong>2.</strong> My Pain will respond to any such notifications within [NUMBER] Business Hours to acknowledge receipt.<br>
<strong>3.</strong> My Pain shall notify the Customer within [NUMBER] Business Hours to inform it of any errors/faults that it detects in the Software.<br>
<strong>4.</strong> My Pain shall use its reasonable endeavours to rectify any error/fault of which it is notified (or which it detects) in accordance with the timescales below.  If a workaround is developed, My Pain shall apply such workaround and, if applicable, the severity shall be downgraded (for the purposes of timescales, the issue shall then be treated as though it has always been of that severity).<br>
<strong>(i) Severity 1:</strong>	[NUMBER] Business Hours of the error/fault being notified to My Pain or My Pain having detected the error/fault.<br>
<strong>(ii) Severity 2:</strong>	[NUMBER] Business Hours of the error/fault being notified to My Pain or My Pain having detected the error/fault.<br>
<strong>(iii) Severity 3:</strong>	[NUMBER] Business Hours of the error/fault being notified to My Pain or My Pain having detected the error/fault.<br>
<strong>(iv) Severity 4:</strong>	[NUMBER] Business Hours of the error/fault being notified to My Pain or My Pain having detected the error/fault.<br>
<strong>5.</strong> If My Pain is unable to fix the relevant error/fault within the times set out above, My Pain's relationship manager for the Customer shall promptly contact the Customer and the parties shall decide what action to take.  In the meantime, My Pain shall continue to attempt to fix the relevant error/fault.  </p>

<p>
 
<p> <br/>
  <br/>
</p>
<table width="681" cellpadding="7" cellspacing="0">
  <col width="51">
  <col width="600">
  <tr valign="top">
    <td width="51"><h2" align="center"> <strong>Severity</strong></h2></td>
    <td width="600"><h2" align="center"> <strong>Description</strong></h2></td>
  </tr>
  <tr valign="top">
    <td width="51"><h2" align="center"> 1</h2></td>
    <td width="600"><h2" align="left"> <strong>Critical.</strong> The Software is &lsquo;down&rsquo; or inoperable or  			there is a severe impact on essential functions/operations.  No  			reasonable workaround is available.</h2></td>
  </tr>
  <tr valign="top">
    <td width="51"><h2" align="center"> 2</h2></td>
    <td width="600"><h2" align="left"> <strong>High.</strong> Key functions/operations are severely impacted.  The  			Software is operable but its use is restricted. Either there is no  			workaround available, or the workaround is cumbersome to use.</h2></td>
  </tr>
  <tr valign="top">
    <td width="51"><h2" align="center"> 3</h2></td>
    <td width="600"><h2" align="left"> <strong>Medium.</strong> There is: (i) a severe impact on essential  			functions/operations or key functions/operations are severely  			impacted but the impact can be reasonably mitigated by a  			reasonable workaround; or (ii) an important but non-essential/key  			function/operation is impacted.</h2></td>
  </tr>
  <tr valign="top">
    <td width="51"><h2" align="center"> 4</h2></td>
    <td width="600"><h2" align="left"> <strong>Low.</strong> Non-critical issues (e.g. cosmetic issues, such as  			misspellings and grammatical mistakes).</h2></td>
  </tr>
</table>
			
			
		</div>
	</div>
</div>