<div class="pageheader">
	<h2><i class="fa fa-hospital-o"></i> About Us</h2>
	<?php if(!loggedUserData()){?>
	<div class="breadcrumb-wrapper">
		<a href="<?php echo URL;?>">&laquo; back to login</a>
	</div>
	<?php }?>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
		<div class="panel-body" style="min-height:800px">
			
			<strong>INFORMATION ABOUT US</strong><br /><br />


			<a href="https://mypainimpact.com" target="_blank">mypainimpact.com</a> is a site operated by My Pain Limited.  We are registered in England and Wales under 
			company number 08691506 and have our registered office at 
			49 Lullingstone Lane, Hither Green, London, SE13 6UH.
		</div>
	</div>
</div>
