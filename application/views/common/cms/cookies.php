<div class="pageheader">
	<h2><i class="fa fa-book"></i> Cookies Policy</h2>
	<?php if(!loggedUserData()){?>
	<div class="breadcrumb-wrapper">
		<a href="<?php echo URL;?>">&laquo; back to login</a>
	</div>
	<?php }?>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
		<div class="panel-body cms" style="min-height:800px">
			<h4>COOKIES</h4>
            <div>
                  <ul>
                    <li>
                     <p> 1 We may also obtain information about your general Internet usage by using a cookie file 
                         which is stored on your browser or the hard drive of your computer.  Cookies contain information that is transferred to your computer's hard drive.  
                        They help us to improve the site and to deliver a better and more personalised service.  
                        Some of the cookies we use are essential for the site to operate. </p>
                    </li>
                    
                    <li>
                     <p> 2 You may block cookies by activating the setting on your browser which allows you to refuse the setting of all or some cookies.
                        However, if you use your browser settings to block all cookies (including essential cookies) 
                        you may not be to access all or parts of the site.  Unless you have adjusted your browser setting so that it will refuse cookies,
                         the system will issue cookies as soon you visit the site. </p>
                    </li>
                    
                    <li>
                      <p>3 Details of the cookies used on the site are set out below.</p>
                       <table border="1px solid" >
                          <tr>
                            <th>Cookie</th> <th>Name</th>  <th>Purpose</th>
                          </tr>
                          <tr>
                             <td>PHPSESSID</td>
                             <td>PHPSESSID</td>
                             <td>To identify a session, a series of related message exchanges, in order that we can identify users visiting our site.</td>
                          </tr>
                          <tr>
                            <td>ci_session</td>
                            <td>ci_session</td>
                            <td>To allow us to store session information in our database and track user activity while using our site.</td>
                          </tr>
                          <tr>
                            <td>adminer_version</td>
                            <td>adminer_version</td>
                            <td>To allow us to access the database in a graphical interface displaying information on user activity.</td>
                          </tr>
                       </table>
                      
                    </li>
                    
                    <li>
                    <p>4 Except for essential cookies, all cookies will expire at the end of session.</p>
                    </li>
                  
            </div>
            
			
		</div>
	</div>
</div>
