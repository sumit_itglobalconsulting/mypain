<div class="pageheader">
	<h2><i class="fa fa-pencil-square-o"></i> Terms of Use</h2>
	<?php if(!loggedUserData()){?>
	<div class="breadcrumb-wrapper">
		<a href="<?php echo URL;?>">&laquo; back to login</a>
	</div>
	<?php }?>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
		<div class="panel-body cms" style="min-height:800px">
			<p>This page (together with the documents referred to on it) tells you the terms of use on which you may make use of our website
        <a href="https://mypainimpact.com" target="_blank">mypainimpact.com</a> and the services and content available on/from it.</p>
            <p>&nbsp;</p>
            
      <p>Please read these terms of use carefully.  By selecting the '[I Accept]' button, you indicate that you accept these terms of use and that you agree to abide by them. 
          If you do not agree to these terms of use, you may not use our site.
      </p>
            
            <h4>1. INFORMATION ABOUT US </h4>
            <div>
            		<p> 1.1 mypainimpact.com is a site operated by My Pain Limited (we).  We are registered in England and Wales under company number 
                          08691506 and have our registered office at 49 Lullingstone Lane, Hither Green, London, SE13 6UH. </p>
            </div>
           
            <h4>2. ACCESSING OUR SITE</h4>
            <div>
                  <ul>
                    <li>
                     <p> 2.1 Access to our site is permitted on a temporary basis and we reserve the right to withdraw or amend the services we provide on our site without notice to you. 
                       We will not be liable to you if for any reason our site is unavailable at any time or for any period. </p>
                    </li>
                    
                    <li>
                     <p>2.2 You have been provided with a user ID and password.  You must treat this information as confidential and you must not disclose it to any third party.  We have the right to disable any user ID and password at any time, 
                     if (in our sole and absolute opinion) you have failed to comply with any of the provisions of these terms of use. </p>
                    </li>
                    
                    <li>
                      <p>2.3 You warrant that the information (e.g. your name) provided to us during the sign-up process is true and accurate.</p>
                    </li>
                    
                    <li>
                    <p>2.4 When using our site, you must comply with the provisions of our <a href="<?php echo URL."page/usepolicy";?>">acceptable use policy.</a></p>
                    </li>
                    
                    <li>
                    <p>2.5 Access to our site is only possible from certain computers within your [healthcare/medical centre] network.
                     Your [healthcare/medical centre] will provide you with exact details about which computers these are.</p>
                    </li>
                  </ul>
            </div>
            
            <h4>3. INTELLECTUAL PROPERTY RIGHTS</h4>
            <div>
               <ul>
                 <li>
                  <p>3.1 We are the owner or the licensee of all intellectual property rights in our site, and in the material published on it. 
                   Those works are protected by copyright laws and treaties around the world.  All such rights are reserved. </p>
                 </li>
                 
                 <li>
                  <p>3.2 You:</p>
                  <ol type="a">
                     <li>
                        <p>may print off and/or download (for your own personal use) materials from our site where 
                        the facility for doing so have been made available (for example, podcasts and vodcasts);</p>
                     </li>
                     <li>
                        <p> must not:</p>
                         <ol type="i">
                            <li>
                               <p>(in any way) modify the paper or digital copies of any materials you have printed off or downloaded;</p>
                            </li>
                            <li>
                             <p>use any part of the materials on our site (or which you have downloaded and/or printed off)
                              for commercial purposes without obtaining a licence to do so from us or our licensors.</p>
                            </li>
                         </ol>
                     </li>
                  </ol>
                 </li>
                 
                 <li>
                 3.3. Contributions must:
                   <ol type="a">
                              	<li>
                                  <p>be accurate (where they state facts); </p>
                                </li>
                                <li>
                                  <p>be genuinely held (where they state opinions); and</p>
                                </li>
                                <li>
                                 <p>comply with applicable law in any country/territory from which they are posted.</p>
                                </li>
                    </ol>
                 </li>
                 
                 <li>
                 3.4. Contributions must not:
                   <ol type="a">
                       <li>
                          <p>contain any material which is:</p>
                          
                          <ol type="i">
                            <li>
                             <p>defamatory of any person; or</p>
                            </li>
                            <li>
                              <p>obscene, offensive, hateful or inflammatory;</p>
                            </li>
                          </ol>
                       </li>
                       
                       <li>
                        <p>promote:</p>
                         
                          <ol type="i">
                            <li>
                               <p>sexually explicit material;</p>
                            </li>
                            <li>
                              <p>violence;</p>
                            </li>
                            <li>
                             <p>discrimination based on race, sex, religion, nationality, disability, sexual orientation or age;</p>
                            </li>
                            <li>
                              <p>any other illegal activity; or</p>
                            </li>
                            
                         </ol> 
                       </li>
                       
                    
                       
                       <li>
                         <p>infringe any intellectual property rights (including, without limitation, copyright, database rights and trade marks) 
                            of any other person;</p>
                       </li>
                       
                       <li>
                         <p>be:</p>
                         
                         <ol type="i">
                           <li>
                               <p>made in breach of any legal duty owed to a third party, such as a contractual duty or a duty of confidence; </p>
                           </li>
                           
                           <li>
                               <p>threatening, abuse or invade another's privacy, or cause annoyance, inconvenience or needless anxiety;</p>
                           </li>
                           
                           <li>
                               <p>likely to:</p>
                                
                              <ol type="A">
                                <li>
                                  <p>deceive any person; or</p>
                                </li>
                                <li>
                                  <p>harass, upset, embarrass, alarm or annoy any other person; or</p>
                                </li>
                                
                             </ol> 
                           </li>
                           
                            <li>
                               <p>(iv) used to impersonate any person, or to misrepresent your identity or affiliation with any person;</p>
                            </li>
                              
                        </ol>
                       </li>
                      
                      <li>
                        <p>give the impression that they emanate from us, if this is not the case; or</p>
                      </li>
                      
                      <li>
                       <p>advocate, promote or assist any unlawful act such as (by way of example only) copyright infringement or computer misuse.</p>
                      </li> 
                      
                   </ol>
                 </li>
               </ul>
            </div>
          
         <h4>4. RELIANCE ON INFORMATION POSTED</h4>  
         <div>
           <p>Our site does not give medical advice in relation to any individual case or patient. 
           Commentary and other materials posted on our site are not intended to amount to advice on which reliance should be placed. 
            We therefore disclaim all liability and responsibility arising from any reliance placed on such materials by you or
             by anyone who may be informed of any of its contents. You should contact a health professional if you are at all concerned about your health.</p>  
         <p>&nbsp;</p>
         
         <p> If you are a medical or health professional then you are encouraged to use our site for general information purposes only. 
         We therefore disclaim all liability and responsibility arising from any reliance placed on such materials by you or by anyone who may be informed of any of its contents.</p>
         
         </div>
         
      <h4>5. OUR SITE CHANGES REGULARLY</h4>
        
        <p>We aim to update our site regularly and may change the content at any time.  
           If the need arises, we may suspend access to our site or close it indefinitely.  
           Any of the material on our site may be out-of-date at any given time and we are under no obligation to you to update such material.</p> 
        
      <h4>6. OUR LIABILITY</h4>  
      <div>
         <ul>
           <li>
              <p>6.1 The material displayed/available on our site is provided without any guarantees, conditions or warranties as to its accuracy.  
             To the extent permitted by applicable law, we hereby expressly exclude:</p>
           
              <ol type="a">
                 <li>
                   <p>all conditions, warranties and other terms which might otherwise be implied by statute, common law or the law of equity;</p>
                 </li>
                 <li>
                   <p>any liability for any direct, indirect or consequential loss or damage incurred by you in connection with 
                    our site and any materials available/posted on it or in connection with the use, inability to use, or results of the use of our site, 
                    any websites linked to it and any materials available/posted on it, including (without limitation) any liability for:</p>
                    <ol type="i">
                      <li>
                        <p>loss of data;</p>
                      </li>
                      <li>
                        <p> wasted time; and </p>
                      </li>
                      <li>
                        <p>any other loss or damage of any kind, however arising and whether caused by tort (including negligence), 
                        breach of contract or otherwise, even if foreseeable, provided that this condition shall not prevent claims for loss of 
                        or damage to your tangible property or any other claims for direct financial loss that are not excluded by any of the categories
                         set out above.</p>
                      </li>
                    </ol>
                 </li>
              </ol>
           </li>
           
           <li>
             <p>6.2 We will not be liable for any loss or damage caused by a distributed denial-of-service attack, 
                viruses or other technologically harmful material that may infect your computer equipment, computer programs, data or 
                other proprietary material due to your use of our site or to your downloading of any material posted 
                on it (or on any website linked to it).</p>
           </li>
           
           <li>
             <p>6.3 The provisions above do not affect our liability for:</p>
             <ol type="a">
               <li>
                 <p>death or personal injury arising from our negligence;</p>
               </li>
               <li>
                <p>our liability for fraud or fraudulent misrepresentation;</p>
               </li>
               <li>
                <p>any other liability which cannot be excluded or limited under applicable law.</p>
               </li>
             </ol>
           </li>
         </ul>
      </div> 
      
      <h4>7. INFORMATION ABOUT YOU AND YOUR VISITS TO OUR SITE</h4>
      
      <p>
       We process information about you in accordance with our <a href="<?php echo URL."page/usepolicy";?>">privacy policy</a>. 
       By using our site and selecting the '[I Accept]' button, you consent to such processing and you warrant that any and all data provided by you is accurate.
      </p>
      
      <h4>8. TRANSACTIONS CONCLUDED VIA OUR SITE & LINKS FROM OUR SITE</h4>
      
      <div>
         <ul>
           <li><p>8.1 Through our site, you may access the website content of, correspond with, and purchase products and services from, 
              third parties via third-party websites.  You acknowledge and agree that you do so solely at your own risk.</p> </li>
           
           <li><p>
           8.2 We makes no representation or commitment and shall have no liability or obligation whatsoever in relation to the content or use of,
            or correspondence with, any such third-party website, or any transactions completed, and any contract entered into by you with any such third party. 
            Any contract entered into and any transaction completed via any third-party website is between you and the relevant third party and not us.
           </p></li> 
           
          <li> <p>8.3 We do not endorse or approve any third-party website (nor the content of any of the third-party website) made available via our site.</p> </li>
          </ul>
      </div>
      
      <h4>9. UPLOADING MATERIAL TO OUR SITE</h4>
      <div>
           <ul>
              <li>
                <p>9.1 Whenever you make use of a feature that allows you to upload material to our site, 
                 or to make contact with other users of our site, you must comply with the content standards set out in our <a href="<?php echo URL."page/usepolicy";?>">acceptable use policy</a>. 
                 You warrant that any such contribution does comply with those standards.</p>
              </li>
              
              <li>
                <p>
                9.2 We have the right to remove any material or posting you make on our site if (in our sole and absolute opinion) 
                such material does not comply with the content standards set out in our <a href="<?php echo URL."page/usepolicy";?>">acceptable use policy</a>.
                </p>
              </li>
              
              <li>
               <p>
                9.3 Any material you upload to our site will be considered non-confidential.  We also have the right to disclose your 
                  identity to any third party who is claiming that any material posted or uploaded by
                 you to our site constitutes a violation of their intellectual property rights or of their right to privacy.
               </p>
              </li>
              
              <li>
               <p>
                9.4 We will not be responsible to you for the content or accuracy of any materials posted by you or any other user of our site.
               </p>
              </li>
           </ul>
      </div>
      
      <h4>10. VIRUSES, HACKING AND OTHER OFFENCES</h4>
      <div>
         <ul>
           <li>
            <p> 10.1 You must not misuse our site by knowingly introducing viruses, trojans, worms, logic bombs or 
             other material which is malicious or technologically harmful.  You must not attempt to gain unauthorised access to our site, 
             the server on which our site is stored or any server, computer or database connected to our site.  
             You must not attack our site via a denial-of-service attack or a distributed denial-of service attack.</p>
           </li>
            <li>
           <p>10.2 By breaching the provision above, you would commit a criminal offence under the Computer Misuse Act 1990. 
            We will report any such breach to the relevant law enforcement authorities and 
            we will co-operate with those authorities by disclosing your identity to them. 
            In the event of such a breach, your right to use our site will cease immediately.</p>
          
           </li>
         </ul>
      </div>
     
     <h4>11. LINKING TO OUR SITE</h4>
     <p>
        You may link to our home page <a href="https://mypainimpact.com" target="_blank">mypainimpact.com</a>  only, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it. 
       However, you must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.
     </p>
     
     <h4>12. JURISDICTION AND APPLICABLE LAW</h4>
      <div>
         <ul>
           <li>
             <p>12.1 The English courts will have exclusive jurisdiction over any claim arising from, or related to, a visit to our site 
               (although we retain the right to bring proceedings against you for breach of these terms in your place of residence
                or any other relevant territory).</p>
           </li>
           
           <li>
             <p>12.2 These terms of use and any dispute or claim arising out of or in connection with them or their subject matter or formation 
             (including non-contractual disputes or claims) shall be governed by and construed in accordance with the law of England and Wales.</p>
           </li>
         </ul>
      </div>       
     
     <h4>13. TRADE MARKS</h4>
       <p>"My Pain - Choices that work", "My Pain Compass" are registered UK trade marks of My Pain Limited.
        "My Pain IMPACT" and "My Pain CLINIC" are pending UK trade marks of My Pain Limited.</p>
        
     <h4>14. VARIATIONS</h4>
       <p>We may revise these terms of use at any time.  Should we do so, we will require you to accept the revised terms before you may use our site again.</p>
		
     <h4>15. YOUR CONCERNS</h4>
      <p>If you have any concerns about material, which appears, on our site, please contact enquiries@my-pain.co.uk.</p>
    </div>
	</div>
</div>