<?php if($list){?>
	<?php 
		foreach($list as $dtl){
			if(USER_TYPE=='P'){
				$caller="MP".$dtl['doctorId'];
				$callee="MP".$dtl['patientId'];
			}
			else{
				$caller="MP".$dtl['patientId'];
				$callee="MP".$dtl['doctorId'];
			}
			
			$url= URL."common/weemoScreen/".encode($dtl['doctorId'])."/".encode($dtl['patientId'])."/0/".$caller."/".$callee;
	?>
		<div class="notiList">
        	<?php if($dtl['status']=='C'){?>
            	Video Chat request cancelled by <?php echo $dtl['toUserName'];?>
            <?php }else if($dtl['status']=='A'){?>
                    <a href="<?php echo URL."common/cancelChat/".$dtl['id'];?>" class="pull-right text-danger cancelChat">Cancel</a>
                    <a href="<?php echo $url;?>" target="_blank">Video Chat request from <?php echo $dtl['userName'];?></a>
            <?php }?>
		</div>
	<?php }?>
	
	<script>
		$(".notiBx").show();
	</script>
<?php }else{?>
	<script>
		$(".notiBx").hide();
	</script>
<?php }?>

<script>
$(".cancelChat").click(function(e){
	e.preventDefault();
	href=$(this).attr('href');
	ajax(href);
});
</script>