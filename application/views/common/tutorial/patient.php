<div class="pageheader">
	<h2><i class="fa fa-book"></i> User Guide</h2>
</div>

<div class="contentpanel">
    <div class="row">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#t1" data-toggle="tab"><strong>Login to My-Pain Impact</strong></a></li>
            <li><a href="#t2" data-toggle="tab"><strong>Message Centre</strong></a></li>
            <li><a href="#t3" data-toggle="tab"><strong>My Pain Forms</strong></a></li>
            <li><a href="#t4" data-toggle="tab"><strong>Report</strong></a></li>
            <li><a href="#t5" data-toggle="tab"><strong>Settings</strong></a></li>
        </ul>
        
        <div class="tab-content mb30">
        	<div class="tab-pane active" id="t1">
                <?php echo embedPdf(URL."assets/tutorials/patient/Login-to-My-Pain-Impact.pdf");?>
            </div>
            
            <div class="tab-pane" id="t2">
            	<?php echo embedPdf(URL."assets/tutorials/patient/Message-centre.pdf");?>
            </div>
            
            <div class="tab-pane" id="t3">
            	<?php echo embedPdf(URL."assets/tutorials/patient/My-pain-forms.pdf");?>
            </div>
            
            <div class="tab-pane" id="t4">
            	<?php echo embedPdf(URL."assets/tutorials/patient/Report.pdf");?>
            </div>
            
            <div class="tab-pane" id="t5">
            	<?php echo embedPdf(URL."assets/tutorials/patient/Settings.pdf");?>
            </div>
        </div>
    </div>
</div>