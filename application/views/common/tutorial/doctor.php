<div class="pageheader">
	<h2><i class="fa fa-book"></i> User Guide</h2>
</div>

<div class="contentpanel">
    <div class="row">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#t1" data-toggle="tab"><strong>Dashboard</strong></a></li>
            <li><a href="#t2" data-toggle="tab"><strong>Patient Management</strong></a></li>
            <li><a href="#t3" data-toggle="tab"><strong>Message</strong></a></li>
            <li><a href="#t4" data-toggle="tab"><strong>My Pain Forms</strong></a></li>
            <li><a href="#t5" data-toggle="tab"><strong>Contact</strong></a></li>
             <li><a href="#t6" data-toggle="tab"><strong>Setting</strong></a></li>
        </ul>
        
        <div class="tab-content mb30">
        	<div class="tab-pane active" id="t1">
                <?php echo embedPdf(URL."assets/tutorials/doctor/c-dash.pdf");?>
                </div>
                <div class="tab-pane" id="t2">
                <?php echo embedPdf(URL."assets/tutorials/doctor/c-pat-mng.pdf");?>
                </div>
            <div class="tab-pane" id="t3">
                <?php echo embedPdf(URL."assets/tutorials/doctor/c-message.pdf");?>
                </div>
            <div class="tab-pane" id="t4">
                <?php echo embedPdf(URL."assets/tutorials/doctor/c-forms.pdf");?>
                </div>
            <div class="tab-pane" id="t5">
                <?php echo embedPdf(URL."assets/tutorials/doctor/c-contact.pdf");?>
                </div>
            <div class="tab-pane" id="t6">
                <?php echo embedPdf(URL."assets/tutorials/doctor/c-setting.pdf");?>
                </div>
        </div>
    </div>
</div>