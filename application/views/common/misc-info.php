<div class="pageheader">
	<h2><i class="fa fa-file-text"></i> <?php echo ucfirst($infoName)=='Faq'?"FAQ's":ucfirst($infoName);?></h2>
</div>

<div class="contentpanel">
    <div class="panel panel-default panel-blog">
        <div class="panel-body">
            <?php echo $dtl[$infoName];?>
        </div>
    </div>
</div>