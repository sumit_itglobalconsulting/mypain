<div class="painPopup">
	<div class="down">
    	<div id="painPopupH" class="painPopupH posRelative">
        	<div style="position:absolute; right:5px; top:5px">
                <a href="javascript:void(0)" id="cancelBtn" class="deleteBtn" title="Close">&nbsp;</a>
            </div>
        	Set your pain meter
        </div>
        
        <div class="painPopupPad">
            <div class="posRelative">
                <div class="faceBox">
                    <img src="<?php echo URL;?>assets/img/face1.jpg" id="facePic" alt="" />
                </div>
                
                <div style="padding-left:130px">
                    <div style="margin:15px 0px 35px 0px" align="center" class="bold">Pain Severity</div>
                    
                    <div class="posRelative">
                        <div class="slideCap2">
                            <div class="posRelative">
                                <div class="cap1">No Pain</div>
                                <div class="cap2">Mild</div>
                                <div class="cap3">Moderate</div>
                                <div class="cap4">Severe</div>
                            </div>
                        </div>
                        
                        <div class="slideCap1">
                            <div class="slideCapBx">
                                <ul>
                                    <?php for($i=0; $i<=10; $i++){?>
                                        <li <?php echo $i==9?'class="last2"':'';?>>
                                            <div style=" <?php echo $i==10?'text-indent:8px':'';?> ">|</div>
                                            <div><?php echo $i;?></div>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
                        </div>
                        
                        <div id="painRate" class="slideLine">
                            <div class="painProgLine">
                                <div class="ppBox">
                                    <div class="ppL1">&nbsp;</div>
                                    <div class="ppL2">&nbsp;</div>
                                    <div class="ppL3">&nbsp;</div>
                                    <div class="ppL4">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div style="margin:75px 0px 15px 0px" align="center" class="bold">Effected Area Size</div>
                    <div class="posRelative">
                    	<div class="posAbs" style="left:2px; top:5px">_</div>
                        <div class="posAbs" style="right:10px; top:10px">+</div>
                       
                        <div class="slideCap1">
                            <div class="slideCapBx posRel">
                                <!--<ul>
                                    <?php for($i=0; $i<=10; $i++){?>
                                        <li <?php echo $i==9?'class="last2"':'';?>>
                                            <div style=" <?php echo $i==10?'text-indent:8px':'';?> ">|</div>
                                            <div><?php echo $i;?></div>
                                        </li>
                                    <?php }?>
                                </ul>-->
                            </div>
                        </div>
                        
                        <div id="area" class="slideLine">
                            <div class="areaProgLine" style="display:block">
                                <div class="ppBox">
                                    <div class="ppL1">&nbsp;</div>
                                    <div class="ppL2">&nbsp;</div>
                                    <div class="ppL3">&nbsp;</div>
                                    <div class="ppL4">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div align="right" style="margin:55px 10px 0px 0px">
                    <button type="button" id="resetBtn">Reset</button>&nbsp;&nbsp;
                    <button type="button" id="okBtn">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
/** Body Map JS **/
$(document).ready(function(){
	curPainMark=0, originAreaVal=0, originOpacity=0, originPainval=0, originL=0, originT=0, originW=0, originH=0;
	popup=true;
	$(".bodyMap").click(function(e) {
		if(!popup)
			return;
		
		popup=false;	
		obj=$(this);
		pos=obj.offset();
		L=e.pageX-pos.left-8;
		T=e.pageY-pos.top-8;
		style='style="left:'+L+'px; top:'+T+'px"';
		curPainMark=$('<p class="painMark" '+style+'></p>').appendTo(obj);
		
		showPopup(e);
		
		areaSlider();
		painSlider();
		
		curPainMark.click(function(e){
			setPainMarkClick(e, $(this));
		});
		
		setPainMarkDraggable();
	});
	
	$(".painMark").click(function(e) {
		setPainMarkClick(e, $(this));
	});
	
	function setPainMarkClick(e, obj) {
		curPainMark=obj;
		
		if(!popup)
			return;
		
		e.stopPropagation();
		
		
		showPopup(e);
		
		areaVal=curPainMark.attr('areaval');
		painVal=curPainMark.attr('painval');
		
		areaSlider(areaVal);
		painSlider(painVal);
		
		originAreaVal=curPainMark.attr('areaval');
		originOpacity=curPainMark.css('opacity');
		originPainval=curPainMark.attr('painval');
		originL=parseInt(curPainMark.css('left'));
		originT=parseInt(curPainMark.css('top'));
		originW=parseInt(curPainMark.css('width'));
		originH=parseInt(curPainMark.css('height'));
		
		setFacePic(painVal);
		
		$(".areaProgLine").width((areaVal-14)*5.81);
		$(".painProgLine").width((painVal-40)*10.2);
	}
	
	
	function setFacePic(v) {
		v=v || 40;
		if(v>=40 && v<=48)
			pic='face1.jpg';
		else if(v>=49 && v<=57)
			pic='face2.jpg';
		else if(v>=58 && v<=66)
			pic='face3.jpg';
		else if(v>=67 && v<=75)
			pic='face4.jpg';
		else if(v>=76 && v<=85)
			pic='face5.jpg';
		else if(v>=86)
			pic='face6.jpg';
			
		$("#facePic").attr('src', URL+'assets/img/'+pic);
	}
	
	function showPopup(e){
		pw=$(".painPopup").outerWidth();
		leftX=e.pageX-Math.ceil(pw/2);
		topX=e.pageY+50;
		
		windowWidth=$(window).width();
		maxLeft=windowWidth-pw-20;
		leftX=leftX>maxLeft?maxLeft:leftX;
		
		leftX=leftX<0?0:leftX;
		
		$(".painPopup").css({'left':leftX+'px', 'top':topX+'px'}).show();
	}
	
	function hidePopup(){
		$(".painPopup").hide();
		$("#facePic").attr('src', URL+'assets/img/face1.jpg');
		$(".areaProgLine, .painProgLine").width(0);
		popup=true;
	}
	
	$("#cancelBtn").click(function(e){
		e.preventDefault();
		hidePopup();
		
		curPainMark.attr({'areaval':originAreaVal, 'painval':originPainval});
		curPainMark.css({'opacity':originOpacity, 'left':originL+'px', 'top':originT+'px', 'width':originW+'px', 'height':originH+'px'});
		
		if(!curPainMark.attr('ok'))
			curPainMark.remove();
	});
	
	$("#okBtn").click(function(){
		hidePopup();
		curPainMark.attr('ok', '1');
	});
	
	$("#resetBtn").click(function(){
		hidePopup();
		curPainMark.remove();
	});
	
	function areaSlider(sliderVal) {
		sliderVal=sliderVal || 14;
		
		$("#area").slider({
			min: 14,
			max: 100,
			step: 2,
			value:sliderVal,
			start: function(e, ui){
				cL=parseInt(curPainMark.css('left'));
				cT=parseInt(curPainMark.css('top'));
				cW=parseInt(curPainMark.css('width'));
				cH=parseInt(curPainMark.css('height'));
			},
			
			slide: function(e, ui){
				v=ui.value;
				curPainMark.attr('areaval', v);
				L=cL-Math.ceil((v-cW)/2);
				T=cT-Math.ceil((v-cH)/2);
				curPainMark.css({'width':v+'px', 'height':v+'px', 'left':L+'px', 'top':T+'px'});
				
				$(".areaProgLine").width((v-14)*5.81);
			}
		});
	}
	
	function painSlider(sliderVal) {
		sliderVal=sliderVal || 40;
		
		$("#painRate").slider({
			min: 40,
			max: 90,
			step: 1,
			value:sliderVal,
			
			slide: function(e, ui){
				v=ui.value;
				curPainMark.attr('painval', v);
				opacity=v/100;
				curPainMark.css({'opacity':opacity});
				
				$(".painProgLine").width((v-40)*10.2);
				setFacePic(v);
			}
		});
	}


	$(".painPopup").draggable({
		//containment: "parent",
		handle: "#painPopupH",
		drag: function(e, ui){
			ui.position.top -= $(window).scrollTop();
		}
	});
	
	
	function setPainMarkDraggable() {
		$(".painMark").draggable({
			containment: "parent",
			
			drag: function(e, ui){
				popup=false;
			},
			
			stop: function(e, ui){
				setTimeout(function(){popup=true;}, 100);
			}
		});
	}
	
	setPainMarkDraggable();
	
	$(".painPopup").disableSelection();
});
/** Body Map JS End **/
</script>