<?php
	$qs=arrayUrlDecode($_GET);
	if(!$users)
		$users=array();
		
	$users=multiArrToKeyValue($users, 'id', 'fullName');
        
	$toId=USER_ID==$dtl[0]['fromId']?$dtl[0]['toId']:$dtl[0]['fromId'];
        $replyUser[$toId] = $users[$toId];
        
	
	$msgcats=msgCats();
?>
<!-- Send Message Box -->
<?php $this->load->view("common/message/compose_msg", array('users'=>$users));?>
<!-- /Send Message Box -->

<div class="pageheader">
	<h2><i class="fa fa-envelope-o"></i> <?php echo $dtl[0]['subject'];?> <span><?php echo $msgcats[$dtl[0]['catId']];?></span></h2>
    <div class="breadcrumb-wrapper">
    	<a href="<?php echo URL."message/".$urlfunc;?>">&laquo; back</a>
    </div>
</div>

<div class="contentpanel">
	<div class="alert alert-success myhide sentMsgAlert">Message sent successfully.</div>
	<?php echo getFlash();?>
	<div class="mb10 smallForm">
    	<div class="pull-left" style="width:100px">
        </div>
        
        <div class="pull-right">
    		<a href="#" class="composeMsg btn btn-primary">Compose New Message</a>
        </div>
        <div class="clearfix"></div>
    </div>
        <style>
            #accordion h3{color: #333; font-size: 14px; line-height: 20px; font-weight: normal;}
            #accordion h3 span{color: #666; float: right; font-size: 12px; line-break: 34px;}
            #accordion h4{color: #333; font-size: 13px; line-height: 20px; font-weight: normal;}
            #accordion h4 span.name{color:#999;}
            #accordion .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active{border:#E6E1DC 1px solid;}
        </style>
    <div class="panel panel-default">
    <div id="accordion" style="padding: 10px;">    
        <?php foreach($dtl as $msg): ?>
    	
        <?php 
          if($msg['fromId']==USER_ID){
            $check_reply="fa fa-share"; 
          }  
          else {
             $check_reply= "fa fa-reply";
          }
          ?>
        <h3><?php echo $msg['fromName'];?><span><?php echo showDate($msg['created'], true);?></span></h3>
                          
        
        
        <div><h4>to : <span class="name"><?php echo $msg['toName'];?></span><span style="float:right" class="<?php echo $check_reply;?>"></span><br><br><span id="msgContent"><?php echo nl2br($msg['message']);?></span></h4></div>
       
        <?php endforeach; ?>
    </div>   
        <div class="panel-body">
            <div class="text-muted">
                Click here to &nbsp;&nbsp;&nbsp;
                <a href="#" id="replyLink"><i class="fa fa-reply"></i> Reply</a>
                <!--&nbsp;&nbsp;or&nbsp;&nbsp;
                <a href="#" id="forwardLink"><i class="fa fa-forward"></i> Forward</a>-->
            </div>
            
            <div class="MsgBox1">
                <div class="content">
                    <form method="post" action="" id="ReplyForwardForm" onsubmit="return validateMsg()">
                        <input type="hidden" name="sendMsg" value="1" />
                        <input type="hidden" name="parent_id" value="<?php echo $dtl[0]['id']; ?>" />
                        <input type="hidden"  name="replyId"  value="<?php echo $dtl[0]['replyId'];?>" />
                        <input type="hidden" name="fwdId" value="<?php echo $dtl[0]['fwdId'];?>" />
                        <div class="down" style="padding:5px">
                            <div class="mb5">
                                <?php echo form_dropdown('toId', $replyUser, $toId, 'id="toId1" class="form-control chosen-select"');?>
                            </div>
                            <div class="mb5">
                                <?php
                                    echo form_dropdown("catId", $msgcats, "", 'id="msgCatId1" class="form-control cat-select"');
                                ?>
                            </div>
                            <div class="mb5"><input type="text" name="subject" id="subject1" placeholder="Subject" class="form-control"  /></div>
                            <div class="mb5"><textarea name="message" id="message1" class="form-control" spellcheck="true"></textarea></div>
                            <button type="submit" class="btn btn-primary">Send</button>&nbsp;
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$("#replyLink").click(function(e){
	e.preventDefault();
	$(this).parent().hide();
	$(".MsgBox1").show();
	$("#subject1").val('Re: <?php echo str_replace('Re:', '', $dtl[0]['subject']);?>').prop('readonly','readonly');
	$("#toId1").val('<?php echo $toId;?>');
        $("#toId1").attr('readonly',true);
        $("input[name='fwdId']").prop('disabled',true); 
        $("input[name='replyId']").prop('disabled',false);
});

$("#forwardLink").click(function(e){
	e.preventDefault();
	$(this).parent().hide();
	$(".MsgBox1").show();
	$("#subject1").val('<?php echo $dtl[0]['subject'];?>').prop('readonly','readonly');;
	$("#toId1").val('');
	$("#message1").html(br2nl($("#msgContent").html()));
        $("input[name='replyId']").prop('disabled', true);
        $("input[name='fwdId']").prop('disabled',false); 
});

function validateMsg() {
	var error=false;
	toId=$.trim($("#toId1").val());
	subj=$.trim($("#subject1").val());
	msg=$.trim($("#message1").val());
	
	if(!toId || !subj || !msg)
		error=true;
		
	return !error;
}
$(function() {
    $( "#accordion" ).accordion({
        collapsible:true,

    beforeActivate: function(event, ui) {
         // The accordion believes a panel is being opened
        if (ui.newHeader[0]) {
            var currHeader  = ui.newHeader;
            var currContent = currHeader.next('.ui-accordion-content');
         // The accordion believes a panel is being closed
        } else {
            var currHeader  = ui.oldHeader;
            var currContent = currHeader.next('.ui-accordion-content');
        }
         // Since we've changed the default behavior, this detects the actual status
        var isPanelSelected = currHeader.attr('aria-selected') == 'true';

         // Toggle the panel's header
        currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));

        // Toggle the panel's icon
        currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);

         // Toggle the panel's content
        currContent.toggleClass('accordion-content-active',!isPanelSelected)    
        if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

        return false; // Cancel the default action
    }
    });
});
</script>
