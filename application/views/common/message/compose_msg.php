<?php $msgcats=msgCats();?>
<div class="MsgBox">
	<div class="headT">
    	<div class="crossM"></div>
    	New Message
    </div>
    
    <div class="content smallForm">
        <form action="javascript:void(0)" id="MsgForm" onsubmit="return sendMsg()">
            <div class="down" style="padding:5px">
                <div class="mb5">
                    <?php echo form_dropdown('toId', array(''=>'To')+$users, '', 'id="toId" class="form-control chosen-select"');?>
                </div>
                <div class="mb5">
                    <?php
                        echo form_dropdown("catId", array(''=>'Select Category')+$msgcats, "", 'id="msgCatId" class="form-control cat-select"');
                    ?>
                </div>
                <div class="mb5"><input type="text" name="subject" id="subject" placeholder="Subject" class="form-control"  /></div>
                <div class="mb5"><textarea name="message" id="message" class="form-control"></textarea></div>
                <button type="submit" class="btn btn-primary">Send</button>&nbsp;
                <button type="reset" class="btn btn-default">Reset</button>
            </div>
        </form>
    </div>
</div>



<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: false});
	jQuery(".cat-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

function sendMsg() {
	var error=false;
	toId=$.trim($("#toId").val());
	subj=$.trim($("#subject").val());
	catId=$.trim($("#msgCatId").val());
	msg=$.trim($("#message").val());
	
	if(!toId){
		showDialog("Please select a recipient", "Error", true);
		return;
	}
	
	if(!catId){
		showDialog("Please select a message category", "Error", true);
		return;
	}
	
	if(!subj){
		showDialog("Please enter subject", "Error", true);
		return;
	}
	
	if(!msg){
		showDialog("Please enter message", "Error", true);
		return;
	}
	
	
	$.ajax({url:SITE_URL+'message/sendMsg', type:'POST', data:$('#MsgForm').serialize(), success:function(data){ 
		if(data>=1){
			$(".sentMsgAlert").show();
			$(".crossM").click();
		}
	}});
}
</script>