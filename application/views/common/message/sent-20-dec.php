<?php
	$qs=arrayUrlDecode($_GET);
	if(!$users)
		$users=array();
		
	$msgcats=msgCats();
?>
<!-- Send Message Box -->
<?php $this->load->view("common/message/compose_msg", array('users'=>multiArrToKeyValue($users, 'id', 'fullName')));?>
<!-- /Send Message Box -->

<div class="pageheader">
    <div class="row">
        <div class="col-md-8">
            <h2><i class="fa fa-envelope-o"></i> Message <span>Sent</span></h2>
        </div>
        
        <div class="col-md-4" style="padding-top:4px">
            <form method="get" id="searchForm" onsubmit="return submitSearch()">
                <div class="smallForm">
                    <div class="row">
                    	<div class="col-md-9">
                            <input type="text" name="k" value="<?php echo $qs['k'];?>" class="form-control" placeholder="Search keyword" />
                        </div>
                        <div class="col-md-3"><button type="submit"class="btn btn-primary">Search</button></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="contentpanel">
	<div class="alert alert-success myhide sentMsgAlert">Message sent successfully.</div>
	<?php echo getFlash();?>
	<div class="mb10 smallForm">
    	<div class="pull-left" style="width:200px">
        	<div class="tabBtns">
                <a href="<?php echo URL."message/inbox";?>">Inbox</a>
                <a href="<?php echo URL."message/sent";?>" class="act">Sent Messages</a>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="pull-right">
    		<a href="#" class="composeMsg btn btn-primary">Compose New Message</a>
        </div>
        <div class="clearfix"></div>
    </div>
    
    <div class="table-responsive">
    	<?php if($result){?>
            <table class="table table-striped mb30">
            	<thead>
                	<tr>
                        <th width="200px">To</th>
                        <th width="200px">Category</th>
                        <th>Message</th>
                        <th width="180px">Date</th>
                        <th width="20px"></th>
                    </tr>
                </thead>
                
                <tbody>
                	<?php foreach($result as $i=>$dtl){$pl=URL."message/read/".encode($dtl['id'])."/inbox";?>
                    	<tr>
                            <td pl="<?php echo $pl;?>"><?php echo $dtl['toName'];?></td>
                            <td pl="<?php echo $pl;?>"><?php echo $msgcats[$dtl['catId']];?></td>
                            <td class="<?php echo $dtl['isNew']?'bold':'';?>" pl="<?php echo $pl;?>">
                                <?php echo $dtl['subject'];?> - <span class="grey"><?php echo shortString($dtl['message'], 30);?></span>
                            </td>
                            <td pl="<?php echo $pl;?>"><?php echo showDate($dtl['created'], true);?></td>
                            <td align="center">
                                <a href="<?php echo URL."message/delete/".encode($dtl['id'])."/sent";?>?sentDeleted=1" class="deleteBtn delLink">&nbsp;</a>
                            </td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
        <?php }else{?>
        	<div class="notFound">Not Found.</div>
        <?php }?>
    </div>
    
    <?php if($page['total_pages']>1){?>
        <div>
            <div class="pull-left" style="width:50px; padding:6px 0px 0px 0px">Pages: </div>
            <div class="pull-left" style="width:80%"> 
                <ul class="pagination nomargin">
                    <?php pagingLinksLI($page, URL."message/send", 'active'); ?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php }?>
</div>




<script type="text/javascript">
function submitSearch(){
	$("#searchForm input, #searchForm select").each(function(){
		ob=$(this);
		if(!ob.val())
			ob.removeAttr('name');
	});
}

$("tr td[pl]").each(function(){
	$(this).click(function(){
		pl=$(this).attr("pl");
		location.href=pl;
	});
});
</script>