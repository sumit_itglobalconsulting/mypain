<?php
	$qs=arrayUrlDecode($_GET);
	if(!$users)
		$users=array();
		
	$msgcats=msgCats();
?>
<!-- Send Message Box -->
<?php $this->load->view("common/message/compose_msg", array('users'=>multiArrToKeyValue($users, 'id', 'fullName')));?>
<!-- /Send Message Box -->

<div class="pageheader">
    <div class="row">
        <div class="col-md-8">
            <h2><i class="fa fa-envelope-o"></i> Message <span>Inbox</span></h2>
        </div>
        
        <div class="col-md-4" style="padding-top:4px">
            <form method="get" id="searchForm" onsubmit="return submitSearch()">
                <div class="smallForm">
                    <div class="row">
                    	<div class="col-md-9">
                            <input type="text" name="k" value="<?php echo $qs['k'];?>" class="form-control" placeholder="Search keyword" />
                        </div>
                        <div class="col-md-3"><button type="submit"class="btn btn-primary">Search</button></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="contentpanel">
	<div class="alert alert-success myhide sentMsgAlert">Message sent successfully.</div>
	<?php echo getFlash();?>
	<div class="mb10 smallForm">
    	<div class="pull-left" style="width:200px">
        	<div class="tabBtns">
                <a href="<?php echo URL."message/inbox";?>" class="act">Inbox</a>
                <a href="<?php echo URL."message/sent";?>">Sent Messages</a>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="pull-right">
    		<a href="#" class="composeMsg btn btn-primary">Compose New Message</a>
        </div>
        <div class="clearfix"></div>
    </div>
    
    <div class="table-responsive">
    	<?php if($result){?>
             <style>
           .y{ 
            background: none repeat scroll 0 0 rgba(255, 255, 255, 0.9);
    color: #222;
           }
           .z{
            background: none repeat scroll 0 0 rgba(243, 243, 243, 0.85);
    color: #222;
           }
        </style>
            <table class="table mb30">
            	<thead>
                	<tr>
                        <th width="10px"></th>
                        <th width="250px">From</th>
                        <th width="200px">Category</th>
                        <th>Message</th>
                        <th width="180px">Date</th>
                        <th width="20px"></th>
                    </tr>
                </thead>
                <style>
                    span.name{color:#333;}
                </style>
                <tbody> 
                    
                	<?php foreach($result as $i=>$dtl){$pl=URL."message/read/".encode($dtl['id'])."/inbox/rcv";?>
                    
                        <?php $class=!empty($dtl['star'])?"star-marked":""; ?>
                         <?php 
                         $classR = ($dtl['readT'])<'1'?'y':'z';
                         $other =  getMsgOtherReply($dtl['id']); 
                        
                         if(!empty($other) > 0){
                            
                            $classR = $other['read']<1?'y':'z';
                         } 
                         
                         ?>
                    	<tr class='<?php echo $classR; ?>'>
                            <td><div><img src="<?php echo URL; ?>assets/images/stars.png" class="star <?php echo $class ?>" pl="<?php echo URL."message/star/".encode($dtl['id'])."/inbox";?>" /></div></td>
                            <td pl="<?php echo $pl;?>"><span class="name"><?php echo $dtl['fromName'];?><?php echo $other['name'] ?></span></td>
                            <td pl="<?php echo $pl;?>"><?php echo $msgcats[$dtl['catId']];?></td>
                            <td class="<?php echo $dtl['isNew']?'bold':'';?>" pl="<?php echo $pl;?>">
                                <?php echo $dtl['subject'];?> - <span class="grey"><?php echo shortString($dtl['message'], 30);?></span>
                            </td>
                            <td pl="<?php echo $pl;?>"><?php echo showDate($dtl['created'], true);?></td>
                            <td align="center">
                                <a href="<?php echo URL."message/delete/".encode($dtl['id'])."/inbox";?>?recievedDeleted=1" class="deleteBtn delLink">&nbsp;</a>
                            </td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
        <?php }else{?>
        	<div class="notFound">Not Found.</div>
        <?php }?>
    </div>
   
    <?php if($page['total_pages']>1){?>
        <div>
            <div class="pull-left" style="width:50px; padding:6px 0px 0px 0px">Pages: </div>
            <div class="pull-left" style="width:80%"> 
                <ul class="pagination nomargin">
                    <?php pagingLinksLI($page, URL."message/inbox", 'active'); ?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php }?>
</div>

<script type="text/javascript">
function submitSearch(){
    $("#searchForm input, #searchForm select").each(function(){
            ob=$(this);
            if(!ob.val())
                    ob.removeAttr('name');
    });
}

$("tr td[pl]").each(function(){
	$(this).click(function(){
		pl=$(this).attr("pl");
		location.href=pl;
	});
});

$("div img.star").click(function(){ 
    cu = $(this);
    pl=$(this).attr("pl");
    $.ajax({
        url: pl,
        success:function(result){ 
            cu.toggleClass('star-marked');
        }
    });
});

</script>