<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Choose Plans</title>

<!-- Bootstrap -->
<link href="<?php echo URL;?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo URL;?>assets/css/plan.css" rel="stylesheet">
<link href="<?php echo URL;?>assets/css/font-awesome.css" rel="stylesheet">
</head>
<?php
	$durationTypes=durationTypeArr();
?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs">
            <?php foreach($plans as $p){ ?>
            <li class="<?php if($p['id']==1) echo 'active'; ?>"><a href="#tab_default_<?php echo $p['id']; ?>" data-toggle="tab"> <?php echo $p['planName']; ?> </a> </li>
            <?php } ?>
          </ul>
          <div class="tab-content">
            <?php foreach($plans as $p){ ?>
            <div class="tab-pane <?php if($p['id']==1) echo 'active'; ?>" id="tab_default_<?php echo $p['id']; ?>">
              <div class="container">
                <div class="row">
                  <div class="num-one">
                    <div class="col-md-12">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4>Forms available: <?php echo getFormAvail($p['id']);?></h4>
                          <?php if($p['id']==1) echo '<span>No. of Licences: 20, </span>&nbsp; &nbsp; '; ?>
                          <?php if($p['id'] != 4) { ?>
                          <span>Licence cost: <?php echo $p['price'];?> GBP, </span>&nbsp; &nbsp;
                          <span>Time: <?php echo $p['duration'].' '.$durationTypes[$p['durationType']];?></span> 
                          <?php } ?>
                        </div>
                        <div class="panel-body">
                          <div class="col-md-12">
                           <h3>Features:</h3>
                             <?php echo $p['features']; ?>
                           <?php if($p['id']!=4) { ?>
                           <p class="buynow"><a  href="<?php echo URL."user/choosePlans/".base64_encode($p['id']); ?>" class="btn btn-info"> BUY NOW </a></p>
                           <?php } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            
            </div>
              <?php } ?>
            
          </div>
        </div>
      
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo URL;?>assets/js/bootstrap.min.js"></script>
</body></html>