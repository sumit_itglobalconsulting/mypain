<?php
	header('Content-type: text/calendar; charset=utf-8');
	header('Content-Disposition: attachment; filename=calendar.ics');

	define('NL', "\r\n");
	
	function dateToCal($timestamp) {
	  //return date('Ymd\THis\Z', $timestamp);
	  return date('Ymd', $timestamp);
	}
	 
	function escapeString($string) {
	  return preg_replace('/([\,;])/','\\\$1', $string);
	}
?>
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//MyPain Impact//EN
CALSCALE:GREGORIAN
METHOD:PUBLISH
<?php 
	foreach($cal as $i=>$c){
		if($c['className']=='EColor1')
			$title=$c['title']." (Follow Up)";
		if($c['className']=='EColor2')
			$title=$c['title']." (Response Date)";
		if($c['className']=='EColor3')
			$title=$c['title'];
			
		$date=strtotime(date('Y-m-d 00:00:00', strtotime($c['start'])));
?>
BEGIN:VEVENT
DTSTAMP:<?php echo date('Ymd\THis\Z', time()).NL;?>
DTSTART:<?php echo dateToCal($date).NL;?>
DTEND:<?php echo dateToCal($date).NL;?>
SUMMARY:<?php echo $title.NL;?>
UID:<?php echo $i+1;?>@mypainimpact.com
DESCRIPTION:\nLOCATION:.
STATUS:BUSY
END:VEVENT
<?php }?>
END:VCALENDAR