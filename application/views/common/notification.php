<?php 
?>
<div class="pageheader">
	<h2><i class="fa fa-cog"></i> Notification Settings</h2>
</div>

<div class="contentpanel">
	<?php if($errors){?>
    	<div class="alert alert-danger">Please fill the fields marked with red color</div>
    <?php }?>
    
	<?php echo getFlash();?>
    
    <div class="panel panel-default">
    	<div class="panel-heading">
        </div>
        
        <div class="panel-body">
        	<form class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
            	<input type="hidden" name="action" value="notification" />
                <div class="form-group" style="border:none">
                    <div class="col-sm-6">
                        <div class="ckbox ckbox-primary">
                            <input type="checkbox" name="notificationEmail" id="notificationEmail" value="1" 
							<?php setCheckChecked($dtl['notificationEmail'], 1);?> />
                            <label for="notificationEmail">Email Notifications</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="ckbox ckbox-primary">
                            <input type="checkbox" name="notificationMsg" id="notificationMsg" value="1" 
                            <?php setCheckChecked($dtl['notificationMsg'], 1);?> />
                            <label for="notificationMsg">Text(SMS) Notifications</label>
                        </div>
                    </div>
                </div>
                
                <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6">
                			<button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
$(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});
</script>