<script type="text/javascript" src="https://download.rtccloud.net/js/webappid/j0cfsxvzordy"></script>
<div id="reportBox">
	<a href="<?php echo URL;?>">
        <img src="<?php echo URL;?>assets/img/logo.png" style="height:64px; margin:5px" alt="My Pain Impact" />
    </a>
	
	<tt>
        <div class="reportPInfo" align="center">
            Video consultation between <?php echo $caller;?> &amp; <?php echo $callee;?>
        </div>
    </tt>
	
	<div id="error">
		<h1>An error occurred:</h1>
		<div id="error-content"></div>
	</div>
	<script type="text/javascript">
		if (window.location.protocol === 'file:') { alert('your project must be served from a webserver and not from the file system'); }
		// Here is the definition of the uid used by this user and the authentication url used in order to get a token
		var UID_USER = '<?php echo $calleeId;?>', 
			// If you are using our Java, Ruby or Node.js server-SDK uncomment the following line and replace the placeholder YOUR_AUTH_URL wuth your authentication client url
			// AUTH_URL = 'http://YOUR_AUTH_URL/gettoken?uid=',
			// If you are using our PHP server-SDK uncomment the following line and replace the placeholder YOUR_AUTH_URL wuth your authentication client url
			AUTH_URL = 'https://mypainimpact.com/weemo/gettoken.php?uid=',
			realTimeClient = '',
			rtcc = {},
			script,
			// Define the optional parameters
			options = {
				debugLevel : 1,
				displayName : '<?php echo $callee;?>',
				container : 'video-container'
			},
			//Intialize Rtcc Function
			initializeRtcc = function (token) {
				// Initialize the Main Object with App Identifier, Token, RtccType, options
				rtcc = new Rtcc('j0cfsxvzordy', token, 'internal', options);
				// Call if the RtccDriver is not running on the client computer and if the browser is not WebRTC-capable
				rtcc.onRtccDriverNotStarted = function (downloadUrl) {
					var answer = confirm('Click OK to download and install the Rtcc client.');
					if (answer === true) { window.location = downloadUrl; }
				};
				// Get the Connection Handler callback when the JavaScript is connected to the real-time client
				rtcc.onConnectionHandler = function (message, code) {
					if (window.console) { console.log('Connection Handler : ' + message + ' ' + code); }
					switch (message) {
					case 'connectedRtccDriver':
						realTimeClient = 'RtccDriver';
						break;
					case 'connectedWebRTC':
						realTimeClient = 'WebRTC';
						break;
					case 'sipOk':
						document.getElementById('connecting').style.color = '#CCCCCC';
						document.getElementById('stat').innerHTML = 'Connected as Callee using ' + realTimeClient;
						document.getElementById('call').innerHTML = 'Waiting for a call';
						break;
					case 'loggedasotheruser':
						// force connection, kick other logged users
						 getToken(UID_USER, function (token){
							rtcc.setToken(token);
							rtcc.authenticate(1);
						});
						break;
					}
				};
				// This function permits to catch events comming from the call
				rtcc.onCallHandler = function (callObj, args) {
					if (args.type === 'call' && args.status === 'active') {
						document.getElementById('call').innerHTML = 'Call active';
					} else if (args.type === 'call' && args.status === 'terminated') {
						document.getElementById('call').innerHTML = 'Waiting for a call';
					} else if (args.type === 'webRTCcall' && args.status === 'active') {
						document.getElementById('call').innerHTML = 'Call active';
					} else if (args.type === 'webRTCcall' && args.status === 'terminated') {
						document.getElementById('call').innerHTML = 'Waiting for a call';
					}
				};
				//Initialize connection between the real-time client and the browser
				rtcc.initialize();
			},
			 getToken = function (uid, callback) {
				var xhr;
				if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
					xhr = new XMLHttpRequest();
				} else { // code for IE6, IE5
					xhr = new ActiveXObject('Microsoft.XMLHTTP');
				}
				xhr.onreadystatechange = function () {
					var response = xhr.responseText,
						responseJson,
						token;
					if (xhr.readyState === 4 && xhr.status === 200) {
						response = xhr.responseText;
						try {
							responseJson = JSON.parse(response);
							//responseJson = response;
	
						} catch (e) {
							document.getElementById('error-content').innerHTML = e.message + '<br />' + e.stack + '<br />Response: ' + response;
							document.getElementById('error').style.display = 'block';
						}
						if (responseJson.error) {
							document.getElementById('error-content').innerHTML = 'error code:' + responseJson.error + '<br />Description:' + responseJson.error_description;
							document.getElementById('error').style.display = 'block';
						} else {
							token = responseJson.token;
							if (typeof callback === "function"){
								callback(token)
							}else{
								initializeRtcc(token);
							}
						}
					} else if (xhr.readyState === 4 && xhr.status !== 200) {
						response = xhr.responseText;
						console.log(response);
						document.getElementById('error-content').innerHTML = response;
						document.getElementById('error').style.display = 'block';
					}
				};
				xhr.open('GET', AUTH_URL + uid, true);
				xhr.send();
			},
			init = function () {
				getToken(UID_USER);
			},
			// Test if browser is IE10
			// Poll for jQuery to come into existence
			checkReady = function (callback) {
				if (window.jQuery) {
					callback();
				} else {
					window.setTimeout(function () { checkReady(callback); }, 100);
				}
			};
	
	
		init();
	</script>
	
	<div class="weemoVbx">
		<h4 id="connecting">Connecting as Callee...</h4>
		<h4 id="stat"></h4>
		<h4 id="call"></h4>
		If using WebRTC, the call window will be placed in this box with a blue background
		<div id="video-container"></div>
	</div>
</div>


<!-- Chat Notification -->
<div class="notiBx">
	<div class="notiTitle">Chat Notifications</div>
	<div id="notiListBx">
	</div>
</div>

<script type="text/javascript">
userType='<?php echo USER_TYPE?>';
function getChatNotification() {
	ajax(SITE_URL+"common/getChatNoti", "notiListBx");
}

if(userType=='P' || userType=='D'){
	setInterval(function(){getChatNotification()}, 5000);
}
</script>
<!-- / -->
