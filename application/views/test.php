<?php
	header('Content-type: text/calendar; charset=utf-8');
	header('Content-Disposition: attachment; filename=calendar.ics');
	
	function dateToCal($timestamp) {
	  return date('Ymd\THis\Z', $timestamp);
	}
	 
	function escapeString($string) {
	  return preg_replace('/([\,;])/','\\\$1', $string);
	}
	
	$datestart=strtotime("+1 days", time());
	$dateend=strtotime("+4 days", time());
	$address="Rohini, New Delhi-110085";
	$description="My First Event";
	$uri="http://yahoo.com";
	$summary="Lorem ipsum";
?>
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//MyPain Impact//EN
CALSCALE:GREGORIAN
METHOD:PUBLISH
BEGIN:VEVENT
DTSTAMP:<?php echo dateToCal(time())."\r\n";?>
DTSTART:<?php echo dateToCal($datestart)."\r\n";?>
DTEND:<?php echo dateToCal($dateend)."\r\n";?>
SUMMARY:Sat Rehearsal
UID:mypainimpact.com
DESCRIPTION:\nLOCATION:.
STATUS:BUSY
END:VEVENT
END:VCALENDAR