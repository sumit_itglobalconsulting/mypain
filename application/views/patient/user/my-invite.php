<div class="pageheader">
	<h2><i class="fa fa-user"></i> Sharing requests by doctors</h2>
</div>

<?php echo getFlash(); ?>
<div class="contentpanel">
	<div style="min-height:800px">
        <div class="table-responsive">
          
            <?php if($dueInvitation){ ?>
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            
                            <th class="vert-align" style="vertical-align:middle;">
                                Doctor Name
                                
                            </th>
                            <th class="text-center" style="vertical-align:middle">
                                Email
                            </th>
                            <th  style="vertical-align:middle">
                                Accept
                            </th>
                             <th  style="vertical-align:middle">
                                Cancel
                            </th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php foreach($dueInvitation as $i=>$dtl){?>
                        <tr>
                            <td>
                                <?php echo $dtl['doctorName'];?>
                                <div class="mb5"></div>
                            </td>
                            
                            <td align="center"><?php echo $dtl['loginEmail'];?></td>
                            <td><a href="<?php echo PATIENT_URL."user/doctorRequest/".encode($dtl['id'])."/".encode(USER_ID)."/".encode(2)."/" ; ?>"><img src="<?php echo URL."assets/img/accept-invi.png";?>" /></a></td>
                            <td><a href="<?php echo PATIENT_URL."user/doctorRequest/".encode($dtl['id'])."/".encode(USER_ID)."/".encode(4)."/"; ?>"><img src="<?php echo URL."assets/img/remove-invi.png";?>" /></a></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            <?php }else{?>
                <div class="notFound">No Data Found</div>
            <?php }?>
        </div>
        
       
    </div>
</div>

