<?php
$overallResHelp="Overall treatment response gives an overview of how your symptoms have been as calculated by different forms. Some forms are used to measure different ways pain is impacting your life and how it as changed from when your treatment was first started. This is reported as 'My Overall treatment response'.";

$feelHelp="This part of overall treatment response is the 'How I (Patient) Feel' section that highlights how you have reported to be 'feeling' with regards to your symptoms. This is based on your responses to the Patient Global Impression of Change form or PGIC.";
?>

<style type="text/css">
.mybtn1			{float:right}
#bmSaveMsg		{position:absolute; left:40%; top:-70px; width:250px; z-index:1000; text-align:center; display:none}
</style>

<div class="posRel" id="bodyMapParentBx">
    <div class="pageheader">
        <h2><i class="fa fa-home"></i> Dashboard</h2>
    </div>
    
    <div class="contentpanel reportDiv">
    	<div style="min-height:800px">
        	<?php if(round($overallRes) && $feelHelp){?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Present Status</h4>
                    </div>
                    
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="posRel" style="width:420px; margin:0 auto">
                                    <div class="help1 helpPos1 tooltips" title="<?php echo $overallResHelp;?>"></div>
                                    
                                    <h4 class="mb5 text-center">My overall treatment response</h4>
                                    <div id="overallResChart" style="width:100%; height:350px; margin:0 auto"><?php echo round($overallRes);?></div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="posRel" style="width:420px; margin:0 auto">
                                    <div class="help1 helpPos1 tooltips" title="<?php echo $feelHelp;?>"></div>
                                    
                                    <h4 class="mb5 text-center">How I feel</h4>
                                    <div id="feelResImage"  class="therm_img">
                                        <?php
                                            $src='';
                                            switch($feels){
                                            case -5:
                                                $src='red_-5';
                                                break;
                                            case -4:
                                                $src='red_-4';
                                                break;
                                            case -3:
                                                $src='orange_-3';
                                                break;
                                            case -2:
                                                $src='orange_-2';
                                                break;
                                            case -1:
                                                $src='orange_-1';
                                                break;
                                            case 0:
                                                $src='yellow_0';
                                                break;
                                            case 1:
                                                $src='light_green_1';    
                                                break;
                                            case 2:
                                                $src='light_green_2';    
                                                break;
                                            case 3:
                                                $src='light_green_3';    
                                                break;
                                            case 4:
                                                $src='green_4';
                                                break;
                                            case 5:
                                                $src='green_5';    
                                                break;
                                            }
                                        ?>
                                        <img src="<?php echo IMG_URL.$src;?>therm.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <script type="text/javascript">
            function overallResChart(id, v, feel) {
                v=parseInt(v);
                if(feel){
                    if(v<1)
                        colorCode='#ff4b31';
                    else if(v>=1 && v<=3)
                        colorCode="#fbd200";
                    else
                        colorCode="#6fba61";
                    var data = google.visualization.arrayToDataTable([['Year', 'How I Feel'], ['PGIC Score',  v],]);
                }
                else{
                    if(v<30)
                        colorCode='#ff4b31';
                    else if(v>=30 && v<50)
                        colorCode="#fbd200";
                    else
                        colorCode="#6fba61";
                        
                    var data = google.visualization.arrayToDataTable([['Year', 'Change from baseline'], ['Change in symptoms',  v],]);
                    
                    var formatter = new google.visualization.NumberFormat({pattern:'#\'%\''});
                    formatter.format(data, 1);
                }
                
                 var view = new google.visualization.DataView(data);
                 view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation" }]);
                
                if(feel){
                    Ticks=[{v:-5, f:'-5'}, {v:-4, f:'-4'}, {v:-3, f:'-3'}, {v:-2, f:'-2'}, {v:-1, f:'-1'}, {v:0, f:'0'}, 
                           {v:1, f:'1'}, {v:2, f:'2'}, {v:3, f:'3'}, {v:4, f:'4'}, {v:5, f:'5'}];
                    verAxis={title: '', textStyle: {fontSize: 12}, ticks: Ticks};
                }
                else{
                    Ticks=[{v:-40, f:'-40%'}, {v:-20, f:'-20%'}, {v:0, f:'0%'}, {v:20, f:'20%'}, {v:40, f:'40%'}, {v:60, f:'60%'}, 
                            {v:80, f:'80%'}, {v:100, f:'100%'}];
                    verAxis={title: '', textStyle: {fontSize: 12}, ticks: Ticks};
                }
                
                var options = {
                  title: '',
                  hAxis: {title: '', titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}},
                  vAxis: verAxis,
                  legend: { position: "none", textStyle: {fontSize: 12}},
                  colors: [colorCode]
                };
                
                var chart = new google.visualization.ColumnChart(document.getElementById(id));
                chart.draw(view, options);
            }
            
            overallResChart("overallResChart", $("#overallResChart").text());
            //overallResChart("feelResChart", $("#feelResChart").text(), true);
            </script>
            <?php }?>
        
			
        
            <div id="calendarBx" class="panel panel-default">
                <div class="panel-heading">
                    <?php if(USER_TYPE=='P'){?>
                    <div class="pull-right">
                        Calendar Url: 
                        <span style="background:#eee; padding:3px; border:1px solid #ccc"><?php echo URL."common/patientCal/".USER_ID."?qs=".time();?></span>
                    </div>
                    <?php }?>
                    <h4 class="panel-title">Calendar</h4>
                </div>
                
                <div class="panel-body">
                    <?php $this->load->view("patient/user/calendar");?>
                    
                    <div class="mb20"></div>
                    <div align="center">
                        <table cellpadding="0">
                            <tr>
                                <td width="21px" style="padding-left:50px"><span class="EColor2Bx"></span></td>
                                <td style="text-align:left">&nbsp;Form Response Due Date</td>
                                <td width="21px" style="padding-left:30px"><span class="EColor4Bx"></span></td>
                                <td style="text-align:left">&nbsp;Form Response Overdue</td>
                                
                                <td width="21px" style="padding-left:30px"><span class="EColor1Bx"></span></td>
                                <td style="text-align:left">Follow-up Date</td>
                                <td width="21px" style="padding-left:30px"><span class="EColor3Bx"></span></td>
                                <td style="text-align:left">&nbsp;Follow Up Done</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            
        
            <div class="panel panel-default">
                <form action="javascript:void(0)" id="bodymapForm" method="post" onsubmit="submitBodymap()">
                    <textarea class="hide" name="bodymap" id="bodymapInpt"></textarea>
                    <div class="panel-heading posRel">
                        <div class="row">
                            <div class="pull-left">
                                <h4 class="panel-title">My Pain Body Map</h4>
                            </div>
                            
                            <div class="pull-right">
                                <button type="submit" class="mybtn1 btn btn-primary btn-xs"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;<span>Save</span></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    
                    <div class="panel-body">
                        <div class="posRel">
                            <div id="bmSaveMsg" class="text-success alert alert-success">Saved successfully.</div>
                            <div class="text-center" style="margin:0 0 10px 0; padding:0; font-size:16px">
                                <span class="text-success">Please click on the body map image to get started. <i class="fa fa-level-down"></i></span>
                            </div>
                            <div class="posRel">
                                <!-- Popup div -->
                                <?php 
                                    $this->load->view("common/bodymap/bodymap.php", array('R'=>'460', 'T'=>'-20'));
                                ?>
                                <!-- /Popup div -->
                                
                                <div class="bodyMap bodyMapUser <?php echo $dtl['gender']=='M'?'bodyMapMaleBg':'bodyMapFemaleBg';?>">
                                    <?php echo $dtl['bodymap'];?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</div>


<script type="text/javascript">
function submitBodymap() {
	var error=false;
	$("#bodymapInpt").html($(".bodyMap").html());
	if(!error){
		$(".mybtn1 span").text('Saving...');
		$.ajax({
			type: 'POST',
			url: SITE_URL+"patient/user/saveBodyMap",
			data: $("#bodymapForm").serialize(),
			success: function(res){
				$(".mybtn1 span").text('Save');
				$("#bmSaveMsg").fadeIn('slow');
				$("#bmSaveMsg").delay(3000).fadeOut('slow');
			}
		});
	}
}
</script>