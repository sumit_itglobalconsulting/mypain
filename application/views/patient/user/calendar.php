<link href='<?php echo URL?>assets/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?php echo URL?>assets/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?php echo URL?>assets/fullcalendar/lib/moment.min.js'></script>
<script src='<?php echo URL?>assets/fullcalendar/fullcalendar.min.js'></script>
<style type="text/css">
.EColor1		{background:#000000; border:none; padding:2px 3px; line-height:16px; text-align:center}
.EColor2		{background:#3a87ad; border:none; padding:2px 3px; line-height:16px; text-align:center}
.EColor3		{background:#6fba61; border:none; padding:2px 3px; line-height:16px; text-align:center}
.EColor4		{background:#ff0000; border:none; padding:2px 3px; line-height:16px; text-align:center}
</style>

<script type="application/javascript">
$(document).ready(function() {
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			//right: 'month,agendaWeek,agendaDay'
			right: 'month,agendaWeek'
		},
		//defaultDate: '2014-06-19',
		editable: false,
		
		events: {
			url: SITE_URL+'common/getPateintCalendar',
			success: function() {
				hideOtherMonth();
			},
			error: function() {
			}
		},
		
		loading: function(bool) {
			$('#loading').toggle(bool);
		},
		eventRender: function(event, element) {
            element.attr('title', event.tip);
        }
	});
	
});

function hideOtherMonth(){
	c=0;
	$("#calendar tr.fc-last td").each(function(){
		if($(this).hasClass( "fc-other-month" ))
			c++;
	});
	if(c>=7)
		$("#calendar tr.fc-last").not("tr.fc-first").hide();
}
</script>
<div id="loading" style="display:none; text-align:center">Loading...</div>
<div id='calendar' style="max-width:800px;width:100%; margin:0 auto; border-bottom:1px solid #ddd"></div>

<script type="text/javascript">
</script>