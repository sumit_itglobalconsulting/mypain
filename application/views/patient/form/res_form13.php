<div class="big mb30">
    <p>Everyone experiences painful situations at some point in their lives. Such experiences include headache, tooth pain, joint or muscle pain.</p>
    <p>People are often exposed to situations that may cause pain,  such as illness, injury, dental procedure or surgery.</p>
    
    <p>We are interested in the types of thoughts and feelings that you have when you are in pain. Listed below are thirteen statements describing different thoughts and 
    feelings that may be asociated with pain.</p>
    <div class="mb5"></div>
    <p class="bold">Using the following scale, please indicate the degree to which you have these thoughts and feelings when you are experiencing pain.</p>
</div>

<div class="pcs">
	<?php 
        $c=0; foreach($ques as $i=>$dtl){
            $c=$c+1; 
            $dtl['ques']=str_replace('{SYMPTOM}', $formDtl['symptom'], $dtl['ques']); 
            $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
            $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
            $dtl['score']=$quesDtl[$dtl['id']]['score'];
        ?>
        
        <div class="quesBx big qbox<?php echo $i;?>">
            <?php echo '<span class="quesNo">'.$c.'.</span> '.$dtl['ques'];?>
            <div class="clr">&nbsp;</div>
        </div>
        <div class="ansBx" quesno="<?php echo $i;?>">
            <?php 
                $options=getOptionDetail($dtl['optionDtl']);
                slideOptions($options, $dtl, $patientDtl, $formDtl);
            ?>
        </div>
        
    <?php }?>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$(".quesSlide").each(function(){
		obj=$(this);
		V=obj.attr('v')*1;
		
		obj.slider({
			range: "min",
			min: obj.attr('min')*1,
			max: obj.attr('max')*1,
			step: 1,
			value: V,
			
			slide: function(e, ui){
				v=ui.value;
			},
			
			stop: function( e, ui ){
				v=ui.value;
				$(".quesOpt", $(this)).val($(".optId"+v, $(this).parent()).val());
				
				scrollQues( parseInt($(this).parent().parent().attr("quesno")) + 1 );
			}
		});
	});
});
</script>