<div class="bpiForm">
    <h4 class="panel-title mb30">Pain intensity scale</h4>
    <?php 
        $c=0; foreach($ques as $i=>$dtl){
            $c=$c+1; 
            $dtl['ques']=str_replace('{SYMPTOM}', $formDtl['symptom'], $dtl['ques']); 
            $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
            $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
            $dtl['score']=$quesDtl[$dtl['id']]['score'];
        ?>
        <?php if($dtl['id']==9){ $c=1;?>
             <div class="mb30"></div>
             <h4 class="panel-title">Relief with Medications</h4>
             
             <div class="mb30"></div>
             <div class="mb30">
                 <div><span style="">Do you take any medicines?</span>
                     <span>
			    <select class="form-control" name="medTake">
                                <option value="0" <?php if(empty($resDtl['medTake']))echo 'selected'; ?>>No</option>
                                <option value="1" <?php if($resDtl['medTake']==1)echo 'selected'; ?>>Yes</option>
                            </select>                                
                     </span>  
                    </div>
                       
             </div>
             
            
        <?php } ?>
    
        <input type="hidden" name="scaleId[<?php echo $dtl['id'];?>]" value="<?php echo $dtl['scaleId'];?>" />
        
        <?php if($dtl['id']==11){$c=1;?>
        <div class="mb30"></div>
        <h4 class="panel-title">Pain interference scale</h4>
        <div class="mb30">Select the response that describes how, <strong>during the past 24 hours</strong>, pain has interfered with</div>
        <?php }?>
    
        <?php
            if($resDtl['medTake']==1){
                $style="style='display:block'";
            }else{
                $style="style='display:none'";
            }
        ?>
        
        <div class="quesBx big qbox<?php echo $i;?>  <?php if($i==4){echo "medi";}?>" <?php if($i==4){echo $style;}?>>
            <?php echo '<span class="quesNo">'.$c.'.</span> '.$dtl['ques'];?>
            <div class="clr">&nbsp;</div>
        </div>
        <div class="ansBx  <?php if($i==4){echo "medi";}?>" quesno="<?php echo $i;?>" <?php if($i==4){echo $style;}?>>
            <?php 
                $options=getOptionDetail($dtl['optionDtl']);
                slideOptions($options, $dtl, $patientDtl, $formDtl);
            ?>
        </div>
        
    <?php }?>
</div>


<script type="text/javascript">
$(document).ready(function() {
	$(".quesSlide").each(function(){
		obj=$(this);
		V=obj.attr('v')*1;
		
		obj.slider({
			range: "min",
			min: obj.attr('min')*1,
			max: obj.attr('max')*1,
			step: 1,
			value: V,
			
			slide: function(e, ui){
				v=ui.value;
			},
			
			stop: function( e, ui ){
				v=ui.value;
				$(".quesOpt", $(this)).val($(".optId"+v, $(this).parent()).val());
				
				scrollQues( parseInt($(this).parent().parent().attr("quesno")) + 1 );
			}
		});
	});
        $("select").change(function(){
            if($(this).val()==1){
                $('.medi').show();
            }else{
                $('.medi').hide();
                $("input[type='hidden'][name='quesOpt[9]']").val(46);
                $(".quesSlide.med").attr('v',0);
                $(".quesSlide.med").each(function(){
                        obj=$(this);
                        V=obj.attr('v')*1;

                        obj.slider({
                                range: "min",
                                min: obj.attr('min')*1,
                                max: obj.attr('max')*1,
                                step: 1,
                                value: V,

                                slide: function(e, ui){
                                        v=ui.value;
                                },

                                stop: function( e, ui ){
                                        v=ui.value;
                                        $(".quesOpt", $(this)).val($(".optId"+v, $(this).parent()).val());

                                        scrollQues( parseInt($(this).parent().parent().attr("quesno")) + 1 );
                                }
                        });
                });
            }
        });
});
</script>