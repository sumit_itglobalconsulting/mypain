<?php
$firstQues=$ques[0];
$lastQues=end($ques);
?>
            
<div class="pad3">
    <div class="down">
    	<div class="big">
            <div class="bold">Please answer these questions carefully.</div>
            <div>Please tick ONE option in each row.</div>
        </div>
        <p>&nbsp;</p>
        <table width="100%" cellspacing="1" cellpadding="6" class="sTbl">
            <?php for($i=0; $i<count($ques); $i++){$dtl=$ques[$i];?>
                <?php
                    $options=getOptionDetail($dtl['optionDtl']);
                    $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
                    $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
                    $dtl['score']=$quesDtl[$dtl['id']]['score'];
                ?>
                <tr class="quesRw">
                    <td width="20px" align="right" valign="top"><?php echo $i+1;?>.</td>
                    <td><?php echo $dtl['ques'];?></td>
                    
                    <?php if($i==7){?>
                        <td></td><td></td><td></td>
                    <?php }?>
                    <?php foreach($options as $c=>$opt){?>
                        <td width="120px">
                            <div class="rdio rdio-primary">
                                <input type="radio" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                                <label for="optR<?php echo $opt['optionId'];?>"><?php echo $opt['option'];?></label>
                            </div>
                        </td>
                    <?php }?>
                </tr>
            <?php }?>
        </table>
    </div>
</div>

<script type="text/javascript">
</script>