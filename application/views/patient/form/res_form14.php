<div class="big mb30">
	<p class="mb5">Pain Disability Index (PDI) is used to assess the degree to which chronic pain interferes with various daily activities.</p>
    <p>For each of the 7 categories of life activity Listed, please select the number on the scale which describes the level of disability you typically experience. A score of 0 means no disability at all, and a score of 10 signifies that all of the activities in which you would normally be involved have been totally 
    disrupted or prevented by your pain.</p>
</div>

<div class="bpiForm">
    <?php 
        foreach($ques as $i=>$dtl){
            $dtl['ques']=str_replace('{SYMPTOM}', $formDtl['symptom'], $dtl['ques']); 
            $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
            $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
            $dtl['score']=$quesDtl[$dtl['id']]['score'];
        ?>
        
        <input type="hidden" name="scaleId[<?php echo $dtl['id'];?>]" value="<?php echo $dtl['scaleId'];?>" />
       
        <div class="quesBx big qbox<?php echo $i;?>">
        	<div>
            	<?php echo '<span class="quesNo">'.($i+1).'.</span> '.$dtl['ques'];?>
            	<div class="clr">&nbsp;</div>
            </div>
            <div style="font-weight:normal; font-size:14px; padding-left:10px"><?php echo $dtl['infoText'];?></div>
        </div>
        <div class="ansBx pdi" quesno="<?php echo $i;?>">
            <?php 
                $options=getOptionDetail($dtl['optionDtl']);
                slideOptions($options, $dtl, $patientDtl, $formDtl);
            ?>
        </div>
        
    <?php }?>
</div>


<script type="text/javascript">
$(document).ready(function() {
	$(".quesSlide").each(function(){
		obj=$(this);
		V=obj.attr('v')*1;
		
		obj.slider({
			range: "min",
			min: obj.attr('min')*1,
			max: obj.attr('max')*1,
			step: 1,
			value: V,
			
			slide: function(e, ui){
				v=ui.value;
			},
			
			stop: function( e, ui ){
				v=ui.value;
				$(".quesOpt", $(this)).val($(".optId"+v, $(this).parent()).val());
				
				scrollQues( parseInt($(this).parent().parent().attr("quesno")) + 1 );
			}
		});
	});
});
</script>