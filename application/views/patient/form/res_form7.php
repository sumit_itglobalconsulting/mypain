<?php if(trim($formDtl['formInfo'])){?>
    <div class="big mb30">
        <?php echo $formDtl['formInfo'];?>
    </div>
<?php }?>
<?php //echo '<pre>';print_r($ques);?>
<div class="pad3">
    <div class="smallForm">
    	<table width="100%" cellspacing="1" cellpadding="8" class="sTbl">
        	<tr class="bold">
            	<td width="20px" style="padding:2px">Sr.</td>
            	<td width="130px">Category</td>
                <td>Question</td>
                <td width="350px">Response</td>
            </tr>
            <?php //pr($quesDtl);
				for($i=0; $i<count($ques); $i++){ $dtl=$ques[$i]; 
					$options=getOptionDetail($dtl['optionDtl']);
					
					$dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
					$dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
					$dtl['score']=$quesDtl[$dtl['id']]['score'];
					//$dtl['ans']=str_replace("|", "", $quesDtl[$dtl['id']]['ans']);
					
					$ans=explode("|", $quesDtl[$dtl['id']]['ans']);
					foreach($ans as $a){
						if($a){
							$dtl['ans']=$a;
							break;
						}
					}
					
					$dtl['image']=str_replace("|", "", $quesDtl[$dtl['id']]['file']);
					
					$optionId=$dtl['optionId']?$dtl['optionId']:$options[0]['optionId'];
					
					/** Sr. No. **/
					$hideQ='';
					$cn=$i+1;
					if($cn==13){
						$cn='12&nbsp;(a)';
						$hideQ='myhide';
					}
					else if($cn>12)
						$cn=$cn-1;
						
					if($cn==15){
						$cn='14&nbsp;(a)';
						$hideQ='myhide';
					}
					else if($cn>14)
						$cn=$cn-1;
                                        if($cn==25)
                                            $cn=20;
					/** Sr. No. End **/
                if($dtl['id']==93){                        
                    $custom_opt = $dtl['optionId'];
                } 
                if(empty($custom_opt) && $dtl['id']>=94 && $dtl['id']<=98) 
                {
                    $hide='religion';
                    $class_add="religion_q";
                }elseif($dtl['id']>=94 && $dtl['id']<=98){
                    $class_add="religion_q";
                }
                        
                                         
								?>
            	<tr id="row<?php echo $dtl['id'];?>" class="<?php echo $hideQ;?><?php echo $hide;?> <?php echo $class_add;?>">
                        <?php unset($hide);unset($class_add);?>
                	<td align="right" valign="top">
                            <?php
                                switch($dtl['id']){
                                    case 94:
                                        $cn="a";
                                        break;
                                    case 95:
                                        $cn="b";
                                        break;
                                    case 96:
                                        $cn= "c";
                                        break;
                                    case 97:
                                        $cn="d";
                                        break;
                                    case 98:
                                        $cn="e";
                                        break;
                                }
                            
                            ?>
                            <?php echo $cn;?>.
                    </td>
                    
                	<td valign="top">
						<?php echo $dtl['catName'];?>
                    </td>
                    
                    <td valign="top">
                    	<?php echo $dtl['ques'];?>
                    </td>
                    
                    <td>
                    	<div>
			<?php if($dtl['type']==1){?>
                                <?php
                                    $optAr=multiArrToKeyValue($options, 'optionId', 'option');
                                    if($options[0]['option']!='No' && $options[0]['option']!='Yes' && $options[0]['option']!='0' && $options[0]['option']!="Not applicable")
                                        $optAr=array(''=>'Do not wish to answer')+$optAr;
										
									$optId=explode(",", $dtl['optionId']);
									$optId=$optId[0];
                                    echo form_dropdown('quesOpt['.$dtl['id'].']', $optAr, $optId, 'id="dd'.$dtl['id'].'" class="form-control chosen-select1"');
                                ?>
                                
                                <?php if($dtl['id']==90){?>
                                    <div id="medListBx" class="greyBx1 myhide" style="margin-top:10px">
                                    	<div style="padding:0px 0px 3px 0px">Please list the medicines you are allergic to:</div>
                                        <textarea name="quesFreeAns[<?php echo $dtl['id'];?>]" class="form-control" spellcheck="false"
                                        placeholder=""><?php echo $dtl['ans'];?></textarea>
                                    </div>
                                    <script type="text/javascript">
										$("#dd90").change(function(){
											if($("option:selected", $(this)).text()=="Yes")
												$("#medListBx").show();
											else
												$("#medListBx").hide();
										});
										if($("option:selected", $("#dd90")).text()=="Yes")
											$("#medListBx").show();
									</script>
                                <?php }?>
                            <?php }?>	
                            
                            
                            <?php if($dtl['type']==3){$opts=explode(",", $dtl['optionId']);?>
                            	<div style="border:1px solid #ccc; border-radius:5px; padding:5px">
                                    <div class="greyBx1" style="height:200px; overflow:auto;">
                                        <?php foreach($options as $c=>$opt){?>
                                            <div style="padding:3px; background:#fff; border-bottom:1px dotted #eee">
                                                <div class="ckbox ckbox-primary">
                                                    <input type="checkbox" name="quesOpt[<?php echo $dtl['id'];?>][]" id="opt<?php echo $dtl['id'].$c;?>" 
                                                    value="<?php echo $opt['optionId'];?>" 
                                                    <?php setCheckChecked(in_array($opt['optionId'], $opts), true);?> />
                                                    <label for="opt<?php echo $dtl['id'].$c;?>"><?php echo $opt['option'];?></label>
                                                </div>
                                            </div>
                                        <?php }?>
                                    </div>
                                </div>
                            <?php }?>
                            
                             <?php if($dtl['type']==5){?>
                                    <textarea name="quesFreeAns[<?php echo $dtl['id'];?>]" class="form-control" spellcheck="false"  
                                    placeholder="Write your answer"><?php echo $dtl['ans'];?></textarea>
                             <?php }?>
                             
                             <?php if($dtl['type']==6){?>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input">
                                                <i class="glyphicon glyphicon-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" id="image" name="image[<?php echo $dtl['id'];?>]" accept="image/*" />
                                            </span>
                                            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                             <?php }?>
                        </div>
                        
                        <?php if($dtl['type']==3){?>
                            <div class="greyBx1" style="margin-top:10px">
                                <textarea name="quesFreeAns[<?php echo $dtl['id'];?>]" class="form-control" spellcheck="false"
                                placeholder="Write other options"><?php echo $dtl['ans'];?></textarea>
                            </div>
                        <?php }?>
                        
                        <?php if($dtl['type']==6){?>
                            <div style="padding:10px 0px 0px 0px" align="center">
								<?php if($dtl['image']){?>
                                    <img id="showImg" src="<?php echo URL."assets/uploads/scans/".$dtl['image']?>" width="150px" alt="" />
                                    <div>
                                    <a href="<?php echo URL."assets/uploads/scans/".$dtl['image']?>" target="_blank">View large image</a>
                                    </div>
                                <?php }?>
                            </div>
                        <?php }?>
                    </td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});
$("select#dd93").change(function(){
    if($(this).val()){
        $("table.sTbl tbody tr.religion_q").removeClass('religion');
    }    
    else{
        $("table.sTbl tbody tr.religion_q").addClass('religion');
        $("select#dd94,select#dd95,select#dd96,select#dd97,select#dd98").prop("selectedIndex",0);
    }    
});

/** **/
function row84(v){
	if(v==488 || v==489)
		$("#row85").show();
	else
		$("#row85").hide();	
}
$("#dd84").change(function() {
	v=$(this).val();
	row84($(this).val());
});
row84($("#dd84").val());
/** **/

/** **/
function row87(v){
	if(v==497)
		$("#row88").show();
	else
		$("#row88").hide();	
}
$("#dd87").change(function() {
	v=$(this).val();
	row87($(this).val());
});
row87($("#dd87").val());
/** **/

$('label').disableSelection();
</script>