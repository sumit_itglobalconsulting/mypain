<?php
	$lastQues=$ques[count($ques)-1];
?>

<div class="big mb30">
	<div class="mb5">
    	<strong>Please select the statements that best describe your own health state today:</strong>
    </div>
</div>
            
<div class="pad3">
    <div class="down">
        <table width="100%" cellspacing="1" cellpadding="6" class="sTbl">
            <?php for($i=0; $i<count($ques)-1; $i++){$dtl=$ques[$i];?>
                <?php
                    $options=getOptionDetail($dtl['optionDtl']);
                    $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
                    $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
                    $dtl['score']=$quesDtl[$dtl['id']]['score'];
                ?>
                
                <tr class="quesRw">
                    <td align="right" valign="top" width="20px"><strong><?php echo $i+1;?>.</strong></td>
                    <td valign="top">
						<div class="bold mb5"><?php echo $dtl['ques'];?></div>
                        <?php foreach($options as $c=>$opt){?>
                        	<div>
                                    <div class="rdio rdio-primary">
                                            <input type="radio" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                        id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                                        <label for="optR<?php echo $opt['optionId'];?>"><?php echo $opt['option'];?></label>
                                    </div>
                                </div>
                        <?php }?>
                    </td>
                </tr>
            <?php }?>
        </table>
        <p>&nbsp;</p>
        <div class="euroQol">
        	<?php 
                        $dtl=$lastQues;
                        $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
                        $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
                        $dtl['score']=$quesDtl[$dtl['id']]['score'];
                ?>
            
            <div class="quesBx big">
            <?php echo $dtl['ques'];?>
            <div class="clr">&nbsp;</div>
            </div>
            <div class="ansBx" style="padding-left:30px">
                <?php 
                    $options=getOptionDetail($dtl['optionDtl']);
                    slideOptions($options, $dtl, $patientDtl, $formDtl);
                ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$(".quesSlide").each(function(){
		obj=$(this);
		V=obj.attr('v')*1;
		
		obj.slider({
			range: "min",
			min: obj.attr('min')*1,
			max: obj.attr('max')*1,
			step: 10,
			value: V,
			
			slide: function(e, ui){
				v=ui.value;
			},
			
			stop: function( e, ui ){
				v=ui.value;
				$(".quesOpt", $(this)).val($(".optId"+v, $(this).parent()).val());
				
				scrollQues( parseInt($(this).parent().parent().attr("quesno")) + 1 );
			}
		});
	});
});

/** **/

</script>