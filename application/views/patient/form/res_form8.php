<?php if(trim($formDtl['formInfo'])){?>
    <div class="big mb30">
        Please help us improve our program by answering some questions about the services you have received. We are interested in your honest opinions, whether they are positive or negative. Please answer all of the questions. We also welcome your comments and suggestions. Thank you very much; we really appreciate your help.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if(!empty($doctorName)){ echo "<b>[ Dr. ".$doctorName."]</b>";}?> 
    </div>
<?php }?>


<div class="csq">
	<?php 
        $c=0; foreach($ques as $i=>$dtl){
            $c=$c+1; 
            $dtl['ques']=str_replace('{SYMPTOM}', $formDtl['symptom'], $dtl['ques']); 
            $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
            $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
            $dtl['score']=$quesDtl[$dtl['id']]['score'];
           
        ?>
        
        <div class="quesBx big qbox<?php echo $i;?>">
            <?php echo '<span class="quesNo">'.$c.'.</span> '.$dtl['ques'];?>
            <div class="clr">&nbsp;</div>
        </div>
        <div class="ansBx" quesno="<?php echo $i;?>" style="padding-left:45px">
            <?php 
               $options=getOptionDetail($dtl['optionDtl']); 
               csqSlideOptions($options, $dtl, $patientDtl, $formDtl);
            ?>
        </div>
        
    <?php }?>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$(".quesSlide").each(function(){
		obj=$(this);
		V=obj.attr('v')*1;
		
		obj.slider({
			range: "min",
			min: obj.attr('min')*1,
			max: obj.attr('max')*1,
			step: 1,
			value: V,
			
			slide: function(e, ui){
				v=ui.value;
			},
			
			stop: function( e, ui ){
				v=ui.value;
				$(".quesOpt", $(this)).val($(".optId"+v, $(this).parent()).val());
				
				scrollQues( parseInt($(this).parent().parent().attr("quesno")) + 1 );
			}
		});
	});
});
</script>