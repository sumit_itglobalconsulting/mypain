<?php 
?>
<div class="big mb30">
	<p class="mb5">When your back hurts, you may find it difficult to do some of the things you normally do.</p>
	<p class="mb5">This list contains sentences that people have used to describe themselves when they have back pain.  When you read them, you may find that some stand out because 
    they describe you today, please select that.</p>
	<p>As you read the list, think of yourself today. If a sentence does not describe you, then leave the space blank and go on to the next one.  Remember, 
    only select the sentence if you are sure it describes you today.</p>
</div>
            
<div class="pad3">
    <div class="down">
        <table width="100%" cellspacing="1" cellpadding="6" class="sTbl">
            <?php for($i=0; $i<count($ques); $i++){$dtl=$ques[$i];?>
                <?php
                    $options=getOptionDetail($dtl['optionDtl']);
                    $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
                    $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
                    $dtl['score']=$quesDtl[$dtl['id']]['score'];
                ?>
                
                <tr class="quesRw1">
                    <td align="right" valign="top" width="20px"><?php echo $i+1;?>.</td>
                    <td valign="top">
                        <?php foreach($options as $c=>$opt){?>
                            <div class="ckbox ckbox-primary">
                                <input type="checkbox" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                                <label for="optR<?php echo $opt['optionId'];?>"><?php echo $dtl['ques'];?></label>
                            </div>
                        <?php }?>
                    </td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>

<script type="text/javascript">
</script>