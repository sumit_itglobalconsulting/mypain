<html>
<head>
	<title>The Euroqol Group EQ-5D demo launchpad </title>
        <script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js')?>"></script>
</head>

<body>

<form action="https://online.euroqol.org/TEGV5/Common/hub/GET_MANAGE_LIC_QUEX_REQUEST.CFM" method="post" id="euroFrm">
<h1>Licensed Partner Sample Connect Page</h1>

<h2>The following fields must/can be supplied</h2>

<table>
	<tr>
		<td colspan="3">
			<h3>Mandatory fields</h3>
			<hr>
		</td>
	</tr>
	<tr>
		<td>LPCID</td><td ><input name="LPCID" value="mypainltd!"></td><td>The license key that was assigned by QMFW managing company.</td>
		
	</tr>
	<tr>
	<td>RID</td><td><input name="RID" value="<?php echo $dtl['userId']."~".$dtl['doctorId'];?>" /></td><td>Respondent ID</td>
		
	</tr>
	<tr>
	<td>PBU</td><td><input style="width:300px;" name="PBU" value="https://mypainimpact.com/patient/form/euroQol/"></td><td>Post back URL The URL at the licensed partner's system that will handle any data returned. Needs to be in full format e.g.: HTTP://xxxxx.xxxx.xxx/xxxx</td>
		
	</tr>
	<tr>	
	<tr>
	<td>RTP</td><td><input style="width:300px;" name="RTP" value="FRM"></td><td>Alphanumeric 3
FRM = Form Variables
XML = Pure XML
FRX  = XML in Form Variable
FRW = WDDX in Form variable
</td>
		
	</tr>

	<tr>		
	<td colspan="3" align="right">
			<table>
				<tr>
					<td>
					</td>
					<td><input type="submit" >  
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td colspan="3">






<h3>Recommended fields (if applicable)</h3>
<hr>
		</td>
	</tr>
	<tr>	
	
	

<td>VID</td><td><input name="VID" value=""></td><td>Version id that is meaningful to the licensed partner. 
When licensed partner used its own versioning, the QMFW system internally will create a new user-account every time 

</td>
		
	</tr>	
	<tr>
	<td>QID</td><td><input name="QID" value=""></td><td>Questionnaire ID that is meaningful to QMFW Web system if more than one potential questionnaires are offered under a license</td>
		
	</tr>
	<tr>
	<td>FRA</td><td><input name="FRA" value="YES"></td><td>Format the questionnaire for display in an IFRAME?
Format: 
YES=Display in IFRAME (so without any headers)
NO=Display in (pop-up)window so with QMFW system headers)
Default=Yes
</td>
		
	</tr>
	<tr>
	<td>LID</td><td><input name="LID" value="9"></td><td>Language ID indicating the language in which a questionnaire will be rendered.  </td>
		
	</tr>
	<tr>
	<tr>
	<td>DTZ</td><td><input name="DTZ" value="24"></td><td>Indication of the time-zone that the respondent is located in. Only relevant when scheduling functions of QMFW are used. Defaults to server default (Eastern Standard Time) </td>
		
	</tr>
	<tr>		
	<td colspan="3" align="right">
			<table>
				<tr>
					<td>
					</td>
					<td><input type="submit" >  
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td colspan="3">
			<h3>Residual fields</h3>
			<hr>
		</td>
	</tr>
	
	<tr>
	<td>FNM</td><td><input name="FNM" value="<?php echo $dtl['firstName']; ?>"></td><td>First Name</td>
		
	</tr>
	<tr>
	<td>MNM</td><td><input name="MNM" value=""></td><td>Middle Name</td>
		
	</tr>
	




<tr>
	<td>LNM</td><td><input name="LNM" value="<?php echo $dtl['lastName']; ?>"></td><td>Last name</td>
		
	</tr>
        <?php 
            if($dtl['gender']=='M')
                $gen=1;
            else
                $gen=2;
            
            $dob = date('Ymd', strtotime($dtl['dob']));
            $yob = date('Y', strtotime($dtl['dob']));
        ?>
	<tr>
	<td>GENDER</td><td><input name="GENDER" value="<?php echo $gen;?>"></td><td>Gender 0=Unknown,1=Male,2=Female</td>
		
	</tr>
	<tr>
	<td>DOB</td><td><input name="DOB" value="<?php echo $dob;?>"></td><td>Date of birth.



Format YYYYMMDD
</td>
		
	</tr>
	<tr>
	<td>YOB</td><td><input name="YOB" value="<?php echo $yob;?>"></td><td><Year of birth. 
Format YYYY
</td>
		
	</tr>
	<tr>
	<td>STEMAILADDRESS</td><td><input name="STEMAILADDRESS" value=""></td><td>E-mail-address</td>
	</tr>
	
	</tr>
	<tr>
	<td>CLO</td><td><input name="CLO" value="https://mypainimpact.com/assets/img/logo.png"></td><td>Client logo (FULL URL!)</td>
	</tr>

	</tr>
	<tr>
	<td>CLP</td><td><input name="CLP" value="left"></td><td>Client log position</td>
	</tr>
	<tr>
	<td>CEVN</td><td><input name="CEVN" value=""></td>
		<td>EQ-5D Custom field mapper, XXX/YYY,XXX/YYY   XXXX= offical EQ-5D variable name, YYY is name client want to see exported in form or ULR.
		
		Example:
		ANXIETYDEPRESSION/AS,CONTROLNUMBER/MYID,MOBILITY/MB,PAINDISCOMFORT/PD,SELFCARE/SC,USUALACTIVITES/US,VAS/VS
		
		</td>
	</tr>
	
	
	<td colspan="3" align="right">
			<table>
				<tr>
					<td>
					</td>
					<td><input type="submit" >  
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
</body>
<script>
    $(function(){
       $('#euroFrm').submit(); 
    });
</script>
</html>


