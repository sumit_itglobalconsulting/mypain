<?php 
	$caps=array('Not present', 'Not at all', 'Somewhat', 'Moderately', 'Quite a bit');
	$scales=array('Pelvic Organ Prolapse Distress Inventory 6 (POPDI - 6)', 'Colorectal-Anal Distress Inventory 8 (CRAD-8)', 'Urinary Distress Inventory 6 (UDI-6)');
?>
<div class="big mb30">
	<p class="mb5">Please answer all questions. These questions will ask you if you have certain bowel, bladder, or pelvic symptoms and if you do, 
    how much do they bother you?</p>
    <p>Answer these by selecting the appropriate number. While answering, please consider your symptoms over the <strong>last 3 months</strong></p>
</div>
            
<div class="pad3">
    <div class="down">
    	<?php foreach($scales as $k=>$sc){?>
        	<h4 class="panel-title mb20"><?php echo $scales[$k];?></h4>
        
            <table width="100%" cellspacing="1" cellpadding="6" class="sTbl">
                <tr>
                    <td width="20px"></td>
                    <td align="center"><strong>Do you</strong></td>
                    <?php foreach($caps as $c){?>
                    <td width="90px" align="center"><?php echo $c;?></td>
                    <?php }?>
                </tr>
                    
                <?php 
					if($k==0){
						$s=0; $e=5;
					}
					else if($k==1){
						$s=6; $e=13;
					}
					else{
						$s=14; $e=19;
					}
					
					for($i=$s; $i<=$e; $i++){
						$dtl=$ques[$i];
                        $options=getOptionDetail($dtl['optionDtl']);
                        $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
                        $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
                        $dtl['score']=$quesDtl[$dtl['id']]['score'];
                ?>
                    
                    <tr class="quesRw">
                        <td align="right" valign="top"><?php echo $i-$s+1;?>.</td>
                        <td valign="top"><?php echo $dtl['ques'];?></td>
                       
                        <?php foreach($options as $c=>$opt){?>
                            <td valign="top">
                                <div style="padding-top:5px">
                                    <div class="rdio rdio-primary tooltips" data-placement="top" title="<?php echo $caps[$c];?>" style="width:20px; margin:0 auto; overflow:hidden">
                                        <input type="radio" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                        id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                                        <label for="optR<?php echo $opt['optionId'];?>"></label>
                                    </div>
                                </div>
                            </td>
                        <?php }?>
                    </tr>
                <?php }?>
            </table>
            
            <div class="mb30"></div>
        <?php }?>
    </div>
</div>

<script type="text/javascript">
</script>