<?php if(trim($formDtl['formInfo'])){?>
    <h4 class="panel-title mb10">Chief Complaint: <?php echo $formDtl['symptom'];?></h4>
    <div class="mb30">
        Since beginning treatment at this clinic, how would you describe the change (if any) in ACTIVITY LIMITATIONS, SYMPTOMS, EMOTIONS, and OVERALL QUALITY OF LIFE, 
        related to your painful condition? 
        <div style="font-weight:bold; margin-top:4px">
            Please select the response below, that matches your degree of change since beginning care at this clinic for the above stated 
            chief complaint.
        </div>
    </div>
<?php }?>


<h4 class="panel-title mb30">Patient Global Impression of change</h4>

<div class="pgic">
	<?php 
        $c=0; 
		for($i=0; $i<count($ques)-1; $i++){
			$dtl=$ques[$i];
            $c=$c+1; 
            $dtl['ques']=str_replace('{SYMPTOM}', $formDtl['symptom'], $dtl['ques']); 
            $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
            $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
            $dtl['score']=$quesDtl[$dtl['id']]['score'];
        ?>
        
        <input type="hidden" name="scaleId[<?php echo $dtl['id'];?>]" value="<?php echo $dtl['scaleId'];?>" />
        
        <div class="quesBx big qbox<?php echo $i;?>">
            <?php echo '<span class="quesNo">'.$c.'.</span> '.$dtl['ques'];?>
            <div class="clr">&nbsp;</div>
        </div>
        <div class="ansBx" quesno="<?php echo $i;?>">
            <?php 
                $options=getOptionDetail($dtl['optionDtl']);
                slideOptions($options, $dtl, $patientDtl, $formDtl, true);
            ?>
        </div>
        
    <?php }?>
</div>

<p>&nbsp;</p>
            
<?php 
	$c=0; 
	foreach($ques as $dtl){
		$c=$c+1; 
		$dtl['ques']=str_replace('{SYMPTOM}', $formDtl['symptom'], $dtl['ques']); 
		if($dtl['type']==1) continue;
		
		$dtl['optionName']	=$quesDtl[$dtl['id']]['optionName'];
		$dtl['optionId']	=$quesDtl[$dtl['id']]['optionId'];
		$dtl['score']		=$quesDtl[$dtl['id']]['score'];
?>
	<input type="hidden" name="scaleId[<?php echo $dtl['id'];?>]" value="<?php echo $dtl['scaleId'];?>" />
    <div class="quesBx big">
        <?php echo $dtl['ques'];?>
        <div class="clr">&nbsp;</div>
    </div>
    <p>&nbsp;</p>
    <div class="ansBx" style="padding-left:45px">
        <?php 
            $options=getOptionDetail($dtl['optionDtl']);
            switch($dtl['type']){
                case 2:
                    slideOptions2ndForm($options, $dtl, $patientDtl, $formDtl);
                break;
            }
        ?>
    </div>
<?php }?>


<script type="text/javascript">
$(document).ready(function() {
	$(".quesSlide").each(function(){
		obj=$(this);
		V=obj.attr('v')*1;
		
		obj.slider({
			range: "min",
			min: obj.attr('min')*1,
			max: obj.attr('max')*1,
			step: 1,
			value: V,
			
			slide: function(e, ui){
				v=ui.value;
			},
			
			stop: function( e, ui ){
				v=ui.value;
				$(".quesOpt", $(this)).val($(".optId"+v, $(this).parent()).val());
				
				scrollQues( parseInt($(this).parent().parent().attr("quesno")) + 1 );
			}
		});
	});
});


$(document).ready(function() {
	$(".quesSlide2").each(function(){
		obj=$(this);
		
		V=obj.attr('v')*1;
		
		obj.slider({
			range: "min",
			min: obj.attr('min')*1,
			max: obj.attr('max')*1,
			step: 1,
			value: V,
			
			slide: function(e, ui){
				v=ui.value;
				$(".quesOpt", $(this)).val($(".optId"+v, $(this).parent()).val());
			}
		});
	});
});
</script>