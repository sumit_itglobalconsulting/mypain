<?php if(trim($formDtl['formInfo'])){?>
    <div class="big mb30">
        Thinking about the <strong>last 2 weeks</strong> select your response to the following questions
    </div>
<?php }?>

<?php
$lastQues=end($ques);
?>
            
<div class="pad3">
    <div class="down">
        <table width="100%" cellspacing="1" cellpadding="6" class="sTbl">
        	<tr>
            	<td></td>
                <td align="center">Agree</td>
                <td align="center">Disagree</td>
            </tr>
            <?php 
                for($i=0; $i<count($ques)-1; $i++){ $dtl=$ques[$i];
					$dtl['ques']=str_replace('{SYMPTOM}', $formDtl['symptom'], $dtl['ques']);
                    $options=getOptionDetail($dtl['optionDtl']); 
					
					$t=$options[0];
					$options[0]=$options[1];
					$options[1]=$t;
					
					
					$dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
					$dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
					$dtl['score']=$quesDtl[$dtl['id']]['score'];
					
					$optionId=$dtl['optionId']?$dtl['optionId']:$options[0]['optionId'];
            ?>
            <tr class="quesRw">
                <td>
					<?php echo $dtl['ques'];?>
                </td>
                <?php foreach($options as $c=>$opt){?>
                	<td align="center" width="80px">
                    	<div style="width:20px; height:28px; overflow:hidden; padding:5px 0px 0px 0px">
                            <div class="rdio rdio-primary">
                                <input type="radio" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                <?php setCheckChecked($dtl['optionName'], $opt['option']);?> id="optR<?php echo $opt['optionId'];?>" />
                                <label for="optR<?php echo $opt['optionId'];?>"></label>
                            </div>
                        </div>
                    </td>
                <?php }?>
            </tr>
             <?php }?>
        </table>
    </div>
    
    <?php 
		$dtl=$lastQues;
		$options=getOptionDetail($dtl['optionDtl']);
		
		$dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
		$dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
		$dtl['score']=$quesDtl[$dtl['id']]['score'];
	?>
    <p>&nbsp;</p>
    <div class="quesBx big">
		<?php echo $dtl['ques'];?>
        <div class="clr">&nbsp;</div>
    </div>
    <p>&nbsp;</p>
    
    <div class="startBack">
        <div class="ansBx">
            <?php slideOptions($options, $dtl, $patientDtl, $formDtl);?>
        </div>
    </div>
    
    
</div>

<script type="text/javascript">
$(document).ready(function() {
	$(".quesSlide").each(function(){
		obj=$(this);
		V=obj.attr('v')*1;
		
		obj.slider({
			range: "min",
			min: obj.attr('min')*1,
			max: obj.attr('max')*1,
			step: 1,
			value: V,
			
			slide: function(e, ui){
				v=ui.value;
			},
			
			stop: function( e, ui ){
				v=ui.value;
				$(".quesOpt", $(this)).val($(".optId"+v, $(this).parent()).val());
			}
		});
	});
});
</script>