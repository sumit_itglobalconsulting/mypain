<div class="pad6">
    <h3><?php echo $quesGroup[0]['title'];?></h3>
</div>

<?php $c=0; foreach($ques as $dtl){$c=$c+1; $dtl['ques']=str_replace('{SYMPTOM}', $formDtl['symptom'], $dtl['ques']);?>
    <div class="quesBx big">
        <?php echo '<span class="quesNo">'.$c.'.</span> '.$dtl['ques'];?>
        <div class="clr">&nbsp;</div>
    </div>
    <div class="ansBx">
        <?php 
            $options=getOptionDetail($dtl['optionDtl']);
            switch($dtl['type']){
                case 1:
                    radioOptions($options, $dtl, $patientDtl, $formDtl);
                break;
                
                case 2:
                    slideOptions($options, $qdtl, $patientDtl, $formDtl);
                break;
                
                case 3:
                    checkOptions($options, $dtl, $patientDtl, $formDtl);
                break;
                
                case 4:
                    bodyOptions($options, $dtl, $patientDtl, $formDtl);
                break;
                
                case 5:
                    textInfoOptions($options, $dtl, $patientDtl, $formDtl);
                break;
                
                default:
                    textMsgInfoOptions($options, $dtl, $patientDtl);
                break;
            }
        ?>
    </div>
<?php }?>