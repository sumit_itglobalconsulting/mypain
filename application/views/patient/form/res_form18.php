<div class="big mb30">
	<div class="">
    	Some women find that bladder, bowel or vaginal symptoms affect their activities, relatioships or feelings. For each question, select the response that best 
        describes how much your activities, relatioships or feeling s have been affected by your bladder, bowel or vaginal symptoms or conditions over that 
        <strong>last 3 months</strong>.
    </div>
</div>
            
<div class="pad3">
    <div class="down">
        <table width="100%" cellspacing="1" cellpadding="6" class="sTbl">
            <?php $n1=7; $n2=14; for($i=0; $i<7; $i++){$dtl=$ques[$i];?>
                <?php
                    $options=getOptionDetail($dtl['optionDtl']);
                    $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
                    $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
                    $dtl['score']=$quesDtl[$dtl['id']]['score'];
					
					$dtl1=$ques[$n1++];
					$options1=getOptionDetail($dtl1['optionDtl']);
                    $dtl1['optionName']=$quesDtl[$dtl1['id']]['optionName'];
                    $dtl1['optionId']=$quesDtl[$dtl1['id']]['optionId'];
                    $dtl1['score']=$quesDtl[$dtl1['id']]['score'];
					
					$dtl2=$ques[$n2++];
					$options2=getOptionDetail($dtl2['optionDtl']);
                    $dtl2['optionName']=$quesDtl[$dtl2['id']]['optionName'];
                    $dtl2['optionId']=$quesDtl[$dtl2['id']]['optionId'];
                    $dtl2['score']=$quesDtl[$dtl2['id']]['score'];
                ?>
                
                <tr class="">
                    <td align="right" valign="top" width="20px"><strong><?php echo $i+1;?>.</strong></td>
                    <td valign="top">
						<div class="bold mb5">Usually affect your <?php echo $dtl['ques'];?></div>
                        
                        <table width="100%" cellspacing="1" cellpadding="4" class="sTbl">
                        	<tr>
                            	<td>Bladder or urine</td>
                                <td>Bowel or rectum</td>
                                <td>Vagina or pelvis</td>
                            </tr>
                            
                            <tr>
                            	<td class="quesRw">
                                	<?php foreach($options as $c=>$opt){?>
                                        <div>
                                            <div class="rdio rdio-primary">
                                                <input type="radio" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                                id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                                                <label for="optR<?php echo $opt['optionId'];?>"><?php echo $opt['caption'];?></label>
                                            </div>
                                        </div>
                                    <?php }?>
                                </td>
                                
                                <td class="quesRw">
                                	<?php foreach($options1 as $c=>$opt){?>
                                        <div>
                                            <div class="rdio rdio-primary">
                                                <input type="radio" name="quesOpt[<?php echo $dtl1['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                                id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl1['optionId'], $opt['optionId']);?> />
                                                <label for="optR<?php echo $opt['optionId'];?>"><?php echo $opt['caption'];?></label>
                                            </div>
                                        </div>
                                    <?php }?>
                                </td>
                                
                                <td class="quesRw">
                                	<?php foreach($options2 as $c=>$opt){?>
                                        <div>
                                            <div class="rdio rdio-primary">
                                                <input type="radio" name="quesOpt[<?php echo $dtl2['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                                id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl2['optionId'], $opt['optionId']);?> />
                                                <label for="optR<?php echo $opt['optionId'];?>"><?php echo $opt['caption'];?></label>
                                            </div>
                                        </div>
                                    <?php }?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>

<script type="text/javascript">
</script>