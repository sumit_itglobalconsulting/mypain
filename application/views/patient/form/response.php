<?php 
if (!$formDtl['symptom'])
    $formDtl['symptom'] = 'Presenting Problem';

$qs = arrayUrlDecode($_GET);

$flashMsg = getFlash(); //Lalan Axis Bank Acc: 912010006012361

$resDT = $qs['resDate'];

if (!$resDT)
    $resDT = $resDates[0];

if ($resDates) {
    $firstResDate = $resDates[count($resDates) - 1];
    $lastResDate = $resDates[0];
}
?>

<style type="text/css">
    .frmInfo a		{display:block; margin:5px 0px; padding-left:24px; white-space:normal; line-height:normal; border-radius:0; text-align:left; 
                 position:relative}
    .frmInfo a i	{position:absolute; left:5px; top:5px; font-size:16px}
    .modal iframe	{width:95%; margin:0 auto}
</style>

<div class="pageheader">
    <div class="posRel">
        <div style="position:absolute; width:190px; right:0; top:5px">
            <?php if ($resDates) { ?>
                <form action="<?php echo REQ_URI; ?>" id="resDateForm">
                    <div class="smallForm tooltips" data-placement="top" title="Response on">
                        <select name="resDate" onchange="$('#resDateForm').submit()" class="form-control chosen-select">
                            <option value="">Select Response Date</option>
                            <?php
                            foreach ($resDates as $i => $rd) {
                                $sel = $rd == $qs['resDate'] ? 'selected="selected"' : '';
                                //if(!$qs['resDate'] && $i==0) $sel='selected="selected"';
                                ?>
                                <option value="<?php echo $rd; ?>" <?php echo $sel; ?>><?php echo showDate($rd, true) ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </form>
            <?php } ?>
        </div>

        <div class="responseFormH">
            <h2><i class="fa fa-edit"></i> <?php echo $formDtl['formTitle']; ?></h2>
            <div style="padding-left:2px">
                <?php echo $formDtl['createdBy'] ? $formDtl['createdBy'] : ''; ?>
                <?php if ($patientId) { ?>
                    <h4>Patient: <strong><?php echo $patientDtl['fullName']; ?></strong></h4>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div class="contentpanel">
    <div class="row posRel">
        <div class="pull-left" style="width:850px">
            <?php if ($formId != 19) {
                echo $flashMsg;
            } ?>

            <!-- Errors Msg -->
            <div class="alert alert-danger myhide" id="ferrMsg">Please answer the question(s) marked with red color</div>
            <!-- /Errors Msg -->



            <!-- Due Forms Alert -->
            <?php if ($dueForms && $flashMsg && strpos($flashMsg, 'updated') === false) { ?>
                <?php
                if ($formId == 19) {
                    $this->load->view('patient/form/euro-report');
                }
                ?> 
                <div class="panel panel-default panel-alt">
                    <div class="panel-heading">
                        <div class="panel-btns">
                            <a href="" class="panel-close">&times;</a>
                        </div>
                        <h5 class="panel-title">Next Forms to be submitted</h5>
                    </div>
                    <div class="panel-body">
    <?php foreach ($dueForms as $i => $f) {
        if ($i > 0) break; ?>
                            <h4 class="mb10"><i class="fa fa-list-alt"></i> &nbsp;<?php echo $f['formTitle']; ?></h4>
                            <div>
                                <a href="<?php echo PATIENT_URL . "form/response/" . encode($f['formId']) . "/" . encode($f['doctorId']); ?>" class="btn btn-info btn-sm">
                                    Click here to go to form submission page
                                </a>
                            </div>
                <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <!-- /Due Forms Alert -->

<?php if ($flashMsg) { ?>
                <div style="height:400px"></div>
<?php } ?>

<?php if (!$flashMsg) { ?>

                <?php if($formDtl['formId'] != 8 ) {  ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-info fade in">

                            <h4>Please take a few minutes to complete these forms, so we can understand the impact of pain on your life and form treatment goals. 
                                Each form taken around 2-5 min to complete.</h4>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="responseForm" action="" method="post" onsubmit="return submitResponse()" enctype="multipart/form-data">
                            <input type="hidden" name="formId" value="<?php echo $formDtl['formId']; ?>" />
                            <input type="hidden" name="dueDate" value="<?php echo $formDtl['nextResDueDate']; ?>" />
                            <input type="hidden" name="doctorId" value="<?php echo $doctorId; ?>" />
                            <input type="hidden" name="firstResDate" value="<?php echo $firstResDate; ?>" />
                            <input type="hidden" name="lastResDate" value="<?php echo $lastResDate; ?>" />
                            <input type="hidden" name="resDT" id="resDT" value="" />

                            <?php
                            $this->load->view("patient/form/res_form" . $formDtl['formId'], array('formDtl' => $formDtl, 'ques' => $ques, 'patientDtl' => $patientDtl, 'quesGroup' => $quesGroup, 'quesDtl' => $resDtl['quesDtl']));
                            ?>

    <?php if (!$patientId) { ?>
                                <div class="mb30"></div>
                                <div>
        <?php if ($qs['resDate']) { ?>
                                        <button type="button" class="btn btn-danger" id="updateResBtn" 
                                                style="float:right"><i class="fa fa-edit"></i> Update This Response</button>
        <?php } ?>

                                    <button type="submit" class="btn btn-primary" id="resFormSubBtn">Submit Response</button>&nbsp;&nbsp;
                                    <button type="button" id="resetResBtn" class="btn btn-default">Reset</button>
                                    <div class="clearfix"></div>
                                </div>
                <?php } ?>
                        </form>
                    </div>
                </div>

        <?php } ?>
        </div>


            <?php if (!$flashMsg) { ?>
            <div class="frmInfo posAbs" style="width:14%; max-width:180px; right:0px; top:0px">
                <?php if (USER_TYPE != 'P') { ?>
                    <?php if ($formDtl['infoForClinician']) { ?>
                        <a href="#" data-toggle="modal" data-target="#infoForClinician" class="btn btn-warning"><i class="fa fa-info-circle"></i> Information for clinicians</a>
                    <?php } ?>

        <?php if ($formDtl['functionsOfForm']) { ?>
                        <a href="#" data-toggle="modal" data-target="#functionsOfForm" class="btn btn-warning"><i class="fa fa-info-circle"></i> Functions of this form</a>
                    <?php } ?>
                <?php } ?>


                <?php if ($formDtl['infoForPatient']) { ?>
                    <a href="#" data-toggle="modal" data-target="#infoForPatient" class="btn btn-warning"><i class="fa fa-info-circle"></i> Information for patients</a>
                <?php } ?>

                <?php if (USER_TYPE != 'P') { ?>    
                    <?php if ($formDtl['pdf']) { ?>
                        <a href="#" data-toggle="modal" data-target="#pdf" class="btn btn-warning"><i class="fa fa-info-circle"></i> Preview paper version</a>
                    <?php } ?>

                    <?php if ($formDtl['toolkit']) { ?>
                        <a href="#" data-toggle="modal" data-target="#toolkit" class="btn btn-warning"><i class="fa fa-info-circle"></i> Toolkit</a>
                    <?php } ?>

                    <?php if ($formDtl['userGuide']) { ?>
                        <a href="#" data-toggle="modal" data-target="#userGuide" class="btn btn-warning"><i class="fa fa-info-circle"></i> User Guide</a>
        <?php } ?>
    <?php } ?>

                <?php if ($formDtl['supportingStatement']) { ?>
                    <a href="#" data-toggle="modal" data-target="#supportingStatement" class="btn btn-warning">
                        <i class="fa fa-info-circle"></i> Information for clinicians and patients
                    </a>
    <?php } ?>
            </div>
<?php } ?>

        <div class="clearfix"></div>
    </div>
</div>



<!-- Hidden divs -->
<div class="modal fade bs-example-modal-lg" id="infoForClinician" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title">Information for clinicians</h4>
            </div>
            <div class="modal-body"><?php echo $formDtl['infoForClinician']; ?></div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id="functionsOfForm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title">Functions of this form</h4>
            </div>
            <div class="modal-body"><?php echo $formDtl['functionsOfForm']; ?></div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id="infoForPatient" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title">Information for patients</h4>
            </div>
            <div class="modal-body"><?php echo $formDtl['infoForPatient']; ?></div>
        </div>
    </div>
</div>

<?php if ($formDtl['pdf']) { ?>
    <div class="modal fade bs-example-modal-lg" id="pdf" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title">Preview paper version</h4>
                </div>
                <div class="modal-body">
    <?php echo embedPdf(URL . "assets/uploads/form_pdf/" . $formDtl['pdf']); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($formDtl['toolkit']) { ?>
    <div class="modal fade bs-example-modal-lg" id="toolkit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title">Toolkit</h4>
                </div>
                <div class="modal-body">
    <?php echo embedPdf(URL . "assets/uploads/form_pdf/" . $formDtl['toolkit']); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($formDtl['supportingStatement']) { ?>
    <div class="modal fade bs-example-modal-lg" id="supportingStatement" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title">Information for clinicians and patients</h4>
                </div>
                <div class="modal-body">
    <?php echo embedPdf(URL . "assets/uploads/form_pdf/" . $formDtl['supportingStatement']); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($formDtl['userGuide']) { ?>
    <div class="modal fade bs-example-modal-lg" id="userGuide" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                    <h4 class="modal-title">User Guide</h4>
                </div>
                <div class="modal-body">
    <?php echo embedPdf(URL . "assets/uploads/form_pdf/" . $formDtl['userGuide']); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- /Hidden divs -->


<script type="text/javascript">
    function submitResponse() {
        var error = false;
        var qerr = false;

        if (!subRes) {
            //error=true;
            //$(".message1").fadeIn("fast");
            
        }

        /* Start Back (Form 6) */
        $(".quesRw").each(function () {
            flg = 0; 
            $("input:radio", $(this)).each(function () { 
                if ($(this).prop("checked"))
                    flg = 1;
            });

            if (flg == 0) {
                qerr = true;
                $(this).addClass("quesErr");
            }
            else
                $(this).removeClass("quesErr");
        });

        if (qerr)
            $("#ferrMsg").show();
        else
            $("#ferrMsg").hide();

        if (!error && !qerr)
            return true;
        else
            return false;
    }

    function scrollQues(n) {
        setTimeout(function () {
            if ($(".qbox" + n).length) {
                T = $(".qbox" + n).offset().top - 80;
                var body = $("html, body");
                //body.animate({scrollTop:T}, '300');
            }
        },
                1000);
    }

    $("#resetResBtn").click(function () {
        $(".quesSlide, .quesSlide2").each(function () {
            obj = $(this);
            v = obj.attr('min') * 1;

            if (obj.hasClass('quesSlide2'))
                v = 0;

            obj.slider({value: v});
            $(".quesOpt", obj).val($(".optId" + v, obj.parent()).val());
        });

        $("#responseForm input, #responseForm textarea, #responseForm select").each(function () {
            $(this).val('');
            $(this).prop('checked', false);
        });
    });
</script>

<?php if (strpos($_SERVER['REQUEST_URI'], 'formPreview') !== false) { ?>
    <script type="text/javascript">
        $(document).ready(function () {
            //$("#responseForm input, #responseForm button").prop('disabled', true);
            $(".quesSlide").slider('disable');
            $(".quesSlide2").slider('disable');
        });
    </script>
<?php } else { ?>

    <div class="message1 myhide" style="font-size:16px" center="true">You can submit form in <span id="sec">60</span> Seconds</div>
    <script type="text/javascript">
        $(document).ready(function () {
            jQuery(".chosen-select").chosen({'width': '100%', 'white-space': 'nowrap', disable_search: true});
        });

        var TI = 0;
        var subRes = false;
        function submitTimer() {
            v = parseInt($("#sec").text());
            if (v <= 1) {
                clearInterval(TI);
                subRes = true;
                $("#resFormSubBtn").prop("disabled", false);
                $(".message1").fadeOut("slow");
            }
            else {
                v--;
                $("#sec").text(v);
            }
        }
        TI = setInterval(function () {
            submitTimer()
        }, 1000);

        /** Delete Response **/
        $("#updateResBtn").click(function () {
            if (confirm("Are you sure to update this response?")) {
                $("#resDT").val('<?php echo $resDT; ?>');
                $("#responseForm").submit();
            }
        });

        /** Navigation Alert If Form not submitted **/
        var submitted = '<?php echo $_SESSION['responsed']; ?>';
        var userType = '<?php echo USER_TYPE; ?>';

        if (submitted != '1' && userType == 'P') {
            $("section a").click(function (e) {
                e.preventDefault();
                url = $(this).attr('href');
                if (!url || url == "" || url == "#" || url == "javascript:void(0)")
                    return;

                $("#okLinkBtn").attr('href', url);
                showDialog($("#navConfirmBx").html(), "Navigation Alert", true);
            });
        }
    </script>
<?php } ?>


<!-- Alerts -->
<div id="navConfirmBx" class="myhide">
    <div align="center" class="smallForm" style="padding-top:10px">
        <p><strong>You have not submitted the from.</strong></p>
        <p>Are you sure to leave this page?</p>
        <p>&nbsp;</p>
        <div>
            <a href="" id="okLinkBtn" class="btn btn-primary">Yes</a>
            &nbsp;&nbsp;&nbsp;
            <button class="btn btn-default" onclick="closeDialog()">No</button>
        </div>
    </div>
</div>

<?php
unset($_SESSION['responsed']);
?>
