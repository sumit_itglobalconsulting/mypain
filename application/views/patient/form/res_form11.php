<?php 
	$caps=array('Never/ Once', 'Several days', 'More than half of the days', 'Almost everyday');
?>
<div class="big mb30">
	Please select the frequency with which you have felt the following symptoms for the last 2 weeks.
</div>
            
<div class="pad3">
    <div class="down">
    	<div class="big">
        	During the <strong>last 2 weeks</strong> how often have you experienced these symptoms with sufficient intensity as to cause discomfort or affect your daily life?
        </div>
        <p>&nbsp;</p>
        <table width="100%" cellspacing="1" cellpadding="6" class="sTbl">
        	<tr>
                <td width="20px"></td>
                <td></td>
                <?php foreach($caps as $c){?>
                <td width="120px" align="center"><?php echo $c;?></td>
                <?php }?>
            </tr>
                
            <?php for($i=0; $i<count($ques); $i++){$dtl=$ques[$i];?>
                <?php
                    $options=getOptionDetail($dtl['optionDtl']);
                    $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
                    $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
                    $dtl['score']=$quesDtl[$dtl['id']]['score'];
                ?>
                
                <tr class="quesRw">
                    <td align="right" valign="top"><?php echo $i%3==0?ceil(($i+1)/3).'.':'';?></td>
                    <td valign="top"><?php echo $dtl['ques'];?></td>
                   
                    <?php foreach($options as $c=>$opt){?>
                        <td valign="top">
                        	<div style="padding-top:5px">
                                <div class="rdio rdio-primary tooltips" data-placement="top" title="<?php echo $caps[$c];?>" style="width:20px; margin:0 auto; overflow:hidden">
                                    <input type="radio" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                    id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                                    <label for="optR<?php echo $opt['optionId'];?>"></label>
                                </div>
                            </div>
                        </td>
                    <?php }?>
                </tr>
            <?php }?>
        </table>
    </div>
</div>

<script type="text/javascript">
</script>