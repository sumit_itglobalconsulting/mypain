<?php
	$catsArr=multiArrToKeyValue($cats, "id", "name");
?>

<div class="pageheader">
	<div class="row">
        <div class="pull-left" style="padding:0px 0px 0px 10px">
            <h2><i class="fa fa-edit"></i> My Pain Forms</h2>
        </div>
        
        <div class="pull-right" style="padding:4px 10px 0px 0px; width:300px">
        	<form id="searchForm" method="get">
        		<?php echo form_dropdown('catId', array(0=>'All Categories')+$catsArr, $_GET['catId'], 'id="catId" class="form-control chosen-select"');?>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


<div class="contentpanel">
	<div class="people-list myListBx1">
        <div class="pad1">
            <div class="row">
                <?php if($list){foreach($list as $i=>$dtl){$resDtl=isFormSubmited(USER_ID, $dtl['formId']);?>
                    <div class="col-md-3">
                        <div class="people-item">
                            <div class="media" align="center">
                                <?php if(isTodayOrBefore($dtl['nextResDueDate']) || $resDtl){?>
                                    <a href="<?php echo PATIENT_URL."form/response/".encode($dtl['formId'])."/".encode($dtl['doctorId']);?>">
                                        <img alt="" src="<?php echo base_url("assets/uploads/form_images/".$dtl['image']);?>">
                                    </a>
                                <?php }else{?>
                                    <img src="<?php echo base_url("assets/uploads/form_images/".$dtl['image']);?>" alt="" />
                                <?php }?>
                            </div>
                            <div class="mb5"></div>
                            
                            <div style="padding:0 10px; min-height:85px" align="center">
                                <?php if($resDtl){?>
                                    <?php if($dtl['resFreq']=='O'){?>
                                        <div>Form submitted on:</div> <div class="myred"><?php echo showDate($resDtl['resDate'], true);?></div>
                                    <?php }else{?>
                                    	<div>Last response on:</div>
                                        <div class="myred"><?php echo showDate($resDtl['resDate'], true);?></div>
                                        
                                        <div>Next Date:</div>
                                        <div class="myred"><?php echo showDate($dtl['nextResDueDate'], true);?></div>
                                    <?php }?>
                                <?php }else{?>
                                    <div>Response Date:</div>
                                    <div class="myred"><?php echo showDate($dtl['nextResDueDate']);?></div>
                                <?php }?>
                            </div>
                            
                            <?php $st=''; if( !(isTodayOrBefore($dtl['nextResDueDate']) || $resDtl) ){$st='style="visibility:hidden"';}?>
                            <div <?php echo $st;?>>
                                <div class="mb5"></div>
                                <a href="<?php echo PATIENT_URL."form/response/".encode($dtl['formId'])."/".encode($dtl['doctorId']);?>">
                                    <button type="button" class="btn btn-warning btn-block">Submit Form</button>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php }}else{?>
                	<div class="notFound">No form found.</div>
                <?php }?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#catId").change(function(e) {
	$("#searchForm").submit();
});
</script>