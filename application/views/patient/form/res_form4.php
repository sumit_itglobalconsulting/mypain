<?php if(trim($formDtl['formInfo'])){?>
    <div class="big mb30">
        Over the <strong>last 2 weeks,</strong> how often have you been bothered by any of the following problems?
    </div>
<?php }?>

<div class="phqForm">
	<?php 
        $c=0; foreach($ques as $i=>$dtl){
            $c=$c+1; 
            $dtl['ques']=str_replace('{SYMPTOM}', $formDtl['symptom'], $dtl['ques']); 
            $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
            $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
            $dtl['score']=$quesDtl[$dtl['id']]['score'];
        ?>
        
        <div class="<?php //echo $c==10?'bdrmark':''?>">
            <div class="quesBx big qbox<?php echo $i;?>">
                <?php echo ($c<10?'<span class="quesNo">'.$c.'.</span> ':'').$dtl['ques'];?>
                <div class="clr">&nbsp;</div>
            </div>
            <div class="ansBx" quesno="<?php echo $i;?>">
                <?php 
                    $options=getOptionDetail($dtl['optionDtl']);
                    slideOptions($options, $dtl, $patientDtl, $formDtl);
                ?>
            </div>
        </div>
        
    <?php }?>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$(".quesSlide").each(function(){
		obj=$(this);
		V=obj.attr('v')*1;
		
		obj.slider({
			range: "min",
			min: obj.attr('min')*1,
			max: obj.attr('max')*1,
			step: 1,
			value: V,
			
			slide: function(e, ui){
				v=ui.value;
			},
			
			stop: function( e, ui ){
				v=ui.value;
				$(".quesOpt", $(this)).val($(".optId"+v, $(this).parent()).val());
				
				scrollQues( parseInt($(this).parent().parent().attr("quesno")) + 1 );
			}
		});
	});
});
</script>