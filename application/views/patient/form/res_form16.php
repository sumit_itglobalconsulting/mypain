<div class="big mb30">
	<div class="mb5">
    	This questionnaire has been designed to give us information as to how your neck pain has affected your ability to manage in everyday life.
    </div>
	<strong>Please answer every section and select only one response in each section that most closely describes your problem.</strong>
</div>
            
<div class="pad3">
    <div class="down">
        <table width="100%" cellspacing="1" cellpadding="6" class="sTbl">
            <?php for($i=0; $i<count($ques); $i++){$dtl=$ques[$i];?>
                <?php
                    $options=getOptionDetail($dtl['optionDtl']);
                    $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
                    $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
                    $dtl['score']=$quesDtl[$dtl['id']]['score'];
                ?>
                
                <tr class="quesRw">
                    <td align="right" valign="top" width="20px"><strong><?php echo $i+1;?>.</strong></td>
                    <td valign="top">
						<div class="bold mb5"><?php echo $dtl['ques'];?></div>
                        <?php foreach($options as $c=>$opt){?>
                        	<div>
                            	<div class="rdio rdio-primary">
                                	<input type="radio" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                    id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                                    <label for="optR<?php echo $opt['optionId'];?>"><?php echo $opt['caption'];?></label>
                                </div>
                            </div>
                        <?php }?>
                    </td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>

<script type="text/javascript">
</script>