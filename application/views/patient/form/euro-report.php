<div class="panel panel-default panel-alt">
    <div class="panel-heading">
                    <h2>Thank you for completing EQ-5D.</h2>
                    <h4>Your scores on the EQ-5D dimensions are compared to the reference score of people in the United Kingdom of your gender and age range (disclaimer). You also find the Visual Analogue Scale (EQ-VAS) score of how you indicated to feel today compared to people of your gender and age range in the United Kingdom. You can download the results as a PDF file, or you can click "Back to Start" to try again.</h4>
                    <div class='clearfix'></div>
                    <div class="userdata">
<table>
<tbody>
<tr>
    <td width="160"><h4><strong>Age group</strong></h4></td>
<td>25 - 34
</td>
</tr>
<tr>
<td><h4><strong>Gender</strong></h4></td>
<td>Male</td>
</tr>
<tr>
<td><strong><h4>Exercise</strong></h4></td>
<td>EQ-5D-3L</td>
</tr>
<tr>
<td><strong><h4>Country</strong></h4></td>
<td>United Kingdom</td>
</tr>
</tbody>
</table>
</div>
                    <div class="results">
<div class="dimension levels">
<div class="resultblock">
<h3>&nbsp;</h3>
<div class="part noopacity">
<div class="level">No problems</div> 
</div>
<div class="part noopacity">
<div class="level">Some problems</div> 
</div>
<div class="part noopacity">
<div class="level">Unable to</div> 
</div>
</div></div>
<div class="dimension">
<div class="resultblock">
<h3>
Mobility
</h3>
<div class="part">
<div class="percentage"> 94.2%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title=" 94.2%" data-percentage=" 94.2%"></div>
</div>
</div>
<div class="part chosen">
<div class="percentage">5.5%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title="5.5%" data-percentage="5.5%"></div>
</div>
</div>
<div class="part">
<div class="percentage">0.3%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title="0.3%" data-percentage="0.3%"></div>
</div>
</div>
<p>
You indicated to have some problems in walking about.
 94.2% 
of the general population in 
the United Kingdom 
in your age group has less problems and 
0.3% 
has more problems.
</p>
</div>
</div>
<div class="dimension">
<div class="resultblock">
<h3>
Self Care
</h3>
<div class="part">
<div class="percentage"> 99.1%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title=" 99.1%" data-percentage=" 99.1%"></div>
</div>
</div>
<div class="part chosen">
<div class="percentage">0.6%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title="0.6%" data-percentage="0.6%"></div>
</div>
</div>
<div class="part">
<div class="percentage">0.3%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title="0.3%" data-percentage="0.3%"></div>
</div>
</div>
<p>
You indicated to have some problems washing or dressing yourself.
>99%
of the general population in 
the United Kingdom 
in your age group has less problems and 
&lt;1%
has more problems.
</p>
</div>
</div>
<div class="dimension">
<div class="resultblock">
<h3>
Usual activities
</h3>
<div class="part">
<div class="percentage"> 92.1%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title=" 92.1%" data-percentage=" 92.1%"></div>
</div>
</div>
<div class="part chosen">
<div class="percentage">7.0%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title="7.0%" data-percentage="7.0%"></div>
</div>
</div>
<div class="part">
<div class="percentage">0.9%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title="0.9%" data-percentage="0.9%"></div>
</div>
</div>
<p>
You indicated to have some problems performing your usual activities.
 92.1% 
of the general population in 
the United Kingdom 
in your age group has less problems and 
0.9% 
has more problems.
</p>
</div>
</div>
<div class="clearfix"></div>
<div class="dimension levels">
<div class="resultblock">
<h3>&nbsp;</h3>
<div class="part noopacity">
<div class="level">No</div> 
</div>
<div class="part noopacity">
<div class="level">Some</div> 
</div>
<div class="part noopacity">
<div class="level">Extreme</div> 
</div>
</div></div>
<div class="dimension">
<div class="resultblock">
<h3>
Pain / Discomfort
</h3>
<div class="part">
<div class="percentage"> 85.5%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title=" 85.5%" data-percentage=" 85.5%"></div>
</div>
</div>
<div class="part">
<div class="percentage"> 13.3%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title=" 13.3%" data-percentage=" 13.3%"></div>
</div>
</div>
<div class="part chosen">
<div class="percentage">1.2%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title="1.2%" data-percentage="1.2%"></div>
</div>
</div>
<p>
You indicated to have extreme pain or discomfort.
 98.8% 
of the general population in 
the United Kingdom 
in your age group has less problems.
</p>
</div>
</div>
<div class="dimension">
<div class="resultblock">
<h3>
Anxiety / Depression
</h3>
<div class="part">
<div class="percentage"> 86.7%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title=" 86.7%" data-percentage=" 86.7%"></div>
</div>
</div>
<div class="part">
<div class="percentage"> 12.1%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title=" 12.1%" data-percentage=" 12.1%"></div>
</div>
</div>
<div class="part chosen">
<div class="percentage">1.2%</div>
<div class="bar">
<div class="percent animatePercentage tooltipblack" title="1.2%" data-percentage="1.2%"></div>
</div>
</div>
<p>
You indicated to be extremly anxious or depressed.
 98.8% 
of the general population in 
the United Kingdom 
in your age group has less problems.
</p>
</div>
</div>
</div>
                        <div>
                            <a href="<?php echo PATIENT_URL."form/response/".encode($f['formId'])."/".encode($f['doctorId']);?>" class="btn btn-info btn-sm">
                                Click here to go to form submission page
                            </a>
                        </div>
                </div>
    
					
        
                    
              
</div>