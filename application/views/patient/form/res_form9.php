<?php if(trim($formDtl['formInfo'])){?>
    <div class="big mb30">
        <?php echo $formDtl['formInfo'];?>
    </div>
<?php }?>

<?php
$firstQues=$ques[0];
$lastQues=end($ques);
?>
            
<div class="pad3">
    <div class="down">
    	<div class="quesBx big"><span class="quesNo">1.</span><?php echo $firstQues['ques'];?></div>
    	<div class="ansBx">
        	<?php
				$dtl=$firstQues; 
				$options=getOptionDetail($dtl['optionDtl']);
				$dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
				$dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
				$dtl['score']=$quesDtl[$dtl['id']]['score'];
			?>
            
            <table width="100%" cellspacing="1" cellpadding="6" class="sTbl quesRw">
            	<?php $n=64; foreach($options as $c=>$opt){$n++;?>
                	<tr>
                    	<td width="20px" align="right"><?php echo chr($n);?>.</td>
                    	<td><?php echo $opt['option'];?></td>
                        <td width="100px" align="center">
                        	<div style="width:20px; height:28px; overflow:hidden; padding:5px 0px 0px 0px">
                            	<div class="rdio rdio-primary">
                                    <input type="radio" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" id="optR<?php echo $opt['optionId'];?>"
                                    <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                                    <label for="optR<?php echo $opt['optionId'];?>"></label>
                            	</div>
                            </div>
                        </td>
                    </tr>
                <?php }?>
            </table>
        </div>
        
        <p>&nbsp;</p>
        
        <div class="quesBx big"><span class="quesNo">2.</span>During the last week, how many hours did you spend on each of the following activities?</div>
        
        <div class="ansBx">
            <table width="100%" cellspacing="1" cellpadding="6" class="sTbl">
                <tr class="tdMid">
                    <td></td>
                    <td></td>
                    <td>None</td>
                    <td>Some but less than 1 hour</td>
                    <td>1 hour but less than 3 hours</td>
                    <td>3 hours or more</td>
                </tr>
                    
                <?php $n=64; for($i=1; $i<=5; $i++){$dtl=$ques[$i]; $n++;?>
                    <?php
                        $options=getOptionDetail($dtl['optionDtl']);
                        $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
                        $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
                        $dtl['score']=$quesDtl[$dtl['id']]['score'];
                    ?>
                    
                        <tr class="quesRw">
                            <td width="20px" align="right"><?php echo chr($n);?>.</td>
                            <td><?php echo $dtl['ques'];?></td>
                            <?php foreach($options as $c=>$opt){?>
                                <td width="100px" align="center">
                                    <div style="width:20px; height:28px; overflow:hidden; padding:5px 0px 0px 0px">
                                        <div class="rdio rdio-primary">
                                            <input type="radio" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                            id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                                            <label for="optR<?php echo $opt['optionId'];?>"></label>
                                        </div>
                                    </div>
                                </td>
                            <?php }?>
                        </tr>
                <?php }?>
            </table>
        </div>
        
        <p>&nbsp;</p>
        <?php
			$dtl=$lastQues; 
			$options=getOptionDetail($dtl['optionDtl']);
			$dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
			$dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
			$dtl['score']=$quesDtl[$dtl['id']]['score'];
		?>
        <div class="quesBx big"><span class="quesNo">3.</span><?php echo $dtl['ques'];?></div>
        
        <div class="ansBx quesRw">
        	<?php foreach($options as $c=>$opt){?>
                <div class="col-md-6">
                    <div class="rdio rdio-primary">
                        <input type="radio" id="optr<?php echo $c;?>" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                        <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                        <label for="optr<?php echo $c;?>"><?php echo $opt['option'];?></label>
                    </div>
                </div>
            <?php }?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
</script>