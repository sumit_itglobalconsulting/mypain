<?php 
?>
<div class="big mb30">
	<p class="mb5">These questions ask about your sexual feelings and responses.</p>
	<p>During the <strong>past 4 weeks</strong>. Please answer the following questions as honestly and clearly as possible. Your responses will be kept completely confidential.</p>
    <div class="mb5"></div>
	<p class="mb5">In answering these questions the following definitions apply:</p>
	
    <div style="padding-left:10px">
		<p>1. Sexual activity can include caressing, foreplay, masturbation and vaginal intercourse.</p>
		<p>2. Sexual intercourse is defined as penile penetration (entry) of the vagina.</p>
		<p>3. Sexual stimulation includes situations like foreplay with a partner, self-stimulation (masturbation), or sexual fantasy.</p>
    </div>
</div>
            
<div class="pad3">
    <div class="down">
    	<div class="big">
        	<strong>Check only one option per question.</strong>
        </div>
        <p>&nbsp;</p>
        <table width="100%" cellspacing="1" cellpadding="6" class="sTbl">
            <?php for($i=0; $i<count($ques); $i++){$dtl=$ques[$i];?>
                <?php
                    $options=getOptionDetail($dtl['optionDtl']);
                    $dtl['optionName']=$quesDtl[$dtl['id']]['optionName'];
                    $dtl['optionId']=$quesDtl[$dtl['id']]['optionId'];
                    $dtl['score']=$quesDtl[$dtl['id']]['score'];
                ?>
                
                <tr class="quesRw">
                    <td align="right" valign="top" width="20px"><strong><?php echo $i+1;?>.</strong></td>
                    <td valign="top">
						<div class="bold mb5"><?php echo $dtl['ques'];?></div>
                        <?php foreach($options as $c=>$opt){?>
                        	<div>
                            	<div class="rdio rdio-primary">
                                	<input type="radio" name="quesOpt[<?php echo $dtl['id'];?>]" value="<?php echo $opt['optionId'];?>" 
                                    id="optR<?php echo $opt['optionId'];?>" <?php setCheckChecked($dtl['optionId'], $opt['optionId']);?> />
                                    <label for="optR<?php echo $opt['optionId'];?>"><?php echo $opt['caption'];?></label>
                                </div>
                            </div>
                        <?php }?>
                    </td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>

<script type="text/javascript">
</script>