<div class="pageheader">
	<div class="row">
    <div class="col-md-8">
            <h2><i class="fa fa-question-circle"></i>Manage Help Issue Category</h2>
        </div>
        <div class="col-md-4" style="padding-top:4px" align="right">
        	<a href="<?php echo URL."faq/addHelpCategory";?>">
            	<button type="button"class="btn btn-primary">+ Add New</button>
            </a>
       
        </div>
    </div>
</div>

<div class="" style="background: #fff;">
    
    <div class="table-responsive">
        <?php if(count($result)){?>
        <table class="table table-striped mb30">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    <th width="85%">Issue Category Name</th>
                    <th width="10%">
                    	Action
                    </th>
                </tr>
            </thead>
            
            <tbody>
                <?php foreach($result as $i=>$dtl){?>
                    <tr>
                        <td><?php echo ($i + 1 ) ;?></td>
                        <td>
                            <?php echo $dtl['name'];?>                            
                        </td>
                        <td>
                            <a href="<?php echo URL."faq/editHelpCategory/".encode($dtl['id']);?>">Edit</a> &nbsp;&nbsp;| &nbsp;&nbsp;  
                            <a href="<?php echo URL."faq/deleteHelpCategory/".encode($dtl['id']);?>" >Delete</a>  
                                                      
                        </td>                                   
                    </tr>
                <?php }?>
            </tbody>
        </table>
        <?php if($page['total_pages']>1){?>
            <div>
                <div class="pull-left" style="width:50px; padding:6px 0px 0px 0px">Pages: </div>
                
                <div class="pull-left" style="width:80%"> 
                    <ul class="pagination nomargin">
                        <?php pagingLinksLI($page, URL."faq/listsHelp", 'active'); ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php }?>
        
        <?php }else{?>
            <div class="notFound" style="background-color: #fff;">Not Found.</div>
        <?php }?>
        
    </div>
</div>
