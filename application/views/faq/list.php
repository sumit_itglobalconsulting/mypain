<?php 
	$loggedUserData = loggedUserData();
?>

<div class="pageheader">
	<div class="row">
    <div class="col-md-8">
        <h2><i class="fa fa-question"></i><?php if($userType == 'T'):?>Manage<?php endif;?> FAQ's</h2>
    </div>
    <div class="col-md-4" style="padding-top:4px" align="right">
    <?php if($userType == 'T'):?>
        <a href="<?php echo URL."faq/add";?>">
            <button type="button"class="btn btn-primary">+ Add New</button>
        </a>
    <?php else :?>
        <a href="<?php echo URL."support/lists"?>" class="btn btn-primary btn-xs">Support Ticket</a>
    <?php endif;?>
    </div>
    </div>
</div>

<form id="searchForm" class="form-horizontal " action="<?php echo URL."faq/lists";?>" method="get" >
<div class="row" style="padding: 20px; background-color: #fff; margin-left:0px">
    <?php echo getFlash();?>
    <?php if($userType == 'T'):?>
    <div class="col-md-2 form-group" style="text-align: right;" align="right">
            <?php
                $categoryList = userCategoryList();
				echo form_dropdown("category", $categoryList, $category,' class="form-control" style="width: 160px;"');
			?>
    </div>
    <?php endif;?>
    <div class="col-md-3 form-group">
        <input type="text" name="search" value="<?php echo h($search);?>" class="form-control" placeholder="Enter keyword" />
    </div>
    <div class="col-md-7 "  class="form-group">
            <input type="submit" class="btn btn-primary fa fa-search" onclick="submitSearch()" name="submit" value="Search">                
    </div>

</div>
</form>


<div class="" style="background: #fff;">
    
    <div class="table-responsive">
        <?php if(count($result)){?>
        <table class="table table-striped mb30">
            <tbody>
                <?php foreach($result as $i=>$dtl){?>
                    <tr>
                        <td>
                           <?php if($dtl['type'] == 'faq'): ?>
                            
                            <div class="row">
                                <div class="col-md-1 text-center"><strong><?php echo 'Q';?>.</strong></div>
                                <div class="col-md-10"><?php echo h($dtl['question']);?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1 text-center"><strong>Answer.</strong></div>
                                <div class="col-md-10">
                                    <div id="answer_less_<?php echo $dtl['id'];?>">
                                        <p><?php echo h(substr($dtl['answer'], 0,300));?> &nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" onclick="$('#answer_more_<?php echo $dtl['id'];?>').show();$('#answer_less_<?php echo $dtl['id'];?>').hide();">Read More</a></p>
                                    </div>
                                    <div style="display: none;" id="answer_more_<?php echo $dtl['id'];?>">    
                                        <p><?php echo h($dtl['answer']);?>&nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" onclick="$('#answer_more_<?php echo $dtl['id'];?>').hide();$('#answer_less_<?php echo $dtl['id'];?>').show();">Less</a></p>
                                    </div>    
                                </div>
                            </div>
                            <?php if($userType == 'T'):?>
                            <div class="row">
                                <div class="col-md-1 text-center">Category</div>
                                <div class="col-md-2 "><?php echo $categoryList[$dtl['category']];?></div>
                                <div class="col-md-6">&nbsp;</div>
                                <div class="col-md-1" style="float: right;"><a href="<?php echo URL."faq/edit/".encode($dtl['id']);?>">Edit</a></div>    
                                <?php if($dtl['visible'] == '1'){?>                        
                                    <div class="col-md-1" style="float: right;"><a href="<?php echo URL."faq/hide/".encode($dtl['id']);?>">Hide</a></div>
                                <?php } else {?>
                                    <div class="col-md-1" style="float: right;"><a href="<?php echo URL."faq/hide/".encode($dtl['id']);?>">Show</a></div>
                                <?php }?>
                                <div class="col-md-1 " style="float: right;"><a href="<?php echo URL."faq/delete/".encode($dtl['id']);?>" >Delete</a></div>                                
                            </div>
                            <?php endif;?>
                            
                            <?php elseif($dtl['type'] == 'ticket'): ?>
                            <?php $messageReplyList = $this->ticket->getTicketReply($dtl['id']); ?>
                            <div class="row">
                                <div class="col-md-2 "><strong>Ticket Number:- <label class="error"><?php echo $dtl['ticketNo'];?></label></strong></div>
                                <div class="col-md-6 text-center " ><strong>Issue:- <label class="error"><?php echo $dtl['helpCategory'];?></label></strong></div>
                                <div class="col-md-4 " style="text-align: right;"><strong><?php echo dateFormat($dtl['created']);?></strong></div>                                
                            </div>
                            <div class="row">
                                <div class="col-md-2 "><strong>From:- <label class="" style="color: orange;"><?php echo $dtl['name'];?></label></strong></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1 text-center"><strong><?php echo 'Q';?>.</strong></div>
                                <div class="col-md-10"><?php echo h($dtl['question']);?></div>
                            </div>
                            <div class="row" style="padding-top: 10px;">
                                <div class="col-md-1 "></div>
                                <div class="col-md-10 ">
                                    <?php if(count($messageReplyList)):?>
                                        <a href="javascript:void(0);?>" onclick="$('#replyMessage_<?php echo $dtl['id'];?>').toggle();"><span id="count_total_reply_<?php echo $dtl['id'];?>"><?php echo count($messageReplyList);?></span> Earlier Convertation with this thread</a>
                                    <?php else:?>
                                        <a href="javascript:void(0);?>"><span id="count_total_reply_<?php echo $dtl['id'];?>">0</span> Earlier Convertation with this thread</a>
                                    <?php endif;?>
                                    
                                </div>                                
                                <div class="col-md-1 " style="float: right; "><a href="javascript:void(0);" onclick="$('#replyTextarea_<?php echo $dtl['id'];?>').toggle();" >Reply</a></div>                                
                            </div>
                            <div class="row bg-info" id="replyMessage_<?php echo $dtl['id'];?>" style="margin: 0px !important; display: none; padding: 5px;max-height: 200px;overflow: auto;">
                                <?php if(count($messageReplyList)):
                                    foreach($messageReplyList as $messageReplyRow):
                                ?>
                                <div class="row" style="margin: 0px !important;">
                                    <div class="row" style="margin: 0px !important;">
                                        <div class="col-md-2 "><strong>From:- <label class="" style="color: orange;"><?php echo $messageReplyRow['name'];?></label></strong></div>
                                        <div class="col-md-6 text-center " ></div>
                                        <div class="col-md-4 " style="text-align: right;"><strong><?php echo dateFormat($messageReplyRow['created']);?></strong></div>                                
                                    </div>
                                    <div class="row" style="margin: 0px !important;">
                                        <div class="col-md-1 text-center"><strong><?php echo 'R';?>.</strong></div>
                                        <div class="col-md-10"><?php echo h($messageReplyRow['reply']);?></div>
                                    </div>
                                    <hr style="margin-bottom: 0px;" />
                                </div>
                                <?php endforeach; endif;?>
                            </div>
                            <div class="row bg-info" id="replyTextarea_<?php echo $dtl['id'];?>" style="margin: 0px !important; display: none;">
                            
                                <div class="row" style="margin: 0px !important;">
                                &nbsp;
                                    <?php 
										$replyPersonName = $loggedUserData?$loggedUserData['firstName'].' '. $loggedUserData['lastName'] : $dtl['name'];
                                          $replyPersonEmail = ($loggedUserData)? $loggedUserData['loginEmail'] : $dtl['email'];  
                                    ?>
                                    <textarea name="reply_<?php echo $dtl['id'];?>" id="reply_<?php echo $dtl['id'];?>" rows="5" placeholder="Enter Reply" class="form-control" ><?php echo $dtl['answer'];?></textarea>  
                                    <br />
                                </div>
                                <div class="row" style="float: right;">
                                		<a href="javascript:void(0);" onclick="$('#replyTextarea_<?php echo $dtl['id'];?>').toggle();" class="btn btn-danger">Cancel</a>&nbsp;
                                        <button type="submit" class="btn btn-primary" onclick="saveReply('<?php echo $dtl['id'];?>','<?php echo $replyPersonName;?>','<?php echo $replyPersonEmail;?>')">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>                          
                            </div>
                            <?php endif;?>
                        </td>                                   
                    </tr>
                <?php }?>
            </tbody>
        </table>
        <?php if($page['total_pages']>1){?>
            <div>
                <div class="pull-left" style="width:50px; padding:6px 0px 0px 0px">Pages: </div>
                
                <div class="pull-left" style="width:80%"> 
                    <ul class="pagination nomargin">
                        <?php pagingLinksLI($page, URL."faq/lists", 'active'); ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php }?>
        
        <?php }else{?>
        <?php if($userType == 'T'):?>
            <div class="notFound" style="background-color: #fff;">Not Found.</div>
        <?php else:?>
            <div class="notFound" style="background-color: #fff;">Your query is not available in FAQ's. <a href="<?php echo URL."faq/ticket"; ?>">Click here</a> to raise a query/ complaints ticket.</div>
        <?php endif;?>
        <?php }?>
        
    </div>
</div>

<script>
function saveReply(id,name,email){
    var message = $('#reply_'+id).val();
    
   if(id && name && email && message){
        $.ajax({
            url: "<?php echo URL.'faq/saveReply';?>",
            type: "post",
            data: {'ticketId':id,'name' : name,'email': email,'message':message},
            success: function(res){
                response = jQuery.parseJSON(res);
                if(res){
                    $('#replyTextarea_'+id).hide();
                    $('#replyMessage_'+id).append('<div class="row" style="margin: 0px !important;"> <div class="row" style="margin: 0px !important;"> <div class="col-md-2 "><strong>From:- <label class="" style="color: orange;">'+name+'</label></strong></div>'+
                                            '<div class="col-md-6 text-center " ></div><div class="col-md-4 " style="text-align: right;"><strong><?php echo dateFormat(date('Y-m-d H:i'));?></strong></div></div><div class="row" style="margin: 0px !important;">'+
                                            '<div class="col-md-1 text-center"><strong>R</strong></div><div class="col-md-10">'+message+'</div></div><hr style="margin-bottom: 0px;" /></div>');
                                            
                    var count = (parseInt($('#count_total_reply_'+id).html()));
                    $('#count_total_reply_'+id).html( (count + 1));
                    $('#reply_'+id).val('');
                }
               
                
            },
            error:function(){
            }
        });
    }
    
}


</script>
