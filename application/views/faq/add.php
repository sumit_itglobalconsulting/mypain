<div class="pageheader">
	<h2><i class="fa fa-question"></i> Add FAQ's</h2>
</div>

<div class="contentpanel">
	<?php echo getFlash();?>
    
    <div class="panel panel-default">
    	<div class="panel-heading">
        </div>
        
        <div class="panel-body panel-body-nopadding">
        	<form class="form-horizontal form-bordered" method="post">
            	<input name="id" id="id" type="hidden" class="form-control" maxlength="255" value="<?php echo $dtl['id'];?>" />
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Category</label>
                    <div class="col-sm-6">
                    	<div class="row" style="padding-top:5px">
                        	<?php $categoryList = userCategoryList();
                                foreach($categoryList as $code => $catogiryName):                            
                            ?>
                            <div class="col-sm-2">
                                <div class="rdio rdio-primary">
                                    <input type="radio" name="category" id="<?php echo $code;?>" value="<?php echo $code;?>" <?php setCheckChecked($dtl['category'], $code); echo (!$dtl && $code == 'A')?'checked="checked"':'';?> />
                                    <label for="<?php echo $code;?>"><?php echo $catogiryName;?></label>
                                </div>
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
                
                 <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Enter Question</label>
                    <div class="col-sm-6">
                        <input name="question" id="question" type="text" class="form-control" maxlength="255" value="<?php echo $dtl['question'];?>" />
                        <label class="error"><?php echo error_msg($errors,'question');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Enter Answer</label>
                    <div class="col-sm-6">
                    	<textarea name="answer" id="answer" rows="10" class="form-control" ><?php echo $dtl['answer'];?></textarea>
                        <label class="error"><?php echo error_msg($errors,'answer');?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label ">Enter Search Keywords</label>
                    <div class="col-sm-6">
                        <input name="keyword" id="keyword" type="text" class="form-control" maxlength="255" value="<?php echo $dtl['keyword'];?>" />
                        <label class="error"><?php echo error_msg($errors,'keyword');?></label>
                    </div>
                </div>
                
                 <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6 col-sm-offset-3">
                			<a href="<?php echo URL."faq/lists"?>" class="btn btn-default">Cancel</a>&nbsp;
                            <button type="submit" class="btn btn-primary">Submit</button>
                    		
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>