<div class="pageheader">
	<h2><i class="fa fa-question-circle"></i> Add issue Help Category</h2>
</div>

<div class="contentpanel">
	<?php echo getFlash();?>
    
    <div class="panel panel-default">
    	<div class="panel-heading">
        </div>
        
        <div class="panel-body panel-body-nopadding">
        	<form class="form-horizontal form-bordered" method="post">
            	<input name="id" id="id" type="hidden" class="form-control" maxlength="255" value="<?php echo $dtl['id'];?>" />
                
                 <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Name</label>
                    <div class="col-sm-6">
                        <input name="name" id="name" type="text" class="form-control" maxlength="255" value="<?php echo $dtl['name'];?>" />
                        <label class="error"><?php echo error_msg($errors,'name');?></label>
                    </div>
                </div>
                 <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6 col-sm-offset-3">
                			<a href="<?php echo URL."faq/listsHelp"?>" class="btn btn-default">Cancel</a>&nbsp;
                            <button type="submit" class="btn btn-primary">Submit</button>
                    		
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>