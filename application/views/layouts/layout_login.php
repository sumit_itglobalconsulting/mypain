<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/fav.jpg')?>" type="image/png">
    
    <title><?php echo isset($page_title)?$page_title:ucfirst($this->uri->rsegments[1]);?></title>
    
    <link href="<?php echo base_url('assets/css/style.default.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap-fileupload.min.css')?>" rel="stylesheet">
    
    <link href="<?php echo base_url('assets/css/mycss.css')?>" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo URL;?>assets/js/html5shiv.js"></script>
    <script src="<?php echo URL;?>assets/js/respond.min.js"></script>
    <![endif]-->

    
    <script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jqajax.js')?>"></script>
    <script src="<?php echo base_url('assets/js/common.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jqcommon.js')?>"></script>
</head>

<body class="signin">
	<?php if(!$_COOKIE['cookieInfo']){?>
	<div class="cookiesInfo">
    	<div style="width:430px; margin:0 auto; padding:0 50px 0 0; position:relative">
    		By using this website you agree to <a href="<?php echo URL."page/cookies";?>">our use of cookies</a>
        	<a class="cookieClose" href="javascript:;;"></a>
        </div>
    </div>
    <?php }?>
    
    <?php
		setcookie('cookieInfo', 1); 
	?>
    
	<!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>
    
    <section>
        <?php $this->load->view($page_view);?>
    </section>
	
	<div class="fnavLogin">
		<div class="fnavPad">
        	<div>
            	<big>&copy;</big> My Pain Ltd <?php echo date('Y');?>. All rights reserved.
                <div class="pull-right text-right">
                    <a href="<?php echo URL."page/about-us";?>">About Us</a>
                    <a href="<?php echo URL."page/contact-us";?>">Contact Us</a>
                    <a href="<?php echo URL."page/usepolicy";?>">Use Policy</a>
                    <a href="<?php echo URL."page/terms";?>">Terms of Use</a>
                    <a href="<?php echo URL."page/privacypolicy";?>">Privacy Policy</a>
                    <a href="<?php echo URL."page/cookies";?>">Cookies Policy</a>
                </div>
            </div>
		</div>
	</div>
    
	<script src="<?php echo base_url('assets/js/site.js')?>"></script>
    
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/modernizr.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/toggles.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/retina.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.cookies.js')?>"></script>
    <script src="<?php echo base_url('assets/js/chosen.jquery.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-fileupload.min.js')?>"></script>
    
    <script src="<?php echo base_url('assets/js/custom.js')?>"></script>
    
    <script>
	$(".cookieClose").click(function() {
		$(".cookiesInfo").hide();
	});
	</script>
</body>
</html>