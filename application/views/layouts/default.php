<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/fav.jpg')?>" type="image/png">
    
    <title><?php echo isset($page_title)?$page_title:ucfirst($this->uri->rsegments[1]);?></title>
    
    <link href="<?php echo base_url('assets/css/style.default.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap-fileupload.min.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/jquery-ui/jquery-ui.css')?>" />
    <link href="<?php echo base_url('assets/css/mycss.css?version='.APP_VERSION)?>" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo URL;?>assets/js/html5shiv.js"></script>
    <script src="<?php echo URL;?>assets/js/respond.min.js"></script>
    <![endif]-->

    
    <script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jqajax.js')?>"></script>
    <script src="<?php echo base_url('assets/js/common.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jqcommon.js')?>"></script>
    
    <script src="<?php echo base_url('assets/jquery-ui/jquery-ui.js')?>"></script>
	
    <script>var SITE_URL='<?php echo URL;?>';</script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

	<script type="text/javascript">
    	google.load("visualization", "1", {packages:["corechart"]});
    </script>
</head>

<body class="stickyheader">
	<!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>
    
    <section>
    	<?php $userDtl=loggedUserData();?>
    	<div class="leftpanel sticky-leftpanel">
            <div class="logopanel" align="center">
            	<?php
					$logo=URL."assets/img/logo.png";
					switch(USER_TYPE){
						case 'S':
						case 'T':
							$logo=URL."assets/img/logo.png";
						break;
						
						case 'H':
							$logo=URL."assets/img/logo.png";
						break;
						
						case 'D':
							$logo=URL."assets/img/logo.png";
							if($userDtl['hospitalLogo'])
								$logo=URL."assets/uploads/user_images/".$userDtl['hospitalLogo'];
						break;
						
						case 'P':
							$logo=URL."assets/img/logo.png";
							/*if($userDtl['image'])
								$logo=URL."assets/uploads/user_images/".$userDtl['image'];*/
						break;
					}
				?>
                <a href="<?php echo URL;?>"><img src="<?php echo $logo;?>" style="width:120px;" /></a>
                <div class="clearfix"></div>
            </div>
            
            <!-- Left Navs -->
            	<?php $this->load->view("elements/left_nav", array('userDtl'=>$userDtl));?>
            <!-- /Left Navs -->
        </div>
        
        <div class="mainpanel">
        	<?php $this->load->view("elements/header", array('userDtl'=>$userDtl));?>
            
            <div id="mainContentBx" class="posRel">
            	<?php $this->load->view($page_view);?>
                
                <p>&nbsp;</p>
                <div class="fnavLogin">
                    <div class="fnavPad">
                        <div>
                            <big>&copy;</big> My Pain Ltd <?php echo date('Y');?>. All rights reserved.
                            <div class="pull-right text-right">
                                <a href="<?php echo URL."page/about-us";?>">About Us</a>
                                <a href="<?php echo URL."page/contact-us";?>">Contact Us</a>
                                <a href="<?php echo URL."page/usepolicy";?>">Use Policy</a>
                                <a href="<?php echo URL."page/terms";?>">Terms of Use</a>
                                <a href="<?php echo URL."page/privacypolicy";?>">Privacy Policy</a>
                                <a href="<?php echo URL."page/cookies";?>">Cookies Policy</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php $this->load->view("elements/footer");?>
        </div>
    </section>
   	
	
	<script src="<?php echo base_url('assets/js/site.js?version='.APP_VERSION)?>"></script>
    
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/modernizr.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/toggles.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/retina.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.cookies.js')?>"></script>
    <script src="<?php echo base_url('assets/js/chosen.jquery.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-fileupload.min.js')?>"></script>
    
    <script src="<?php echo base_url('assets/js/custom.js')?>"></script>
</body>
</html>