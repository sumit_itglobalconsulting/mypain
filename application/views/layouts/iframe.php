<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css?version='.APP_VERSION)?>" />
<!-- JQuery UI CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/jquery-ui/jquery-ui.css')?>" />
<!-- /JQuery UI CSS-->
<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css?version='.APP_VERSION)?>" />

<script src="<?php echo base_url('assets/js/jquery.js')?>"></script>
<!-- JQuery UI JS -->
<script src="<?php echo base_url('assets/jquery-ui/jquery-ui.js')?>"></script>
<!-- /JQuery UI JS-->
<script src="<?php echo base_url('assets/js/jqajax.js')?>"></script>
<script src="<?php echo base_url('assets/js/common.js?version='.APP_VERSION)?>"></script>
<script src="<?php echo base_url('assets/js/jqcommon.js?version='.APP_VERSION)?>"></script>

</head>

<body>
    <?php $this->load->view($page_view);?>
    
    <script src="<?php echo base_url('assets/js/site.js')?>"></script>
</body>
</html>