<?php $userDtl=loggedUserData();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/fav.jpg')?>" type="image/png">
    
    <title><?php echo isset($page_title)?$page_title:ucfirst($this->uri->rsegments[1]);?></title>
    <style type="text/css">
		<?php require "assets/css/style.css";?>
		
		/** Notification **/
		.notiBx			{width:400px; background:#eee; box-shadow:3px 3px 5px #999; position:fixed; right:5px; bottom:5px; z-index:3000; display:none}
		.notiTitle		{background:#000; padding:5px 10px; color:#fff; text-transform:uppercase; font-size:12px}
		.notiList		{border-bottom:1px solid #ccc; padding:10px}
	</style>
   
    
    <script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jqajax.js')?>"></script>
    <script>var SITE_URL='<?php echo URL;?>';</script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

	<script type="text/javascript">
    	google.load("visualization", "1", {packages:["corechart"]});
		//google.load("visualization", "1", {packages: ["imagechart"]});
    </script>
</head>

<body>
	<div class="outer">
        <?php $this->load->view($page_view);?>
    </div>
</body>
</html>