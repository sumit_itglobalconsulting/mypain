<div class="signinpanel">
    <div class="row form-group">
    	<div class="col-md-6" style="margin:0 auto; float:none">
        	<?php echo getFlash();?>
            
            <form method="post">
            	<div align="center">
                    <a href="<?php echo URL;?>"><img src="<?php echo URL;?>assets/img/logo.png" /></a>
                </div>
                <div class="mb30"></div>
                
                <input type="text" name="username" id="username" class="form-control uname" placeholder="Enter your Email-ID" value="<?php echo set_value('username'); ?>" />
                <label for="username" class="error"><?php echo error_msg($errors,'username');?></label>
                
                <button class="btn btn-success btn-block">Submit</button>
            </form>
            
            <div class="mb20"></div>
            <div align="right">
            	<a href="<?php echo URL;?>">&laquo; Back to login</a>
            </div>
        </div>
    </div>
</div>