<?php $catsArr=multiArrToKeyValue($cats, "id", "name");?>

<div class="pageheader">
    <div class="row">
        <div class="pull-left" style="padding:0px 0px 0px 10px">
            <h2><i class="fa fa-edit"></i> My Response Forms</h2>
        </div>
        
        <div class="pull-right" style="padding:4px 10px 0px 0px; width:300px">
        	<form id="searchForm" method="get">
        		<?php echo form_dropdown('catId', array(0=>'All Categories')+$catsArr, $_GET['catId'], 'id="catId" class="form-control chosen-select"');?>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="contentpanel">
	<div class="people-list myListBx1">
    	<div class="row">
            <div class="pad1">
                <div class="row">
                    <?php if($list){foreach($list as $i=>$dtl){?>
                    <div class="col-md-3">
                        <div class="people-item">
                            <div class="media" align="center">
                                <a href="<?php echo URL."common/formPreview/".encode($dtl['id']);?>">
                                    <img alt="" src="<?php echo base_url("assets/uploads/form_images/".$dtl['image']);?>">
                                </a>
                            </div>
                            <div class="mb5"></div>
                            <a href="<?php echo URL."common/formPreview/".encode($dtl['id']);?>">
                                <button type="button" class="btn btn-warning btn-block"><i class="fa fa-eye"></i> View Form</button>
                            </a>
                        </div>
                    </div>
                    <?php }}else{?>
                    <div class="notFound">No form found.</div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#catId").change(function(e) {
	$("#searchForm").submit();
});
</script>