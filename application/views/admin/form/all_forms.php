<?php
$catsArr=multiArrToKeyValue($cats, "id", "name");

$addedFormIds=array();
if($addedForms){
	$addedFormIds=array_map(function($arg){return $arg['formId'];}, $addedForms);
}

//$noOfAllowedForms=$planDtl['noOfFormsAllowed'];
$noOfAllowedForms = array();
$noOfAllowedForms=explode(',',$planDtl['formId']);
?>

<div class="pageheader">
    <div class="row">
        <div class="pull-left" style="padding:0px 0px 0px 10px">
            <h2><i class="fa fa-edit"></i> All Response Forms</h2>
        </div>
        
        <div class="pull-right" style="padding:4px 10px 0px 0px; width:300px">
        	<form id="searchForm" method="get">
        		<?php echo form_dropdown('catId', array(0=>'All Categories')+$catsArr, $_GET['catId'], 'id="catId" class="form-control chosen-select"');?>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="contentpanel">
	<div class="people-list myListBx1">
        <div class="pad1">
            <div class="row">
                <?php foreach($list as $i=>$dtl){$isAdded='0'; $cls='makeFav'; if(in_array($dtl['id'], $addedFormIds)){$isAdded='1';$cls='favIcon';}?>
                <div class="col-md-3 posRel">
                   <?php if(in_array($dtl['id'],$noOfAllowedForms)){ ?>
                    <a
                        href="<?php echo ADM_URL."hospital/addForm/".encode($dtl['id']);?>" 
                        isAdded="<?php echo $isAdded;?>" class="formAddBtn <?php echo $cls;?>">
                        <i class="fa fa-star"></i>
                    </a>
                   <?php } ?>
                    <div class="people-item">
                        <div class="media" align="center">
                            <a href="<?php echo URL."common/formPreview/".encode($dtl['id']);?>">
                                <img alt="" src="<?php echo base_url("assets/uploads/form_images/".$dtl['image']);?>">
                            </a>
                        </div>
                        <div class="mb5"></div>
                        <a href="<?php echo URL."common/formPreview/".encode($dtl['id']);?>">
                            <button type="button" class="btn btn-warning btn-block"><i class="fa fa-eye"></i> View Form</button>
                        </a>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#catId").change(function(e) {
	$("#searchForm").submit();
});

function countAddedForms() {
	c=0;
	$("[isAdded='1']").each(function(){
		c++;
	});
	return c;
}

$(".formAddBtn").click(function(e){
	e.preventDefault();
	countForms=countAddedForms();
	noOfAllwed='<?php echo $noOfAllowedForms;?>'*1;
	
	if(countForms>=noOfAllwed){
		showDialog('Your plan allows you to add <?php echo $noOfAllowedForms;?> form(s) only.', 'Alert', true);
		return;
	}
	
	obj=$(this);
	isAdded=obj.attr("isAdded");
	if(isAdded==1)
		return;
		
	url=obj.attr("href");
	obj.removeClass("makeFav").removeClass("favIcon").addClass("favLoader").html('');
	ajax(url, "", "", "callbackFav(obj)");
});

function callbackFav(obj){
	obj.removeClass("favLoader").removeClass("makeFav").addClass("favIcon").html('<i class="fa fa-star"></i>');
	obj.attr("isAdded", "1");
}
</script>