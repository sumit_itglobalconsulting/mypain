<?php 
	$loggedDtl=loggedUserData();
	if(!$dtl){
		$dtl=loggedUserData();
	}
	
	$dtl['hospitalName']=$loggedDtl['hospitalName'];
	$dtl['regNo']=$loggedDtl['regNo'];
	
	$durationTypes=durationTypeArr();
?>

<div class="pageheader">
	<h2><i class="fa fa-user"></i> My Profile</h2>
</div>

<div class="contentpanel">
	<?php if($errors){?>
    	<div class="alert alert-danger">Please fill the fields marked with red color</div>
    <?php }?>
    
	<?php echo getFlash();?>
    
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<h4 class="panel-title">Profile details</h4>
        </div>
        
        <div class="panel-body panel-body-nopadding">
        	<form class="form-horizontal form-bordered" method="post" onsubmit="return submitForm()" enctype="multipart/form-data">
            	<div class="form-group">
                    <label class="col-sm-3 control-label myreq">Hospital Name</label>
                    <div class="col-sm-6">
                        <input name="hospitalName" id="hospitalName" type="text" class="form-control" value="<?php echo h($dtl['hospitalName']);?>" maxlength="50" />
                    	<label class="error"><?php echo error_msg($errors,'hospitalName');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Hospital Registration Number</label>
                    <div class="col-sm-6">
                        <input name="regNo" id="regNo" type="text" class="form-control" value="<?php echo h($dtl['regNo']);?>" maxlength="50" />
                    	<label class="error"><?php echo error_msg($errors,'regNo');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Branch Name</label>
                    <div class="col-sm-6">
                        <input name="branchName" id="branchName" type="text" class="form-control" value="<?php echo h($dtl['branchName']);?>" maxlength="50" />
                    <label class="error"><?php echo error_msg($errors,'branchName');?></label>
                    </div>
                </div>
             
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">First Name</label>
                    <div class="col-sm-6">
                        <input name="firstName" id="firstName" type="text" class="form-control" value="<?php echo h($dtl['firstName']);?>" maxlength="100" />
                        <label class="error"><?php echo error_msg($errors,'firstName');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Last Name</label>
                    <div class="col-sm-6">
                    	<input name="lastName" id="lastName" type="text" class="form-control" value="<?php echo h($dtl['lastName']);?>" maxlength="100" />
                        <label class="error"><?php echo error_msg($errors,'lastName');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Login Email</label>
                    <div class="col-sm-6">
                    	<input name="loginEmail" id="loginEmail" type="text" class="form-control" value="<?php echo h($dtl['loginEmail']);?>" maxlength="100" />
                        <label class="error" id="loginEmailErr"><?php echo error_msg($errors,'loginEmail');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Administrator's Email</label>
                    <div class="col-sm-6">
                    	<input name="contactEmail" id="contactEmail" type="text" class="form-control" value="<?php echo h($dtl['contactEmail']);?>" maxlength="100" />
                        <label class="error" id="contactEmailErr"><?php echo error_msg($errors,'contactEmail');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Contact Number (1)</label>
                    <div class="col-sm-6">
                    	<div>
                            <div class="pull-left" style="width:80px; padding-right:10px">
                                <?php echo form_dropdown('stdCd1', stdCodesArr(), $dtl['stdCd1'], 'class="form-control chosen-select" data-placeholder="STD Code"');?>
                            </div>
                            
                            <div class="pull-left">
                                <input type="text" class="form-control" name="phone1" id="phone1" value="<?php echo h($dtl['phone1']);?>" maxlength="10" valid="int" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <label class="error" id="phone1Err"><?php echo error_msg($errors,'phone1');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label">Contact Number (2)</label>
                    <div class="col-sm-6">
                    	<div>
                            <div class="pull-left" style="width:80px; padding-right:10px">
                                <?php echo form_dropdown('stdCd2', stdCodesArr(), $dtl['stdCd2'], 'class="form-control chosen-select" data-placeholder="STD Code"');?>
                            </div>
                            
                            <div class="pull-left">
                                <input type="text" class="form-control" name="phone2" id="phone2" value="<?php echo h($dtl['phone2']);?>" maxlength="10" valid="int" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <label class="error" id="phone2Err"><?php echo error_msg($errors,'phone2');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Contact Address</label>
                    <div class="col-sm-6">
                    	<input name="address" id="address" type="text" class="form-control" value="<?php echo h($dtl['address']);?>" maxlength="100" />
                        <label class="error"><?php echo error_msg($errors,'address');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">City</label>
                    <div class="col-sm-6">
                    	<input name="city" id="city" type="text" class="form-control" value="<?php echo h($dtl['city']);?>" maxlength="100" />
                        <label class="error"><?php echo error_msg($errors,'city');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Zipcode</label>
                    <div class="col-sm-6">
                    	<input name="zipcode" id="zipcode" type="text" class="form-control" value="<?php echo h($dtl['zipcode']);?>" maxlength="100" />
                        <label class="error"><?php echo error_msg($errors,'zipcode');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">State</label>
                    <div class="col-sm-6">
                    	<input name="state" id="state" type="text" class="form-control" value="<?php echo h($dtl['state']);?>" maxlength="100" />
                        <label class="error"><?php echo error_msg($errors, 'state');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Country</label>
                    <div class="col-sm-6">
                    	<input name="country" id="country" type="text" class="form-control" value="<?php echo h($dtl['country']);?>" maxlength="100" />
                        <label class="error"><?php echo error_msg($errors, 'country');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label">Punch Line</label>
                    <div class="col-sm-6">
                    	<input name="punchLine" id="punchLine" type="text" class="form-control" value="<?php echo h($dtl['punchLine']);?>" maxlength="50" />
                    	<label class="error"><?php echo error_msg($errors,'punchLine');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label">Hospital Logo (120 x 120)</label>
                    <div class="col-sm-6">
                    	<div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="input-append">
                                <div class="uneditable-input">
                                    <i class="glyphicon glyphicon-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileupload-new">Select file</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" name="image" accept="image/*" />
                                </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                        
                        <label class="error"><?php echo error_msg($errors,'image');?></label>
                    </div>
                </div>
                
                
                <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6 col-sm-offset-3">
                			<button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                    		<button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
$(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

function submitForm() {
	var error=false;
	if(!checkValidPhone($("#phone1"), $("#phone1Err")))
		error=true;
	if(!checkValidPhone($("#phone2"), $("#phone2Err")))
		error=true;
	
	if(!checkValidEmail($("#loginEmail")))
		error=true;
		
	if(!checkValidEmail($("#contactEmail")))
		error=true;
	
	return !error;
}


function checkValidPhone(obj, errBxOb) {
	phone=$.trim(obj.val());
	errBxOb.text('');
	obj.removeClass('redBdr');
	if(phone){
		if(!isvalidPhone(phone)){
			obj.addClass('redBdr');
			errBxOb.text("Please enter a valid phone number");
			return false;
		}
	}
	return true;
}

$("#loginEmail, #contactEmail, #supportEmail").blur(function(){
	checkValidEmail($(this));
});

$("#phone1, #phone2").blur(function(){
	checkValidPhone($(this), $("#"+$(this).attr("id")+"Err"));
});
</script>