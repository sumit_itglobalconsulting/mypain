<?php
	$treatmentOpts=treatmentOptionsArr();
	function getTreatmentOpt($dtl, $treatOpt){
		$str='<ol>';
		$i=1;
		$str.=$dtl['treatment1']?'<li>'.$treatOpt[1]."</li>":"";
		$str.=$dtl['treatment2']?'<li>'.$treatOpt[2]."</li>":"";
		$str.=$dtl['treatment3']?'<li>'.$treatOpt[3]."</li>":"";
		$str.=$dtl['treatment4']?'<li>'.$treatOpt[4]."</li>":"";
		
		return $str.'</ol>';
	}
	
	$qs=arrayUrlDecode($_GET);
?>

<div class="pageheader">
	<h2><i class="fa fa-user"></i> Patients</h2>
</div>


<div class="contentpanel">
    <div class="table-responsive" style="min-height:830px">
        <form method="get" id="searchForm" onsubmit="return submitSearch()">
            <h4 class="subtitle mb5">Refine Results</h4>
            <div class="myWhiteBx smallForm">
                <table class="filterTbl">
                    <tr>
                        <td width="200px">
                            <span class="tooltips" data-placement="top" title="Select Clinician">
                            <?php 
                                echo form_dropdown("doctorId", $doctors, $qs['doctorId'], 'class="form-control chosen-select"');
                            ?>
                            </span>
                        </td>
                        
                        <td width="280px">
                            <span class="tooltips" data-placement="top" title="Treatment Option">
                            <?php 
                                echo form_dropdown("treatOpt", array(''=>'Treatment Option (All)')+$treatmentOpts, $qs['treatOpt'], 
                                'class="form-control chosen-select"');
                            ?>
                            </span>
                        </td>
                        
                        <td width="170px">
                            <span class="tooltips" data-placement="top" title="Response">
                            <?php 
                                $resTypes=array('P'=>'Poor Response', 'A'=>'Average Response', 'G'=>'Good Response');
                                echo form_dropdown("resType", array(''=>'All Responses')+$resTypes, $qs['resType'], 'class="form-control chosen-select"');
                            ?>
                            </span>
                        </td>
                        
                        <td width="250px">
                            <input type="text" name="k" value="<?php echo $qs['k'];?>" placeholder="Search by Name, DOB, Hospital ID" class="form-control" />
                        </td>
                        
                        <td><button type="submit" class="btn btn-primary">Search</button></td>
                    </tr>
                </table>
            </div>
        </form>
        
        <div class="mb20"></div>
        
    	<?php if($result){?>
            <table class="table table-striped mb30">
                <thead>
                	<tr>
                        <th width="100px">Hospital #</th>
                        <th>Patient Name</th>
                        <th width="270px">Treatment Options</th>
                        <th width="120px" class="text-center">DOB</th>
                        <th width="300px" colspan="2" class="text-center">Patient Status</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php foreach($result as $i=>$dtl){$feels=getFeels($dtl['id']);?>
                    <tr>
                        <td><?php echo $dtl['hospitalId'];?></td>
                        <td>
							<?php echo $dtl['name'];?>
                            <div class="mb10"></div>
                            <?php if($dtl['discharged']){?>
                                <div class="text-muted">Status: Discharged</div>
                                <div class="mb10"></div>
                            <?php }else if($dtl['archived']){?>
                                <div class="text-muted">Status: Archived</div>
                                <div class="mb10"></div>
                            <?php }?>
                            
                            <a href="<?php echo ADM_URL."patient/reports/".encode($dtl['id'])."/".encode($doctorId);?>">
                                <i class="fa fa-bar-chart-o"></i> View Report
                            </a>
                        </td>
                        
                        <td>
							<?php echo getTreatmentOpt($dtl, $treatmentOpts);?>
                        </td>
                        
                        <td align="center"><?php echo showDate($dtl['dob']);?></td>
            
                        <td width="150px" align="center">
                        	<?php
								if(!$dtl['overallRes'])
									$dtl['overallRes']=0;
									
								if($dtl['overallRes']<30)
									$resBg='#ff4b31';
								else if($dtl['overallRes']>=30 && $dtl['overallRes']<50)
									$resBg='#fbd200';
								else
									$resBg='#6fba61';
								$resBg='background:'.$resBg;
							?>
                            <div class="stBx" style=" <?php echo $resBg;?> "><?php echo round($dtl['overallRes']);?>%</div>
                            <div>Overall Response</div>
                        </td>
                        
                        <td width="150px" align="center">
                        	<?php
								if($feels<=0)
									$feelBg='#FF0000';
								else if($feels>0 && $feels<=1)
									$feelBg='#FFA500';
								else if($feels>1 && $feels<=3)
									$feelBg='#FDD017';
								else
									$feelBg='#008000';
								$feelBg='background:'.$feelBg;
							?>
                            <div class="stBx" style=" <?php echo $feelBg;?> "><?php echo $feels;?></div>
                            <div>Feels</div>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
        <?php }else{?>
        	<div class="notFound">No Data Found</div>
        <?php }?>
    </div>
    
    <?php if($page['total_pages']>1){?>
        <div>
            <div class="pull-left" style="width:50px; padding:6px 0px 0px 0px">Pages: </div>
            
            <div class="pull-left" style="width:80%"> 
                <ul class="pagination nomargin">
                    <?php pagingLinksLI($page, ADM_URL."patients/lists", 'active'); ?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php }?>
</div>


<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

function submitSearch(){
	$("#searchForm input, #searchForm select").each(function(){
		ob=$(this);
		if(!ob.val())
			ob.removeAttr('name');
	});
}
</script>