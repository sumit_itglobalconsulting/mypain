<?php
$deptsArr=array(''=>'');
if($depts)
	$deptsArr=$deptsArr+multiArrToKeyValue($depts, 'id', 'name');
	
$admUrl=ADM_URL;
?>

<div class="pageheader">
	<h2><i class="fa fa-user-md"></i> Clinician <span><?php echo $dtl['id']?"Update ".$dtl['firstName']." profile":"Add New"?></span></h2>
    <div class="breadcrumb-wrapper">
    	<a href="<?php echo ADM_URL."hospital/clinicians";?>">&laquo; back to list</a>
    </div>
</div>


<div class="contentpanel">
	<?php if($errors){?>
    	<div class="alert alert-danger">Please fill the fields marked with red color</div>
    <?php }?>
    
	<?php echo getFlash();?>
    
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<h4 class="panel-title">Profile details</h4>
        </div>
        
        <div class="panel-body panel-body-nopadding">
        	<form class="form-horizontal form-bordered" method="post" onsubmit="return submitForm()" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Name</label>
                    <div class="col-sm-6">
                        <input name="firstName" id="firstName" type="text" class="form-control" value="<?php echo h($dtl['firstName']);?>" maxlength="100" />
                        <label class="error"><?php echo error_msg($errors,'firstName');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Department</label>
                    <div class="col-sm-6">
                    	<div style="width:235px">
                        	<?php echo form_dropdown('deptId', $deptsArr, $dtl['deptId'], 'id="deptId" class="form-control chosen-select" data-placeholder="Select"');?>
                        </div>
                        <label class="error"><?php echo error_msg($errors,'deptId');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Email</label>
                    <div class="col-sm-6">
                    	<input name="loginEmail" id="loginEmail" type="text" class="form-control" value="<?php echo h($dtl['loginEmail']);?>" maxlength="100" />
                        <label class="error" id="loginEmailErr"><?php echo error_msg($errors,'loginEmail');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Contact Number</label>
                    <div class="col-sm-6">
                    	<div>
                            <div class="pull-left" style="width:80px; padding-right:10px">
                                <?php echo form_dropdown('stdCd1', stdCodesArr(), $dtl['stdCd1'], 'class="form-control chosen-select" data-placeholder="STD Code"');?>
                            </div>
                            
                            <div class="pull-left">
                                <input type="text" class="form-control" name="phone1" id="phone1" value="<?php echo h($dtl['phone1']);?>" maxlength="10" valid="int" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <label class="error" id="phone1Err"><?php echo error_msg($errors,'phone1');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Patient Invitation Limit</label>
                    <div class="col-sm-6">
                    	<div style="width:235px">
                        	<?php 
								echo form_dropdown('patientInvitationLimit', array(100000=>'Unlimited', 20=>20, 50=>50, 100=>100, 200=>200, 500=>500, 1000=>1000, 5000=>5000), 
								$dtl['patientInvitationLimit'], 'class="form-control chosen-select" data-placeholder=""');
							?>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Assign Form(s)</label>
                    <div class="col-sm-6">
                    	<div id="formListBx" style="width:250px">
                        	<?php if($deptForms){?>
								<?php 
                                    $formIds=array();
                                    if($clinicianForms){
                                        $formIds=array_map(function($arg){return $arg['formId'];}, $clinicianForms);
                                    }
                                ?>
                                <?php foreach($deptForms as $i=>$f){?>
                                    <div class="ckbox ckbox-primary">
                                        <input type="checkbox" id="f<?php echo $i;?>" name="formId[]" value="<?php echo $f['formId'];?>" 
                                        <?php if(in_array($f['formId'], $formIds)) echo 'checked="checked"';?> />&nbsp;
                                        <label for="f<?php echo $i;?>"><?php echo $f['formName'];?></label>
                                    </div>
                                <?php }?>
                            <?php }else if($dtl['id']){?>
                                <div class="notFound">Not Found</div>
                            <?php }else{?>
                                <div class="notFound">Select a department first</div>
                            <?php }?>
                        </div>
                    </div>
                </div>
           
                <div class="form-group">
                	<label class="col-sm-3 control-label">Profile Image (100 x 100)</label>
                    <div class="col-sm-6">
                    	<div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="input-append">
                                <div class="uneditable-input">
                                    <i class="glyphicon glyphicon-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileupload-new">Select file</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" name="image" accept="image/*" />
                                </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                        
                        <label class="error"><?php echo error_msg($errors,'image');?></label>
                    </div>
                </div>
                
                
                <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6 col-sm-offset-3">
                			<button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                    		<button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

function submitForm() {
	var error=false;
	if(!checkValidPhone($("#phone1"), $("#phone1Err")))
		error=true;
	
	if(!checkValidEmail($("#loginEmail")))
		error=true;
		
	return !error;
}

function checkValidPhone(obj, errBxOb) {
	phone=$.trim(obj.val());
	errBxOb.text('');
	obj.removeClass('redBdr');
	if(phone){
		if(!isvalidPhone(phone)){
			obj.addClass('redBdr');
			errBxOb.text("Please enter a valid phone number");
			return false;
		}
	}
	return true;
}

$("#loginEmail").blur(function(){
	checkValidEmail($(this));
});

$("#phone1").blur(function(){
	checkValidPhone($(this), $("#"+$(this).attr("id")+"Err"));
});


/** Department's form listing **/
$("#deptId").change(function(){
	obj=$(this);
	deptId=obj.val();
	url="<?php echo ADM_URL.'hospital/deptFormAjax/';?>"+deptId;
	$("#formListBx").html('Wait...');
	$.getJSON(url, function(data){deptForms(data);});
});

function deptForms(data) {
	var doctorForms=<?php echo json_encode($formIds);?>;
	if(doctorForms==null)
		doctorForms=new Array(0);
	
	el='';
	$.each( data, function( key, val ) {
		chk="";
		if(doctorForms.indexOf(val.formId)>=0)
			chk='checked="checked"';
		
		el+='<div class="ckbox ckbox-primary">';
		el+='<input type="checkbox" id="f'+key+'" name="formId[]" value="'+val.formId+'" '+chk+' />&nbsp;';
		el+='<label for="f'+key+'">'+val.formName+'</label>';
		el+='</div>';
	});
	if(!el)
		el='<div class="notFound">Not Found</div>';
	$("#formListBx").html(el);
	$('label').disableSelection();
}
</script>