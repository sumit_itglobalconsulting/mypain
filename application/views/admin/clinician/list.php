<div class="pageheader">
	<div class="row">
        <div class="col-md-8">
            <h2><i class="fa fa-user-md"></i> Clinicians</h2>
        </div>
        
        <div class="col-md-4" style="padding-top:4px" align="right">
        	<a href="<?php echo ADM_URL."hospital/addClinician";?>">
            	<button type="button"class="btn btn-primary">+ Add New</button>
            </a>
        </div>
    </div>
</div>

<div class="contentpanel">
	<?php echo getFlash();?>
    
	<div class="people-list">
    	<div class="row">
        	<?php if($result){foreach($result as $i=>$dtl){?>
        	<div class="col-md-6 myListBx1">
            	<div class="people-item">
                	<div class="media">
                    	<span class="pull-left">
                        	<img alt="" src="<?php echo base_url("assets/uploads/user_images/".$dtl['image']);?>" class="thumbnail media-object">
                        </span>
                        <div class="media-body">
                        	<h4 class="person-name"><?php echo $dtl['firstName'];?></h4>
                            <div class="mb5"></div>
                            <div><span class="text-muted"><i class="fa fa-edit"></i> Treatment Response Forms:</span> <?php echo $dtl['noFForms'];?></div>
                            <div><span class="text-muted"><i class="fa fa-suitcase"></i> Department:</span> <?php echo $dtl['deptName'];?></div>
                            <div>
                            	<span class="text-muted"><i class="fa fa-envelope-o"></i> Email:</span> 
								<a href="mailto:<?php echo $dtl['loginEmail'];?>"><?php echo $dtl['loginEmail'];?></a>
                            </div>
                            <div><span class="text-muted"><i class="fa fa-phone"></i> Phone:</span> <?php echo $dtl['phone1'];?></div>
                        </div>
                    </div>
                    
                    <div class="mb5"></div>
                    <div class="smallForm" align="right">
                        <a href="<?php echo ADM_URL."hospital/resetClinicianPassword/".encode($dtl['id']);?>">
                            <button type="button" class="btn btn-primary"><i class="fa fa-cog"></i> Change Password</button>
                        </a>
                        
                        <a href="<?php echo ADM_URL."hospital/editClinician/".encode($dtl['id']);?>">
                            <button type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button>
                        </a>
                        
                        <a href="<?php echo ADM_URL."hospital/deleteClinician/".encode($dtl['id']);?>">
                            <button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
                        </a>
                    </div>
                </div>
            </div>
        	<?php }}else{?>
            	<div class="notFound">Not Found</div>
            <?php }?>
        </div>
        
        <?php if($page['total_pages']>1){?>
            <div>
                <div class="pull-left" style="width:50px; padding:6px 0px 0px 0px">Pages: </div>
                
                <div class="pull-left" style="width:80%"> 
                    <ul class="pagination nomargin">
                        <?php pagingLinksLI($page, ADM_URL."hospital/clinicians", 'active'); ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php }?>
    </div>
</div>