<?php
	$invAr=array(array('Month', 'Invitations'));
	if($patientsInvitaion){
		foreach($patientsInvitaion as $pi){
			array_push($invAr, array($pi['monthN'], intval($pi['noOfInv'])));
		}
	}
	
	$treatOpt=treatmentOptionsArr();
?>
<div class="pageheader">
        <h2 <?php if(!empty(getFlash())) { ?> style="float:left" <?php } ?>><i class="fa fa-home"></i> Dashboard</h2>
        <?php if(!empty(getFlash())) { ?>
        <div class="bs-example">
            <div class="alert alert-warning" style="float:right;margin-bottom: 0px">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> <?php echo getFlash(); ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php } ?>
</div>

 

<div class="contentpanel">
	<div class="people-list myListBx1">
    	<div class="row">
        	
            <div class="col-md-4">
            	<div class="people-item">
                    <div class="media">
                        <span class="pull-left mrgn1">
                        	<a href="<?php echo ADM_URL."hospital/departments";?>">
                        		<img alt="" src="<?php echo URL."assets/img/dept-icon.png"?>" class="thumbnail">
                            </a>
                        </span>
                        <div class="media-body">
                            <h4 class="person-name" align="right"><?php echo $info['totalDepts'];?></h4>
                           	Total department created
                            <div><i class="fa fa-eye"></i> <a href="<?php echo ADM_URL."hospital/departments";?>">View</a></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
            	<div class="people-item">
                    <div class="media">
                        <span class="pull-left mrgn1">
                        	<a href="<?php echo ADM_URL."hospital/clinicians";?>">
                        		<img alt="" src="<?php echo URL."assets/img/doctor-icon.png"?>" class="thumbnail">
                            </a>
                        </span>
                        <div class="media-body">
                            <h4 class="person-name" align="right"><?php echo $info['totalDocts'];?></h4>
                           	Total clinician created
                            <div><i class="fa fa-eye"></i> <a href="<?php echo ADM_URL."hospital/clinicians";?>">View</a></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
            	<div class="people-item">
                    <div class="media">
                        <span class="pull-left mrgn1">
                        	<a href="<?php echo ADM_URL."hospital/patients";?>">
                        		<img alt="" src="<?php echo URL."assets/img/response-pending.png"?>" class="thumbnail">
                            </a>
                        </span>
                        <div class="media-body">
                            <h4 class="person-name" align="right"><?php echo $info['noOfResDue'];?></h4>
                           	Responses due but not submitted on time
                            <div><i class="fa fa-eye"></i> <a href="<?php echo ADM_URL."hospital/patients";?>">View</a></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <div class="mb20"></div>
    <div class="panel panel-default" style="min-height:700px">
        <div class="panel-heading">
        	<div>
            	<div class="pull-left myL1"><h4 class="panel-title text-left">Reports</h4></div>
                
                <div class="pull-right" style="width:300px">
                	<form id="chartForm">
                        <div class="form-group smallForm">
                            <label class="col-sm-4 control-label text-right">Report for</label>
                            <div class="col-sm-8">
                                <?php echo form_dropdown("doctorId", array(''=>'ALL')+$doctors, '', 'id="doctorId" class="form-control chosen-select"');?>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        
        <div class="panel-body">
        	<div id="reportAcco">
            	<h3>Patient response analysis</h3>
                <div>
                    <div style="width:550px; height:330px; margin:0 auto; overflow:hidden">
                        <div id="resAnaChart" style="width:600px; height:350px; margin:0 auto; overflow:auto"></div>
                    </div>
                </div>
                
                <h3>Patient under treatment</h3>
                <div><div id="treatTypeChart" style="width:600px; height:350px; margin:0 auto"></div></div>
                
                <h3>Invitation to patient over time</h3>
                <div>
                	<div class="row smallForm">
                    	<label class="col-sm-1 control-label">Select Year:</label>
                    	
                        <div class="col-sm-3">
							<?php
                                $years=array(2014=>2014, 2015=>2015, 2016=>2016);
                                echo form_dropdown("year", $years, date('Y'), 'id="year" class="form-control chosen-select"');
                            ?>
                        </div>
                    </div>
                    
                	<div id="invChart" style="width:600px; height:350px; margin:0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

resAna=new Array();
invCh=new Array();
tType=new Array();

function drawResAnaChart(noOfPoorRes, noOfAvgRes, noOfGoodRes) {
	resAna[0]=noOfPoorRes=parseInt(noOfPoorRes); 
	resAna[1]=noOfAvgRes=parseInt(noOfAvgRes); 
	resAna[2]=noOfGoodRes=parseInt(noOfGoodRes);
	
	var data = google.visualization.arrayToDataTable([
	  ['Response Analysis', 	'No. Of Patients'],
	  ['Poor Response', 		noOfPoorRes],
	  ['Average Response', 		noOfAvgRes],
	  ['Good Response',   		noOfGoodRes]
	]);
	
	var options = {
	  title: '',
	  chartArea: {left:"100", top:"60", width:"90%"},
	  colors: ['#ff4b31','#fbd200', '#6fba61'],
	  is3D: true
	};
	
	var chart = new google.visualization.PieChart(document.getElementById('resAnaChart'));
	chart.draw(data, options);
}

drawResAnaChart('<?php echo $noOfPoorRes;?>', '<?php echo $noOfAvgRes;?>', '<?php echo $noOfGoodRes;?>');

/** **/
function invChart(invdata) {
	invCh[0]=invdata;
	if(!invdata){
		$("#invChart").html("<div class='notFound'>Data not Found</div>");
		return;
	}
	
	var data = google.visualization.arrayToDataTable(invdata);
	
	maxv=0;
	$.each(invdata, function(k, v){
		if(k==0) return;
		if(maxv<v[1])
			maxv=v[1];
	});
	
	if(maxv<5){
		if(maxv==0)
			gcount=0;
		else
			gcount=maxv+2;
			
		verAxis={title: 'No of Invitations', titleTextStyle: {color: '#FF0000', italic: false}, viewWindow:{min:0}, gridlines:{count:gcount}, format: '0'};
	}
	else{
		verAxis={title: 'No of Invitations', titleTextStyle: {color: '#FF0000', italic: false}, viewWindow:{min:0}, format: '0'};
	}
	
	var options = {
	  title: '',
	  vAxis: verAxis,
	  legend: { position: "right", textStyle: {fontSize: 13}},
	  //chartArea: {left:"55", top:"50", width:"80%"},
	  colors: ['#3366cc','#000000', '#990099', '#0099C6', '#5574A6', '#AAAA11', '#22AA99']
	};
	
	var chart = new google.visualization.ColumnChart(document.getElementById('invChart'));
	chart.draw(data, options);
}
invChart(<?php echo json_encode($invAr);?>);


function treatTypeChart(t1, t2, t3, t4) {
	tType[0]=t1=parseInt(t1); 
	tType[1]=t2=parseInt(t2); 
	tType[2]=t3=parseInt(t3); 
	tType[3]=t4=parseInt(t4);
	
	var data = google.visualization.arrayToDataTable([
	  ['Treatment Type', 'No. Of Patients'],
	  ['<?php echo $treatOpt[1]?>',     t1],
	  ['<?php echo $treatOpt[2]?>', 	t2],
	  ['<?php echo $treatOpt[3]?>', 	t3],
	  ['<?php echo $treatOpt[4]?>',   	t4]
	]);
	
	var options = {
	  title: '',
	  chartArea: {left:"100", top:"60", width:"90%"},
	  colors: ['#3366cc','#000000', '#990099', '#0099C6', '#5574A6', '#AAAA11', '#22AA99'],
	  is3D: true
	};
	
	var chart = new google.visualization.PieChart(document.getElementById('treatTypeChart'));
	chart.draw(data, options);
}

treatTypeChart('<?php echo $NPatients[1]?>', '<?php echo $NPatients[2]?>', '<?php echo $NPatients[3]?>', '<?php echo $NPatients[4]?>');


/** **/
$("#doctorId").change(function() {
	doctId=$(this).val();
	$.ajax({
		url: "<?php echo ADM_URL."user/resAnalysis"?>/"+doctId,
		dataType: "JSON",
		success: function(res){
			drawResAnaChart(res.noOfPoorRes, res.noOfAvgRes, res.noOfGoodRes);
		}
	});
	
	patientsInvitation($("#year").val(), doctId);
	noOfPatientTreatOptWise(doctId);
});

$("#year").change(function() {
	year=$(this).val();
	patientsInvitation(year, $("#doctorId").val());
});

function patientsInvitation(year, doctorId) {
	doctorId=doctorId || 0;
	$.ajax({
		url: "<?php echo ADM_URL."user/patientsInvitaion"?>?year="+year+"&doctorId="+doctorId,
		dataType: "JSON",
		success: function(res){
			invChart(res);
		}
	});
}

function noOfPatientTreatOptWise(doctorId) {
	doctorId=doctorId || 0;
	$.ajax({
		url: "<?php echo ADM_URL."user/noOfPatientTreatOptWise"?>?doctorId="+doctorId,
		dataType: "JSON",
		success: function(res){
			treatTypeChart(res[1], res[2], res[3], res[4]);
		}
	});
}



$( "#reportAcco" ).accordion({ heightStyle: "content" });

$("#reportAcco h3").click(function() {
	drawResAnaChart(resAna[0], resAna[1], resAna[2], resAna[3]);
	treatTypeChart(tType[0], tType[1], tType[2], tType[3]);
	invChart(invCh[0]);
});
</script>