<div class="pageheader">
	<h2><i class="fa fa-suitcase"></i> Department <span><?php echo $dtl['id']?"Update {$dtl['name']} Details":'Add New';?></span></h2>
    <div class="breadcrumb-wrapper">
    	<a href="<?php echo ADM_URL."hospital/departments";?>">&laquo; back to list</a>
    </div>
</div>


<div class="contentpanel">
	<?php if($errors){?>
    	<div class="alert alert-danger">Please fill the fields marked with red color</div>
    <?php }?>
    
	<?php echo getFlash();?>
    
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<h4>Profile details</h4>
        </div>
        
        <div class="panel-body panel-body-nopadding">
        	<form class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
            	<div class="form-group">
                    <label class="col-sm-3 control-label myreq">Department Name</label>
                    <div class="col-sm-6">
                        <input name="name" id="name" type="text" class="form-control" value="<?php echo h($dtl['name']);?>" maxlength="50" />
                    	<label class="error"><?php echo error_msg($errors,'name');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Head of department</label>
                    <div class="col-sm-6">
                        <input name="deptHead" id="deptHead" type="text" class="form-control" value="<?php echo h($dtl['deptHead']);?>" maxlength="50" />
                    	<label class="error"><?php echo error_msg($errors,'deptHead');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Contact Number</label>
                    <div class="col-sm-6">
                    	<div>
                            <div class="pull-left" style="width:80px; padding-right:10px">
                                <?php echo form_dropdown('stdCd', stdCodesArr(), $dtl['stdCd'], 'class="form-control chosen-select" data-placeholder="STD Code"');?>
                            </div>
                            
                            <div class="pull-left">
                                <input type="text" class="form-control" name="phone" id="phone" value="<?php echo h($dtl['phone']);?>" maxlength="10" valid="int" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <label class="error" id="phoneErr"><?php echo error_msg($errors,'phone');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Email</label>
                    <div class="col-sm-6">
                    	<input name="email" id="email" type="text" class="form-control" value="<?php echo h($dtl['email']);?>" maxlength="100" />
                        <label class="error" id="emailErr"><?php echo error_msg($errors,'email');?></label>
                    </div>
                </div>
                
                
                <?php if($hospitalForms){?>
                <div class="form-group">
                	<label class="col-sm-3 control-label">Assign Form(s)</label>
                    <div class="col-sm-6">
                    	<?php 
							$formIds=array();
							if($deptForms){
								$formIds=array_map(function($arg){return $arg['formId'];}, $deptForms);
							}
						?>
						<?php foreach($hospitalForms as $i=>$f){?>
							<div class="ckbox ckbox-primary">
								<input type="checkbox" id="f<?php echo $i;?>" name="formId[]" value="<?php echo $f['formId'];?>" 
								<?php if(in_array($f['formId'], $formIds)) echo 'checked="checked"';?> />&nbsp;
								<label for="f<?php echo $i;?>"><?php echo $f['formName'];?></label>
							</div>
						<?php }?>
                    </div>
                </div>
                <?php }?>
                
                
                <div class="form-group">
                	<label class="col-sm-3 control-label">Department Logo (100 x 100)</label>
                    <div class="col-sm-6">
                    	<div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="input-append">
                                <div class="uneditable-input">
                                    <i class="glyphicon glyphicon-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileupload-new">Select file</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" name="image" accept="image/*" />
                                </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                        
                        <label class="error"><?php echo error_msg($errors,'image');?></label>
                    </div>
                </div>
                
                
                <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6 col-sm-offset-3">
                			<button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                    		<button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="whiteBox">
	<div class="pad3">
    	<div class="down">
            <form action="<?php echo REQ_URI;?>" method="post" enctype="multipart/form-data">
                <table width="100%" cellpadding="8">
                 
                    
                   
                </table>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
$(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});
</script>