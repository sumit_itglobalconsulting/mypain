<div class="pageheader">
	<div class="row">
        <div class="col-md-8">
            <h2><i class="fa fa-suitcase"></i> Departments</h2>
        </div>
        
        <div class="col-md-4" style="padding-top:4px" align="right">
        	<a href="<?php echo ADM_URL."hospital/addDepartment";?>">
            	<button type="button"class="btn btn-primary">+ Add New</button>
            </a>
        </div>
    </div>
</div>

<div class="contentpanel">
	<div class="people-list">
    	<div class="row">
        	<?php if($list){foreach($list as $i=>$dtl){?>
        	<div class="col-md-4 myListBx1">
            	<div class="people-item">
                	<div class="media">
                    	<span class="pull-left">
                        	<img alt="" src="<?php echo base_url("assets/uploads/department_images/".$dtl['image']);?>" class="thumbnail media-object">
                        </span>
                        <div class="media-body">
                        	<h4 class="person-name"><?php echo $dtl['name'];?></h4>
                            <div class="mb5"></div>
                            <div><span class="text-muted"><i class="fa fa-edit"></i> Treatment Response Forms:</span> <?php echo $dtl['noFForms'];?></div>
                            <div><span class="text-muted"><i class="fa fa-user"></i> Head of department:</span> <?php echo $dtl['deptHead'];?></div>
                            <div>
                            	<span class="text-muted"><i class="fa fa-envelope-o"></i> Email:</span> 
								<a href="mailto:<?php echo $dtl['email'];?>"><?php echo $dtl['email'];?></a>
                            </div>
                            <div><span class="text-muted"><i class="fa fa-phone"></i> Phone:</span> <?php echo $dtl['phone'];?></div>
                            
                            <div class="smallForm" align="right">
                                <a href="<?php echo ADM_URL."hospital/editDepartment/".encode($dtl['id']);?>">
                                    <button type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        	<?php }}else{?>
            	<div class="notFound">Not Found</div>
            <?php }?>
            
        </div>
    </div>
</div>