<?php
/** Error reporting */
//error_reporting(E_ALL);
/** Include path **/
ini_set('include_path', FCPATH.'Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("My Pain Impact");
$objPHPExcel->getProperties()->setLastModifiedBy("MyPain");
$objPHPExcel->getProperties()->setTitle("Patient Report (PGHI)");
$objPHPExcel->getProperties()->setSubject("Patient Report (PGHI)");
$objPHPExcel->getProperties()->setDescription("Patient Report (PGHI)");


// Add some data
$objPHPExcel->setActiveSheetIndex(0);
$sheet=$objPHPExcel->getActiveSheet();

/** Data **/
//Name and date
$sheet->SetCellValue('A1', 'Patient Name: '.$dtl['fullName']);
$sheet->getStyle("A1:B1")->getFont()->setBold(true);
$sheet->mergeCells("A1:B1");

$sheet->SetCellValue('C1', 'Date: '.showDate(time()));
$sheet->getStyle("C1:D1")->getFont()->setBold(true);
$sheet->mergeCells("C1:D1");

/** Form Response **/
$res=$resDtl['quesDtl'];

$sheet->SetCellValue('A3', 'Sr.');
$sheet->SetCellValue('B3', 'Category');
$sheet->SetCellValue('C3', 'Question');
$sheet->SetCellValue('D3', 'Response');
$sheet->SetCellValue('E3', 'Text Answer');
$sheet->getStyle("A3:D3")->getFont()->setBold(true);
$sheet->getStyle("A3:D3")->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'CCCCCC'))));

$i=0;
foreach($res as $rs){
	$sheet->SetCellValue('A'.($i+4), ($i+1).". ");
	$sheet->SetCellValue('B'.($i+4), $rs['catName']);
	$sheet->SetCellValue('C'.($i+4), $rs['ques']); 
	$sheet->SetCellValue('D'.($i+4), str_replace("|", ", ", $rs['optionName']));
	$sheet->SetCellValue('E'.($i+4), str_replace("|", "", $rs['ans']));
	$i++;
}

// Rename sheet
$sheet->setTitle('My Pain Impact PGHI Report');

/** Auto Width **/
$LC = $sheet->getHighestColumn();
for($c='A'; $c<=$LC; $c++){
	//if($c!='C')
	$sheet->getColumnDimension($c)->setAutoSize(true);
}
/** Formatting End **/


// Save Excel 2007 file
/*$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save(FCPATH."assets/temp/report.xlsx");
redirect(URL."test/report.xlsx");
*/

//Direct Download
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="maypain-impact-phgi-report.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
