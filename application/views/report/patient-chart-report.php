<!--[{"optionId":"168","questionId":"28"},{"optionId":"175","questionId":"29"},{"optionId":"183","questionId":"30"},{"optionId":"190","questionId":"31"},{"optionId":"197","questionId":"32"}]

{"Payload":{"response":"[{"optionId":"168","questionId":"28"},{"optionId":"175","questionId":"29"},{"optionId":"183","questionId":"30"},{"optionId":"190","questionId":"31"},{"optionId":"197","questionId":"32"}]","userId":"16"},"Status":"T","Message":"Request was successful.","Code":"OK"}-->

<?php
	$dataAr=array(0=>array('Time'));
	
	$forms=array();
	$dates=array();
	
	$minRes=0;
	
	if($reports){
		foreach($reports as $r){
			$forms[$r['formId']][$r['responseDate']]=$r['overallRes'];
			if($r['overallRes']<$minRes)
				$minRes=$r['overallRes'];
		}
		
		foreach($forms as $fid=>$f){
			array_push($dataAr[0], formName($fid));
		}
	
		foreach($forms as $fid=>$f){
			foreach($f as $d=>$v){
				$dates[]=$d;
			}
		}
		
		$dates=array_unique($dates);
		
		foreach($dates as $d){
			$ar1=array($d);
			foreach($forms as $fid=>$f){
				array_push($ar1, $forms[$fid][$d]?round(intval($forms[$fid][$d])):0);
			}
			array_push($dataAr, $ar1);
		}
	}
?>
<div id="chart_div" style="max-width:800px; width:100%; height: 400px; margin:0 auto"></div>

<script type="text/javascript">
function createChart(ex) {
	c='<?php echo count($dataAr[0])-1;?>';
	minRes='<?php echo $minRes;?>';
	minRes=parseInt(minRes);
	var data = google.visualization.arrayToDataTable(<?php echo json_encode($dataAr);?>);
	var view = new google.visualization.DataView(data);
	
	if(data){
  		var formatter = new google.visualization.NumberFormat({pattern:'#\'%\''});
		
		anno='[0, ';
		for(i=1; i<=c*1; i++){
  			formatter.format(data, i);
			anno+=' '+i+', { calc: "stringify", sourceColumn: '+i+', type: "string", role: "annotation" },';
		}
		anno+=']';
		
		anno=eval(anno);
		view.setColumns(anno);
	}
	
	Ticks=[{v:-40, f:'-40%'}, {v:-20, f:'-20%'}, {v:0, f:'0%'}, {v:20, f:'20%'}, {v:40, f:'40%'}, {v:60, f:'60%'}, 
					{v:80, f:'80%'}, {v:100, f:'100%'}];
	verAxis={title: 'Baseline', titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}, ticks: Ticks};
			
	var options = {
	  title: '',
	  vAxis: verAxis,
	  legend: { position: "right", textStyle: {fontSize: 13}},
	  colors: ['#3366cc','#000000', '#990099', '#0099C6', '#5574A6', '#AAAA11', '#22AA99'],
	};
	
	chartDiv=document.getElementById('chart_div');
	var chart = new google.visualization.ColumnChart(chartDiv);
	
	if(ex==1){
		google.visualization.events.addListener(chart, 'ready', function () {
			chartDiv.innerHTML = '<img id="trendResImg" src="' + chart.getImageURI() + '">';
		});
	}
	
	chart.draw(view, options);
}

createChart('<?php echo $_GET['export'];?>');
</script>