<style>
    .therm_img{float: right;
    height: 350px;
    margin-left: 40px;
    margin-right: 150px;
    margin-top: 40px;}
    
    .resAnaBx .posRel {
    border-radius: 5px;
    float: left;
    height: 140px;
    margin: 15px;
    padding: 5px 10px;
    text-align: center;
    width: 180px;
    }
    .boxPoorRes {
        background: none repeat scroll 0 0 #ff4b31;
    }
    .boxAvgRes{background:#fbd200}
    .boxGoodRes	{background:#6fba61}
    a.boxResPic, .boxResPic {
    color: #fff;
    display: block;
    font-size: 16px;
    font-weight: bold;
}
    
</style>
<?php
	$rfArr=responseFrequencyArr();
?>

<div id="reportBox">
    <a href="<?php echo URL;?>">
        <img src="<?php echo URL;?>assets/img/logo.png" style="height:64px; margin:5px" alt="My Pain Impact" />
    </a>
    
    <tt>
        <div class="reportPInfo">
            <div class="col1"><?php echo $dtl['fullName'];?></div>
            <div class="col1 textCenter">My Pain Impact Report</div>
            <div class="col1 textRight">Date: <?php echo showDate(time());?></div>
            <div class="clr"></div>
        </div>
    </tt>
    
    <div class="down">
        <div class="titleH">Overall treatment response</div>
        <div class="down">
            <div class="col2">
            	<div class="textCenter"><h4 class="pd1"><?php echo USER_TYPE=='P'?'My ':''?>overall treatment response</h4></div>
                <div id="overallResChart" style="width:70%; height:300px; margin:0 auto"><?php echo round($overallRes);?></div>
            </div>
            
            <div class="col2">
            	<div class="textCenter"><h4 class="pd1">How <?php echo USER_TYPE=='P'?'I feel':'Patient feels'?></h4></div>
                <!--<div id="feelResChart" style="width:70%; height:300px; margin:0 auto"><?php //echo $feels;?></div>-->
-                <div id="feelResImage"  class="therm_img">
                        <?php
                            $src='';
                            switch($feels){
                            case -5:
                                $src='red_-5';
                                break;
                            case -4:
                                $src='red_-4';
                                break;
                            case -3:
                                $src='orange_-3';
                                break;
                            case -2:
                                $src='orange_-2';
                                break;
                            case -1:
                                $src='orange_-1';
                                break;
                            case 0:
                                $src='yellow_0';
                                break;
                            case 1:
                                $src='light_green_1';    
                                break;
                            case 2:
                                $src='light_green_2';    
                                break;
                            case 3:
                                $src='light_green_3';    
                                break;
                            case 4:
                                $src='green_4';
                                break;
                            case 5:
                                $src='green_5';    
                                break;
                            }
                        ?>
                        <img src="<?php echo IMG_URL.$src;?>therm.png" />
                </div>
            </div>
            <div class="clr"></div>
        </div>
        <p>&nbsp;</p>
        
        <?php if($patientForms){foreach($patientForms as $i=>$pf){?>
            <?php
                if(USER_TYPE=='D')
                    $userId=$dtl['id'];
                else if(USER_TYPE=='H')
                    $userId=$dtl['id'];
                else
                    $userId=USER_ID;
                    
                $scales=scalesReport($pf['formId'], $pf['doctorId'], $userId);
                $scaleRes=$scales['scales'];
                $scalesReport=$scales['scalesReport'];
                $lastResTime=showDate($scales['lastResTime']);
            ?>
            <div class="titleH">Scores for <?php echo $pf['formName'];?> <?php echo USER_TYPE=='P'?'( '.$pf['doctorName'].')':'';?></div>
            
            <div class="pad1">
                <?php 
					foreach($scaleRes as $j=>$s){
                                                if($pf['formId']==1){
                                                   if($s['id']==2){
                                                       if(empty($s['medTake']))
                                                           continue;
                                                   }
                                                       
                                               }
						$s['curRes']=round($s['curRes']);
						
						$baselineScore=is_numeric($s['baselineScore'])?round($s['baselineScore']):'';
						$lastScore=is_numeric($s['lastScore'])?round($s['lastScore']):'';
						
						/* In case of EuroQol */
						if($pf['formId']==19){
							$s['curRes']=$s['lastScore'];
						}
						
						if($s['id']==36){
							$baselineScore	=number_format($s['baselineScore'],3);
							$lastScore		=number_format($s['lastScore'],3);
							$s['totalScore']=number_format($s['totalScore'],3);
						}
						/* In case of EuroQol End */
						
						if(!$baselineScore && $baselineScore!==0)
							$baselineScore='N/A';
							
						if(!$lastScore && $lastScore!==0)
							$lastScore='N/A';
				?>
                    <fieldset>
                        <?php if($s['scale']!='N/A'){?><legend><?php echo $s['scale'];?></legend><?php }?>
                        <div class="pad1">
                            <div class="down">
                                <div class="col4">
                                    <div class="blueBx posRel">
                                        <p>&nbsp;</p>
                                        Base Line Score: <strong><?php echo $baselineScore;?></strong>
                                        <p>&nbsp;</p>
                                        Last Score: <strong><?php echo $lastScore;?></strong>
                                        
                                        <div class="finfoTxt">
                                            <table>
                                                <tr>
                                                    <td>Max Score</td>
                                                    <td width="3px">:</td>
                                                    <td><strong><?php echo $s['totalScore'];?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Response frequency</td>
                                                    <td width="3px">:</td>
                                                    <td><strong><?php echo $rfArr[$pf['resFreq']];?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Last response at</td>
                                                    <td width="3px">:</td>
                                                    <td><strong><?php echo $lastResTime;?></strong></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col4">
                                    <div class="textCenter">
                                    	<h4>Present Status</h4>
                                        <?php if($pf['formId']!=19){?><?php if($pf['formId']!=6){?>
                                        (Percentage change from baseline score)
                                        <?php }}?>
                                    </div>
                                    <?php if($pf['formId']!=6){    ?>
                                    <div class="resChart<?php echo $pf['formId']==19?'Euro':'';?>" id="resChart<?php echo $i.$j;?>" 
                                    goodRes="<?php echo $s['goodRes'];?>"  
                                    avgRes="<?php echo $s['avgRes'];?>" 
                                    scaleid="<?php echo $s['id'];?>" 
                                    style="width:90%; height:225px; margin:0 auto"><?php echo $s['curRes'];?></div>
                                    <?php } ?>
                                     <?php if($pf['formId']==6){    
                                                $CI = & get_instance();
                                                $result=$CI->star_back_risk($pf['formId'], $pf['doctorId'], $userId); ?>
                                            <div class="resAnaBx ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active" id="ui-accordion-dashAcco-panel-0" aria-labelledby="ui-accordion-dashAcco-header-0" role="tabpanel" aria-expanded="true" aria-hidden="false" style="display: block;">
                   
                                                <div class="boxResPic <?php echo $result['class']; ?> posRel " style="margin-top:60px;margin-left:80px" title="This is tooltip<br>Again">
                        <div style="padding: 45px; cursor: pointer;"><?php echo $result['risk'] ?></div>
                       
                    </div>
                                            </div>
                                        <?php   } ?>   
                                </div>
                                
                                <div class="col4">
                                    <?php  if($pf['formId']==6){
                                            
                                        $dataAr=$CI->star_back_risk($pf['formId'], $pf['doctorId'], $userId,true);
                                            
                                        }else{
										$dataAr=array(0=>array('Time'));
										$dates=array();
										$scalesAr=array();
										if($scalesReport){
											$scalesAr[$s['id']]=$scalesReport[$s['id']];
											foreach($scalesAr as $f){
												array_push($dataAr[0], '');
											}
										
											foreach($scalesAr as $f){
												foreach($f as $d=>$v){
													$dates[]=$d;
												}
											}
											
											$dates=array_unique($dates);
											
											foreach($dates as $d){
												$ar1=array($d);
												foreach($scalesAr as $k=>$f){
													array_push($ar1, $scalesAr[$k][$d]?floatval($scalesAr[$k][$d]):0);
												}
												array_push($dataAr, $ar1);
											}
											
											foreach($dataAr as $k=>&$v){
												if($k==0){
													$v[1]='';
													$v[2]['role']="annotation";
													
													if($s['id']!=36){
														$v[3]='Goal';
														$v[4]['role']="tooltip";
														
														if($s['id']!=37){
															$v[5]='Average';
															$v[6]['role']="tooltip";
														}
													}
												}
												else{
													if($s['id']!=36 && $s['id']!=37){
														$v[1]=round($v[1]);
													}
													
													$v[2]=$pf['formId']==19?$v[1]:$v[1]."%";
													
													if($s['id']!=36){
														$v[3]=$s['goodRes']*1;
														$v[4]='Goal: '.$s['goodRes'].'%';
														
														if($s['id']!=37){
															$v[5]=$s['avgRes']*1;
															$v[6]='Average: '.$s['avgRes'].'%';
														}
														
														if($s['id']==37){
															$diff = abs(time() - strtotime($dtl['dob']));
															$years = floor($diff / (365*60*60*24));
															
															$v[3]=eqVasGoal($years);
															$v[4]='Mean: '.$v[3].'%';
														}
													}
												}
											}
											
											if(count($dataAr)==2){
												if($s['id']!=36){
													$dataAr[2]=$dataAr[1];
													$dataAr[1][0]=$dataAr[1][1]=$dataAr[1][2]=null;
												
													$dataAr[3]=$dataAr[2];
													$dataAr[3][0]=$dataAr[3][1]=$dataAr[3][2]=null;
												}
											}
										}
                                        }     
									?>
                                    
                                    <div class="textCenter">
                                    	<h4>Trend Over Time</h4>
                                        <?php if($pf['formId']!=19){?>
                                        (Percentage change from baseline score)
                                        <?php }?>
                                    </div>
                                    <div class="trendChart<?php echo $pf['formId']==19?'Euro':'';?><?php echo $pf['formId']==6?' starback':'';?>" id="trendChart<?php echo $i.$j;?>" 
                                    minres="<?php echo $minRes;?>" scaleid="<?php echo $s['id'];?>" 
                                    style="width:90%; height:225px; margin:0 auto"><?php echo json_encode($dataAr);?></div>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </fieldset>
                    <p>&nbsp;</p>
                <?php }?>
            </div>
        <?php }}?>
        </div>
    
    <div class="titleH">Trend Over Time (% Change from baseline)</div>
    <div class="down">
        <?php
            $fromD=showDate($firstLastDates['firstRes']);
            $toD=showDate($firstLastDates['lastRes']);
        ?>
        <form action="javascript:void(0)" id="chartReportForm" onsubmit="chartReport()" style="display:none">
            <div>
                <div style="margin-bottom:5px">
                    <table>
                        <tr>
                            <td>From Date</td>
                            <td><input type="text" class="form-control hasCal" id="fromDate" name="fromDate" value="<?php echo $fromD;?>"></td>
                            <td>To Date</td>
                            <td><input type="text" class="form-control hasCal" id="toDate" name="toDate" value="<?php echo $toD;?>"></td>
                            <td><button type="submit" id="repSubBtn">Submit</button></td>
                        </tr>
                    </table>
                </div>
                
                <div class="row" style="padding-left:10px">
                    <?php if($patientForms){foreach($patientForms as $i=>$pf){if($pf['formId']==8) continue;?>
                        <input type="hidden" name="doctorId[<?php echo $pf['formId'];?>]" value="<?php echo $pf['doctorId'];?>" />
                        <input type="checkbox" name="formId[]" value="<?php echo $pf['formId'];?>" id="c<?php echo $i;?>" 
                        checked="checked" onchange="chartReport()" />
                        <label for="c<?php echo $i;?>"><?php echo $pf['formName'];?></label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php }}?>
                </div>
            </div>
        </form>
        <p>&nbsp;</p>
        <div class="textCenter">From <strong><?php echo $fromD;?></strong> to <strong><?php echo $toD;?></strong></div>
        <div id="chartBox" class="chartBox" style="min-height:400px"><!-- Chart --></div>
    </div>
</div>

<p>&nbsp;</p>
<script type="text/javascript">
function overallResChart(id, v, feel) {
	if(isNaN(v))
		return;
	v=parseInt(v);
	if(feel){
		if(v<1)
			colorCode='#ff4b31';
		else if(v>=1 && v<=3)
			colorCode="#fbd200";
		else
			colorCode="#6fba61";
		var data = google.visualization.arrayToDataTable([
				['Year', 'How <?php echo USER_TYPE=='P'?'I':'Patient'?> Feel'], ['PGIC Score',  v],]);
	}
	else{
		if(v<20)
			colorCode='#ff4b31';
		else if(v>=20 && v<50)
			colorCode="#fbd200";
		else
			colorCode="#6fba61";
			
		var data = google.visualization.arrayToDataTable([
				['Year', 'Change from baseline'], ['<?php echo USER_TYPE=='P'?'My ':''?>Overall treatment response',  v],]);
		
		var formatter = new google.visualization.NumberFormat({pattern:'#\'%\''});
		formatter.format(data, 1);
	}
	
	 var view = new google.visualization.DataView(data);
	 view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation" }]);
	
	if(feel){
		Ticks=[{v:-5, f:'-5'}, {v:-4, f:'-4'}, {v:-3, f:'-3'}, {v:-2, f:'-2'}, {v:-1, f:'-1'}, {v:0, f:'0'}, 
			   {v:1, f:'1'}, {v:2, f:'2'}, {v:3, f:'3'}, {v:4, f:'4'}, {v:5, f:'5'}];
		verAxis={title: '', textStyle: {fontSize: 12}, ticks: Ticks};
	}
	else{
		Ticks=[{v:-40, f:'-40%'}, {v:-20, f:'-20%'}, {v:0, f:'0%'}, {v:20, f:'20%'}, {v:40, f:'40%'}, {v:60, f:'60%'}, 
				{v:80, f:'80%'}, {v:100, f:'100%'}];
		verAxis={title: '', textStyle: {fontSize: 12}, ticks: Ticks};
	}
	
	var options = {
	  title: '',
	  hAxis: {title: '', titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}},
	  vAxis: verAxis,
	  legend: { position: "none"},
	  colors: [colorCode],
	};
	
	chartDiv=document.getElementById(id);
	var chart = new google.visualization.ColumnChart(chartDiv);
	
	google.visualization.events.addListener(chart, 'ready', function () {
		chartDiv.innerHTML = '<img src="' + chart.getImageURI() + '">';
	});
	
	chart.draw(view, options);
}
	
overallResChart("overallResChart", $("#overallResChart").text());
//overallResChart("feelResChart", $("#feelResChart").text(), true);


function resChart(id, v, goodRes, avgRes) {
	if(!v){
		//$("#"+id).html('<div class="na1">N/A</div>');
		//return;
		v=0;
	}
	
	if(v<avgRes)
		colorCode='#ff4b31';
	else if(v>=avgRes && v<goodRes)
		colorCode="#fbd200";
	else
		colorCode="#6fba61";
		
	var data = google.visualization.arrayToDataTable([
		['Year', 'Change from baseline', {role: 'annotation'}, 'Goal'], 
		[null,  null, '', goodRes],
		['',  v, v+'%', goodRes],
		[null,  null, '', goodRes],
	]);
	
	
	var formatter = new google.visualization.NumberFormat({pattern:'#\'%\''});
  	formatter.format(data, 1);
	formatter.format(data, 3);
	
	Ticks=[{v:-40, f:'-40%'}, {v:-20, f:'-20%'}, {v:0, f:'0%'}, {v:20, f:'20%'}, {v:40, f:'40%'}, {v:60, f:'60%'}, 
					{v:80, f:'80%'}, {v:100, f:'100%'}];
	verAxis={title: '', textStyle: {fontSize: 12}, ticks: Ticks};
	
	var options = {
	  title: '',
	  hAxis: {title: '', titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}, format: '#\'%\''},
	  vAxis: verAxis,
	  legend: { position: "none", textStyle: {fontSize: 12}},
	  tooltip: {textStyle:  {fontSize: 12}},
	  seriesType: "bars",
	  series: {1: {type: "line", color: "#6fba61"}},
	  colors: [colorCode]
	};
	
	chartDiv=document.getElementById(id);
	var chart = new google.visualization.ComboChart(chartDiv);
	
	google.visualization.events.addListener(chart, 'ready', function () {
		chartDiv.innerHTML = '<img src="' + chart.getImageURI() + '">';
	});
	
	chart.draw(data, options);
}

$(".resChart").each(function(){
	id=$(this).attr('id');
	v=parseInt($(this).text());
	
	goodRes=parseInt($(this).attr('goodRes'));
	avgRes=parseInt($(this).attr('avgRes'));
	resChart(id, v, goodRes, avgRes);
});


function resChartEuro(id, v, scaleId) {
	if(!v){
		//$("#"+id).html('<div class="na1">N/A</div>');
		//return;
		v=0;
	}
		
	var data = google.visualization.arrayToDataTable([['Year', 'Present Score'], ['',  v],]);
	
	var view = new google.visualization.DataView(data);
    view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation",}]);
	
	if(scaleId==36) {
		verAxis={title: '', textStyle: {fontSize: 12}, ticks: [{v:-0.6, f:'-0.6'}, {v:0.0, f:'0.0'}, {v:0.5, f:'0.5'}, {v:1.0, f:'1.0'}]};
	}
	else{
		verAxis={title: '', textStyle: {fontSize: 12}, 
				ticks: [{v:0, f:'0'}, {v:20, f:'20'}, {v:40, f:'40'}, {v:60, f:'60'}, {v:80, f:'80'}, {v:100, f:'100'}]};
	}
	
	var options = {
	  title: '',
	  hAxis: {title: '', textStyle: {fontSize: 12}},
	  vAxis: verAxis,
	  legend: { position: "none", textStyle: {fontSize: 12}},
	  tooltip: {textStyle:  {fontSize: 12}},
	};
	
	chartDiv=document.getElementById(id);
	var chart = new google.visualization.ColumnChart(chartDiv);
	
	google.visualization.events.addListener(chart, 'ready', function () {
		chartDiv.innerHTML = '<img src="' + chart.getImageURI() + '">';
	});
	
	chart.draw(view, options);
}

$(".resChartEuro").each(function(){
	id=$(this).attr('id');
	scaleId=$(this).attr('scaleid');
	v=parseFloat($(this).text());
	
	resChartEuro(id, v, scaleId);
});


function trendChart(id, jdata) {
	c=0;
	if(jdata)
		c=jdata[0].length-1;
	
	if(c<=0){
		$("#"+id).html('<div class="na1">N/A</div>');
		return;
	}
	
	var data = google.visualization.arrayToDataTable(jdata);
	if(data){
		var formatter = new google.visualization.NumberFormat({pattern:'#\'%\''});
		for(i=1; i <= c; i++){
			formatter.format(data, i);
		}
	}
  
	
	Ticks=[{v:-40, f:'-40%'}, {v:-20, f:'-20%'}, {v:0, f:'0%'}, {v:20, f:'20%'}, {v:40, f:'40%'}, {v:60, f:'60%'}, 
					{v:80, f:'80%'}, {v:100, f:'100%'}];
	verAxis={title: '', textStyle: {fontSize: 12}, ticks: Ticks};
	
	var options = {
	  title: '',
	  hAxis: {title: '', titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}, format: '#\'%\''},
	  vAxis: verAxis,
	  legend: { position: "none"},
	  colors: ['#3366cc','#000000', '#990099', '#0099C6', '#5574A6', '#AAAA11', '#22AA99'],
	  tooltip: {textStyle:  {fontSize: 12}},
	  seriesType: "bars",
	  series: {1: {type: "line", color: "#6fba61"}, 2: {type: "line", color: "#fbd200"}},
	};
	
	chartDiv=document.getElementById(id);
	var chart = new google.visualization.ComboChart(chartDiv);
	
	
	google.visualization.events.addListener(chart, 'ready', function () {
		chartDiv.innerHTML = '<img src="' + chart.getImageURI() + '">';
	});
	
	chart.draw(data, options);
}

function trendChartStarBack(id, jdata) { 
                c=0;
                if(jdata)
                        c=jdata[0].length-1;

                if(c<=0){
                        $("#"+id).html('<div class="na1">N/A</div>');
                        return;
                }
                //var data = google.visualization.arrayToDataTable(jdata);
         var data = google.visualization.arrayToDataTable(jdata);

      var options = {
        
        legend: { position: 'top', maxLines: 5 },
        bar: { groupWidth: '60%' },
        isStacked: true,
      };
        chartDiv=document.getElementById(id);
	var chart = new google.visualization.ColumnChart(chartDiv);
       
        google.visualization.events.addListener(chart, 'ready', function () {
                chartDiv.innerHTML = '<img  src="' + chart.getImageURI() + '">';
        });
	
	
	chart.draw(data, options);
}

$(".trendChart").each(function(){
        if(!$(this).hasClass('starback') ){
           trendChart($(this).attr('id'), eval($.trim($(this).text())));
       }else{
           trendChartStarBack($(this).attr('id'), eval($.trim($(this).text())));
       }
	
});



function trendChartEuro(id, jdata, scaleId) {
	c=0;
	if(jdata)
		c=jdata[0].length-1;
	
	if(c<=0){
		$("#"+id).html('<div class="na1">N/A</div>');
		return;
	}
	
	var data = google.visualization.arrayToDataTable(jdata);
	var view = new google.visualization.DataView(data);
	
	if(scaleId==36) {
		verAxis={title: '', textStyle: {fontSize: 12}, ticks: [{v:-0.6, f:'-0.6'}, {v:0.0, f:'0.0'}, {v:0.5, f:'0.5'}, {v:1.0, f:'1.0'}]};
	}
	else{
		verAxis={title: '', textStyle: {fontSize: 12}, 
				ticks: [{v:0, f:'0'}, {v:20, f:'20'}, {v:40, f:'40'}, {v:60, f:'60'}, {v:80, f:'80'}, {v:100, f:'100'}]};
	}
	
	if(data){
		var formatter = new google.visualization.NumberFormat({pattern:'#.###'});
		for(i=1; i <= c; i++){
			formatter.format(data, i);
		}
	}
	
	chartDiv=document.getElementById(id);
	
	if(scaleId==36) {
		var options = {
		  title: '',
		  hAxis: {title: '', textStyle: {fontSize: 12}, format: '#.###'},
		  vAxis: verAxis,
		  legend: { position: "none"},
		  colors: ['#3366cc','#000000', '#990099', '#0099C6', '#5574A6', '#AAAA11', '#22AA99'],
		  tooltip: {textStyle:  {fontSize: 12}},
		};
		
		var chart = new google.visualization.ColumnChart(chartDiv);
	}
	else{
		var options = {
		  title: '',
		  hAxis: {title: '', textStyle: {fontSize: 12}, format: '#.###'},
		  vAxis: verAxis,
		  legend: { position: "none"},
		  colors: ['#3366cc','#000000', '#990099', '#0099C6', '#5574A6', '#AAAA11', '#22AA99'],
		  tooltip: {textStyle:  {fontSize: 12}},
		  seriesType: "bars",
	  	  series: {1: {type: "line", color: "#6fba61"}},
		};
		
		var chart = new google.visualization.ComboChart(chartDiv);
	}
	
	google.visualization.events.addListener(chart, 'ready', function () {
		chartDiv.innerHTML = '<img src="' + chart.getImageURI() + '">';
	});
	
	chart.draw(data, options);
}


$(".trendChartEuro").each(function(){
	trendChartEuro($(this).attr('id'), eval($.trim($(this).text())), $(this).attr('scaleid'));
});



function chartReport(){
	ajax(SITE_URL+"common/chartReport/<?php echo $dtl['id'];?>?export=1", "chartBox", "chartReportForm", "", "POST");
}
chartReport();
</script>



<!-- Export function -->
<div align="center">
    <form id="exportForm" method="post">
    	<input type="hidden" name="exType" id="exType" value="P" />
        <input type="hidden" name="resImg" id="resImg" value="" />
        <textarea name="pdf" id="pdf" style="display:none"></textarea>
        <button type="button" class="exportBtn btnPdf">Download as PDF</button>
        <button type="button" class="exportBtn btnExl">Download as Excel</button>
    </form>
</div>
<script type="text/javascript">
function exportReport() {
	$(".exportBtn").hide();
	html=document.documentElement.outerHTML;
        $("#pdf").val(html);
	$(".exportBtn").show();
	$("#exportForm").submit();
}

$(".btnPdf").click(function() {
	$("#exType").val('P');
	exportReport();
});

$(".btnExl").click(function() {
	$("#exType").val('E');
	$("#resImg").val($("#trendResImg").attr('src'));
	$("#exportForm").submit();
});
</script>
<!-- /Export function -->