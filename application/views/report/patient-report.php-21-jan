<?php
	$rfArr=responseFrequencyArr();
	$today=date('Y-m-d 00:00:00');
	
	$overallResHelp="Overall treatment response gives an overview of how your symptoms have been as calculated by different forms. Some forms are used to measure different ways pain is impacting your life and how it as changed from when your treatment was first started. This is reported as 'My Overall treatment response'.";
	
	$feelHelp="This part of overall treatment response is the 'How I (Patient) Feel' section that highlights how you have reported to be 'feeling' with regards to your symptoms. This is based on your responses to the Patient Global Impression of Change form or PGIC.";
	
	$presentStatusHelp="This is 'Present Status', which is based on the change between baseline and the most recently submitted responses.";
	
	$trendHelp="This box represents a 'Trend over time' for that particular form or scale. This figure shows the change that is captured by the answers to the individual forms or scales.";
	
	$presentStatusEuro36Help=$presentStatusHelp;
	$presentStatusEuro37Help=$presentStatusHelp;
	$trendEuro36Help=$trendHelp;
	$trendEuro37Help=$trendHelp; //Sat@#123
?>
<div class="pageheader">
	<h2><i class="fa fa-bar-chart-o"></i> <?php echo (USER_TYPE=='P')?'':$dtl['fullName']." : ";?>Report</h2>
    <div class="breadcrumb-wrapper">
    	<?php
			$expUrl=URL."common/exportReport";
			switch(USER_TYPE){
				case 'P':
				break;
				
				case 'D':
					$expUrl=URL."common/exportReport/".encode($patientId)."/".encode($doctorId);
				break;
				
				case 'H':
					$expUrl=URL."common/exportReport/".encode($patientId)."/".encode($doctorId);
				break;
			}
		?>
    	<a href="<?php echo $expUrl;?>" class="btn btn-primary btn-xs">Export Report</a>
		<?php if($pghi){?>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="<?php echo URL."common/exportPghiForm/".encode($patientId)."/".encode($pghiDoctorId);?>" class="btn btn-primary btn-xs">Export PGHI Report</a>
		<?php }?>
    </div>
</div>

<div class="contentpanel reportDiv" style="padding-bottom:0px">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Overall Treatment Response</h4>
        </div>
        
        <div class="panel-body">
            <div class="row">
            	<div class="col-md-6">
                	<div class="posRel" style="width:100%; margin:0 auto">
                        <div class="help1 helpPos2 tooltips" title="<?php echo $overallResHelp;?>"></div>
                        
                        <h4 class="mb5 text-center">
                            <?php echo USER_TYPE=='P'?'My ':''?>overall treatment response
                        </h4>
                        <div id="overallResChart" style="width:100%; height:350px; margin:0 auto"><?php echo round($overallRes);?></div>
                    </div>
                </div>
                
                <div class="col-md-6">
                	<div class="posRel" style="width:100%; margin:0 auto">
                    	<div class="help1 helpPos2 tooltips" title="<?php echo $feelHelp;?>"></div>
                        
                        <h4 class="mb5 text-center">How <?php echo USER_TYPE=='P'?'I Feel':'Patient Feels'?></h4>
                        <!--<div id="feelResChart" style="width:100%; height:350px; margin:0 auto"><?php //echo $feels;?></div>-->
                        <div id="feelResImage"  class="therm_img">
                        <?php
                            $src='';
                            switch($feels){
                            case -5:
                                $src='red_-5';
                                break;
                            case -4:
                                $src='red_-4';
                                break;
                            case -3:
                                $src='orange_-3';
                                break;
                            case -2:
                                $src='orange_-2';
                                break;
                            case -1:
                                $src='orange_-1';
                                break;
                            case 0:
                                $src='yellow_0';
                                break;
                            case 1:
                                $src='light_green_1';    
                                break;
                            case 2:
                                $src='light_green_2';    
                                break;
                            case 3:
                                $src='light_green_3';    
                                break;
                            case 4:
                                $src='green_4';
                                break;
                            case 5:
                                $src='green_5';    
                                break;
                            }
                        ?>
                                        <img src="<?php echo IMG_URL.$src;?>therm.png"/>
                                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
	function overallResChart(id, v, feel) {
		v=parseInt(v);
		if(feel){
			if(v<1)
				colorCode='#ff4b31';
			else if(v>=1 && v<=3)
				colorCode="#fbd200";
			else
				colorCode="#6fba61";
			var data = google.visualization.arrayToDataTable([
				['Year', 'How <?php echo USER_TYPE=='P'?'I':'Patient'?> Feel'], ['PGIC Score',  v],]);
		}
		else{
			if(v<30)
				colorCode='#ff4b31';
			else if(v>=30 && v<50)
				colorCode="#fbd200";
			else
				colorCode="#6fba61";
				
			var data = google.visualization.arrayToDataTable([
				['Year', 'Change from baseline'], ['<?php echo USER_TYPE=='P'?'My ':''?>Overall treatment response',  v],]);
			
			var formatter = new google.visualization.NumberFormat({pattern:'#\'%\''});
  			formatter.format(data, 1);
		}
		
		 var view = new google.visualization.DataView(data);
		 view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation" }]);
		
		if(feel){
			Ticks=[{v:-5, f:'-5'}, {v:-4, f:'-4'}, {v:-3, f:'-3'}, {v:-2, f:'-2'}, {v:-1, f:'-1'}, {v:0, f:'0'}, 
				   {v:1, f:'1'}, {v:2, f:'2'}, {v:3, f:'3'}, {v:4, f:'4'}, {v:5, f:'5'}];
			verAxis={title: '', textStyle: {fontSize: 12}, ticks: Ticks};
		}
		else{
			Ticks=[{v:-40, f:'-40%'}, {v:-20, f:'-20%'}, {v:0, f:'0%'}, {v:20, f:'20%'}, {v:40, f:'40%'}, {v:60, f:'60%'}, 
					{v:80, f:'80%'}, {v:100, f:'100%'}];
			verAxis={title: '', textStyle: {fontSize: 12}, ticks: Ticks};
		}
		
		var options = {
		  title: '',
		  hAxis: {title: '', titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}},
		  vAxis: verAxis,
		  legend: { position: "none", textStyle: {fontSize: 12}},
		  colors: [colorCode]
		};
		
		var chart = new google.visualization.ColumnChart(document.getElementById(id));
		chart.draw(view, options);
	}
	
	overallResChart("overallResChart", $("#overallResChart").text());
	//overallResChart("feelResChart", $("#feelResChart").text(), true);
	</script>
    
    
     <?php if($patientForms){foreach($patientForms as $i=>$pf){ 
	 if($pf['formId']==8) //Added on 10-jan-2015
                  continue;
	?>
     	<div class="panel panel-default">
            <div class="panel-heading">
            	<?php if($firstLastDates['firstRes']){?>
            	<div class="pull-right">
                	<?php
						if(USER_TYPE=='D')
							$resUrl=DOCT_URL."patients/response/".encode($pf['formId'])."/".encode($dtl['id']);
						else if(USER_TYPE=='H')
							$resUrl=ADM_URL."hospital/response/".encode($pf['formId'])."/".encode($dtl['id'])."/".encode($pf['doctorId']);
						else
							$resUrl=PATIENT_URL."form/response/".encode($pf['formId'])."/".encode($pf['doctorId']);
					?>
                    <a href="<?php echo $resUrl;?>"><i class="fa fa-eye"></i> View Responses</a>
                </div>
                <?php }?>
                
                <h4 class="panel-title"><?php echo $pf['formName'];?> <?php echo USER_TYPE=='P'?'( '.$pf['doctorName'].')':'';?></h4>
            </div>
            
            <?php 
				if(USER_TYPE=='D')
					$userId=$dtl['id'];
				else if(USER_TYPE=='H')
					$userId=$dtl['id'];
				else
					$userId=USER_ID;
					
				$scales=scalesReport($pf['formId'], $pf['doctorId'], $userId); 
                                
				$scaleRes=$scales['scales']; 
				$scalesReport=$scales['scalesReport'];
				$lastResTime=showDate($scales['lastResTime']);
			?>
            
            <div class="panel-body">
            	<?php 
					foreach($scaleRes as $j=>$s){
						$s['curRes']=round($s['curRes']);
						
						$baselineScore=is_numeric($s['baselineScore'])?round($s['baselineScore']):'';
						$lastScore=is_numeric($s['lastScore'])?round($s['lastScore']):'';
						
						/* In case of EuroQol */
						if($pf['formId']==19){
							$s['curRes']=$s['lastScore'];
						}
						
						if($s['id']==36){
							$baselineScore=number_format($s['baselineScore'],3);
							$lastScore=number_format($s['lastScore'],3);
							$s['totalScore']=number_format($s['totalScore'],3);
						}
						/* In case of EuroQol End */
						
						if(!$baselineScore && $baselineScore!==0)
							$baselineScore='N/A';
							
						if(!$lastScore && $lastScore!==0)
							$lastScore='N/A';
				?>
                <fieldset>
                	<?php if($s['scale']!='N/A'){?>
                	<legend><?php echo $s['scale'];?></legend>
                    <?php }?>
                    
                    <div style="padding:10px">
                    	<div class="row">
                            <div class="col-md-4">
                            	<?php if($pf['formId']==7){?>
                                	<div class="tdBx1 blueBx posRel" style="margin-bottom:5px">
                                    	<div style="padding:20px 0px 40px 0px">
                                    		<?php echo $pf['formTitle'];?>
                                        </div>
                                        <div class="finfoTxt" align="right">
                                            Last response at: <strong><?php echo $lastResTime?$lastResTime:'N/A';?></strong>
                                        </div>
                                    </div>
                                <?php }else{?>
                                	<div class="tdBx blueBx posRel" style="margin-bottom:5px">
                                        <div style="padding-top:20px">
                                            <?php if($pf['formId']!=9){ ?>
                                            Base Line Score: <strong><?php echo $baselineScore;?></strong><?php } ?>
                                            <p>&nbsp;</p>
                                            
                                            Last Score: <strong><?php echo $lastScore;?></strong>
                                        </div>
                                        <div class="finfoTxt" align="right">
                                            <table class="filterTbl">
                                                <tr>
                                                    <td>Max Score</td>
                                                    <td width="3px">:</td>
                                                    <td>
                                                        <strong class="numCss"><?php echo $s['totalScore'];?></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Response frequency</td>
                                                    <td width="3px">:</td>
                                                    <td><strong><?php echo $rfArr[$pf['resFreq']];?></strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Last response at</td>
                                                    <td width="3px">:</td>
                                                    <td><strong><?php echo $lastResTime?$lastResTime:'N/A';?></strong></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                            
                            <div class="col-md-4">
                            	<div class="posRel" style="width:100%; margin:0 auto">
                                	<?php if($pf['formId']==7){?>
                                    	<div style="text-align:center; padding-top:30px">
                                    		<a href="<?php echo $resUrl;?>" class="btn btn-primary"><i class="fa fa-eye"></i> View Responses</a>
                                        </div>
                                    <?php }else{?>
										<?php if($s['id']==36){?>
                                            <div class="help1 helpPos3 tooltips" title="<?php echo $presentStatusEuro36Help;?>"></div>
                                        <?php }else if($s['id']==37){?>
                                            <div class="help1 helpPos3 tooltips" title="<?php echo $presentStatusEuro37Help;?>"></div>
                                        <?php }else{?>
                                            <div class="help1 helpPos2 tooltips" title="<?php echo $presentStatusHelp;?>"></div>
                                        <?php }?>
                                        
                                        <div class="text-center mb5">
                                            <h4 class="" style="margin:0">Present Status</h4>
                                            <?php if($pf['formId']!=19){?>
                                            <?php if($pf['formId']!=6 && $pf['formId']!=9){?>
                                            (Percentage change from baseline score)
                                            <?php }?>
                                            <?php } ?>
                                        </div>
                                        <?php if($pf['formId']!=6 && $pf['formId']!=9){    ?>
                                        <div class="resChart<?php echo $pf['formId']==19?'Euro':'';?>" id="resChart<?php echo $i.$j;?>" 
                                        goodRes="<?php echo $s['goodRes'];?>" 
                                        avgRes="<?php echo $s['avgRes'];?>" 
                                        scaleid="<?php echo $s['id'];?>" 
                                        style="width:100%; height:230px; margin:0 auto"><?php echo $s['curRes'];?></div>
                                        <?php } ?>
                                         <?php if($pf['formId']==6){    
                                                $CI = & get_instance();
                                                $result=$CI->star_back_risk($pf['formId'], $pf['doctorId'], $userId); ?>
                                                <div class="star_back_pre <?php echo $result['class']; ?>" title="This is tooltip<br>Again">
                        <div style="padding: 58px 0; cursor: pointer;"><?php echo $result['risk'] ?></div>
                        
                    </div>
                                           
                                        <?php   }
                                            if($pf['formId']==9){ 
                                                $gp_src='';
                                                if($lastScore>=3)
                                                    $gp_src='gp_active';
                                                elseif($lastScore>=2 and $lastScore<3)    
                                                    $gp_src='gp_mod_active';
                                                elseif($lastScore>=1 and $lastScore<2)    
                                                    $gp_src='gp_mod_inactive';
                                                else
                                                    $gp_src='gp_inactive';
                                           
                                           
                                        ?>   
                                            <div class="therm_img">
                                                 <img src="<?php echo IMG_URL.$gp_src;?>.jpg"/>
                                            </div>
                                    <?php  } }?>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <?php 
                                        if($pf['formId']==6){
                                            
                                        $dataAr=$CI->star_back_risk($pf['formId'], $pf['doctorId'], $userId,true);
                                            
                                        }
                                        elseif($pf['formId']==9){
                                          $CI = & get_instance();
                                          $dataAr=$CI->gppaq_trend($pf['formId'], $pf['doctorId'], $userId);
                                       }
                                       else{
                                        
                                        $dataAr=array(0=>array('Time'));
                                        $dates=array();
                                        $scalesAr=array();
                                        if($scalesReport){
                                                $scalesAr[$s['id']]=$scalesReport[$s['id']];
                                                foreach($scalesAr as $f){
                                                        array_push($dataAr[0], '');
                                                }

                                                foreach($scalesAr as $f){
                                                        foreach($f as $d=>$v){
                                                                $dates[]=$d;
                                                        }
                                                }

                                                $dates=array_unique($dates);
                                                $dates=array_slice($dates,-5);
                                                foreach($dates as $d){
                                                        $ar1=array($d);
                                                        foreach($scalesAr as $k=>$f){
                                                                array_push($ar1, $scalesAr[$k][$d]?floatval($scalesAr[$k][$d]):0);
                                                        }
                                                        array_push($dataAr, $ar1);
                                                }

                                                foreach($dataAr as $k=>&$v){
                                                        if($k==0){
                                                                $v[1]='';
                                                                $v[2]['role']='annotation';

                                                                if($s['id']!=36){
                                                                        $v[3]='Goal';
                                                                        $v[4]['role']='tooltip';

                                                                        if($s['id']!=37){
                                                                                $v[5]='Average';
                                                                                $v[6]['role']='tooltip';
                                                                        }
                                                                }
                                                        }
                                                        else{
                                                                if($s['id']!=36 && $s['id']!=37){
                                                                        $v[1]=round($v[1]);
                                                                }

                                                                $v[2]=$pf['formId']==19?$v[1]:$v[1]."%";

                                                                if($s['id']!=36){
                                                                        $v[3]=$s['goodRes']*1;
                                                                        $v[4]='Goal: '.$s['goodRes'].'%';

                                                                        if($s['id']!=37){
                                                                                $v[5]=$s['avgRes']*1;
                                                                                $v[6]='Average: '.$s['avgRes'].'%';
                                                                        }

                                                                        if($s['id']==37){
                                                                                $diff = abs(time() - strtotime($dtl['dob']));
                                                                                $years = floor($diff / (365*60*60*24));

                                                                                $v[3]=eqVasGoal($years);
                                                                                $v[4]='Mean: '.$v[3].'%';
                                                                        }
                                                                }
                                                        }
                                                }

                                                if(count($dataAr)==2){
                                                        if($s['id']!=36){
                                                                $dataAr[2]=$dataAr[1];
                                                                $dataAr[1][0]=$dataAr[1][1]=$dataAr[1][2]=null;

                                                                $dataAr[3]=$dataAr[2];
                                                                $dataAr[3][0]=$dataAr[3][1]=$dataAr[3][2]=null;
                                                        }
                                                }
                                        }
                                       }//End of else for if form id 6 
                                ?>
                                
                                <div class="posRel" style="width:100%; margin:0 auto">
                                	<?php if($pf['formId']==7){?>
                                    	<div style="text-align:center; padding-top:30px">
                                    		<a href="<?php echo URL."common/exportPghiForm/".encode($patientId)."/".encode($pghiDoctorId);?>" 
                                            class="btn btn-primary"><i class="fa fa-bar-chart-o"></i> Export Report</a>
                                        </div>
                                    <?php }else{?>
										<?php if($s['id']==36){?>
                                            <div class="help1 helpPos3 tooltips" title="<?php echo $trendEuro36Help;?>"></div>
                                        <?php }else if($s['id']==37){?>
                                            <div class="help1 helpPos3 tooltips" title="<?php echo $trendEuro37Help;?>"></div>
                                        <?php }else{?>
                                            <div class="help1 helpPos2 tooltips" title="<?php echo $trendHelp;?>"></div>
                                        <?php }?>
                                        
                                        <div class="text-center mb5">
                                            <h4 class="" style="margin:0">Trend Over Time</h4>
                                            <?php if($pf['formId']!=19){?> 
                                            (Percentage change from baseline score)
                                            <?php }?>
                                        </div>
                                        <?php //echo '<pre>';print_r($dataAr); ?>
                                        <div class="trendChart<?php echo $pf['formId']==19?'Euro':'';?><?php echo ($pf['formId']==6 ||$pf['formId']==9)?' starback':'';?>" id="trendChart<?php echo $i.$j;?>" 
                                        minres="<?php echo $minRes;?>" scaleid="<?php echo $s['id'];?>" 
                                        style="width:100%;height:230px; margin:0 auto"><?php echo json_encode($dataAr); ?>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <p>&nbsp;</p>
                <?php }?>
            </div>
        </div>
    <?php }}?>
    
    <div class="clearfix"></div>
    
    <div class="panel panel-default">
    	<div class="panel-heading">
           <h4 class="panel-title">Trend Over Time (% Change from baseline)</h4>
        </div>
        
        <div class="panel-body">
        	<?php
				$fromD=showDate($firstLastDates['firstRes']);
				$toD=showDate($firstLastDates['lastRes']);
			?>
			<form action="javascript:void(0)" id="chartReportForm" onsubmit="chartReport()">
				<div class="form-group smallForm" style="width:550px; margin:0 auto">
                	<div class="row">
                        <label class="col-sm-2 control-label text-left">From Date</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control hasCal" id="fromDate" name="fromDate" value="<?php echo $fromD;?>">
                        </div>
                        
                        <label class="col-sm-2 control-label text-left">To Date</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control hasCal" id="toDate" name="toDate" value="<?php echo $toD;?>">
                        </div>
                        
                        <div class="col-md-2">
                            <button type="submit" id="repSubBtn" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    <div class="mb5"></div>
                    
                    <div class="row" style="padding-left:10px">
			<?php if($patientForms){
                        foreach($patientForms as $i=>$pf){
                            if($pf['formId']==7 || $pf['formId']==8) continue;?>
                            <input type="hidden" name="doctorId[<?php echo $pf['formId'];?>]" value="<?php echo $pf['doctorId'];?>" />
                            <div style="float:left; width:110px">
                            <input type="checkbox" name="formId[]" value="<?php echo $pf['formId'];?>" id="c<?php echo $i;?>" 
                            checked="checked" onchange="chartReport()" />
                            <label for="c<?php echo $i;?>"><?php echo $pf['formName'];?></label>
                            </div>
                        <?php }}?>
                        <div class="clearfix"></div>
                    </div>
				</div>
			</form>
            
            <div id="chartBox" class="chartBox" style="min-height:500px;">
                <!-- Chart -->
            </div>
        </div>
    </div>
</div>

<div style="padding:0 20px 20px 0; margin-bottom:30px; border-bottom:1px solid #ccc">
	<a href="<?php echo $expUrl;?>" class="btn btn-primary pull-right">Export Report</a>
    
	<?php
		$dif=floor((strtotime($dtl['followUpDate'])-time())/3600/24)+1;
		if($dif>2){
			$aclass="greenBg";
		}
		else if($dif<=2 && $dif>=0){
			$aclass="amberBg";
		}
		else {
			$aclass="redBg";
		}
		
		if($dtl['followUpDone']) {
			$chk='checked'; 
			$aclass="greenBg";
		}
		else{
			$chk='';
		}
	?>
    
    <?php if(USER_TYPE=='D' && $dtl['followUpDate']<=$today){?>
        <div class="pull-right" style="width:250px">
            <div class="pull-left" style="width:90px">
                <div class="followUpStBx <?php echo $aclass;?>" title="Follow Up Status"></div>
            </div>
            <div class="pull-left" style="width:100px; padding-top:6px">
                <div class="ckbox ckbox-primary">
                    <input type="checkbox" class="followCheck" id="followCheck" u="<?php echo $dtl['id'];?>" d="<?php echo USER_ID;?>" 
                    cclass="<?php echo $aclass;?>" <?php echo $chk;?> />
                    <label for="followCheck">Followed</label>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php }?>
    
    <div class="clearfix"></div>
</div>


<script type="text/javascript">
$(".followCheck").change(function() {
	obj=$(this);
	u=obj.attr("u");
	d=obj.attr("d");
	st=obj.prop("checked");
	if(st) st=1; else st=0;
	pUrl="<?php echo DOCT_URL;?>patients/followUpChecked/"+u+"/"+d+"/"+st;
	
	$.ajax({
		url:pUrl,
		success:function(res){
			cclass=obj.attr('cclass');
			$(".followUpStBx").removeClass("redBg amberBg greenBg");
			if(res=='Y')
				$(".followUpStBx").addClass("greenBg");
			else{
				if(cclass=='greenBg')
					cclass='redBg';
				$(".followUpStBx").addClass(cclass);
			}
		}
	});
});

function resChart(id, v, goodRes, avgRes) {
	if(!v){
		//$("#"+id).html('<div class="na1">N/A</div>');
		//return;
		v=0;
	}
	
	if(v<avgRes)
		colorCode='#ff4b31';
	else if(v>=avgRes && v<goodRes)
		colorCode="#fbd200";
	else
		colorCode="#6fba61";
		
	var data = google.visualization.arrayToDataTable([
		['Year', 'Change from baseline', {role: 'annotation'}, 'Goal'], 
		[null,  null, '', goodRes],
		['',  v, v+'%', goodRes],
		[null,  null, '', goodRes],
	]);
	
	
	var formatter = new google.visualization.NumberFormat({pattern:'#\'%\''});
  	formatter.format(data, 1);
	formatter.format(data, 3);
	
	Ticks=[{v:-40, f:'-40%'}, {v:-20, f:'-20%'}, {v:0, f:'0%'}, {v:20, f:'20%'}, {v:40, f:'40%'}, {v:60, f:'60%'}, 
					{v:80, f:'80%'}, {v:100, f:'100%'}];
	verAxis={title: '', textStyle: {fontSize: 12}, ticks: Ticks};
	
	var options = {
	  title: '',
	  hAxis: {title: '', titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}, format: '#\'%\''},
	  vAxis: verAxis,
	  legend: { position: "none", textStyle: {fontSize: 12}},
	  tooltip: {textStyle:  {fontSize: 12}},
	  seriesType: "bars",
	  series: {1: {type: "line", color: "#6fba61"}},
	  colors: [colorCode]
	};
	
	var chart = new google.visualization.ComboChart(document.getElementById(id));
	chart.draw(data, options);
}

$(".resChart").each(function(){
	id=$(this).attr('id');
	v=parseInt($(this).text());
	
	goodRes=parseInt($(this).attr('goodRes'));
	avgRes=parseInt($(this).attr('avgRes'));
	resChart(id, v, goodRes, avgRes);
});


function resChartEuro(id, v, scaleId) {
	if(!v){
		//$("#"+id).html('<div class="na1">N/A</div>');
		//return;
		v=0;
	}
		
	var data = google.visualization.arrayToDataTable([['Year', 'Present Score'], ['',  v],]);
	
	var view = new google.visualization.DataView(data);
    view.setColumns([0, 1, { calc: "stringify", sourceColumn: 1, type: "string", role: "annotation",}]);
	
	if(scaleId==36) {
		verAxis={title: '', textStyle: {fontSize: 12}, ticks: [{v:-0.6, f:'-0.6'}, {v:0.0, f:'0.0'}, {v:0.5, f:'0.5'}, {v:1.0, f:'1.0'}]};
	}
	else{
		verAxis={title: '', textStyle: {fontSize: 12}, 
				ticks: [{v:0, f:'0'}, {v:20, f:'20'}, {v:40, f:'40'}, {v:60, f:'60'}, {v:80, f:'80'}, {v:100, f:'100'}]};
	}
	
	var options = {
	  title: '',
	  hAxis: {title: '', textStyle: {fontSize: 12}},
	  vAxis: verAxis,
	  legend: { position: "none", textStyle: {fontSize: 12}},
	  tooltip: {textStyle:  {fontSize: 12}},
	};
	
	var chart = new google.visualization.ColumnChart(document.getElementById(id));
	chart.draw(view, options);
}

$(".resChartEuro").each(function(){
	id=$(this).attr('id');
	scaleId=$(this).attr('scaleid');
	v=parseFloat($(this).text());
	
	resChartEuro(id, v, scaleId);
});
</script>

<script type="text/javascript">
/** Chart Report **/
function chartReport(){
	ajax(SITE_URL+"common/chartReport/<?php echo $dtl['id'];?>/<?php echo $doctorId;?>", "chartBox", "chartReportForm", "", "POST");
}
chartReport();
</script>



<script type="text/javascript">
function trendChart(id, jdata) { 
	c=0;
	if(jdata)
		c=jdata[0].length-1;
	
	if(c<=0){
		$("#"+id).html('<div class="na1">N/A</div>');
		return;
	}
	
	var data = google.visualization.arrayToDataTable(jdata);
	if(data){
		var formatter = new google.visualization.NumberFormat({pattern:'#\'%\''});
		for(i=1; i <= c; i++){
			formatter.format(data, i);
		}
	}
  
	
	Ticks=[{v:-40, f:'-40%'}, {v:-20, f:'-20%'}, {v:0, f:'0%'}, {v:20, f:'20%'}, {v:40, f:'40%'}, {v:60, f:'60%'}, 
					{v:80, f:'80%'}, {v:100, f:'100%'}];
	verAxis={title: '', textStyle: {fontSize: 12}, ticks: Ticks};
	
	var options = {
	  title: '',
	  hAxis: {title: '', titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}, format: '#\'%\''},
	  vAxis: verAxis,
	  legend: { position: "none"},
	  colors: ['#3366cc','#000000', '#990099', '#0099C6', '#5574A6', '#AAAA11', '#22AA99'],
	  tooltip: {textStyle:  {fontSize: 12}},
	  seriesType: "bars",
	  series: {1: {type: "line", color: "#6fba61"}, 2: {type: "line", color: "#fbd200"}},
	};
	
	chartDiv=document.getElementById(id);
	var chart = new google.visualization.ComboChart(chartDiv);
	
	chart.draw(data, options);
}
function trendChartStarBack(id, jdata) { 
        c=0;
        if(jdata)
                c=jdata[0].length-1;

        if(c<=0){
                $("#"+id).html('<div class="na1">N/A</div>');
                return;
        }
                //var data = google.visualization.arrayToDataTable(jdata);
        var data = google.visualization.arrayToDataTable(jdata);
        verAxis={title: '', textStyle: {fontSize: 12}};
        var options = {
          hAxis: {title: '', titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}, format: '#\'%\''},
          vAxis: verAxis,
          legend: { position: 'top', maxLines: 5 },
          colors: ['#d90116','#ee7a15', '#ffca00', '#5ab417'],
          bar: { groupWidth: '60%' },
          isStacked: true,
        };
        chartDiv=document.getElementById(id);
	var chart = new google.visualization.ColumnChart(chartDiv);
	
	chart.draw(data, options);
//            google.load("visualization", "1", {packages:["corechart"]});
//            google.setOnLoadCallback(drawChart);
//        
//           var data = google.visualization.arrayToDataTable([
//            ['Genre', 'Fantasy & Sci Fi', 'Romance', 'Mystery/Crime', 'General',
//             'Western', 'Literature', { role: 'annotation' } ],
//            ['2010', 10, 24, 20, 32, 18, 5, ''],
//            ['2020', 16, 22, 23, 30, 16, 9, ''],
//            ['2030', 28, 19, 29, 30, 12, 13, '']
//          ]);
//
//          var options = {
//            width: 600,
//            height: 400,
//            legend: { position: 'top', maxLines: 3 },
//            bar: { groupWidth: '75%' },
//            isStacked: true,
//          };
//
//          var view = new google.visualization.DataView(data);
//          
//          
//  
//	
//           chartDiv=document.getElementById(id);
//            var chart = new google.visualization.ColumnChart(chartDiv);
//          chart.draw(view, options);
}

$(".trendChart").each(function(){
        if(!$(this).hasClass('starback') ){
           trendChart($(this).attr('id'), eval($.trim($(this).text())));
       }else{
           trendChartStarBack($(this).attr('id'), eval($.trim($(this).text())));
       }
	
});

function resizeTrendChart(){
    $(".trendChart").each(function(){
        if(!$(this).hasClass('starback') ){ alert('yes');
           trendChart($(this).attr('id'), eval($.trim($(this).text())));
       }else{
           trendChartStarBack($(this).attr('id'), eval($.trim($(this).text())));
       }
	
});
}

//$(window).resize(function(){
//  //resizeTrendChart();
//});


function trendChartEuro(id, jdata, scaleId) {
	c=0;
	if(jdata)
		c=jdata[0].length-1;
	
	if(c<=0){
		$("#"+id).html('<div class="na1">N/A</div>');
		return;
	}
	
	var data = google.visualization.arrayToDataTable(jdata);
	var view = new google.visualization.DataView(data);
	
	if(scaleId==36) {
		verAxis={title: '', textStyle: {fontSize: 12}, ticks: [{v:-0.6, f:'-0.6'}, {v:0.0, f:'0.0'}, {v:0.5, f:'0.5'}, {v:1.0, f:'1.0'}]};
	}
	else{
		verAxis={title: '', textStyle: {fontSize: 12}, 
				ticks: [{v:0, f:'0'}, {v:20, f:'20'}, {v:40, f:'40'}, {v:60, f:'60'}, {v:80, f:'80'}, {v:100, f:'100'}]};
	}
	
	if(data){
		var formatter = new google.visualization.NumberFormat({pattern:'#.###'});
		for(i=1; i <= c; i++){
			formatter.format(data, i);
		}
	}
	
	chartDiv=document.getElementById(id);
	
	if(scaleId==36) {
		var options = {
		  title: '',
		  hAxis: {title: '', textStyle: {fontSize: 12}, format: '#.###'},
		  vAxis: verAxis,
		  legend: { position: "none"},
		  colors: ['#3366cc','#000000', '#990099', '#0099C6', '#5574A6', '#AAAA11', '#22AA99'],
		  tooltip: {textStyle:  {fontSize: 12}},
		};
		
		var chart = new google.visualization.ColumnChart(chartDiv);
	}
	else{
		var options = {
		  title: '',
		  hAxis: {title: '', textStyle: {fontSize: 12}, format: '#.###'},
		  vAxis: verAxis,
		  legend: { position: "none"},
		  colors: ['#3366cc','#000000', '#990099', '#0099C6', '#5574A6', '#AAAA11', '#22AA99'],
		  tooltip: {textStyle:  {fontSize: 12}},
		  seriesType: "bars",
	  	  series: {1: {type: "line", color: "#6fba61"}},
		};
		
		var chart = new google.visualization.ComboChart(chartDiv);
	}
	
	chart.draw(data, options);
}


$(".trendChartEuro").each(function(){
	trendChartEuro($(this).attr('id'), eval($.trim($(this).text())), $(this).attr('scaleid'));
});

/** Remove Help icon from blank charts**/
$("div[scaleid]").each(function(){
	if( $(".na1", $(this)).length ){
		$('.help1', $(this).parent()).remove();
	}
});
</script>
