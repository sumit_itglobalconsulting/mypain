<?php
/** Error reporting */
//error_reporting(E_ALL);



/** Include path **/
ini_set('include_path', FCPATH.'Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("My Pain Impact");
$objPHPExcel->getProperties()->setLastModifiedBy("MyPain");
$objPHPExcel->getProperties()->setTitle("Patient Report");
$objPHPExcel->getProperties()->setSubject("Patient Report");
$objPHPExcel->getProperties()->setDescription("Patient Report");


// Add some data
$objPHPExcel->setActiveSheetIndex(0);
$sheet=$objPHPExcel->getActiveSheet();

/** Data **/
//Name and date
$sheet->SetCellValue('A1', 'Patient Name:');
$sheet->SetCellValue('B1', $dtl['fullName']);
$sheet->getStyle("B1")->getFont()->setBold(true);

$sheet->SetCellValue('C1', 'Date:');
$sheet->SetCellValue('D1', showDate(time()));
$sheet->getStyle("D1")->getFont()->setBold(true);

//Overall response
$sheet->SetCellValue('A3', 'Overall Treatment Response');
$sheet->getStyle("A3")->getFont()->setBold(true);
$sheet->mergeCells("A3:B3");

$sheet->SetCellValue('A4', 'Present Status');
$sheet->SetCellValue('B4', 'Feels');
$sheet->getStyle("A4:B4")->getFont()->setBold(true);
$sheet->getStyle("A4:B4")->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'CCCCCC'))));
$sheet->SetCellValue('A5', $overallRes?$overallRes."%":"N/A");
$sheet->SetCellValue('B5', $feels);
$sheet->getStyle("A5")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
//1caf9a
$rw=7;
foreach($patientForms as $i=>$pf) {
	if(USER_TYPE=='D') $userId=$dtl['id']; else if(USER_TYPE=='H') $userId=$dtl['id']; else $userId=USER_ID;
	$scales=scalesReport($pf['formId'], $pf['doctorId'], $userId);
	$scaleRes=$scales['scales'];
	$scalesReport=$scales['scalesReport'];
	$ts=strtotime($scales['lastResTime']);
	if($ts>0)
		$lastResTime=date('n/j/Y', $ts);
	else
		$lastResTime='N/A';
	
	$title="Scores for {$pf['formName']} ".(USER_TYPE=='P'?'( '.$pf['doctorName'].')':'');
	$sheet->SetCellValue("A".$rw, $title)->getStyle("A".$rw)->getFont()->setBold(true)->getColor()->setRGB('FFFFFF');
	$sheet->getStyle("A".$rw.":G".$rw)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => '1caf9a'))));
	$sheet->mergeCells("A".$rw.":G".$rw);
	
	$rw++;
	
	$cols=array('Base Line Score', 'Last Score', 'Maximum form score', 'Last Response Date', 'Present Status', 'Goal');
	foreach($scaleRes as $j=>$s){
                if($pf['formId']==1){
                    if($s['id']==2){
                        if(empty($s['medTake']))
                            continue;
                    }

                }
		if($s['scale']!='N/A'){
			$sheet->SetCellValue('A'.$rw, $s['scale'])->getStyle("A".$rw)->getFont()->setBold(true);
			$sheet->mergeCells("A".$rw.":G".$rw);
			$rw++;
		}
		
		$k='A';
		
		foreach($cols as $c){
			$sheet->SetCellValue($k.$rw, $c)->getStyle($k.$rw)->getFont()->setBold(true);
			$sheet->getStyle($k.$rw)->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'CCCCCC'))));
			$k++;
		}
		$rw++;
		$sheet	->SetCellValue("A".$rw, $s['baselineScore']?$s['baselineScore']:'N/A')
				->SetCellValue("B".$rw, $s['lastScore']?$s['lastScore']:'N/A')
				->SetCellValue("C".$rw, $s['totalScore'])
				->SetCellValue("D".$rw, date('d/m/Y' ,strtotime($lastResTime)) )
				->SetCellValue("E".$rw, intval($s['curRes'])?intval($s['curRes']).'%':'N/A') 
				->SetCellValue("F".$rw, $s['goodRes'].'%')
				->getStyle("D".$rw.":"."F".$rw)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
		$rw=$rw+2;
	}
	
	$rw++;
}


if(file_exists("assets/temp/".$resImg)){
	$sheet->SetCellValue('J4', "Trend Over Time (% Change from baseline)")->getStyle("J4")->getFont()->setBold(true);
	$sheet->getStyle("J4")->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'CCCCCC'))));
	$sheet->mergeCells("J4:O4");
	
	$objDrawingPType = new PHPExcel_Worksheet_Drawing();
	$objDrawingPType->setWorksheet($sheet);
	$objDrawingPType->setName("My Pain Impact");
	$objDrawingPType->setPath(FCPATH."assets/temp/".$resImg);
	$objDrawingPType->setCoordinates('J6');
	$objDrawingPType->setOffsetX(1);
	$objDrawingPType->setOffsetY(5);
}

// Rename sheet
$sheet->setTitle('My Pain Impact Report');

/** Formatting **/
/** Bold First Row **/
//$objPHPExcel->getActiveSheet()->getStyle("A1:".$LC."1")->getFont()->setBold(true);
/** Background **/
//$objPHPExcel->getActiveSheet()->getStyle("A1:".$LC."1")->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'CCCCCC'))));

/** Auto Width **/
$LC = $sheet->getHighestColumn();
for($c='A'; $c<=$LC; $c++){
	$sheet->getColumnDimension($c)->setAutoSize(true);
}
/** Formatting End **/


// Save Excel 2007 file
/*$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save(FCPATH."assets/temp/report.xlsx");
redirect(URL."test/report.xlsx");
*/

//Direct Download
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="maypain-impact-report.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
