<?php 
$filename = 'phge-report.html';
header('Content-disposition: attachment; filename=' . $filename);
header('Content-type: text/html');


function repText($txt) {
	$a1=array('|', '*');
	$a2=array('', '');
	
	return str_replace($a1, $a2, $txt);
}
?>

<div id="reportBox">
	<a href="<?php echo URL;?>">
        <img src="<?php echo URL;?>assets/img/logo.png" style="height:64px; margin:5px" alt="My Pain Impact" />
    </a>
    
    <tt>
        <div class="reportPInfo" style="margin-bottom:0px">
            <div class="col1"><?php echo $dtl['fullName'];?></div>
            <div class="col1 textCenter">My Pain PGHE Report</div>
            <div class="col1 textRight">Date: <?php echo showDate(time());?></div>
            <div class="clr"></div>
        </div>
    </tt>
    
    <div class="">
        <div class="down">
            <div class="bTitle" style="background:#EAF2FA">Name:</div>
            <div class="ans"><?php echo $dtl['firstName'].' '.$dtl['lastName'];?></div>
            
        	<div class="bTitle">Hospital where seen or location of choice (if applicable):</div>
            <div class="ans"><?php echo $dtl['hospitalName'];?></div>
            
            <div class="bTitle">Email-ID:</div>
            <div class="ans"><?php echo $dtl['loginEmail'];?></div>
            <hr>
            <?php foreach($resDtl['quesDtl'] as $k=>$rs){?>
            	<div class="bTitle">
					<?php 
						if($rs['catName'] && $k!=99)
							echo repText($rs['catName']);
						else
							echo $rs['ques'];
					?>
                </div>
            	<div class="ans">
                	<?php
                            if($rs['optionName']){
                                    $res=str_replace('|', ', ', $rs['optionName']);
                                    echo $res;
                            }
                        ?>
                    <?php if($ans=repText($rs['ans'])){?>
                    	<?php if($rs['optionName']){?>
                        	<div style="font-weight:bold; padding:5px 0 3px 0">Other Response:</div>
                        	<div style="padding-left:15px"><?php echo $ans;?></div>
                        <?php }else{?>
                        	<div><?php echo repText($rs['ans']);?></div>
                        <?php }?>
                    <?php }?>
                </div>
                
                <hr>
            <?php }?>
        
        	<?php
				//pr($dtl);
				//pr($resDtl);
			?>
        </div>
    </div>
</div>