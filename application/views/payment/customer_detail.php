<div class="signuppanel">

<div class="customer-detail-panel">
   <div class="row">
        <?php echo getFlash();?>
        <?php if($errors){?>
            <div class="alert alert-danger">Please fill the field(s) marked with red color!</div>
        <?php }?>
    </div>
       
        <form  method="post"  onsubmit="return submitRegistration()">
            <div class="row">
                <div class="col-md-6">
                <div class="signup-info">
                    <div class="logopanel">
                        <a href="<?php echo URL;?>"><img src="<?php echo URL;?>assets/img/logo.png" /></a>
                    </div>
                    
                    <div class="mb30"></div>
                    
                    <h4 class="text-success" style="text-transform:uppercase">Choose Other Subscription:</h4>
                    <div class="mb20"></div>
                    <div>
                        <div><a href="<?php echo URL;?>user/choosePlans/">Choose Plans</a></div>
                       
                        <div>Note: Above cost relate to per patient.</div><p>&nbsp;</p>
                            
                            <div class="mb10">
                                <label class="control-label">No. of Patients</label>
                                <div  id="select-patient"></div>
                                
                            </div>
                           <div class="mb10">
                               <label class="control-label">Cost:GBP</label>
                                <input class="form-control" type="text" name="cost" id="cost" value="<?php echo $price*100; ?>"  />
                           </div>    
                    </div>
                    
                    
                </div>
            </div>
                <div class="col-md-6">
                     <h3 class="nomargin">Customer Detail</h3>
                     <p>&nbsp;</p>
                     <div class="mb10">
                        <label class="control-label myreq">First Name</label>
                        <input name="firstName" id="firstName" type="text" class="form-control" value="<?php echo h($dtl['firstName']);?>" maxlength="50" />
                        <label class="error"><?php echo error_msg($errors,'firstName');?></label>
                    </div>
                     <div class="mb10">
                        <label class="control-label myreq">Last Name</label>
                        <input name="lastName" id="lastName" type="text" class="form-control" value="<?php echo h($dtl['lastName']);?>" maxlength="50" />
                         <label class="error"><?php echo error_msg($errors,'lastName');?></label>
                    </div>
                     <div class="mb10">
                        <label class="control-label myreq">Email</label>
                        <input name="loginEmail" id="loginEmail" type="email" class="form-control" value="<?php echo h($dtl['loginEmail']);?>" maxlength="50" />
                        <label class="error" id="emailErr"><?php echo error_msg($errors,'loginEmail');?></label>
                    </div>
                     <div class="mb10">
                    <label class="control-label myreq">Contact Address</label>
                    <input name="address" id="address" type="text" class="form-control" value="<?php echo h($dtl['address']);?>" maxlength="200" />
                    <label class="error"><?php echo error_msg($errors,'address');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">City</label>
                    <input name="city" id="city" type="text" class="form-control" value="<?php echo h($dtl['city']);?>" maxlength="50" />
                    <label class="error"><?php echo error_msg($errors,'city');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">Postcode</label>
                    <input name="zipcode" id="zipcode" type="text" class="form-control" value="<?php echo h($dtl['zipcode']);?>" maxlength="10" />
                    <label class="error"><?php echo error_msg($errors,'zipcode');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">State</label>
                    <input name="state" id="state" type="text" class="form-control" value="<?php echo h($dtl['state']);?>" maxlength="50" />
                    <label class="error"><?php echo error_msg($errors,'state');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">Country</label>
                    <select name="country" class="form-control">
                        <option value="">Select Country</option>
                        <option value="GB">United Kingdom</option>
                        <option value="US">United States</option>
                    </select>    
                    <label class="error"><?php echo error_msg($errors,'country');?></label>
                </div>
                <div class="mb10">
                    <label class="control-label myreq">Description</label>
                    <input name="description" id="description" type="text" class="form-control" value="<?php echo h($dtl['description']);?>" maxlength="200" />
                    <label class="error"><?php echo error_msg($errors,'description');?></label>
                </div>     
                  <div class="mb10" style="padding-top:10px">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" id="terms" />
                        <label for="terms">I accept, licence &amp; agreement</label>
                    </div>
                    
                    <label class="error hide" id="termsErr">Please accept licence &amp; agreement</label>
                </div>
                
                
                <br />
                <div class="row mb10">
                    <div class="col-sm-6"><button type="submit" class="btn btn-success btn-block">Submit</button></div>
                    <div class="col-sm-6"><button type="reset" class="btn btn-default btn-block">Reset</button></div>
                </div>
              </div>    
                <div>
                    <input type="hidden" name="planid" value="<?php echo $planid ?>">
                    <input type="hidden" name="price" value="<?php echo $price ?>">
                </div>    
        </form>
       
</div>  
</div>
<p>&nbsp;</p>

<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
function submitRegistration() {
	var error=false;
	if($("#terms").prop("checked")!=true){
		$("#termsErr").removeClass('hide');
		error=true;
	}
	else{
		$("#termsErr").addClass('hide');
	}
		
	if(!checkValidEmail($("#loginEmail")))
		error=true;
	return !error;
}

$("#loginEmail").blur(function(){
	checkValidEmail($(this));
});

$("#loginEmail").blur(function(){
    aUrl="<?php echo URL;?>user/showCustomerDetail/";
    $.ajax({
        url:aUrl, 
        type:'POST', 
        data:{'email': $('#loginEmail').val()}, 
        success:function(data){
            if(data != ''){
                var info = jQuery.parseJSON(data);
                $("#firstName").val(info.firstName);
                $("#lastName").val(info.lastName);
                $("#address").val(info.address);
                $("#city").val(info.city);
                $("#zipcode").val(info.zipcode);
                $("#state").val(info.state);
                $("select[name='country']").val(info.country);
                $("#emailErr").text('Do you want to update plan for existing email, if no please change email');
            }else{
                $("#emailErr").text('');
            }   
	}
    });
});
function createPatientDropDown(){
    var newDiv= document.createElement('div');
    var html = '<select name="noOfPatient" class="form-control">';
    for(i=100;i<=2000;i+=100){
      html +="<option>"+i+"</option>";
    }
    html += '</select>';
    newDiv.innerHTML = html;
    document.getElementById('select-patient').appendChild(newDiv);
}
$(function(){
    createPatientDropDown();
    $("select[name='noOfPatient']").change(function(){
        var patient = $(this).val();
        var price = $("input[name='price']").val();
        var tp = patient*price;
        $("#cost").val(tp);
    });
})
</script>