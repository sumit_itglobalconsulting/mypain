<div class="contentpanel">
    <div class="row posRel">
        <div class="panel panel-default">
            <div class="panel-body">
                <div><h3>Your order has not been successful</h3></div>
                <p>The form transaction did not completed successfully and you have been returned to this  page for the following reason: <br></p>
<br />
                <p class="warning"><strong><?php echo $failure_reason ?></strong></p>
                <br/>
                <p>
                    The order number, for reference is: <strong><?php echo $VendorTxCode ?></strong><br>
                </p>
                <div class="greyHzShadeBar">&nbsp;</div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>





