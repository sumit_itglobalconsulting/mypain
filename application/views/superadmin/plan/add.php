<?php 
	if($dtl['type']=='F')
		$dtl['price']='0.00';
?>

<div class="pageheader">
	<h2><i class="fa fa-gbp"></i> Plan <span><?php echo $dtl['id']?"Update {$dtl['planName']} Details":'Add New';?></span></h2>
    <div class="breadcrumb-wrapper">
    	<a href="<?php echo SADM_URL."form/plans";?>">&laquo; Back to list</a>
    </div>
</div>

<div class="contentpanel">
	<?php if($errors){?>
    	<div class="alert alert-danger">Please fill the fields marked with red color</div>
    <?php }?>
    
	<?php echo getFlash();?>
    
    <div class="panel panel-default">
    	<div class="panel-heading"><h4>Plan details</h4></div>
        
        <div class="panel-body panel-body-nopadding">
        	<form class="form-horizontal form-bordered" method="post">
            	<div class="form-group">
                    <label class="col-sm-3 control-label myreq">Plan Name</label>
                    <div class="col-sm-6">
                        <input name="planName" id="planName" type="text" class="form-control" value="<?php echo h($dtl['planName']);?>" maxlength="50" />
                    	<label class="error"><?php echo error_msg($errors,'planName');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Plan Type</label>
                    <div class="col-sm-6">
                    	<div class="row" style="padding-top:5px">
                        	<div class="col-sm-2">
                                <div class="rdio rdio-primary">
                                    <input type="radio" name="type" id="type1" value="F" <?php setCheckChecked($dtl['type'], 'F');?> />
                                    <label for="type1">Free</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="rdio rdio-primary">
                                    <input type="radio" name="type" id="type2" value="P" <?php setCheckChecked($dtl['type'], 'P'); echo !$dtl?'checked="checked"':'';?> />
                                    <label for="type2">Paid</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Plan Cost</label>
                    <div class="col-md-6">
                        <div>
                            <div class="pull-left" style="padding-right:10px">
                                <input type="text" name="price" id="price" valid="int" maxlength="6" class="form-control" style="width:90px" value="<?php echo $dtl['price'];?>" 
                        		<?php echo $dtl['type']=='F'?'disabled="disabled"':'';?> />
                            </div>
                            
                            <div class="pull-left" style="width:100px">
                            	<?php echo form_dropdown("currency", currencyArr(), $dtl['currency'], 'class="form-control chosen-select" data-placeholder="Currency"');?>
                            </div>
                        </div>
                        
                    	<label class="error"><?php echo error_msg($errors,'price');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Plan Duration</label>
                    <div class="col-md-6">
                        <div>
                            <div class="pull-left" style="width:100px; padding-right:10px">
                            	<?php echo form_dropdown("duration", numbersArr(1, 10), $dtl['duration'], 'class="form-control chosen-select" data-placeholder=""');?>
                            </div>
                            
                            <div class="pull-left" style="width:100px">
                            	<?php echo form_dropdown("durationType", durationTypeArr(), $dtl['durationType'], 'class="form-control chosen-select" data-placeholder=""');?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq"># of Forms Allowed</label>
                    <div class="col-sm-6">
                    	<div style="width:200px">
							<?php 
                                echo form_dropdown("noOfFormsAllowed", array(100000=>'Unlimited')+numbersArr(1, 10), $dtl['noOfFormsAllowed'], 
                                'class="form-control chosen-select" data-placeholder=""');
                            ?>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq"># of Doctors Allowed</label>
                    <div class="col-sm-6">
                    	<div style="width:200px">
                            <?php 
								echo form_dropdown("noOfDoctorsAllowed", array(100000=>'Unlimited')+numbersArr(1, 50), $dtl['noOfDoctorsAllowed'], 
								'class="form-control chosen-select" data-placeholder=""');
							?>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Data Usage</label>
                    <div class="col-sm-6">
                    	<div style="width:200px">
                            <?php echo form_dropdown("dataUsage", dataUsageArr(), $dtl['dataUsage'], 'class="form-control chosen-select" data-placeholder=""');?>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Patient Invitation Limit</label>
                    <div class="col-sm-6">
                    	<div style="width:200px">
                            <?php 
								echo form_dropdown("patientInvitationLimit", array(100000=>'Unlimited', 20=>20, 50=>50, 100=>100, 200=>200, 500=>500, 1000=>1000, 5000=>5000), 
									$dtl['patientInvitationLimit'], 
									'class="form-control chosen-select" data-placeholder=""');
							?>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Patient Report Tracking</label>
                    <div class="col-sm-6">
                    	<div style="width:200px">
                            <?php 
								echo form_dropdown("patientReportTracking", reportTrackingTypeArr(), $dtl['patientReportTracking'], 
								'class="form-control chosen-select" data-placeholder=""');
							?>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Log - In User</label>
                    <div class="col-sm-6">
                    	<div style="width:200px">
                             <?php echo form_dropdown("loginUser", loginUserOptArr(), $dtl['loginUser'], 'id="loginUser" class="form-control chosen-select" data-placeholder=""');?>
                        </div>
                    </div>
                </div>
                
                <?php if($dtl['loginUser']!='S'){?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Log - In User's Limit</label>
                    <div class="col-sm-6">
                    	<div style="width:200px">
                             <?php 
							 	echo form_dropdown("loginUsersLimit", loginUserLimitOptArr(), $dtl['loginUsersLimit'], 
							 	'class="form-control chosen-select" data-placeholder=""');
							 ?>
                        </div>
                    </div>
                </div>
                <?php }?>
                
                <input type="hidden" name="id" value="<?php echo $dtl['id'];?>" />
                
                <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6 col-sm-offset-3">
                			<button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                    		<button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
$(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

$("#type1, #type2").change(function(){
	if($("#type1").prop("checked")==true){
		$("#price").val('0.00');
		$("#price").attr('disabled', 'disabled');
	}
	else{
		$("#price").val('');
		$("#price").removeAttr('disabled');
	}
});

$("#loginUser").change(function() {
	if($(this).val()=='S')
		$("#loginLimitBx").hide();
	else
		$("#loginLimitBx").show();
});
</script>