<div class="pageheader">
	<h2><i class="fa fa-gbp"></i> Plans</h2>
</div>


<div class="contentpanel">
    <div class="table-responsive">
        <table class="table table-striped mb30">
            <thead>
                <tr>
                    <th width="50px">#</th>
                    <th>Plan Name</th>
                    <th width="100px">
                    	<div class="smallForm" align="right">
                            <a href="<?php echo SADM_URL."form/addPlan";?>">
                                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</button>
                            </a>
                        </div>
                    </th>
                </tr>
            </thead>
            
            <tbody>
                <?php foreach($list as $i=>$dtl){?>
                <tr>
                    <td><?php echo $i+1;?>.</td>
                    <td><?php echo h($dtl['planName']);?></td>
                    <td align="center" class="table-action">
                    	<a href="<?php echo SADM_URL."form/editPlan/".encode($dtl['id']);?>"><i class="fa fa-edit"></i> Edit</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>