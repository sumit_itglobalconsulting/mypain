<?php 
if($plan)
	$purl1=SADM_URL."user/registeredUser/$plan";
else
	$purl1=SADM_URL."user/registeredUser/0";
?>

<div class="pageheader">
	<div class="row">
        <div class="col-md-8">
            <h2><i class="fa fa-user"></i> Users <span><?php echo $planName;?></span></h2>
        </div>
        
        <div class="col-md-4" style="padding-top:4px">
            <form method="get" action="<?php echo $purl1;?>" id="userSearchFrm" onsubmit="return submitUserSearch()">
                <div class="smallForm">
                    <div class="row">
                    	<div class="col-md-9">
                            <input type="text" name="searchVal" value="<?php echo @$_GET['searchVal']; ?>" class="form-control" 
                            placeholder="Search by name, user, email, hospital name">
                        </div>
                        <div class="col-md-3"><button type="submit"class="btn btn-primary">Search</button></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="contentpanel">
	<div class="people-list">
    	<div class="row">
        	
			<?php 
				if($result){foreach($result as $user) {
					switch($user['durationType']) {
						case 'M' : $Duration = $user['duration']*30;
									$expir_date = strtotime($user['created'] . " +$Duration day");
								break;
								
						case 'D' : $Duration = $user['duration'];
									$expir_date = strtotime($user['created'] . " +$Duration day");
								break;
								
						case 'Y' : $Duration = $user['duration']*365;
									$expir_date = strtotime($user['created'] . " +$Duration day");
								break;
					}
					
					if($user['image'])
						$image = URL."assets/uploads/user_images/".$user['image'];
					else
						$image = URL."assets/img/user-hos-icon.png";
			?>
                <div class="col-md-4 myListBx1">
                    <div class="people-item">
                        <div class="media">
                            <span class="pull-left"><img alt="" src="<?php echo $image;?>" width="90px" class="thumbnail media-object"></span>
                            <div class="media-body">
                                <h4 class="person-name"><?php echo $user['firstName'].' '.$user['lastName'];?></h4>
                                <div class="mb5"></div>
                                <div class="text-muted"><i class="fa fa-h-square"></i> <?php echo $user['hospitalName']." (".$user['city'].")";?></div>
                                <div class="text-muted">
                                	<i class="fa fa-envelope-o"></i> <a href="mailto:<?php echo $user['contactEmail'];?>"><?php echo $user['contactEmail'];?></a>
                                </div>
                                <div>
                                	<i class="fa fa-calendar"></i> <span class="text-muted">Registration Date:</span> <?php echo showDate($user['created']);?>
                                </div>
                                <div><i class="fa fa-gbp"></i> &nbsp;&nbsp;<span class="text-muted">Plan:</span> <?php echo $user['planName'] ?></div>
                                <div>
                                	<i class="fa fa-calendar"></i> <span class="text-muted">Expiry Date:</span> <?php echo showDate($expir_date);?>
                                </div>
                                
                                <div class="smallForm" align="right">
                                    <a href="<?php echo SADM_URL."user/editUser/".encode($user['id']) ?>">
                                        <button type="button" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }}else{?>
            	<div align="center">No user found.</div>
            <?php }?>
            
        </div>
    </div>
    
    <?php if($page['total_pages']>1){?>
    	<div>
            <div class="pull-left" style="width:50px; padding:6px 0px 0px 0px">Pages: </div>
            
            <div class="pull-left" style="width:80%"> 
                <ul class="pagination nomargin">
                    <?php 
                        if($plan)
                            $purl=SADM_URL."user/registeredUser/$plan";
                        else
                            $purl=SADM_URL."user/registeredUser/0";
                    ?>
                    <?php pagingLinksLI($page, $purl, 'active'); ?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
	<?php }?>
</div>




<script type="text/javascript">
function submitUserSearch() {
	$("#userSearchFrm input").each(function() {
		if(!$.trim($(this).val()))
			$(this).removeAttr('name');
	});
	
	return true;
}
</script>
