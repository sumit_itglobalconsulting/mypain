<div class="pageheader">
	<h2><i class="fa fa-home"></i> Dashboard</h2>
</div>

<div class="contentpanel">
	<div class="people-list myListBx1">
    	<div class="row">
        	
            <div class="col-md-3">
            	<div class="people-item">
                    <div class="media">
                        <span class="pull-left"><img alt="" src="<?php echo URL."assets/img/user-hos-icon.png"?>" class="thumbnail"></span>
                        <div class="media-body">
                            <h4 class="person-name" align="right"><?php echo $users['total'];?></h4>
                            <div class="mb10"></div>
                            Total registered users with My Pain
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3">
            	<div class="people-item">
                    <div class="media">
                        <span class="pull-left"><img alt="" src="<?php echo URL."assets/img/user-hos-icon.png"?>" class="thumbnail"></span>
                        <div class="media-body">
                            <h4 class="person-name" align="right"><?php echo $users['totalCurMonth'];?></h4>
                            <div class="mb10"></div>
                            Total registered users in current month
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3">
            	<div class="people-item">
                    <div class="media">
                        <span class="pull-left"><img alt="" src="<?php echo URL."assets/img/user-hos-icon.png"?>" class="thumbnail"></span>
                        <div class="media-body">
                            <h4 class="person-name" align="right"><?php echo $users['expireCurWeek'];?></h4>
                            <div class="mb10"></div>
                            Users account expired in current week
                        </div>
                    </div>
                </div>
            </div>
            
            <?php if($users['countUserByGroup']){ foreach($users['countUserByGroup'] as $pu){?>
            	<div class="col-md-3">
                    <div class="people-item">
                        <div class="media">
                            <span class="pull-left"><img alt="" src="<?php echo URL."assets/img/user-hos-icon.png"?>" class="thumbnail"></span>
                            <div class="media-body">
                                <h4 class="person-name" align="right"><?php echo $pu['users'];?></h4>
                                <div class="mb10"></div>
                                <strong><?php echo $pu['planName'];?></strong> registered in current Month
                            </div>
                        </div>
                    </div>
                </div>
            <?php }}?>
            
        </div>
        
        <div class="mb20"></div>
        <!-- Month VS Users -->
        <div class="panel panel-default">
        	<div class="panel-heading">
            	<div class="row">
                    <div class="col-md-6">
                        <h4 class="panel-title">Month Vs Users</h4>
                    </div>
                    
                    <div class="col-md-6">
                        <form id="chartForm">
                            <div class="form-group smallForm">
                                <label class="col-sm-2 control-label text-right">From Date</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control hasCal" id="fdate" name="fdate" value="">
                                </div>
                                
                                <label class="col-sm-2 control-label text-right">To Date</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control hasCal" id="tdate" name="tdate" value="">
                                </div>
                                
                                <div class="col-md-2">
                                    <button type="button" id="repSubBtn" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="panel-body">
            	<div id="piechart" style="width:600px; height:350px; margin:0 auto"></div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
  google.setOnLoadCallback(drawChart);
  function drawChart() {
	var data = google.visualization.arrayToDataTable([
	  ['Plan', 'No of Users'],
	  <?php foreach($users['countUserByGroup'] as $Groupuser) { ?>
			['<?php echo $Groupuser['planName'] ?>', <?php echo $Groupuser['users'] ?>],
	  <?php } ?>
	  
	]);
	var options = {title: '', is3D: true,};

	var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	chart.draw(data, options);
  }
  
  $(document).ready(function (){
	$('#repSubBtn').click(function (){
		if( !$("#fdate").val() && !$("#tdate").val() ){
			return;
		}
		
		$.ajax({
			url:'<?php echo SADM_URL;?>user/countUserGroupBy',
			type : 'POST',
			data : $("#chartForm").serialize(),
			dataType: 'JSON', 
			success: function(data){
				cdata=[['Plan', 'No of Users']];
				$.each(data, function(key, val){
					cdata.push(new Array(val.planName,  parseInt(val.users)));
				});
				
			
				var data = google.visualization.arrayToDataTable(cdata);
				var options = {title: '', is3D: true,};

				var chart = new google.visualization.PieChart(document.getElementById('piechart'));
				chart.draw(data, options);
			}
		});
	});
  })
</script>