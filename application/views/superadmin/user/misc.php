<script src="<?php echo URL;?>assets/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<div class="pageheader">
	<h2><i class="fa fa-book"></i> Misc Information</h2>
</div>

<div class="contentpanel">
	<?php echo getFlash();?>
    
    <div class="panel panel-default">
        <div class="panel-body panel-body-nopadding">
            <form class="form-horizontal form-bordered" method="post">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Privacy policy & disclaimer</label>
                    <div class="col-sm-9">
                        <textarea name="policy" id="policy" style="height:550px; width:100%"><?php echo $dtl['policy'];?></textarea>
                    </div>
                </div>
                
                <!-- Submit Btn -->
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
getTiny('policy');
</script>