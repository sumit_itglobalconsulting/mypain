<?php
	//pr($formDtl);
	echo getFlash();
	$fcats=explode(",", $formDtl['catIds']);
?>
<form id="formCat" action="javascript:void(0)" onsubmit="submitFormCat(<?php echo $formDtl['id'];?>)">
	<input type="hidden" name="formId" value="<?php echo $formDtl['id'];?>" />
    <div class="table-responsive">
        <table class="table">
        	<?php foreach($cats as $c):?>
            <tr>
                <td width="20px" style="padding-left:30px">
					<input type="checkbox" name="cat[<?php echo $c['id'];?>]" 
                    id="cat<?php echo $c['id'];?>" <?php setCheckChecked(in_array($c['id'], $fcats), true);?> />
                </td>
                <td><label for="cat<?php echo $c['id'];?>"><?php echo $c['name'];?></label></td>
            </tr>
            <?php endforeach;?>
            
            <tr>
                <td></td>
                <td><button type="submit" class="btn btn-primary" id="formCatSaveBtn">Save</button></td>
            </tr>
        </table>
    </div>
</form>

<script type="text/javascript">
setAJXPTitle("Form Categories (<?php echo $formDtl['formName'];?>)");
</script>