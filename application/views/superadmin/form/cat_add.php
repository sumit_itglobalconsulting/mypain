<form id="formCat" action="javascript:void(0)" onsubmit="submitFormCat()">
	<input type="hidden" name="id" value="<?php echo $dtl['id'];?>" />
    <div class="table-responsive">
        <table class="table">
            <tr>
                <td width="150px">Category Name</td>
                <td>
                    <input name="name" id="catName" type="text" class="form-control" value="<?php echo $dtl['name'];?>" maxlength="100" />
                    <label class="error"><?php echo error_msg($errors,'name');?></label>
                </td>
            </tr>
            
            <tr>
                <td></td>
                <td><button type="submit" class="btn btn-primary" id="formCatSaveBtn">Submit</button></td>
            </tr>
        </table>
    </div>
</form>

<?php set_error_class($errors, 'redBdr');?>