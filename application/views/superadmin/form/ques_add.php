<?php 
	$QTArr=questionTypesArr();
?>

<div class="headBox">
	<div class="headBoxL">
    	<h1><?php echo $formDtl['formName'];?>: <span><?php echo $dtl['id']?'Update Question':'Add New Question';?></span></h1>
    </div>
    <div class="headBoxR">
        <a href="<?php echo SADM_URL."form/questions/".encode($formDtl['id']);?>">&laquo; back to list</a>
    </div>
    <div class="clr">&nbsp;</div>
</div>

<?php echo getFlash();?>

<div class="whiteBox">
	<div class="pad3">
    	<div class="down">
            <form action="<?php echo REQ_URI;?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $dtl['id'];?>" />
                <table width="100%" cellpadding="8">
                	<tr>
                        <td width="200px" valign="top">Category Name</td>
                        <td>
                            <input type="text" name="catName" id="catName" style="width:400px" value="<?php echo h($dtl['catName'])?>" >
                        </td>
                    </tr>
                    
                    <tr>
                        <td width="200px" valign="top">Question</td>
                        <td>
                            <textarea name="ques" id="ques" style="width:400px; height:70px" spellcheck="false"><?php echo h($dtl['ques'])?></textarea>
                            <div class="red"><?php echo error_msg($errors,'ques');?></div>
                        </td>
                    </tr>
                   
                    <tr>
                        <td valign="top">Type</td>
                        <td>
                            <?php echo form_dropdown('type', array(''=>'--Select--')+$QTArr, $dtl['type']);?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Info Text</td>
                        <td>
                            <textarea name="infoText" id="infoText" style="width:400px; height:70px" spellcheck="false"><?php echo h($dtl['infoText'])?></textarea>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Group</td>
                        <td>
                            <?php echo form_dropdown('groupId', array(''=>'--Select--')+$quesGroup, $dtl['groupId']);?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td style="padding-top:10px">
                            <button type="submit" class="btn1">Save</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
		$('#showImg').attr('src', e.target.result);
	}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#image").change(function(){
	readURL(this);
});
</script>