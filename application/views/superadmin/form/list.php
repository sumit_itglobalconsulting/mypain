<?php
	$catsArr=multiArrToKeyValue($cats, "id", "name");
?>

<div class="pageheader">
    <div class="row">
        <div class="pull-left" style="padding:0px 0px 0px 10px">
            <h2><i class="fa fa-edit"></i> View Forms</h2>
        </div>
        
        <div class="pull-right" style="padding:4px 10px 0px 0px; width:300px">
        	<form id="searchForm" method="get">
        		<?php echo form_dropdown('catId', array(0=>'All Categories')+$catsArr, $_GET['catId'], 'id="catId" class="form-control chosen-select"');?>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="contentpanel">
	<?php echo getFlash();?>
	<div class="people-list myListBx1">
        <div class="pad1">
            <div class="row">
                <?php if($list){foreach($list as $i=>$dtl){?>
                <div class="col-md-3">
                    <div class="posRel">
                        <div class="setCatBx">
                        	<i class="fa fa-cog"></i> <a href="<?php echo SADM_URL."form/setCat/".$dtl['id'];?>" class="setCat">Set Category</a>
                        </div>
                    </div>
                    
                    <div class="people-item">
                        <div class="mb20"></div>
                        <div class="media" align="center">
                            <a href="<?php echo URL."common/formPreview/".encode($dtl['id']);?>">
                                <img alt="" src="<?php echo base_url("assets/uploads/form_images/".$dtl['image']);?>">
                            </a>
                        </div>
                        <div class="mb5"></div>
                        <a href="<?php echo URL."common/formPreview/".encode($dtl['id']);?>">
                            <button type="button" class="btn btn-warning btn-block"><i class="fa fa-eye"></i> View Form</button>
                        </a>
                    </div>
                </div>
                <?php }}else{?>
                <div class="notFound">No form found.</div>
                <?php }?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".setCat").click(function(e) {
	e.preventDefault();
	url=$(this).attr('href');
	popupAjax(url, "Form Categories", "600");
});

$("#catId").change(function(e) {
	$("#searchForm").submit();
});

function submitFormCat(formId) {
	$("#formCatSaveBtn").text('Saving...');
	ajax("<?php echo SADM_URL."form/setCat";?>/"+formId, "AJXPContent", "formCat", "closeAJXP()", "POST");
}
</script>