<div class="headBox">
	<div class="headBoxL">
    	<h1><?php echo $formDtl['formName'];?>: Questions</h1>
    </div>
    <div class="headBoxR">
    	<a href="<?php echo SADM_URL."form/addQues/".encode($formDtl['id']);?>">Add New Question</a>
    </div>
    <div class="clr">&nbsp;</div>
</div>

<div class="dt_grd">
	<?php if($list){?>
    <table cellspacing="1" class="oddEven">
        <tr class="h h1">
            <td width="15px">Sr.</td>
            <td width="60px">Ques ID</td>
            <td align="left">Question</td>
            <td width="150px" colspan="2"></td>
        </tr>
        
        <?php foreach($list as $i=>$dtl){?>
        <tr class="i">
            <td align="right"><?php echo $i+1;?>.</td>
            <td><?php echo $dtl['id'];?></td>
            <td <?php echo $dtl['isParent']?'class="bold"':'';?>><?php echo $dtl['ques'];?></td>
            <td align="center" width="90px">
                <a href="<?php echo SADM_URL."form/editQues/".encode($formDtl['id'])."/".encode($dtl['id']);?>">Edit</a> |
                <a href="<?php echo SADM_URL."form/deleteQues/".encode($formDtl['id'])."/".encode($dtl['id']);?>" class="delLink">Delete</a>
            </td>
            <td align="center" width="60px">
                <?php if($dtl['type']==1 || $dtl['type']==2 || $dtl['type']==3){?>
                <a href="<?php echo SADM_URL."form/quesOptions/".encode($dtl['id']);?>" class="popup">Options</a>
                <?php }?>
            </td>
        </tr>
        <?php }?>
    </table>
    <?php }else{?>
    <div class="notFound">Not Found</div>
    <?php }?>
</div>

<script type="text/javascript">
$(".popup").click(function(e){
	e.preventDefault();
	url=$(this).attr("href");
	popupAjax(url, "Options", "800");
});
</script>