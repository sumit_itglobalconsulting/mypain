<div class="small quesBgBx">
[<strong>Question:</strong> <?php echo $quesDtl['ques'];?>]
</div>

<div class="down">
	<form id="optionForm" action="javascript:void(0)" onsubmit="saveOption()">
    	<input type="hidden" name="id" value="<?php echo $dtl['id'];?>" />
        <table border="0">
            <tr>
                <td>Option Name:</td>
                <td><input type="text" name="optionName" id="optionName" class="padd1" value="<?php echo $dtl['optionName'];?>" />&nbsp;&nbsp;</td>
                <td>Score:</td>
                <td><input type="text" name="score" id="score" class="padd1" value="<?php echo $dtl['score'];?>" />&nbsp;&nbsp;</td>
                <td>Caption:</td>
                <td><input type="text" name="caption" id="caption" class="padd1" value="<?php echo $dtl['caption'];?>" />&nbsp;&nbsp;</td>
                <td><button type="submit" id="optSaveBtn">Save</button></td>
            </tr>
        </table>
    </form>
</div>
<p>&nbsp;</p>

<div class="dt_grd" style="height:250px; overflow:auto; padding-right:4px">
	<?php if($list){?>
    <table cellspacing="1">
        <tr class="h">
            <td width="15px">Sr.</td>
            <td width="200px" align="left">Option Name</td>
            <td width="100px">Score</td>
            <td align="left">Caption</td>
            <td width="40px" colspan="2"></td>
        </tr>
        
        <?php foreach($list as $i=>$dtl){?>
        <tr class="i">
            <td align="right"><?php echo $i+1;?>.</td>
            <td><?php echo $dtl['optionName'];?></td>
            <td align="right"><?php echo $dtl['score'];?></td>
            <td><?php echo $dtl['caption'];?></td>
            <td align="center" width="20px" valign="middle">
                <a href="<?php echo SADM_URL."form/quesOptions/".encode($quesId)."/".encode($dtl['id']);?>" class="editBtn optEdit"></a>
            </td>
            <td align="center" width="20px" valign="middle">
                <a href="<?php echo SADM_URL."form/quesOptions/".encode($quesId)."/".encode($dtl['id'])."/Delete";?>" class="deleteBtn optDelete"></a>
            </td>
        </tr>
        <?php }?>
    </table>
    <?php }?>
</div>

<script type="text/javascript">
function saveOption() {
	if($.trim($("#optionName").val()) && $.trim($("#score").val())) {
		$("#optSaveBtn").text('Saving..');
		ajax("<?php echo SADM_URL."form/quesOptions/".encode($quesId);?>", "AJXPContent", "optionForm", "", "POST");
	}
}

$(".optEdit, .optDelete").click(function(e){
	e.preventDefault();
	url=$(this).attr("href");
	if(url.indexOf("Delete")>=0){
		if(confirm("Are you sure?")){
			ajax(url, "AJXPContent");
		}
	}
	else
		ajax(url, "AJXPContent");
});
</script>