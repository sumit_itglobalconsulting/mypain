<script src="<?php echo URL;?>assets/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<div class="headBox">
	<div class="headBoxL">
    	<h1>Forms: <span><?php echo $dtl['id']?'Update Details':'Add New';?></span></h1>
    </div>
    <div class="headBoxR">
        <a href="<?php echo SADM_URL."form";?>">&laquo; back to list</a>
    </div>
    <div class="clr">&nbsp;</div>
</div>

<?php echo getFlash();?>

<div class="whiteBox">
	<div class="pad3">
    	<div class="down">
            <form action="<?php echo REQ_URI;?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $dtl['id'];?>" />
                <table width="100%" cellpadding="8">
                    <tr>
                        <td width="200px">Form Name</td>
                        <td>
                            <input type="text" name="formName" id="formName" value="<?php echo h($dtl['formName'])?>" class="inpt1" maxlength="100" />
                            <span class="red"><?php echo error_msg($errors,'formName');?></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td width="200px">Form Title</td>
                        <td>
                            <input type="text" name="formTitle" value="<?php echo h($dtl['formTitle'])?>" class="inpt1" maxlength="300" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Image</td>
                        <td>
                            <input type="file" name="image" id="image" accept="image/*" class="inpt1" />
                            <span class="red"><?php echo error_msg($errors,'image');?></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td>
                            <?php if($dtl['image']){?>
                                <img id="showImg" src="<?php echo base_url("assets/uploads/form_images/".$dtl['image']);?>" width="100px" alt="" />
                            <?php }else{?>
                                <img id="showImg" src="" width="100px" alt="" />
                            <?php }?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">PDF File</td>
                        <td>
                            <input type="file" name="pdf" id="pdf" accept="application/pdf" class="inpt1" />
                            <span class="red"><?php echo error_msg($errors,'pdf');?></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Toolkit</td>
                        <td>
                            <input type="file" name="toolkit" id="toolkit" accept="application/pdf" class="inpt1" />
                            <span class="red"><?php echo error_msg($errors,'toolkit');?></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Supporting Statement</td>
                        <td>
                            <input type="file" name="supportingStatement" id="supportingStatement" accept="application/pdf" class="inpt1" />
                            <span class="red"><?php echo error_msg($errors,'supportingStatement');?></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">User Guide</td>
                        <td>
                            <input type="file" name="userGuide" accept="application/pdf" class="inpt1" />
                            <span class="red"><?php echo error_msg($errors,'userGuide');?></span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Information for clinicians</td>
                        <td>
                            <textarea name="infoForClinician" id="infoForClinician" style="width:800px; height:200px"><?php echo $dtl['infoForClinician'];?></textarea>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Functions of this form</td>
                        <td>
                            <textarea name="functionsOfForm" id="functionsOfForm" style="width:800px; height:200px"><?php echo $dtl['functionsOfForm'];?></textarea>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Information for patients</td>
                        <td>
                            <textarea name="infoForPatient" id="infoForPatient" style="width:800px; height:200px"><?php echo $dtl['infoForPatient'];?></textarea>
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Created By</td>
                        <td>
                            <input type="text" name="createdBy" class="inpt2" value="<?php echo h($dtl['createdBy']);?>" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">Form Info (Showing on top)</td>
                        <td>
                            <textarea name="formInfo" id="formInfo" style="width:800px; height:200px"><?php echo $dtl['formInfo'];?></textarea>
                        </td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td style="padding-top:10px">
                            <button type="submit" class="btn1">Save</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
		$('#showImg').attr('src', e.target.result);
	}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#image").change(function(){
	readURL(this);
});

getTinySmall('infoForClinician,functionsOfForm,infoForPatient, formInfo');
</script>