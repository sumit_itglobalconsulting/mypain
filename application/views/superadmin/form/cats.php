<div class="pageheader">
	<h2><i class="fa fa-sitemap"></i> Form Categories</h2>
</div>

<div class="contentpanel">
	<?php echo getFlash();?>
    <div class="table-responsive">
        <table class="table table-striped mb30">
            <thead>
                <tr>
                    <th width="50px">#</th>
                    <th>Category Name</th>
                    <th width="150px">
                    	<div class="smallForm" align="right">
                            <a href="<?php echo SADM_URL."form/addCat";?>" class="addcat">
                                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</button>
                            </a>
                        </div>
                    </th>
                </tr>
            </thead>
            
            <tbody>
                <?php foreach($list as $i=>$dtl){?>
                <tr>
                    <td><?php echo $i+1;?>.</td>
                    <td><?php echo h($dtl['name']);?></td>
                    <td align="center" class="table-action">
                    	<a href="<?php echo SADM_URL."form/addCat/".$dtl['id'];?>" class="addcat"><i class="fa fa-edit"></i> Edit</a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo SADM_URL."form/deleteCat/".$dtl['id'];?>" class="delcat"><i class="fa fa-trash-o"></i> Delete</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
$(".addcat").click(function(e) {
	e.preventDefault();
	url=$(this).attr('href');
	popupAjax(url, "Add Category", "600");
});

$(".delcat").click(function(e) {
	e.preventDefault();
	url=$(this).attr('href');
	if(confirm("Are you sure to delete this category?")){
		location.href=url;
	}
});

function submitFormCat() {
	if($("#catName").val()){
		$("#formCatSaveBtn").text('Saving...');
		ajax("<?php echo SADM_URL."form/addCat"?>", "AJXPContent", "formCat", "location.href=location.href", "POST");
	}
}
</script>