<div class="pageheader">
	<h2><i class="fa fa-file-text"></i> Report</h2>
</div>

<div class="contentpanel">
	<div class="panel panel-default">
    	<div class="panel-heading">
           <form id="reportForm">
                <div class="form-group smallForm">
                	<div>
                    	<div class="pull-left myL1">Select Form</div>
                        <div class="pull-left myL2">
                        	<?php 
								$farr=multiArrToKeyValue($forms, "id", "formName");
								echo form_dropdown("formId", $farr, "", 'id="formId" class="form-control chosen-select"');
							?>
                        </div>
                        <div class="pull-left myL1">From Date</div>
                        <div class="pull-left myL1">
                        	<input type="text" class="form-control hasCal" id="fromDate" name="fromDate" value="<?php echo showDate($sedates['sdate']);?>">
                        </div>
                        
                        <div class="pull-left myL1">To Date</div>
                        <div class="pull-left myL1">
                        	<input type="text" class="form-control hasCal" id="toDate" name="toDate" value="<?php echo showDate($sedates['edate']);?>">
                        </div>
                        
                        <div class="pull-left myL1"><button type="button" id="repSubBtn" class="btn btn-primary">Filter</button></div>
                    	<div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
        
        <div class="panel-body">
        	<div id="myReportBx">
            	<!-- Ajax Content -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

function getReport() {
	$("#rloader").show();
	ajax("<?php echo SADM_URL."report/getReport"?>", "myReportBx", "reportForm", false, "POST");
}

getReport();

$("#repSubBtn").click(function() {
	getReport();
});
</script>

