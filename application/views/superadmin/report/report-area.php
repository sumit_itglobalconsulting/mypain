<h3><?php echo $noOfUsers;?> Patient(s) <?php echo $noOfUsers>1?'are':'is';?> using this form</h3>

<div class="posRel">
	<div id="rloader" class="hide" style="position:absolute; left:450px; top:50px; width:100px; padding:10px; background:#333; color:#fff; z-index:2000">
    	Loading...
    </div>
    
    <div class="down">
        <table cellpadding="5">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <p>&nbsp;</p>
    <div class="down" id="dashAcco">
        <h3>Form analysis over time</h3>
        <div>
			<?php
				$inv=array(array('Time', '# Invitations'));
				if($invitations){
					foreach($invitations as $a){
						array_push($inv, array($a['time'], intval($a['n'])));
					}
				}
				
				$max=0;
				for($i=1; $i<count($inv); $i++){
					if($inv[$i][1]>$max)
						$max=$inv[$i][1];
				}
				$inv=json_encode($inv);
			?>
           <div id="formAnaChart" style="width:700px; height:400px; margin:0 auto"></div>
        </div>
            
        <h3>Goal analysis</h3>
        <div class="">
        	<div>Favourite goal score is <?php echo $goalAnalys['defaultGoodRes'];?> % for this form</div>
           	<div id="goalAnaChart" style="width:600px; height:350px; margin:0 auto"></div>
        </div>
            
        <h3>Response analysis</h3>
        <div class="resAnaBx">
            <div class="boxResPic boxPoorRes posRel">
                <div>Poor Response</div>
                <div class="b1BtmText"><?php echo $noOfPoorRes;?> Patient(s)</div>
            </div>
            
            <div class="boxResPic boxAvgRes posRel">
                <div>Average Response</div>
                <div class="b1BtmText"><?php echo $noOfAvgRes;?> Patient(s)</div>
            </div>
            
            <div class="boxResPic boxGoodRes posRel">
                <div>Good Response</div>
                <div class="b1BtmText"><?php echo $noOfGoodRes;?> Patient(s)</div>
            </div>
            <div class="clear">&nbsp;</div>
        </div>
    </div>
</div>

<script type="text/javascript">
function formAnaChart() {
	jdata=<?php echo $inv?$inv:0;?>;
	cmax=parseInt(<?php echo $max;?>);
	if(jdata.length<=1)
		return;
		
	if(cmax<5){
		if(cmax==0)
			gcount=0;
		else
			gcount=cmax+2;
		verAxis={title: '', viewWindow:{min:0}, gridlines:{count:gcount}, format: '0'};
	}
	else
		verAxis={title: '', viewWindow:{min:0}, format: '0'};
	
	var data = google.visualization.arrayToDataTable(jdata);
	
	var options = {
	  title: '',
	  vAxis: verAxis,
	  hAxis: {title: '', titleTextStyle: {color: 'red'}},
	};
	
	var chart = new google.visualization.ColumnChart(document.getElementById('formAnaChart'));
	chart.draw(data, options);
}

function goalAnaChart() {
	var data = google.visualization.arrayToDataTable([
		['Task', 'Hours per Day'], 
		['Going with us', <?php echo $goalAnalys['withUs']?>], 
		['Setting his own', <?php echo $goalAnalys['settingOwn']?>]
	]);

	var options = {
	  title: '',
	  is3D: true,
	};

	var chart = new google.visualization.PieChart(document.getElementById('goalAnaChart'));
	chart.draw(data, options);
}

formAnaChart();
goalAnaChart();
	  
	  
$( "#dashAcco" ).accordion({heightStyle: "content"});
</script>