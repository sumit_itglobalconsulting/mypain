<?php
switch(USER_TYPE){
	case 'S':
		$URL=SADM_URL;
	break;
	
	case 'H':
		$URL=ADM_URL;
	break;
	
	case 'D':
		$URL=DOCT_URL;
		$doctInfo=$userDtl['deptName'].", ".$userDtl['hospitalName'].", ".$userDtl['branchName'];
	break;
	
	case 'P':
		$URL=PATIENT_URL;
	break;
}
?>

<div class="leftpanelinner">
	<h5 class="sidebartitle">Navigation</h5>
    
    <!-- This is only visible to small devices -->
    <?php if($userDtl){?>
        <div class="visible-xs hidden-sm hidden-md hidden-lg">   
            <div class="media userlogged">
                <?php 
                    if(USER_TYPE=='S' || USER_TYPE=='H'){
                        $loggedImg=URL.'assets/images/loggeduser.png';
						if($userDtl['image'])
							$loggedImg=URL.'assets/uploads/user_images/'.$userDtl['image'];
					}
                    else if(USER_TYPE=='D'){
                        $loggedImg=URL.'assets/img/doctor-icon.png';
						if($userDtl['image'])
							$loggedImg=URL.'assets/uploads/user_images/'.$userDtl['image'];
					}
                    else if(USER_TYPE=='P'){
                        $loggedImg=URL.'assets/img/patient-icon.png';
						if($userDtl['image'])
							$loggedImg=URL."assets/uploads/user_images/".$userDtl['image'];
					}
                ?>
                <img alt="" src="<?php echo $loggedImg;?>" class="media-object" style="width:26px; height:26px">
                <div class="media-body">
                    <h4><?php echo $userDtl['fullName'];?></h4>
                </div>
            </div>
          
            <h5 class="sidebartitle actitle">Account</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
              <li><a href="<?php echo $URL."myaccount";?>"><i class="fa fa-user"></i> <span>Profile</span></a></li>
              <li><a href="<?php echo $URL."changePassword";?>"><i class="fa fa-cog"></i> <span>Change Password</span></a></li>
              <li><a href="<?php echo URL."user/logout";?>"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>
        
        <?php
            $data['userDtl']=$userDtl;
            switch($userDtl['type']){
                case 'S':
                    $this->load->view("elements/left_nav_superadmin", $data);
                break;
                
                case 'H':
                    $this->load->view("elements/left_nav_admin", $data);
                break;
                
                case 'D':
                    $this->load->view("elements/left_nav_doctor", $data);
                break;
                
                case 'P':
                    $this->load->view("elements/left_nav_patient", $data);
                break;
				 case 'T':
                    $this->load->view("elements/left_nav_ticket", $data);
                break;
            }
        ?>
    <?php } else {
        $this->load->view("elements/left_nav_without_login", $data);
    }?>
</div>
