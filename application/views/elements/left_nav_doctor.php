<ul class="nav nav-pills nav-stacked nav-bracket">
    <li><a href="<?php echo DOCT_URL."user/dashboard";?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
    
    <li class="nav-parent">
        <a href=""><i class="fa fa-user"></i> <span>Patient Management</span></a>
        <ul class="children">
            <li><a href="<?php echo DOCT_URL."patients";?>"><i class="fa fa-caret-right"></i> Status</a></li>
            <li><a href="<?php echo DOCT_URL."patients/followUps";?>"><i class="fa fa-caret-right"></i> Follow Up</a></li>
            <?php if(allowPatAdd()){ ?>
            <li><a href="<?php echo DOCT_URL."patients/add";?>"><i class="fa fa-caret-right"></i> Add New Patient</a></li>
            <?php }else{ ?>
            <li><a href="#" class="addpatnotallow"><i class="fa fa-caret-right"></i> Add New Patient</a></li>
            <?php } ?>
            <li><a href="<?php echo DOCT_URL."patients/archivedPatients";?>"><i class="fa fa-caret-right"></i> Archived Patients</a></li>
            <li><a href="<?php echo DOCT_URL."patients/dischargedPatients";?>"><i class="fa fa-caret-right"></i> Discharged Patients</a></li>
        </ul>
    </li>
    
    <li><a href="<?php echo URL."message/inbox";?>"><i class="fa fa-envelope-o"></i> <span>Message</span></a></li>
    
    <li>
        <a href="<?php echo DOCT_URL."user/myForms";?>" title="Treatment Response Forms"><i class="fa fa-edit"></i> <span>My Pain Forms</span></a>
    </li>
	
    <li><a href="<?php echo DOCT_URL."user/contactMyPainSupport";?>"><i class="fa fa-fax"></i> <span>Support</span></a></li>
	
	<li class="nav-parent">
        <a href=""><i class="fa fa-cog"></i> <span>Settings</span></a>
        <ul class="children">
            <li><a href="<?php echo DOCT_URL."myaccount";?>"><i class="fa fa-caret-right"></i> My Profile</a></li>
            <li><a href="<?php echo DOCT_URL."changePassword";?>"><i class="fa fa-caret-right"></i> Change Password</a></li>
            <li><a href="<?php echo URL."common/notificationSetting";?>"><i class="fa fa-caret-right"></i> Notifications</a></li>
        </ul>
    </li>
</ul>

<h5 class="sidebartitle">Information Summary</h5>

<ul class="nav nav-pills nav-stacked nav-bracket">
	<li><a href="<?php echo URL."faq/lists"?>"><i class="fa fa-question"></i> <span>FAQ's</span></a></li>
    <li><a href="<?php echo URL."support/lists"?>"><i class="fa fa-question-circle"></i> <span>Support Ticket</span></a></li>
    <li><a href="<?php echo URL."common/tutorial"?>"><i class="fa fa-book"></i> <span>User Guide</span></a></li>
    <!--<li>
    	<a href="<?php echo URL."common/privacyPolicyDisclaimer"?>" class="posRel" style="display:block">
        	<i class="fa fa-folder" style="position:absolute; left:10px; top:10px"></i> 
            <span style="display:block; padding:0 0 0 30px">Privacy Policy and Disclaimer</span>
        </a>
    </li>-->
</ul>

<div id="patnotallow" class="myhide">
        <div align="center" class="smallForm" style="padding-top:10px">
            <p>All Licence used, please remove some patient or upgrade your licence, Thankyou</p>
            <!--<p>&nbsp;</p>
            <div>
                <a href="<?php //echo DOCT_URL."patients/makeUnArchive/".encode($dtl['id']);?>" class="btn btn-primary">Yes</a>
                &nbsp;&nbsp;&nbsp;
                <button class="btn btn-default" onclick="closeDialog()">No</button>
            </div>-->
        </div>
 </div>


<div id="dialog-message" title="All Licence Used" class="myhide">
    <span class="ui-state-default"><span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 0 0;"></span></span>
    <div style="margin-left: 23px;">
        <p>
            All Licence used, please remove some patient or upgrade your licence, Thankyou.
        </p>
    </div>
</div>


<script type="text/javascript">
    $(".addpatnotallow").click(function(e) {
        e.preventDefault();
        $("#dialog-message").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            position: ['center', 'top'],
            show: 'blind',
            hide: 'blind',
            width: 400,
            dialogClass: 'ui-dialog-osx'
        });
    });
</script>    