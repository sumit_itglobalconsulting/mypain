<ul class="nav nav-pills nav-stacked nav-bracket">
    <li><a href="<?php echo SADM_URL."user/dashboard";?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
    
    <li class="nav-parent">
    	<a href=""><i class="fa fa-user"></i> <span>View Registered User</span></a>
        
        <ul class="children">
        	<li><a href="<?php echo SADM_URL."user/registeredUser"?>"><i class="fa fa-caret-right"></i> All</a></li>
            
        	<?php $plansList = $this->common->getAllPlan(); ?>
        	<?php foreach($plansList as $plan){?>
        		<li><a href="<?php echo SADM_URL."user/registeredUser/".$plan['id'];?>"><i class="fa fa-caret-right"></i> <?php echo $plan['planName'];?></a></li>
            <?php }?>
        </ul>
    </li>
    
    <li><a href="<?php echo SADM_URL."report";?>"><i class="fa fa-file-text"></i> <span>Report</span></a></li>
    
    <li class="nav-parent">
    	<a href="<?php echo SADM_URL."form";?>"><i class="fa fa-edit"></i> <span>Treatment Forms</span></a>
        <ul class="children">
        	<li><a href="<?php echo SADM_URL."form";?>"><i class="fa fa-caret-right"></i> View Forms</a></li>
        	<li><a href="<?php echo SADM_URL.'form/cats';?>"><i class="fa fa-caret-right"></i> Form Categories</a></li>
        </ul>
    </li>
    
    <li class="nav-parent">
    	<a href=""><i class="fa fa-gbp"></i> <span>Manage Plans &amp; Pricing</span></a>
        
        <ul class="children">
        	<li><a href="<?php echo SADM_URL."form/plans";?>"><i class="fa fa-caret-right"></i> View Plans</a></li>
        	<li><a href="<?php echo SADM_URL.'form/addPlan';?>"><i class="fa fa-caret-right"></i> Add a new plan</a></li>
        </ul>
    </li>
    
    <li class="nav-parent">
        <a href=""><i class="fa fa-cog"></i> <span>Settings</span></a>
        <ul class="children">
            <li><a href="<?php echo SADM_URL."myaccount";?>"><i class="fa fa-caret-right"></i> Profile</a></li>
            <li><a href="<?php echo SADM_URL.'changePassword';?>"><i class="fa fa-caret-right"></i> Change Password</a></li>
        </ul>
    </li>
</ul>

<h5 class="sidebartitle">Information Summary</h5>
<ul class="nav nav-pills nav-stacked nav-bracket">
	<li>
    	<a href="<?php echo SADM_URL.'user/misc';?>" class="posRel" style="display:block">
        	<i class="fa fa-folder-open-o" style="position:absolute; left:10px; top:10px"></i> 
            <span style="display:block; padding:0 0 0 30px">Misc Information</span>
        </a>
    </li>
</ul>
