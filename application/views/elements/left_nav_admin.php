<ul class="nav nav-pills nav-stacked nav-bracket">
    <li><a href="<?php echo ADM_URL."user/dashboard";?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
    
    <li><a href="<?php echo ADM_URL."hospital/departments";?>"><i class="fa fa-suitcase"></i> <span>Departments</span></a></li>
    
    <li><a href="<?php echo ADM_URL."hospital/clinicians";?>"><i class="fa fa-user-md"></i> <span>Clinicians</span></a></li>
    <li><a href="<?php echo ADM_URL."hospital/patients";?>"><i class="fa fa-user"></i> <span>Patients</span></a></li>
    
    <li class="nav-parent">
        <a href="" title="Treatment Response Forms"><i class="fa fa-edit"></i> <span>My Pain Forms</span></a>
        <ul class="children">
            <li><a href="<?php echo ADM_URL."hospital/myForms";?>"><i class="fa fa-caret-right"></i> My Response Forms</a></li>
            <li><a href="<?php echo ADM_URL."hospital/allForms";?>"><i class="fa fa-caret-right"></i> All Responses Forms</a></li>
        </ul>
    </li>
    
    <li><a href="<?php echo ADM_URL."user/contactMyPainSupport";?>"><i class="fa fa-fax"></i> <span>Contact My Pain Support</span></a></li>
    
    <li class="nav-parent">
        <a href=""><i class="fa fa-cog"></i> <span>Settings</span></a>
        <ul class="children">
            <li><a href="<?php echo ADM_URL."myaccount";?>"><i class="fa fa-caret-right"></i> My Profile</a></li>
            <li><a href="<?php echo ADM_URL."changePassword";?>"><i class="fa fa-caret-right"></i> Change Password</a></li>
        </ul>
    </li>
</ul>

<h5 class="sidebartitle">Information Summary</h5>

<ul class="nav nav-pills nav-stacked nav-bracket">
	<li><a href="<?php echo URL."faq/lists"?>"><i class="fa fa-question"></i> <span>FAQ's</span></a></li>
    <li><a href="<?php echo URL."support/lists"?>"><i class="fa fa-question-circle"></i> <span>Support Ticket</span></a></li>
    
    <li><a href="<?php echo URL."common/tutorial"?>"><i class="fa fa-book"></i> <span>User Guide</span></a></li>
    <li>
    	<a href="<?php echo URL."common/privacyPolicyDisclaimer"?>" class="posRel" style="display:block">
        	<i class="fa fa-folder" style="position:absolute; left:10px; top:10px"></i> 
            <span style="display:block; padding:0 0 0 30px">Privacy Policy and Disclaimer</span>
        </a>
    </li>
</ul>
