<ul class="nav nav-pills nav-stacked nav-bracket">
    <li><a href="<?php echo PATIENT_URL."user/dashboard";?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
    <li><a href="<?php echo PATIENT_URL."user/myForms";?>"><i class="fa fa-edit"></i> <span>My Pain Forms</span></a></li>
    <li><a href="<?php echo PATIENT_URL."user/reports";?>"><i class="fa fa-bar-chart-o"></i> <span>Report</span></a></li>
    <li><a href="<?php echo URL."message/inbox";?>"><i class="fa fa-envelope-o"></i> <span>Message</span></a></li>
    
    <li class="nav-parent">
        <a href=""><i class="fa fa-cog"></i> <span>Settings</span></a>
        <ul class="children">
            <li><a href="<?php echo PATIENT_URL."myaccount";?>"><i class="fa fa-caret-right"></i> Profile</a></a></li>
            <li><a href="<?php echo PATIENT_URL."changePassword";?>"><i class="fa fa-caret-right"></i> Change Password</a></li>
            <li><a href="<?php echo URL."common/notificationSetting";?>"><i class="fa fa-caret-right"></i> Notifications</a></li>
        </ul>
    </li>
</ul>


<h5 class="sidebartitle">Information Summary</h5>

<ul class="nav nav-pills nav-stacked nav-bracket">
	<li><a href="<?php echo URL."faq/lists"?>"><i class="fa fa-question"></i> <span>FAQ's</span></a></li>
    <li><a href="<?php echo URL."support/lists"?>"><i class="fa fa-question-circle"></i> <span>Support Ticket</span></a></li>
    
    <li><a href="<?php echo URL."common/tutorial"?>"><i class="fa fa-book"></i> <span>User Guide</span></a></li>
    <!--<li>
    	<a href="<?php //echo URL."common/privacyPolicyDisclaimer"?>" class="posRel" style="display:block">
        	<i class="fa fa-folder" style="position:absolute; left:10px; top:10px"></i> 
            <span style="display:block; padding:0 0 0 30px">Privacy Policy and Disclaimer</span>
        </a>
    </li>-->
</ul>