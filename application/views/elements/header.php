<?php
	/** Notification **/
	$GLOBALS['notify']=array();
	$GLOBALS['i']=0;
        
	function dueForms() {
                $dueForms=patientDueForms(USER_ID); 
		if($dueForms){
			$forms=array_map(function($arg){return $arg['formName'];}, $dueForms);
			$GLOBALS['notify'][$GLOBALS['i']]['h']='Response Due';
			
			//$GLOBALS['notify'][$GLOBALS['i']]['s']=implode(", ", $forms);
			$forms=array();
			foreach($dueForms as $df){
				$link='<a class="notiA"title="Show '.$df['formName'].' form" href="'.PATIENT_URL."form/response/".encode($df['formId'])."/".encode($df['doctorId']).'">'.$df['formName'].'</a>';
				$forms[]=$link;
			}
			$GLOBALS['notify'][$GLOBALS['i']]['s']=implode(", ", $forms);
			
			$GLOBALS['notify'][$GLOBALS['i']]['url']=PATIENT_URL."user/myForms";
			$GLOBALS['i']++;
		}
	}
	
	function followUps() {
		$due=followUpDue(USER_ID);
		if($due){
			$names=array_map(function($arg){return $arg['name'];}, $due);
			$GLOBALS['notify'][$GLOBALS['i']]['h']='Follow Up Due';
			$GLOBALS['notify'][$GLOBALS['i']]['s']=implode(", ", $names);
			$GLOBALS['notify'][$GLOBALS['i']]['url']=DOCT_URL."patients/followUps";
			$GLOBALS['i']++;
		}
	}
	
	function responseDue() {
		$rs=resDue(USER_ID);
		if($rs){
			$names=array_map(function($arg){return $arg['name'];}, $rs);
			$GLOBALS['notify'][$GLOBALS['i']]['h']='Response Due';
			$GLOBALS['notify'][$GLOBALS['i']]['s']=implode(", ", $names);
			$GLOBALS['notify'][$GLOBALS['i']]['url']=DOCT_URL."patients?invAna=RD";
			$GLOBALS['i']++;
		}
	}
        
        function dueDoctorInvitation(){ 
            $dueInvitation=doctorInvitationDue(USER_ID);
            if($dueInvitation){
                $doctorNames=array_map(function($arg){return $arg['doctorName'];}, $dueInvitation);
                $GLOBALS['notify'][$GLOBALS['i']]['h']='Dr. Invitation Due.';
                //$doctors=array();
//                foreach($dueInvitation as $di){
//                    $link='<a class="notiA" title="Accept '.$di['doctorName'].' request" href="'.PATIENT_URL."user/doctorRequest/".encode($di['id'])."/".encode(USER_ID).'">'.$di['doctorName'].'</a>';
//                    $doctors[]=$link;
//		}
                $GLOBALS['notify'][$GLOBALS['i']]['s']=implode(", ", $doctorNames);
		$GLOBALS['notify'][$GLOBALS['i']]['url']=PATIENT_URL."user/myInvitations";
		$GLOBALS['i']++;
            }
        }
        function invitationApprove(){
            $approveInvitation=doctorInvitationApprove(USER_ID);
            if($approveInvitation){
                $patientNames=array_map(function($arg){return $arg['patientName'];}, $approveInvitation);
                $GLOBALS['notify'][$GLOBALS['i']]['h']='Share Request Approved.';
                $GLOBALS['notify'][$GLOBALS['i']]['s']=implode(", ", $patientNames);
		$GLOBALS['notify'][$GLOBALS['i']]['url']=DOCT_URL."patients?invAna=IA";
		$GLOBALS['i']++;
            }
        }
        
        
	/** Notification End **/
						
	switch(USER_TYPE){
		case 'S':
			$URL=SADM_URL;
		break;
		
		case 'H':
			$URL=ADM_URL;
		break;
		
		case 'D':
			$URL=DOCT_URL;
			$doctInfo=$userDtl['deptName'].", ".$userDtl['hospitalName'].", ".$userDtl['branchName'];
			//followUps();
			responseDue();
                        invitationApprove();
		break;
		
		case 'P':
			$URL=PATIENT_URL;
			dueForms();
                        dueDoctorInvitation();
		break;
	}
	
	$countUnreadMsg=countUnreadMsg();
	$countNotify=count($GLOBALS['notify']);
?>

<div class="headerbar">
	<a class="menutoggle"><i class="fa fa-bars"></i></a>
    
	<?php if(loggedUserData()){?>
    	<div class="header-right">
    	<ul class="headermenu">
            <?php if(USER_TYPE!='S' && USER_TYPE!='H'){?>
            	<li>
                	<div class="onlineStatusBx">
                        <?php
							switch($userDtl['onlineStatus']){
								case 'A':
									echo '<i class="fa fa-circle text-success"></i>';
								break;
								case 'B':
									echo '<i class="fa fa-circle text-danger"></i>';
								break;
								case 'N':
									echo '<i class="fa fa-circle text-default"></i>';
								break;
							}
						?>
                        <?php echo form_dropdown('onlineStatus', array('A'=>'Available', 'B'=>'Busy', 'N'=>'Not Available'), $userDtl['onlineStatus'], 'id="onlineStatus"');?>
                    </div>
				</li>
                
				<li id="unReadMsg">
					<div class="btn-group" title="Messages">
						<button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown" page-link="<?php echo URL."message/inbox";?>">
							<i class="glyphicon glyphicon-envelope"></i>
							<span class="badge"><?php echo $countUnreadMsg?$countUnreadMsg:'';?></span>
						</button>
					</div>
				</li>
				
				<li>
					<div class="btn-group" title="Notifications">
						<button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
							<i class="fa fa-bell"></i>
							<span class="badge"><?php echo $countNotify?$countNotify:'';?></span>
						</button>
						
						<?php if($notify=$GLOBALS['notify']){?>
							<div class="dropdown-menu dropdown-menu-head pull-right">
								<h5 class="title">You Have <?php echo $countNotify;?> New Notification(s)</h5>
								<div style="max-height:400px; overflow:auto">
									<ul class="dropdown-list gen-list">
										<?php foreach($notify as $n){?>
										<li class="new">
											<a href="<?php echo $n['url'];?>">
												<span>
												  <span class="name" title="Show all due response forms" ><?php echo $n['h'];?></span>
												  <span class="msg"><?php echo $n['s'];?></span>
												</span>
											</a>
										</li>
										<?php }?>
									</ul>
								</div>
							</div>
						<?php }?>
					</div>
				</li>
				
				<?php 
					if(USER_TYPE=='P' || USER_TYPE=='D'){
						if(USER_TYPE=='P'){
							$usersList=$this->common->doctorsOfPatient(USER_ID, true);
							$title="Online Doctors";
							$iconclass='fa fa-user-md';
							$link=URL."common/gotoWeemo/P/";
						}
						else{
							$usersList=$this->common->listPatients(USER_ID, true);
							$title="Online Patients";
							$iconclass='fa fa-user';
							$link=URL."common/gotoWeemo/D/";
						}
							
						$noOfOnline=count($usersList);
				?>
					<?php if($noOfOnline){?>
					<li id="notifyOnlineStatus">
						<div class="btn-group">
							<button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
								<i class="<?php echo $iconclass;?>"></i>
								<span class="badge"><?php echo $noOfOnline?$noOfOnline:'';?></span>
							</button>
							
							<div class="dropdown-menu dropdown-menu-head pull-right">
								<h5 class="title"><?php echo $title;?></h5>
								<div style="max-height:400px; overflow:auto">
									<ul class="dropdown-list gen-list">
										<li class="new">
											<?php foreach($usersList as $u){?>
												<span>
													<span class="name">
														<div class="pull-right">
                                                        	<?php
																switch($u['onlineStatus']){
																	case 'A':
																		echo '<i class="fa fa-circle text-success"></i> Available';
																	break;
																	
																	case 'B':
																		echo '<i class="fa fa-circle text-danger"></i> Busy';
																	break;
																	
																	case 'N':
																		echo '<i class="fa fa-circle text-default"></i> Not Available';
																	break;
																}
															?>
                                                        </div>
                                                        
														<a href="<?php echo $link.$u['doctorId']."/".$u['patientId'];?>" target="_blank"><?php echo $u['fullName'];?></a>
													</span>
												</span>
											<?php }?>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</li>
					<?php }?>
                <?php }?>
            <?php }?>
            
            <li>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    	<?php 
							if(USER_TYPE=='S' || USER_TYPE=='H'){
								$loggedImg=URL.'assets/images/loggeduser.png';
								if($userDtl['image'])
									$loggedImg=URL.'assets/uploads/user_images/'.$userDtl['image'];
							}
							else if(USER_TYPE=='D'){
								$loggedImg=URL.'assets/img/doctor-icon.png';
								if($userDtl['image'])
									$loggedImg=URL.'assets/uploads/user_images/'.$userDtl['image'];
							}
							else if(USER_TYPE=='P'){
								$loggedImg=URL.'assets/img/patient-icon.png';
								if($userDtl['image'])
									$loggedImg=URL."assets/uploads/user_images/".$userDtl['image'];
							}
						?>
                        <img src="<?php echo $loggedImg;?>" width="26px" height="26px" alt="" />
                        <span title="<?php echo $doctInfo;?>"><?php echo $userDtl['fullName'];?></span>
                        <span class="caret"></span>
                    </button>
                    <?php if(loggedUserData()):?>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        <li><a href="<?php echo $URL."myaccount";?>"><i class="glyphicon glyphicon-user"></i> Profile</a></li>
                        <li><a href="<?php echo $URL."changePassword";?>"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                        <li><a href="<?php echo URL."user/logout";?>"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
                    </ul>
                    <?php endif;?>
                </div>
            </li>
        </ul>
    </div>
	<?php }?>
</div>

<script>
$(".onlineStatusBx select").change(function() {
	iconOb=$(".onlineStatusBx i");
	v=$(this).val();
	switch(v){
		case 'A':
			iconOb.attr('class', 'fa fa-circle text-success');
		break;
		
		case 'B':
			iconOb.attr('class', 'fa fa-circle text-danger');
		break;
		
		case 'N':
			iconOb.attr('class', 'fa fa-circle text-default');
		break;
	}
	
	ajax(SITE_URL+"common/setOnlineStatus/"+v);
});
</script>