<div class="signuppanel">
   <div class="row">
        <?php echo getFlash();?>
        <?php if($errors){?>
            <div class="alert alert-danger">Please fill the field(s) marked with red color!</div>
        <?php }?>
    </div>
    
    <form method="post" enctype="multipart/form-data" onsubmit="return submitRegistration()">
        <div class="row">
            <div class="col-md-6">
                <div class="signup-info">
                    <div class="logopanel">
                        <a href="<?php echo URL;?>"><img src="<?php echo URL;?>assets/img/logo.png" /></a>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <h3 class="nomargin">Register</h3>
                <p class="mt5 mb20">Already a member? <a href="<?php echo URL;?>"><strong>Sign In</strong></a></p>
                
                <div class="mb10">
                    <label class="control-label myreq">Hospital Name</label>
                    <input name="hospitalName" id="hospitalName" type="text" class="form-control" value="<?php echo h($dtl['hospitalName']);?>" maxlength="50" />
                    <label class="error"><?php echo error_msg($errors,'hospitalName');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">Hospital Registration Number</label>
                    <input name="regNo" id="regNo" type="text" class="form-control" value="<?php echo h($dtl['regNo']);?>" maxlength="50" />
                    <label class="error"><?php echo error_msg($errors,'regNo');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">Branch Name</label>
                    <input name="branchName" id="branchName" type="text" class="form-control" value="<?php echo h($dtl['branchName']);?>" maxlength="50" />
                    <label class="error"><?php echo error_msg($errors,'branchName');?></label>
                </div>
                
                <label class="control-label myreq">Administrator's Name</label>
                <div class="row mb10">
                    <div class="col-sm-6">
                        <input name="firstName" id="firstName" type="text" class="form-control" value="<?php echo h($dtl['firstName']);?>" maxlength="50" 
                        placeholder="Firstname" />
                        <label class="error"><?php echo error_msg($errors,'firstName');?></label>
                    </div>
                    <div class="col-sm-6">
                        <input name="lastName" id="lastName" type="text" class="form-control" value="<?php echo h($dtl['lastName']);?>" maxlength="50" 
                        placeholder="Lastname" />
                        <label class="error"><?php echo error_msg($errors,'lastName');?></label>
                    </div>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">Login Email</label>
                    <input name="loginEmail" id="loginEmail" type="text" class="form-control" value="<?php echo h($dtl['loginEmail']);?>" maxlength="50" readonly />
                    <input type="hidden" name="email" value="1" />
                    <label class="error" id="loginEmailErr"><?php echo error_msg($errors,'loginEmail');?></label>
                </div>
                
                
                
                <label class="control-label myreq">Contact Number (1)</label>
                <div class="row mb10">
                    <div class="col-sm-3">
                        <?php echo form_dropdown('stdCd1', stdCodesArr(), $dtl['stdCd1'], 'class="form-control chosen-select" data-placeholder="STD Code"');?>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="phone1" id="phone1" value="<?php echo h($dtl['phone1']);?>" maxlength="10" valid="int" />
                    </div>
                    <label class="error" id="phone1Err"><?php echo error_msg($errors,'phone1');?></label>
                </div>
                
                <label class="control-label">Contact Number (2)</label>
                <div class="row mb10">
                    <div class="col-sm-3">
                        <?php echo form_dropdown('stdCd2', stdCodesArr(), $dtl['stdCd2'], 'class="form-control chosen-select" data-placeholder="STD Code"');?>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="phone2" id="phone2" value="<?php echo h($dtl['phone2']);?>" maxlength="10" valid="int" />
                    </div>
                    <label class="error" id="phone2Err"><?php echo error_msg($errors,'phone2');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">Contact Address</label>
                    <input name="address" id="address" type="text" class="form-control" value="<?php echo h($dtl['address']);?>" maxlength="200" />
                    <label class="error"><?php echo error_msg($errors,'address');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">City</label>
                    <input name="city" id="city" type="text" class="form-control" value="<?php echo h($dtl['city']);?>" maxlength="50" />
                    <label class="error"><?php echo error_msg($errors,'city');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">Zipcode</label>
                    <input name="zipcode" id="zipcode" type="text" class="form-control" value="<?php echo h($dtl['zipcode']);?>" maxlength="10" />
                    <label class="error"><?php echo error_msg($errors,'zipcode');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">State</label>
                    <input name="state" id="state" type="text" class="form-control" value="<?php echo h($dtl['state']);?>" maxlength="50" />
                    <label class="error"><?php echo error_msg($errors,'state');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label myreq">Country</label>
                    <?php $country = showCountry(); ?>
                    <?php $js='class="form-control"'; ?>
                    <?php echo form_dropdown('country', $country, $dtl['country'], $js); ?>
                    <label class="error"><?php echo error_msg($errors,'country');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label">Punch Line</label>
                    <input name="punchLine" id="punchLine" type="text" class="form-control" value="<?php echo h($dtl['punchLine']);?>" maxlength="50" />
                    <label class="error"><?php echo error_msg($errors,'punchLine');?></label>
                </div>
                
                <div class="mb10">
                    <label class="control-label">Hospital Logo (120 x 120)</label>
                    
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input">
                                <i class="glyphicon glyphicon-file fileupload-exists"></i>
                                <span class="fileupload-preview"></span>
                            </div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new">Select file</span>
                                <span class="fileupload-exists">Change</span>
                                <input type="file" name="image" accept="image/*" />
                            </span>
                            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                        </div>
                    </div>
                    
                    <label class="error"><?php echo error_msg($errors,'image');?></label>
                </div>
                
                <div class="mb10" style="padding-top:10px">
                    <div class="ckbox ckbox-primary">
                        <input type="checkbox" id="terms" />
                        <label for="terms">I accept, licence &amp; agreement</label>
                    </div>
                    
                    <label class="error hide" id="termsErr">Please accept licence &amp; agreement</label>
                </div>
                
                
                <br />
                <div class="row mb10">
                    <div class="col-sm-6"><button type="submit" class="btn btn-success btn-block">Submit</button></div>
                    <div class="col-sm-6"><button type="reset" class="btn btn-default btn-block">Reset</button></div>
                </div>
            </div>
        </div>
    </form>
</div>

<p>&nbsp;</p>

<script>
    jQuery(document).ready(function(){
        // Chosen Select
        jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
    });
</script>

<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
function submitRegistration() {
	var error=false;
	if($("#terms").prop("checked")!=true){
		$("#termsErr").removeClass('hide');
		error=true;
	}
	else{
		$("#termsErr").addClass('hide');
	}
	
	if(!checkValidPhone($("#phone1"), $("#phone1Err")))
		error=true;
	if(!checkValidPhone($("#phone2"), $("#phone2Err")))
		error=true;
		
	if(!checkValidEmail($("#loginEmail")))
		error=true;
		
	
	return !error;
}


function checkValidPhone(obj, errBxOb) {
	phone=$.trim(obj.val());
	errBxOb.text('');
	obj.removeClass('redBdr');
	if(phone){
		if(!isvalidPhone(phone)){
			obj.addClass('redBdr');
			errBxOb.text("Please enter a valid phone number");
			return false;
		}
	}
	return true;
}

$("#loginEmail").blur(function(){
	checkValidEmail($(this));
});

$("#phone1, #phone2").blur(function(){
	checkValidPhone($(this), $("#"+$(this).attr("id")+"Err"));
});

</script>