<div style="width:95%; background:#f1f1f1; padding:6px">
	<div style="background:#fff; padding:10px">
    	<div>
        	<div style="margin:0 0 10px 0; padding:0 0 10px 0; border-bottom:1px solid #f1f1f1; font-size:18px">
            	<a href="<?php echo URL;?>"><img src="<?php echo URL;?>assets/img/logo.png" width="100px" alt="My Pain Impact" /></a>
            </div>
            
            <div>
            	Dear <?php echo $doctorName;?>,<br /><br />
                You have archived <strong><?php echo $patientName;?></strong> from My Pain Impact.
            </div>
            
            <div style="font-weight:bold; padding:10px 0px">Current report:</div>
        	<table border="0" cellpadding="5">
                <tr>
                	<td width="140px">Overall Response</td>
                    <td width="3px">:</td>
                    <td><?php echo $overallRes;?> %</td>
                </tr>
                
                <tr>
                	<td>Feels</td>
                    <td>:</td>
                    <td><?php echo $feels;?></td>
                </tr>
                
                <?php if($reportForms){foreach($reportForms as $i=>$pf){?>
                	<tr>
                    	<td colspan="3">
                        	<strong>Scores for <?php echo $pf['formName'];?></strong>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Base Line Score</td>
                        <td>:</td>
                        <td><?php echo $pf['baselineScore'];?> <span style="color:#999">(Max score: <?php echo $pf['totalScore'];?>)</span></td>
                    </tr>
                    
                    <tr>
                        <td>Current Respone</td>
                        <td>:</td>
                        <td><?php echo $pf['curRes'];?> %</td>
                    </tr>
                <?php }}?>
            </table>
            
            <div style="border-top:1px solid #f1f1f1; padding:10px 0 0 0; margin:20px 0 0 0">
            	<strong>M:</strong> (925) 303-4227 &nbsp;&nbsp;&nbsp; <strong>Email: </strong>info@mypainimpact.com
            </div>
        </div>
    </div>
</div>