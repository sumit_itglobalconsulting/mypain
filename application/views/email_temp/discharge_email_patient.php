<div style="width:95%; background:#f1f1f1; padding:6px">
	<div style="background:#fff; padding:10px">
    	<div>
        	<div style="margin:0 0 10px 0; padding:0 0 10px 0; border-bottom:1px solid #f1f1f1; font-size:18px">
            	<a href="<?php echo URL;?>"><img src="<?php echo URL;?>assets/img/logo.png" width="100px" alt="My Pain Impact" /></a>
            </div>
            
            <div>
            	Dear <?php echo $patientName;?>,<br /><br />
                You have been discharged from My Pain Impact by <strong><?php echo $doctorName;?></strong>.
            </div>
           
            <div style="font-weight:bold; padding:10px 0px">Your current report:</div>
        	<table border="0" cellpadding="5" class="res-tbl" style="font-family: Helvetica,Arial,sans-serif;">
                <tr>
                    <td width="160px"><p style="font-size: 14px; font-weight:normal; color: #333;">Overall Response</p></td>
                    <td width="3px">:</td>
                    <td><?php echo $overallRes;?> %</td>
                </tr>
                
                <tr>
                    <td><p style="font-size: 14px; font-weight:normal; color: #333;">Feels</p></td>
                    <td>:</td>
                    <td> <?php
                            $src='';
                            switch($feels){
                            case -5:
                                $src='Much worse';
                                break;
                            case -4:
                                 $src='Much worse';
                                break;
                            case -3:
                                $src='Slightly worse';
                                break;
                            case -2:
                                $src='Slightly worse';
                                break;
                            case -1:
                                $src='Slightly worse';
                                break;
                            case 0:
                                $src='No change';
                                break;
                            case 1:
                                $src='Better';    
                                break;
                            case 2:
                                $src='Better';    
                                break;
                            case 3:
                                $src='Better';    
                                break;
                            case 4:
                                $src='Much better';
                                break;
                            case 5:
                                $src='Much better';    
                                break;
                            } echo $src;
                        ?></td>
                </tr>
                
                <?php if($reportForms){foreach($reportForms as $i=>$pf){  
                            $scales=scalesReport($pf['formId'], $pf['doctorId'], $patientId); 
                            $scaleRes = $scales['scales'];
                           
                ?>
                	<tr>
                            <td colspan="3" style=" border-top:1px solid #d2d2d2;">
                            <p style="font-size: 14px; font-weight:normal; color: #333;"><span style="font-size: 18px; font-weight:bold; color: #000; padding: 5px 0;">Scores for <?php echo $pf['formName'];?></span></p>
                               
                        </td>
                    </tr>
                    <?php foreach($scaleRes as $j=>$s){
                                               if($pf['formId']==1){
                                                   if($s['id']==2){
                                                       if(empty($s['medTake']))
                                                           continue;
                                                   }
                                                       
                                               }
						$s['curRes']=round($s['curRes']);
						
						$baselineScore=is_numeric($s['baselineScore'])?round($s['baselineScore']):'';
						$lastScore=is_numeric($s['lastScore'])?round($s['lastScore']):'';
						
						/* In case of EuroQol */
						if($pf['formId']==19){
							$s['curRes']=$s['lastScore'];
						}
						
						if($s['id']==36){
							$baselineScore=number_format($s['baselineScore'],3);
							$lastScore=number_format($s['lastScore'],3);
							$s['totalScore']=number_format($s['totalScore'],3);
						}
						/* In case of EuroQol End */
						
						if(!$baselineScore && $baselineScore!==0)
							$baselineScore='N/A';
							
						if(!$lastScore && $lastScore!==0)
							$lastScore='N/A'; ?>
                    <?php if($s['scale']!='N/A'){ ?>
                        <tr>
                    	<td colspan="3">
                            <p style="font-size: 14px; font-weight:normal; color: #333;"><span style="font-size: 18px; font-weight:bold; color: #000; padding: 5px 0;font-size: 16px; font-weight:bold; color: #333;" class="cat3"><?php echo $s['scale'];?></span></p>
                               
                        </td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td><p style="font-size: 14px; font-weight:normal; color: #333;">Base Line Score</p></td>
                        <td>:</td>
                        <td><?php echo $baselineScore;?> <span style="color:#999">(Max score: <?php echo $s['totalScore'];?>)</span></td>
                    </tr>
                    
                    <tr>
                        <td><p style="font-size: 14px; font-weight:normal; color: #333;">Current Respone</p></td>
                        <td>:</td>
                        <td><?php echo $s['curRes'];?> %</td>
                    </tr>
                    <?php } ?>
                <?php }}?>
            </table>
            
            <div style="border-top:1px solid #f1f1f1; padding:10px 0 0 0; margin:20px 0 0 0">
            	<strong>M:</strong> (925) 303-4227 &nbsp;&nbsp;&nbsp; <strong>Email: </strong>info@mypainimpact.com
            </div>
        </div>
    </div>
</div>