<div style="width:95%; background:#f1f1f1; padding:6px">
	<div style="background:#fff; padding:10px">
    	<div>
        	<div style="margin:0 0 10px 0; padding:0 0 10px 0; border-bottom:1px solid #f1f1f1; font-size:18px">
            	<a href="<?php echo URL;?>"><img src="<?php echo URL;?>assets/img/logo.png" width="100px" alt="My Pain Impact" /></a>
            </div>
            
            <div>
            	Dear <?php echo $dtl['firstName'];?>,<br /><br />
                Your password has been reset successfully.
               
                <div style="text-transform:uppercase; margin-top:15px">Account details:</div>
            </div>
            
        	<table border="0" cellpadding="5">
                <tr>
                	<td width="140px">Name</td>
                    <td width="3px">:</td>
                    <td><?php echo $dtl['firstName'];?></td>
                </tr>
                
                <tr>
                	<td>Login Email</td>
                    <td>:</td>
                    <td><?php echo $dtl['loginEmail'];?></td>
                </tr>
                
                <tr>
                	<td>New Password</td>
                    <td>:</td>
                    <td><?php echo $dtl['realPass'];?></td>
                </tr>
            </table>
            
            <div style="border-top:1px solid #f1f1f1; padding:10px 0 0 0; margin:20px 0 0 0">
            	<strong>M:</strong> (925) 303-4227 &nbsp;&nbsp;&nbsp; <strong>Email: </strong>info@mypainimpact.com
            </div>
        </div>
    </div>
</div>