<div style="width:95%; background:#f1f1f1; padding:6px">
	<div style="background:#fff; padding:10px">
    	<div>
        	<div style="margin:0 0 10px 0; padding:0 0 10px 0; border-bottom:1px solid #f1f1f1; font-size:18px">
            	<a href="<?php echo URL;?>"><img src="<?php echo URL;?>assets/img/logo.png" width="100px" alt="My Pain Impact" /></a>
            </div>
            
            <div>
            	Dear <?php echo $patientDtl['firstName'].' '.$patientDtl['lastName'];?>,<br /><br />
                Dr. <?php echo $doctorDtl['firstName'].' '.$doctorDtl['lastName'];?> would like to request your permission for accessing your health record on My Pain Impact.
                
                <!--<div style="margin:15px 0px">
                    <a href="<?php //echo URL.'user/activateAccount/'.encode($dtl['userId']).'~'.urlencode($dtl['loginEmail']);?>">Click here to activate your account</a>
                </div>-->
                
            </div>
            
        	
            <p>&nbsp;</p>
            <a href="<?php echo PATIENT_URL;?>">Click here to login and accept request in your notification section.</a>
            <p>&nbsp;</p>
            
            <div style="border-top:1px solid #f1f1f1; padding:10px 0 0 0; margin:20px 0 0 0">
            	<strong>M:</strong> (925) 303-4227 &nbsp;&nbsp;&nbsp; <strong>Email: </strong>info@mypainimpact.com
            </div>
        </div>
    </div>
</div>
