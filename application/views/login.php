<?php
?>
<div class="signinpanel container">
    <div class="row form-group">
    	<div class="col-md-6 posRel" style="margin:0 auto; float:none">
        	<?php echo getFlash();?>
            <form method="post">
            <!--	<div style="text-align:right; position:absolute; right:18px; top:5px">
                	<a href="<?php // echo URL."support/lists"?>" style="color:#428bca; text-decoration:underline">Support Ticket</a>
                </div> -->
                
            	<div align="center">
                    <a href="<?php echo URL;?>"><img src="<?php echo URL;?>assets/img/logo.png" /></a>
                </div>
                <div class="mb30"></div>
                
                <input type="text" name="username" id="username" class="form-control uname" placeholder="Email-ID" 
                value="<?php echo set_value('username'); ?>" />
                <label for="username" class="error"><?php echo error_msg($errors,'username');?></label>
                
                <input type="password" name="password" id="password" class="form-control pword" placeholder="Password" />
                <label for="password" class="error"><?php echo error_msg($errors,'password');?></label>
                
                <button class="btn btn-success btn-block">Sign In</button>
            </form>
            
            <div class="mb20"></div>
            <div>
            	<div class="pull-right">
                	<a href="<?php echo URL."user/forgotPass";?>">Forgot Password?</a>
                </div>
                
            	<!--<strong>Not a member? <a href="<?php echo URL."user/register";?>">Regsiter Now</a></strong>-->
                <strong>Not a member? <a href="<?php echo URL."user/choosePlans";?>">Regsiter Now</a></strong>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <!--<div class="signup-footer">
        <div class="pull-left">
            &copy; My Pain Ltd 2014. All rights reserved.
        </div>
        <div class="pull-right">
            Created By: <a href="http://itglobalconsulting.com/" target="_blank">IT Global Consulting Pvt. Ltd.</a>
        </div>
    </div>-->
</div>