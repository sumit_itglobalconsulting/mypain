<?php
$doctUrl = DOCT_URL;
?>

<div class="posRel" id="bodyMapParentBx">
    <!-- Ajax Loader -->
    <div class="ajaxLoader1">&nbsp;</div>
    <!-- /Ajax Loader -->

    <div class="pageheader">
        <h2>
            <i class="fa fa-user"></i>
            <?php echo $dtl['id'] ? "<a href='{$doctUrl}patients'>Patients</a> &rarr;<span> {$dtl['fullName']}</span>" : 'New Patient'; ?>
        </h2>
        <div class="breadcrumb-wrapper">
            <a href="<?php echo DOCT_URL . "patients"; ?>">&laquo; back to list</a>
        </div>
    </div>

    <?php if ($dtl['id']) { ?>
        <div class="contentpanel" style="padding-bottom:0px">
            <?php if (!$dtl['discharged']) { ?>
                <?php if ($dtl['archived']) { ?>
                    <a href="#" class="unArcBtn btn btn-primary">Retrieve</a>
                <?php } else { ?>
                    <a href="#" class="arcBtn btn btn-warning">Archive</a>
                <?php } ?>
            <?php } ?>

            <?php if ($dtl['discharged']) { ?>
                <a href="#" class="unDisBtn btn btn-success">Re-admit</a> (This patient has been discharged)
            <?php } else { ?>
                <a href="#" class="disBtn btn btn-danger">Discharge</a>
            <?php } ?>
        </div>
    <?php } ?>

    <!-- Alerts -->
    <div id="arcConfirmBx" class="myhide">
        <div align="center" class="smallForm" style="padding-top:10px">
            <p>Are you sure to archive this patient?</p>
            <p>&nbsp;</p>
            <div>
                <a href="<?php echo DOCT_URL . "patients/makeArchive/" . encode($dtl['id']); ?>" class="btn btn-primary">Yes</a>
                &nbsp;&nbsp;&nbsp;
                <button class="btn btn-default" onclick="closeDialog()">No</button>
            </div>
        </div>
    </div>

    <div id="unArcConfirmBx" class="myhide">
        <div align="center" class="smallForm" style="padding-top:10px">
            <p>Are you sure to retrieve this patient?</p>
            <p>&nbsp;</p>
            <div>
                <a href="<?php echo DOCT_URL . "patients/makeUnArchive/" . encode($dtl['id']); ?>" class="btn btn-primary">Yes</a>
                &nbsp;&nbsp;&nbsp;
                <button class="btn btn-default" onclick="closeDialog()">No</button>
            </div>
        </div>
    </div>

    <div id="disConfirmBx" class="myhide">
        <div align="center" class="smallForm" style="padding-top:10px">
            <?php if ($dtl['needFollowUp'] == 'Y') { ?>
                <p>This patient can't be discharged until Needs Follow Up status is set as 'No'</p>
            <?php } else { ?>
                <p>Are you sure to discharge this patient?</p>
                <p>&nbsp;</p>
                <div>
                    <a href="<?php echo DOCT_URL . "patients/makeDischarge/" . encode($dtl['id']); ?>" class="btn btn-primary">Yes</a>
                    &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-default" onclick="closeDialog()">No</button>
                </div>
            <?php } ?>
        </div>
    </div>

    <div id="unDisConfirmBx" class="myhide">
        <div align="center" class="smallForm" style="padding-top:10px">
            <p>Are you sure to re-admit this patient?</p>
            <p>&nbsp;</p>
            <div>
                <a href="<?php echo DOCT_URL . "patients/makeUnDischarge/" . encode($dtl['id']); ?>" class="btn btn-primary">Yes</a>
                &nbsp;&nbsp;&nbsp;
                <button class="btn btn-default" onclick="closeDialog()">No</button>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(".arcBtn").click(function (e) {
            e.preventDefault();
            showDialog($("#arcConfirmBx").html(), "Confirm Archive", true);
        });

        $(".unArcBtn").click(function (e) {
            e.preventDefault();
            showDialog($("#unArcConfirmBx").html(), "Confirm Retrieve", true);
        });

        $(".disBtn").click(function (e) {
            e.preventDefault();
            showDialog($("#disConfirmBx").html(), "Confirm Discharge", true);
        });

        $(".unDisBtn").click(function (e) {
            e.preventDefault();
            showDialog($("#unDisConfirmBx").html(), "Confirm Re-admit", true);
        });



    </script>
    <!-- /Alerts -->


    <div class="contentpanel" style="min-width:1020px">
        <?php
        echo getFlash();

        if ($errors)
            $hclass = "";
        else
            $hclass = "myhide";
        ?>

        <div id="errMsgDiv" class="alert alert-danger <?php echo $hclass; ?>">Please fill the field(s) marked with red color!</div>

        <form id="patientForm" method="post" enctype="multipart/form-data" onsubmit="return submitForm()">
            <input type="hidden" name="id" value="<?php echo $dtl['id']; ?>" />
            <!-- ParentId for patient will be hospital user id -->
            <input type="hidden" name="parentId" value="<?php echo PARENT_ID; ?>" />
            
            <!-- if doctor want to treat already added patient -->
            <input type="hidden" name="share" value="<?php echo $share; ?>" />
            <!-- -->
            <textarea class="myhide" name="bodymap" id="bodymapInpt"></textarea>

            <div class="panel panel-default posRel">
                <div class="bodymapP">
                    <div class="panel-body panel-body-nopadding posRel">
                        <!-- Popup div -->
                        <?php
                        $this->load->view("common/bodymap/bodymap.php", array('L' => '-510', 'T' => '-20'));
                        ?>
                        <!-- /Popup div -->
                        <div class="bodyMap bodyMapDoctor <?php echo $dtl['gender'] == 'F' ? 'bodyMapFemaleBg' : 'bodyMapMaleBg'; ?>">
                            <?php echo $dtl['bodymap']; ?>
                        </div>
                    </div>
                </div>
                <?php if($share==1){$readonly='readonly';}else{$readonly='';} ?>
                <div class="panel-body">
                    <table class="formTbl" width="100%">
                        <tr>
                            <td width="150px" class="myreq">Hospital #</td>
                            <td>
                                <input type="text" name="hospitalId" id="hospitalId" class="form-control sz1" 
                                       value="<?php echo h($dtl['hospitalId']); ?>" maxlength="20" <?php echo $readonly; ?> />
                                <label class="error"><?php echo error_msg($errors, 'hospitalId'); ?></label>
                            </td>
                        </tr>

                        <tr>
                            <td class="myreq">Email-ID</td>
                            <td>
                                <input name="loginEmail" id="loginEmail" type="text" class="form-control sz1" 
                                       value="<?php echo h($dtl['loginEmail']); ?>" maxlength="50" <?php echo $readonly; ?> />
                                <label class="error" id="loginEmailErr"><?php echo error_msg($errors, 'loginEmail'); ?></label>
                                <a href="#" class="sendInviBtn btn btn-danger myhide">Send add request</a>
                                <div id="sendInvi" class="myhide">
                                    <div align="center" class="smallForm" style="padding-top:10px">
                                        <p>Are you sure to send add request to this patient?</p>
                                        <p>&nbsp;</p>
                                        <div>
                                            <a href="" id="sendInvitation" class="btn btn-primary">Yes</a>
                                            &nbsp;&nbsp;&nbsp;
                                            <button class="btn btn-default" onclick="closeDialog()">No</button>
                                        </div>
                                    </div>


                                </div>
                                <script>
                                    $(".sendInviBtn").click(function (e) {
                                        e.preventDefault();
                                        showDialog($("#sendInvi").html(), "Confirm request to add", true);
                                    });
                                </script>

                            </td>
                        </tr>

                        <tr>
                            <td class="myreq">First Name</td>
                            <td>
                                <input type="text" name="firstName" id="firstName" class="form-control sz1" value="<?php echo h($dtl['firstName']); ?>" 
                                       maxlength="50" <?php echo $readonly; ?> />
                                <label class="error"><?php echo error_msg($errors, 'firstName'); ?></label>
                            </td>
                        </tr>

                        <tr>
                            <td class="myreq">Last Name</td>
                            <td>
                                <input type="text" name="lastName" id="lastName" class="form-control sz1" value="<?php echo h($dtl['lastName']); ?>" 
                                       maxlength="50" />
                                <label class="error"><?php echo error_msg($errors, 'lastName'); ?></label>
                            </td>
                        </tr>

                        <tr>
                            <td>Sex</td>
                            <td>
                                <div style="width:120px">
                                    <?php
                                    $genderArr = array('M' => 'Male', 'F' => 'Female');
                                    echo form_dropdown('gender', $genderArr, $dtl['gender'], 'id="gender" class="form-control select2"');
                                    ?>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="myreq">DOB</td>
                            <td>
                                <input type="text" name="dob" id="dob" class="form-control sz0 hasCal" year-range="1900:2014" 
                                       value="<?php echo showDate($dtl['dob']); ?>" />
                                <label class="error"><?php echo error_msg($errors, 'dob'); ?></label>
                            </td>
                        </tr>

                        <tr>
                            <td class="myreq">Contact Number (1)</td>
                            <td>
                                <div>
                                    <div class="pull-left" style="width:80px; padding-right:10px">
                                        <?php
                                        echo form_dropdown('stdCd1', stdCodesArr(), $dtl['stdCd1'], 'class="form-control select2" data-placeholder="STD Code" style="width:100%"');
                                        ?>
                                    </div>

                                    <div class="pull-left">
                                        <input type="text" class="form-control" name="phone1" id="phone1" value="<?php echo h($dtl['phone1']); ?>" 
                                               maxlength="10" valid="int" />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <label class="error" id="phone1Err"><?php echo error_msg($errors, 'phone1'); ?></label>
                            </td>
                        </tr>

                        <tr>
                            <td>Contact Number (2)</td>
                            <td>
                                <div>
                                    <div class="pull-left" style="width:80px; padding-right:10px">
                                        <?php
                                        echo form_dropdown('stdCd2', stdCodesArr(), $dtl['stdCd2'], 'class="form-control select2" data-placeholder="STD Code" style="width:100%"');
                                        ?>
                                    </div>

                                    <div class="pull-left">
                                        <input type="text" class="form-control" name="phone2" id="phone2" value="<?php echo h($dtl['phone2']); ?>" 
                                               maxlength="10" valid="int" />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="myreq">Postcode</td>
                            <td>
                                <input type="text" name="zipcode" id="zipcode" class="form-control sz0" maxlength="16" 
                                       value="<?php echo $dtl['zipcode']; ?>" />
                                <label class="error"><?php echo error_msg($errors, 'zipcode'); ?></label>
                            </td>
                        </tr>

                        <tr>
                            <td>Case Summary</td>
                            <td style="padding-top:70px">
                                <textarea name="caseSummary" class="form-control" id="caseSummary" style="width:370px; height:120px" 
                                          spellcheck="false" maxlength="500"><?php echo $dtl['caseSummary']; ?></textarea>
                                <div align="right" id="taLengthCountBx" style="width:370px">500</div>
                            </td>
                        </tr>

                        <tr>
                            <td>Needs Follow-up</td>
                            <td>
                                <div style="width:170px">
                                    <?php
                                    $opt1 = array('Y' => 'Yes', 'N' => 'No');
                                    echo form_dropdown('needFollowUp', $opt1, $dtl['needFollowUp'], 'id="needFollowUp" class="form-control select1"');

                                    if ($dtl['needFollowUp'] == 'N')
                                        $dis = 'disabled="disabled"';
                                    ?>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>Follow-up Type</td>
                            <td>
                                <div style="width:170px">
                                    <?php
                                    echo form_dropdown('followUpType', followUpTypesArr(), $dtl['followUpType'], 'id="followUpType" class="form-control select2" ' . $dis);
                                    ?>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td style="padding-top:20px">Follow-up</td>
                            <td>
                                <div>
                                    <div class="pull-left" style="width:125px; padding-right:10px">
                                        <div class="text-muted">Date</div>
                                        <input type="text" name="followUpDate" id="followUpDate" class="form-control sz0 hasCal" 
                                               value="<?php echo showDate($dtl['followUpDate']); ?>" <?php echo $dis; ?> />
                                    </div>

                                    <div class="pull-left" style="width:190px">
                                        <div class="text-muted">Frequency</div>
                                        <?php
                                        echo form_dropdown("followUpFreq", array('' => 'Select') + followUpFrequencyArr(), $dtl['followUpFreq'], "id='followUpFreq' class='form-control select2'" . $dis);
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <label class="error"><?php echo error_msg($errors, 'followUpDate'); ?></label>
                                <label class="error"><?php echo error_msg($errors, 'followUpFreq'); ?></label>
                            </td>
                        </tr>

                        <tr>
                            <td>Assisted follow up required?</td>
                            <td>
                                <div style="width:170px">
                                    <?php
                                    $opt2 = array('N' => 'No', 'Y' => 'Yes');
                                    echo form_dropdown('needAssistance', $opt2, $dtl['needAssistance'], 'class="form-control select2"');
                                    ?>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>On Opioid Therapy</td>
                            <td>
                                <div style="width:170px">
                                    <?php
                                    echo form_dropdown('onOpioidTherapy', $opt2, $dtl['onOpioidTherapy'], 'class="form-control select2"');
                                    ?>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td valign="top">Treatment Options</td>
                            <td>
                                <?php $opts = treatmentOptionsArr(); ?>
                                <div class="ckbox ckbox-primary">
                                    <input type="checkbox" id="t1" name="treatment1" value="1" <?php setCheckChecked($dtl['treatment1'], 1); ?> />
                                    <label for="t1"><?php echo $opts[1]; ?></label>
                                </div>
                                <div class="ckbox ckbox-primary">
                                    <input type="checkbox" id="t2" name="treatment2" value="1" <?php setCheckChecked($dtl['treatment2'], 1); ?> />
                                    <label for="t2"><?php echo $opts[2]; ?></label>
                                </div>

                                <div class="ckbox ckbox-primary">
                                    <input type="checkbox" id="t3" name="treatment3" value="1" <?php setCheckChecked($dtl['treatment3'], 1); ?> />
                                    <label for="t3"><?php echo $opts[3]; ?></label>
                                </div>
                                <div class="ckbox ckbox-primary">
                                    <input type="checkbox" id="t4" name="treatment4" value="1" <?php setCheckChecked($dtl['treatment4'], 1); ?> />
                                    <label for="t4"><?php echo $opts[4]; ?></label>
                                </div>
                            </td>
                        </tr>

                        <?php if ($doctorForms) { ?>
                            <tr>
                                <td colspan="2">
                                    <h4 class="subtitle">Assign Form(s)</h4>
                                    <div class="panel-body panel-body-nopadding">
                                        <div class="alert alert-danger myhide" id="formAssignErr">Please assign at least one form!</div>
                                        <div class="alert alert-danger myhide" id="nextResDueDateErr">First response due date must be less than Follow-up Date!</div>
                                        <div class="alert alert-danger myhide" id="nextResDueDateEmptyErr">Please enter first response due date!</div>
                                        <div class="alert alert-danger myhide" id="goalAvgPoorErr">Average Response value must be in between Good and Poor Response!</div>
                                        <div class="alert alert-danger myhide" id="weightageErr">Total of form's weightage must be 100%!</div>
                                        <div class="alert alert-danger myhide" id="pgicSymptomErr">Please enter the symptom for PGIC from!</div>

                                        <?php
                                        $cats = multiArrToKeyValue($cats, "id", "name");
                                        $dfcats = array();

                                        foreach ($doctorForms as $df) {
                                            $dfc = explode(",", $df['catIds']);
                                            if ($dfc) {
                                                foreach ($dfc as $c) {
                                                    if ($c)
                                                        $dfcats[] = $c;
                                                }
                                            }
                                        }

                                        $dfcats = array_unique($dfcats);
                                        sort($dfcats);
                                        $catsArr = array();
                                        foreach ($dfcats as $dc) {
                                            $catsArr[$dc] = $cats[$dc];
                                        }
                                        ?>
                                        <fieldset>
                                            <legend>Form Categories</legend>
                                            <div class="pad1">
                                                <div class="formCatR">
                                                    <?php foreach ($catsArr as $id => $cat) { ?>
                                                        <div style="float:left; width:250px; margin:0px 20px 0px 0px">
                                                            <div class="ckbox ckbox-primary">
                                                                <input type="checkbox" id="cat<?php echo $id; ?>" value="<?php echo $id; ?>" checked="checked"  />
                                                                <label for="cat<?php echo $id; ?>"><?php echo $cat; ?></label>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="mb20"></div>

                                        <div>
                                            <!-- Tab -->
                                            <div class="tabBox">
                                                <div class="tabBtns">
                                                    <?php
                                                    foreach ($doctorForms as $i => $df) {
                                                        $class = $i == 0 ? 'act' : '';
                                                        ?>
                                                        <a href="#" class="<?php echo $class; ?>"  catid="<?php echo $df['catIds']; ?>"
                                                           title="<?php echo $df['formTitle']; ?>"><?php echo $df['formName']; ?></a>
                                                       <?php } ?>
                                                    <div class="clearfix"></div>
                                                </div>

                                                <div class="tabContentBx" style="min-height:330px">
                                                    <?php
                                                    $assignedFormsIds = array();
                                                    if ($assignedForms)
                                                        $assignedFormsIds = multiArrToKeyValue($assignedForms, 'id', 'formId');
                                                    else
                                                        $assignedForms = array();
                                                    ?>

                                                    <?php
                                                    foreach ($doctorForms as $i => $df) {
                                                        $hide = $i > 0 ? 'myhide' : '';
                                                        $formId = $df['formId'];
                                                        $formDtl = arrayFilter($assignedForms, 'formId', $formId);
                                                        $formDtl = array_shift($formDtl);
                                                        ?>
                                                        <div class="tabContent <?php echo $hide; ?> posRel" catid="<?php echo $df['catIds']; ?>">
                                                            <?php
                                                            $showForm = true;
                                                            if ($formDtl['doctorId']) {
                                                                if ($formDtl['status'] == 0)
                                                                    $showForm = true;
                                                                else if ($formDtl['doctorId'] != USER_ID)
                                                                    $showForm = false;
                                                            }
                                                            else {
                                                                $showForm = true;
                                                            }
                                                            ?>

                                                            <?php if ($showForm) { ?>

                                                                <?php
                                                                if ($_POST && !$checkFlag) {
                                                                    $af = $_POST['assignForm'];
                                                                    if ($af) {
                                                                        foreach ($af as $a)
                                                                            $assignedFormsIds[] = $a;
                                                                    }

                                                                    $formDtl['symptom'] = $_POST['symptom'][$formId];
                                                                    $formDtl['nextResDueDate'] = $_POST['nextResDueDate'][$formId];
                                                                    $formDtl['resFreq'] = $_POST['resFreq'][$formId];
                                                                    $formDtl['weightage'] = $_POST['weightage'][$formId];
                                                                }
                                                                ?>

                                                                <div class="posRel" style="padding:5px 0px 15px 0px; font-weight:bold">
                                                                    Assign Form:&nbsp;&nbsp;&nbsp;
                                                                    <input type="radio" name="assignForm[<?php echo $formId; ?>]" id="r1<?php echo $formId; ?>" 
                                                                           value="<?php echo $formId; ?>" <?php echo in_array($formId, $assignedFormsIds) ? 'checked' : '' ?> class="yesRadio" />
                                                                    <label for="r1<?php echo $formId; ?>">Yes</label>
                                                                    &nbsp;&nbsp;
                                                                    <input type="radio" name="assignForm[<?php echo $formId; ?>]" id="r2<?php echo $formId; ?>" 
                                                                           value="" <?php echo (!in_array($formId, $assignedFormsIds)) ? 'checked' : '' ?> class="noRadio" />
                                                                    <label for="r2<?php echo $formId; ?>">No</label>

                                                                    <!-- Hidden Fields -->
                                                                    <input type="hidden" name="formStatus[<?php echo $formId; ?>]" value="<?php echo $formDtl['status']; ?>" />
                                                                    <input type="hidden" name="formCreated[<?php echo $formId; ?>]" value="<?php echo $formDtl['created']; ?>" />

                                                                    <!-- Symptom Field -->
                                                                    <?php if ($formId == 2) { ?>
                                                                        <div style="position:absolute; right:160px; top:5px; font-weight:normal">
                                                                            <span class="tooltips" data-placement="top" title="Symptom">
                                                                                <input name="symptom[<?php echo $formId; ?>]" id="symptom<?php echo $formId; ?>" type="text" 
                                                                                       class="form-control sz2" value="<?php echo h($formDtl['symptom']); ?>" maxlength="200" 
                                                                                       placeholder="Symptom" formId="<?php echo $formId; ?>" />
                                                                            </span>
                                                                        </div>
                                                                    <?php } ?>

                                                                    <?php if (isFormSubmited($dtl['id'], $formId)) { ?>
                                                                        <div class="pull-right text-right">
                                                                            <a href="<?php echo DOCT_URL . "patients/response/" . encode($formId) . "/" . encode($dtl['id']); ?>">
                                                                                <button class="btn btn-success">
                                                                                    <i class="fa fa-eye"></i> View responses
                                                                                </button>
                                                                            </a>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <div class="clearfix"></div>
                                                                </div>

                                                                <div class="posRel">
                                                                    <table border="0" width="100%">
                                                                        <tr>
                                                                            <td style="padding-bottom:35px" align="center">
                                                                                <?php $resDtl = isFormSubmited($dtl['id'], $formId); ?>
                                                                                <p class="mb5">
                                                                                    <?php
                                                                                    if ($resDtl && $formDtl['resFreq'] != 'O')
                                                                                        echo 'Next';
                                                                                    else
                                                                                        echo 'First';
                                                                                    ?> Response Date:
                                                                                </p>
                                                                                <input type="text" class="form-control sz0 hasCal firstResDate" name="nextResDueDate[<?php echo $formId; ?>]" 
                                                                                       id="nextResDueDate<?php echo $formId ?>" value="<?php echo showDate($formDtl['nextResDueDate']); ?>" 
                                                                                       style="border-radius:0" formId="<?php echo $formId; ?>" />

                                                                                <label class="error" id="nextResDueDate<?php echo $formId ?>Err">&nbsp;</label>
                                                                            </td>

                                                                            <td align="center">
                                                                                <p class="mb5">Frequency of Response:</p>
                                                                                <div style="width:130px">
                                                                                    <?php
                                                                                    echo form_dropdown("resFreq[{$df['formId']}]", responseFrequencyArr(), $formDtl['resFreq'], "id='resFreq$formId' class='select1' formId='$formId'");
                                                                                    ?>
                                                                                </div>
                                                                            </td>

                                                                            <td align="center">
                                                                                <?php if ($df['setGoal']) { ?>
                                                                                    <p class="mb5">Form Weightage:</p>
                                                                                    <div style="width:130px">
                                                                                        <?php
                                                                                        echo form_dropdown("weightage[$formId]", array(0 => 'All Equal') + weightageArr(), $formDtl['weightage'], "id='weightage$formId' class='select1' formId='$formId'");
                                                                                        ?>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </td>
                                                                        </tr>

                                                                        <!-- Meters -->

                                                                        <?php
                                                                        if ($df['setGoal']) {
                                                                            $scales = formScales($formId, $dtl['id'], USER_ID);
                                                                            ?>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <h4 class="subtitle mb5">Good</h4>(Percentage change from baseline score)
                                                                                </td>
                                                                                <td align="center">
                                                                                    <h4 class="subtitle mb5">Average Response</h4>(Percentage change from baseline score)
                                                                                </td>
                                                                                <td align="center">
                                                                                    <h4 class="subtitle mb5">Poor Response</h4>(Percentage change from baseline score)
                                                                                </td>
                                                                            </tr>

                                                                            <?php
                                                                            foreach ($scales as $sc) {
                                                                                $scaleId = $sc['id'];
                                                                                ?>
                                                                                <?php
                                                                                $goodRes = $sc['goodRes'] ? $sc['goodRes'] : $sc['defaultGoodRes'];
                                                                                $avgRes = $sc['avgRes'] ? $sc['avgRes'] : $sc['defaultAvgRes'];

                                                                                if ($_POST && !$checkFlag) {
                                                                                    $goodRes = $_POST['goodRes'][$scaleId];
                                                                                    $avgRes = $_POST['avgRes'][$scaleId];
                                                                                }
                                                                                ?>

                                                                                <tr>
                                                                                    <td colspan="3">
                                                                                        <div>
                                                                                            <fieldset>
                                                                                                <?php if ($sc['groupName'] != 'N/A') { ?>
                                                                                                    <legend><?php echo $sc['groupName']; ?></legend>
                                                                                                <?php } ?>
                                                                                                <div style="padding:10px" align="center">
                                                                                                    <div style="float:left; width:33%">
                                                                                                        Above 
                                                                                                        <?php
                                                                                                        echo form_dropdown("goodRes[$scaleId]", weightageArr(), $goodRes, "class='select1 goodRes' id='goodRes$scaleId'  
																									defres='{$sc['defaultGoodRes']}' formId='$formId' scaleId='$scaleId'");
                                                                                                        ?>
                                                                                                    </div>

                                                                                                    <div style="float:left; width:33%">
                                                                                                        Between 
                                                                                                        <?php
                                                                                                        echo form_dropdown("avgRes[$scaleId]", weightageArr(), $avgRes, "class='select1 avgRes' id='avgRes$scaleId' 
																										defres='{$sc['defaultAvgRes']}' formId='$formId' scaleId='$scaleId'");
                                                                                                        ?>
                                                                                                        and 
                                                                                                        <span class="bwGood<?php echo $scaleId ?> bold">
                                                                                                            <?php echo $goodRes; ?>
                                                                                                        </span>
                                                                                                        <strong>%</strong>
                                                                                                    </div>

                                                                                                    <div style="float:left; width:33%">
                                                                                                        <div style="font-size:18px; padding-top:5px">
                                                                                                            Below
                                                                                                            <span class="poorRes<?php echo $scaleId ?> bold">
                                                                                                                <?php echo $avgRes; ?>
                                                                                                            </span>
                                                                                                            <strong>%</strong>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="clearfix"></div>
                                                                                                </div>
                                                                                            </fieldset>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                        <?php } ?>

                                                                        <?php if ($formId == 2) { ?>
                                                                            <tr>
                                                                                <td colspan="3" width="100%">
                                                                                    <div id="feelResChart" style="width:300px; margin:0 auto">
                                                                                        <?php echo isset($feels) ? $feels : 0; ?>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                        <!-- /Meters -->
                                                                    </table>
                                                                </div>

                                                            <?php } else { ?>
                                                                <div class="notFound">
                                                                    <p>This form has been already assigned by other doctor.</p>
                                                                </div>
                                                            <?php } ?>

                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>

                        <?php if (!$isDischarged) { ?>
                            <tr>
                                <td colspan="2" style="padding-top:15px">
                                    <button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                                    <a href="<?php echo DOCT_URL . "patients"; ?>">
                                        <button type="button" class="btn btn-default">Cancel</button>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </form>
    </div>

</div>

<?php set_error_class($errors, 'redBdr'); ?>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".chosen-select").chosen({'width': '100%', 'white-space': 'nowrap', disable_search: true});
    });

    $("#loginEmail").blur(function () {
        var pathname = window.location.pathname;
        var index = pathname.toLowerCase().indexOf("add");
        if (index < 0) {
            return false;
        }
        var email = $("#loginEmail").val();
        email = email.trim();
        if (!email) {
            return false;
        }

        var file_url = SITE_URL + "clinician/patients/isPatientExist";
        $.ajax({
            type: "POST",
            url: file_url,
            data: {'email': email},
            dataType: "json",
            cache: false
        }).done(function (msg) {
            if (msg != '') {
                $("#loginEmailErr").text(msg["text"]);
                $("a#sendInvitation").attr("href", SITE_URL + "clinician/patients/sendInvitation/" + msg['id']);
                $("a.sendInviBtn,#loginEmailErr").show();

            }
        });

    });


    if (typeof (setTabBox) == 'function')
        setTabBox();

    function submitForm() {
        $("#patientForm .alert").hide();
        $("#patientForm input, #patientForm select").removeClass('redBdr');


        var error = false;
        $("#bodymapInpt").html($(".bodyMap").html());

        if (!checkValidPhone($("#phone1"), $("#phone1Err")))
            error = true;
        if (!checkValidPhone($("#phone2"), $("#phone2Err")))
            error = true;

        if (!checkValidEmail($("#loginEmail")))
            error = true;

        var weightage = 0, eflag = 0, nflag = 0;
        $(".yesRadio").each(function () {
            if ($(this).prop('checked')) {
                fId = $(this).val();
                if (!$("#nextResDueDate" + fId).val()) {
                    error = true;
                    $("#nextResDueDateEmptyErr").show();
                    $("#nextResDueDate" + fId).addClass('redBdr');
                }

                if ($("#weightage" + fId).length) {
                    w = $("#weightage" + fId).val() * 1;
                    weightage += w;
                }
                if (w == 0)
                    eflag = 1;
                if (w != 0)
                    nflag = 1;
            }
        });

        if ((weightage != 0 && weightage != 100)) {
            error = true;
            $("#weightageErr").show();
        }

        if (followupdate = $("#followUpDate").val()) {
            followupdate = new Date(followupdate);
            $(".firstResDate").each(function () {
                if (d = $(this).val()) {
                    d = new Date(d);
                    if (followupdate <= d) {
                        //error=true;
                        //$("#nextResDueDateErr").show();
                        //$(this).addClass('redBdr');
                    }
                }
            });
        }

        if ($("#r12").prop("checked") && !$.trim($("#symptom2").val())) {
            error = true;
            $("#pgicSymptomErr").show();
            $("#symptom2").addClass('redBdr');
        }

        if (error) {
            $("#errMsgDiv").show();
        }
        else
            $("#errMsgDiv").hide();

        return !error;
    }


    /** Reponse Dials Value validation **/
    $(".goodRes").click(function () {
        cv = $(this).val();
        defRes = $(this).attr('defres');
        $(this).change(function () {
            gv = $(this).val();

            scaleId = $(this).attr('scaleId');
            av = $("#avgRes" + scaleId).val();
            $(".bwGood" + scaleId).text(gv);

            if (av >= gv) {
                $(this).val(cv);
                showDialog("Good response must be greater than average response", "Error", true);
            }
        });
    });

    $(".avgRes").click(function () {
        cv = $(this).val();
        defRes = $(this).attr('defres');
        $(this).change(function () {
            av = $(this).val();
            scaleId = $(this).attr('scaleId');
            gv = $("#goodRes" + scaleId).val();
            $(".poorRes" + scaleId).text(av);

            if (av >= gv) {
                $(this).val(cv);
                showDialog("Average response must be less than good response", "Error", true);
            }
        });
    });
    /** Reponse Dials Value validation End **/



    function checkValidPhone(obj, errBxOb) {
        phone = $.trim(obj.val());
        errBxOb.text('');
        obj.removeClass('redBdr');
        if (phone) {
            if (!isvalidPhone(phone)) {
                obj.addClass('redBdr');
                errBxOb.text("Please enter a valid phone number");
                return false;
            }
        }
        return true;
    }


    $("#loginEmail").blur(function () {
        checkValidEmail($(this));
    });

    $("#gender").change(function () {
        if ($(this).val() == 'F') {
            $(".bodyMap").removeClass("bodyMapMaleBg").addClass("bodyMapFemaleBg");
        }
        else {
            $(".bodyMap").removeClass("bodyMapFemaleBg").addClass("bodyMapMaleBg");
        }
    });

    $("#phone1, #phone2").blur(function () {
        checkValidPhone($(this), $("#" + $(this).attr("id") + "Err"));
    });


    $("#caseSummary").on("keypress keyup paste", function () {
        clength = $(this).val().length;
        $("#taLengthCountBx").text(500 - clength);
    });

    $("#taLengthCountBx").text(500 - $("#caseSummary").val().length);

    $('label').disableSelection();


    $("#needFollowUp").change(function () {
        if ($(this).val() == 'N') {
            $("#followUpFreq").val("");
            $("#followUpType, #followUpDate, #followUpFreq").prop('disabled', true);
        }
        else
            $("#followUpType, #followUpDate, #followUpFreq").prop('disabled', false);
    });


    /** Assign form [Yes/No] **/
    $(".yesRadio, .noRadio").click(function () {
        formId = $(this).attr('id').replace('r1', '').replace('r2', '');
        el = "[formId='" + formId + "']";
        if ($(this).val()) {
            $(el).prop('disabled', false);
        }
        else {
            $(el).prop('disabled', true);
        }
    });

    $(".noRadio").each(function () {
        if ($(this).prop('checked')) {
            formId = $(this).attr('id').replace('r2', '');
            el = "[formId='" + formId + "']";
            $(el).prop('disabled', true);
        }
    });


    /** Checking if patient exists **/
    if (location.href.indexOf('add') !== -1) {
        $("#loginEmail, #hospitalId").change(function () {
            hosId = $.trim($("#hospitalId").val());
            email = $.trim($("#loginEmail").val());

            if (!hosId || !email)
                return;

            $(".ajaxLoader1").show();
            $("#patientForm button").prop('disabled', true);
            ajax(SITE_URL + "clinician/patients/checkPatientExists", "mainContentBx", "patientForm", '', "POST");
        });
    }

    /** Charts **/
    function feelResChart(id, v) {
        if (!$("#" + id))
            return;

        v = parseInt(v);
        if (v < 1)
            colorCode = '#ff4b31';
        else if (v >= 1 && v <= 3)
            colorCode = "#fbd200";
        else
            colorCode = "#6fba61";
        var data = google.visualization.arrayToDataTable([['Year', 'Feel Response'], ['Feels', v], ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"}
        ]);

        verAxis = {title: '', viewWindow: {max: 5, min: -5}, gridlines: {count: 11}, titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}};

        var options = {
            title: '',
            hAxis: {title: '', titleTextStyle: {color: '#FF0000', italic: false}, textStyle: {fontSize: 12}},
            vAxis: verAxis,
            legend: {position: "none", textStyle: {fontSize: 12}},
            //chartArea: {left:"50", top:"30", width:"100%"},
            colors: [colorCode]
        };

        var chart = new google.visualization.ColumnChart(document.getElementById(id));
        chart.draw(view, options);
    }
    feelResChart("feelResChart", $.trim($("#feelResChart").text()));
    /** \charts **/


    /** Form Categories **/
    $(".formCatR input:checkbox").change(function () {
        catTabs();
    });

    function catTabs() {
        $(".tabBtns a, .tabContent").hide();
        $(".tabContent").removeAttr('curcat');
        $(".tabBtns a").removeClass('act');

        $(".formCatR input:checkbox").each(function () {
            if ($(this).prop('checked') == true) {
                catid = $(this).val();

                $(".tabBtns a").each(function (i) {
                    cats = $(this).attr('catid');
                    carr = cats.split(",");

                    if (carr.indexOf(catid) != -1) {
                        $(this).show();
                    }
                });

                $(".tabContent").each(function () {
                    cats = $(this).attr('catid');
                    carr = cats.split(",");

                    if (carr.indexOf(catid) != -1) {
                        $(this).attr('curcat', '1');
                    }
                });

                //$(".tabBtns a[catid='"+catid+"']").show();
                //$(".tabContent[catid='"+catid+"']").attr('curcat', '1');
            }
        });

        $(".tabBtns a:visible").eq(0).addClass('act');
        $(".tabContent[curcat='1']").eq(0).show();
    }
    /** **/
    catTabs();

    setDateToInputs();


    /** Navigation Alert If Form not submitted **/
//var submitted = '<?php //echo $_SESSION['pformSubmitted'];     ?>'; 

    var submitted = '<?php echo $this->session->flashdata("flash_edit"); ?>';

    if (!parseInt(submitted)) {
        $("section a").click(function (e) {
            e.preventDefault();
            url = $(this).attr('href');
            if (!url || url == "" || url == "#" || url == "javascript:void(0)")
                return;

            $("#okLinkBtn").attr('href', url);
            showDialog($("#navConfirmBx").html(), "Navigation Alert", true);
        });
    }
</script>

<!-- Alerts -->
<?php
//echo $cId=$dtl['edit']?'A':'navConfirmBx';
?>       

<div id="<?php echo 'navConfirmBx'; ?>" class="myhide">
    <div align="center" class="smallForm" style="padding-top:10px">
        <p><strong>You have not submitted the from.</strong></p>
        <p>Are you sure to leave this page?</p>
        <p>&nbsp;</p>
        <div>
            <a href="" id="okLinkBtn" class="btn btn-primary">Yes</a>
            &nbsp;&nbsp;&nbsp;
            <button class="btn btn-default" onclick="closeDialog()">No</button>
        </div>
    </div>
</div>