<?php
	$treatmentOpts=treatmentOptionsArr();
	function getTreatmentOpt($dtl, $treatOpt){
		$str="";
		$str.=$dtl['treatment1']?$treatOpt[1].", ":"";
		$str.=$dtl['treatment2']?$treatOpt[2].", ":"";
		$str.=$dtl['treatment3']?$treatOpt[3].", ":"";
		$str.=$dtl['treatment4']?$treatOpt[4].", ":"";
		return substr($str, 0, -2);
	}
	
	$qs=arrayUrlDecode($_GET);
?>

<div class="tabBtns" style="margin-bottom:5px">
	<div style="width:40%; float:left">
        <a href="<?php echo DOCT_URL."patients";?>">Status</a>
        <a href="<?php echo DOCT_URL."patients/followUps";?>">Follow Up</a>
        <a href="<?php echo URL."message/inbox";?>" class="act">Message</a>
        <div class="clr">&nbsp;</div>
    </div>
    
    <div style="width:60%; float:left">
    	<!-- Search Form -->
    	<form method="get" id="searchForm" onsubmit="return submitSearch()">
        	<table class="searchFormTbl">
            	<tr>
                	<td>Filter:</td>
                    <td>
                    	<?php echo form_dropdown("treatOpt", array(''=>'All')+$treatmentOpts, $qs['treatOpt'], 'class="padd1"');?>
                    </td>
                    
                    <td align="right">
                    	<input type="text" name="k" value="<?php echo $qs['k'];?>" placeholder="Search by Name, DOB, Hospital ID" class="inpt1 pad4" />
                    </td>
                    
                    <td><button type="submit">Search</button></td>
                </tr>
            </table>
        </form>
        <!-- /Search Form -->
    </div>
    <div class="clr">&nbsp;</div>
</div>

<div class="dt_grd">
	<?php if($result){?>
    <table cellspacing="1" class="oddEven">
        <tr class="h h1">
            <td width="100px">Hospital #</td>
            <td>Patient Name</td>
            <td width="300px">Treatment Options</td>
            <td width="110px" align="center">DOB</td>
        </tr>
        
        <?php foreach($result as $i=>$dtl){?>
        <tr class="i hover">
            <td><?php echo $dtl['hospitalId'];?></td>
            <td>
            	<a href="<?php echo DOCT_URL."patients/edit/".encode($dtl['id']);?>">
					<?php echo $dtl['name'];?>
                </a>
            </td>
            
            <td>
				<?php echo getTreatmentOpt($dtl, $treatmentOpts);?>
            </td>
            
            <td align="center"><?php echo showDate($dtl['dob']);?></td>
        </tr>
        <?php }?>
    </table>
    <?php }else{?>
    <div class="notFound">Not Found</div>
    <?php }?>
</div>

<?php if($page['total_pages']>1){?>
    <div class="paging">
        Pages: <?php pagingLinks($page, DOCT_URL."patients/lists", 'act', ' | ');?>
    </div>
<?php }?>


<script type="text/javascript">
function submitSearch(){
	$("#searchForm input, #searchForm select").each(function(){
		ob=$(this);
		if(!ob.val())
			ob.removeAttr('name');
	});
}
</script>