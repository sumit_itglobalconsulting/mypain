<?php
	$treatmentOpts=treatmentOptionsArr();
	$treatmentOpts=$treatmentOpts+array('OT'=>'On Opioid Therapy');
	
	function getTreatmentOpt($dtl, $treatOpt){
		$str='<ol>';
		$i=1;
		$str.=$dtl['treatment1']?'<li>'.$treatOpt[1]."</li>":"";
		$str.=$dtl['treatment2']?'<li>'.$treatOpt[2]."</li>":"";
		$str.=$dtl['treatment3']?'<li>'.$treatOpt[3]."</li>":"";
		$str.=$dtl['treatment4']?'<li>'.$treatOpt[4]."</li>":"";
		
		return $str.'</ol>';
	}
	
	$qs=arrayUrlDecode($_GET);
?>

<div class="pageheader">
	<h2><i class="fa fa-user"></i> Status</h2>
</div>

<div class="contentpanel">
	<div style="min-height:800px">
		<?php echo getFlash();?>
        <div class="table-responsive">
            <form method="get" id="searchForm" onsubmit="return submitSearch()">
                <input type="hidden" name="orderBy" id="orderBy" />
                <h4 class="subtitle mb5">Refine Results</h4>
                <div class="myWhiteBx smallForm">
                    <table class="filterTbl">
                        <tr>
                            <td width="280px">
                                <span class="tooltips" data-placement="top" title="Treatment Option">
                                <?php 
                                    echo form_dropdown("treatOpt", array(''=>'Treatment Option (All)')+$treatmentOpts, $qs['treatOpt'], 
                                    'class="form-control chosen-select"');
                                ?>
                                </span>
                            </td>
                            
                            <td width="170px">
                                <span class="tooltips" data-placement="top" title="Response">
                                <?php 
                                    $resTypes=array('P'=>'Poor Response', 'A'=>'Average Response', 'G'=>'Good Response');
                                    echo form_dropdown("resType", array(''=>'All Responses')+$resTypes, $qs['resType'], 'class="form-control chosen-select"');
                                ?>
                                </span>
                            </td>
                            
                            <td width="200px">
                                <span class="tooltips" data-placement="top" title="Invitations Analysis">
                                <?php 
                                    echo form_dropdown("invAna", array(''=>'All')+invAnaArr(), $qs['invAna'], 'class="form-control chosen-select"');
                                ?>
                                </span>
                            </td>
                            
                            <td width="250px">
                                <input type="text" name="k" value="<?php echo $qs['k'];?>" placeholder="Search by Name, DOB, Hospital ID" class="form-control" />
                            </td>
                            
                            <td><button type="submit" class="btn btn-primary">Search</button></td>
                        </tr>
                    </table>
                </div>
            </form>
            
            <div class="mb20"></div>
            
            <?php if($result){if($qs['orderBy']) $order=explode(":", $qs['orderBy']);?>
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th width="100px" style="vertical-align:middle">Hospital #</th>
                            <th class="vert-align" style="vertical-align:middle; min-width:200px">
                                Patient Name
                                <?php if($order[0]=="name" && $order[1]=="DESC"){?>
                                    <span class="arrowB sortBtn" v="name"></span>
                                <?php }else{?>
                                    <span class="arrowT sortBtn" v="name"></span>
                                <?php }?>
                            </th>
                            <th width="200px" style="vertical-align:middle">Treatment Options</th>
                            <th width="100px" class="text-center" style="vertical-align:middle">
                                DOB
                                <?php if($order[0]=="dob" && $order[1]=="DESC"){?>
                                    <span class="arrowB sortBtn" v="dob"></span>
                                <?php }else{?>
                                    <span class="arrowT sortBtn" v="dob"></span>
                                <?php }?>
                            </th>
                            <th colspan="2" width="200px" class="text-center" style="vertical-align:middle">
                                Patient Status
                                <?php if($order[0]=="overallRes" && $order[1]=="DESC"){?>
                                    <span class="arrowB sortBtn" v="overallRes" title="Overall Response"></span>
                                <?php }else{?>
                                    <span class="arrowT sortBtn" v="overallRes" title="Overall Response"></span>
                                <?php }?>
                            </th>
                        </tr>
                    </thead>
                    <?php $qs = addSlash(arrayTrim(arrayUrlDecode($_GET)));
                            $invAna = $qs['invAna'];
                            if($invAna == 'IA'){
                               $share = 1;
                            }else{
                                $share = 0;
                            }
                            $share = "0/".encode($share);
                    ?>        
                    <tbody>
                        <?php foreach($result as $i=>$dtl){$feels=getFeels($dtl['id']);?>
                        <tr>
                            <td><?php echo $dtl['hospitalId'];?></td>
                            <td>
                                <?php echo $dtl['name'];?>
                                <div class="mb5"></div>
                                <div class="w1">
                                    <a href="<?php echo DOCT_URL."patients/edit/".encode($dtl['id'])."/".$share;?>">
                                        <i class="fa fa-edit"></i> Edit
                                    </a>
                                </div>
                                <div class="w1">
                                    <a href="<?php echo DOCT_URL."patient/reports/".encode($dtl['id'])."/".encode($doctorId)."/".$share;?>">
                                        <i class="fa fa-bar-chart-o"></i> View Report
                                    </a>
                                </div>
                            </td>
                            
                            <td>
                                <?php echo getTreatmentOpt($dtl, $treatmentOpts);?>
                            </td>
                            
                            <td align="center"><?php echo showDate($dtl['dob']);?></td>
                
                            <td width="100px" align="center">
                                <?php
                                    if(!$dtl['overallRes'])
                                        $dtl['overallRes']=0;
                                        
                                    if($dtl['overallRes']<30)
                                        $resBg='#ff4b31';
                                    else if($dtl['overallRes']>=30 && $dtl['overallRes']<50)
                                        $resBg='#fbd200';
                                    else
                                        $resBg='#6fba61';
                                    $resBg='background:'.$resBg;
                                ?>
                                <div class="stBx" style=" <?php echo $resBg;?> "><?php echo round($dtl['overallRes']);?>%</div>
                                <div>Overall</div>
                            </td>
                            
                            <td width="100px" align="center">
                                <?php
//                                    if($feels<=0)
//                                        $feelBg='#FF0000';
//                                    else if($feels>0 && $feels<=1)
//                                        $feelBg='#FFA500';
//                                    else if($feels>1 && $feels<=3)
//                                        $feelBg='#FDD017';
//                                    else
//                                        $feelBg='#008000';
//                                    $feelBg='background:'.$feelBg;
                                      switch($feels){
                                            case -5:
                                                $feelBg='#eb1e30';
                                                break;
                                            case -4:
                                                $feelBg='#eb1e30';
                                                break;
                                            case -3:
                                                $feelBg='#f59440';
                                                break;
                                            case -2:
                                                $feelBg='#f59440';
                                                break;
                                            case -1:
                                                $feelBg='#f59440';
                                                break;
                                            case 0:
                                                $feelBg='#ffc908';
                                                break;
                                            case 1:
                                                $feelBg='#bcd433';    
                                                break;
                                            case 2:
                                                $feelBg='#bcd433';    
                                                break;
                                            case 3:
                                                $feelBg='#bcd433';    
                                                break;
                                            case 4:
                                                $feelBg='#8ac75b';
                                                break;
                                            case 5:
                                                $feelBg='#8ac75b';    
                                                break;
                                            } 
                                            $feelBg='background:'.$feelBg;
                                ?>
                                <div class="stBx" style=" <?php echo $feelBg;?> "><?php echo $feels;?></div>
                                <div>Feels</div>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            <?php }else{?>
                <div class="notFound">No Data Found</div>
            <?php }?>
        </div>
        
        <?php if($page['total_pages']>1){?>
            <div>
                <div class="pull-left" style="width:50px; padding:6px 0px 0px 0px">Pages: </div>
                
                <div class="pull-left" style="width:80%"> 
                    <ul class="pagination nomargin">
                        <?php pagingLinksLI($page, DOCT_URL."patients/lists", 'active'); ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php }?>
    </div>
</div>


<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

function submitSearch(){
	$("#searchForm input, #searchForm select").each(function(){
		ob=$(this);
		if(!ob.val())
			ob.removeAttr('name');
	});
}

$(".sortBtn").click(function() {
	if($(this).hasClass('arrowB'))
		ordr="ASC";
	else
		ordr="DESC";
	
	orderby=$(this).attr("v")+":"+ordr;
	$("#orderBy").val(orderby);
	$("#searchForm").submit();
});
</script>