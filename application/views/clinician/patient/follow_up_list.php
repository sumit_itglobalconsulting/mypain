<?php
	$treatmentOpts=treatmentOptionsArr();
	$treatmentOpts=$treatmentOpts+array('OT'=>'On Opioid Therapy');
	
	function getTreatmentOpt($dtl, $treatOpt){
		$str='<ol>';
		$i=1;
		$str.=$dtl['treatment1']?'<li>'.$treatOpt[1]."</li>":"";
		$str.=$dtl['treatment2']?'<li>'.$treatOpt[2]."</li>":"";
		$str.=$dtl['treatment3']?'<li>'.$treatOpt[3]."</li>":"";
		$str.=$dtl['treatment4']?'<li>'.$treatOpt[4]."</li>":"";
		
		return $str.'</ol>';
	}
	
	$qs=arrayUrlDecode($_GET);
	$followTypes=followUpTypesArr();
	
	$today=date('Y-m-d 00:00:00');
?>

<div class="pageheader">
	<h2><i class="fa fa-user"></i> Follow Up</h2>
</div>


<div class="contentpanel">
	<div style="min-height:800px">
        <div class="table-responsive">
            <form method="get" id="searchForm" onsubmit="return submitSearch()">
                <input type="hidden" name="orderBy" id="orderBy" />
                <h4 class="subtitle mb5">Refine Results</h4>
                <div class="myWhiteBx smallForm">
                    <table class="filterTbl">
                        <tr>
                            <td width="280px">
                                <span class="tooltips" data-placement="top" title="Treatment Option">
                                <?php 
                                    echo form_dropdown("treatOpt", array(''=>'Treatment Option (All)')+$treatmentOpts, $qs['treatOpt'], 
                                    'class="form-control chosen-select"');
                                ?>
                                </span>
                            </td>
                            
                            <td width="250px">
                                <input type="text" name="k" value="<?php echo $qs['k'];?>" placeholder="Search by Name, DOB, Hospital ID" class="form-control" />
                            </td>
                            
                            <td><button type="submit" class="btn btn-primary">Search</button></td>
                        </tr>
                    </table>
                </div>
            </form>
            
            <div class="mb20"></div>
            
            <?php if($result){if($qs['orderBy']) $order=explode(":", $qs['orderBy']);?>
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th width="100px" style="vertical-align:middle">Hospital #</th>
                            <th class="vert-align" style="vertical-align:middle; min-width:250px">
                                Patient Name
                                <?php if($order[0]=="name" && $order[1]=="DESC"){?>
                                    <span class="arrowB sortBtn" v="name"></span>
                                <?php }else{?>
                                    <span class="arrowT sortBtn" v="name"></span>
                                <?php }?>
                            </th>
                            <th width="140px" class="text-center" style="vertical-align:middle">
                                Follow up status
                                <?php if($order[0]=="followUpDate" && $order[1]=="DESC"){?>
                                    <span class="arrowB sortBtn" v="followUpDate"></span>
                                <?php }else{?>
                                    <span class="arrowT sortBtn" v="followUpDate"></span>
                                <?php }?>
                            </th>
                            <th width="200px" style="vertical-align:middle">Treatment Options</th>
                            <th width="100px" class="text-center" style="vertical-align:middle">
                                DOB
                                <?php if($order[0]=="dob" && $order[1]=="DESC"){?>
                                    <span class="arrowB sortBtn" v="dob"></span>
                                <?php }else{?>
                                    <span class="arrowT sortBtn" v="dob"></span>
                                <?php }?>
                            </th>
                            <th colspan="2" width="200px" class="text-center" style="vertical-align:middle">
                                Patient Status
                                <?php if($order[0]=="overallRes" && $order[1]=="DESC"){?>
                                    <span class="arrowB sortBtn" v="overallRes" title="Overall Response"></span>
                                <?php }else{?>
                                    <span class="arrowT sortBtn" v="overallRes" title="Overall Response"></span>
                                <?php }?>
                            </th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php foreach($result as $i=>$dtl){$feels=getFeels($dtl['id']);?>
                        <tr>
                            <td><?php echo $dtl['hospitalId'];?></td>
                            <td>
                                <div class="posRel">
                                    <?php echo $dtl['name'];?>
                                    <div class="mb5"></div>
                                    
                                    <div class="mb5"><span class="text-muted">Follow Up Type:</span><br /><?php echo $followTypes[$dtl['followUpType']];?></div>
                                    
                                    <div>
                                        <a href="<?php echo DOCT_URL."patient/reports/".encode($dtl['id'])."/".encode($doctorId);?>">
                                            <i class="fa fa-bar-chart-o"></i> View Report
                                        </a>
                                    </div>
                                </div>
                            </td>
                            
                            <td>
                                <?php
                                    $dif=floor((strtotime($dtl['followUpDate'])-time())/3600/24)+1;
                                    if($dif>2){
                                        $aclass="greenBg";
                                    }
                                    else if($dif<=2 && $dif>=0){
                                        $aclass="amberBg";
                                    }
                                    else {
                                        $aclass="redBg";
                                    }
                                    
                                    if($dtl['followUpDone']) {
                                        $chk='checked'; 
                                        $aclass="greenBg";
                                    }
                                    else{
                                        $chk='';
                                    }
                                ?>
                                <div class="followUpAlert mb5 <?php echo $aclass;?>"></div>
                                <div class="mb5"><span class="text-muted">Date:</span><br /><?php echo showDate($dtl['followUpDate']);?></div>
                                
                                <div>
                                    <?php 
                                        $fdates=explode(",", $dtl['followedDates']); 
                                        if($fdates){
                                            foreach($fdates as $i=>$fd){
                                                $fdates[$i]=showDate($fd, true);
                                            }
                                        }
                                        if($dtl['followedDates'])
                                            echo form_dropdown('', array(''=>'Followed On')+$fdates, "", 'class="dd1"');
                                    ?>
                                </div>
                            </td>
                            
                            <td>
                                <?php echo getTreatmentOpt($dtl, $treatmentOpts);?>
                            </td>
                            
                            <td align="center"><?php echo showDate($dtl['dob']);?></td>
                
                            <td width="100px" align="center">
                                <?php
                                    if(!$dtl['overallRes'])
                                        $dtl['overallRes']=0;
                                        
                                    if($dtl['overallRes']<30)
                                        $resBg='#ff4b31';
                                    else if($dtl['overallRes']>=30 && $dtl['overallRes']<50)
                                        $resBg='#fbd200';
                                    else
                                        $resBg='#6fba61';
                                    $resBg='background:'.$resBg;
                                ?>
                                <div class="stBx tooltips" data-placement="top" style=" <?php echo $resBg;?> " 
                                title="Overall Response"><?php echo round($dtl['overallRes']);?>%</div>
                                <div>Overall</div>
                            </td>
                            
                            <td width="100px" align="center">
                                <?php
                                    if($feels<=0)
                                        $feelBg='#FF0000';
                                    else if($feels>0 && $feels<=1)
                                        $feelBg='#FFA500';
                                    else if($feels>1 && $feels<=3)
                                        $feelBg='#FDD017';
                                    else
                                        $feelBg='#008000';
                                    $feelBg='background:'.$feelBg;
                                ?>
                                <div class="stBx tooltips" data-placement="top" style=" <?php echo $feelBg;?> " title="Feels"><?php echo $feels;?></div>
                                <div>Feels</div>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            <?php }else{?>
                <div class="notFound">No Data Found.</div>
            <?php }?>
        </div>
        
        <?php if($page['total_pages']>1){?>
            <div>
                <div class="pull-left" style="width:50px; padding:6px 0px 0px 0px">Pages: </div>
                
                <div class="pull-left" style="width:80%"> 
                    <ul class="pagination nomargin">
                        <?php pagingLinksLI($page, DOCT_URL."patients/followUps", 'active'); ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php }?>
    </div>
</div>


<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

function submitSearch(){
	$("#searchForm input, #searchForm select").each(function(){
		ob=$(this);
		if(!ob.val())
			ob.removeAttr('name');
	});
}

$(".sortBtn").click(function() {
	if($(this).hasClass('arrowB'))
		ordr="ASC";
	else
		ordr="DESC";
	
	orderby=$(this).attr("v")+":"+ordr;
	$("#orderBy").val(orderby);
	$("#searchForm").submit();
});
</script>