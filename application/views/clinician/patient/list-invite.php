<?php $qs=arrayUrlDecode($_GET);?>
<div class="pageheader">
	<h2><i class="fa fa-user"></i> Sharing requests send to patients</h2>
</div>


<div class="contentpanel">
	<div style="min-height:800px">
        <div class="table-responsive">
            <form method="get" id="searchForm" onsubmit="return submitSearch()">
                <input type="hidden" name="orderBy" id="orderBy" />
                <h4 class="subtitle mb5">Refine Results</h4>
                <div class="myWhiteBx smallForm">
                    <table class="filterTbl">
                        <tr>
                            <td width="250px">
                                <input type="text" name="k" value="<?php echo $qs['k'];?>" placeholder="Search by Name, Email" class="form-control" />
                            </td>
                            
                            <td><button type="submit" class="btn btn-primary">Search</button></td>
                        </tr>
                    </table>
                </div>
            </form>
            
            <div class="mb20"></div>
            
            <?php if($result){ if($qs['orderBy']) $order=explode(":", $qs['orderBy']);?>
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            
                            <th class="vert-align" style="vertical-align:middle; min-width:200px">
                                Patient Name
                                <?php if($order[0]=="name" && $order[1]=="DESC"){?>
                                    <span class="arrowB sortBtn" v="name"></span>
                                <?php }else{?>
                                    <span class="arrowT sortBtn" v="name"></span>
                                <?php }?>
                            </th>
                            <th width="100px" class="text-center" style="vertical-align:middle">
                                Email
                                <?php if($order[0]=="loginEmail" && $order[1]=="DESC"){?>
                                    <span class="arrowB sortBtn" v="loginEmail"></span>
                                <?php }else{?>
                                    <span class="arrowT sortBtn" v="loginEmail"></span>
                                <?php }?>
                            </th>
                            
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php foreach($result as $i=>$dtl){?>
                        <tr>
                            <td>
                                <?php echo $dtl['name'];?>
                                <div class="mb5"></div>
                            </td>
                            
                            <td align="center"><?php echo $dtl['loginEmail'];?></td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            <?php }else{?>
                <div class="notFound">No Data Found</div>
            <?php }?>
        </div>
        
        <?php if($page['total_pages']>1){?>
            <div>
                <div class="pull-left" style="width:50px; padding:6px 0px 0px 0px">Pages: </div>
                
                <div class="pull-left" style="width:80%"> 
                    <ul class="pagination nomargin">
                        <?php pagingLinksLI($page, DOCT_URL."patients/invitePatients", 'active'); ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php }?>
    </div>
</div>


<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

function submitSearch(){
	$("#searchForm input, #searchForm select").each(function(){
		ob=$(this);
		if(!ob.val())
			ob.removeAttr('name');
	});
}

$(".sortBtn").click(function() {
	if($(this).hasClass('arrowB'))
		ordr="ASC";
	else
		ordr="DESC";
	
	orderby=$(this).attr("v")+":"+ordr;
	$("#orderBy").val(orderby);
	$("#searchForm").submit();
});
</script>
