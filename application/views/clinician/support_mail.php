<?php
	$dtl=loggedUserData();
	if($_POST)
		$dtl=$_POST;
?>
<div class="pageheader">
	<h2><i class="fa fa-envelope-o"></i> Contact My Pain Support</h2>
</div>

<div class="contentpanel">
	<?php if($errors){?>
    	<div class="alert alert-danger">Please fill the fields marked with red color</div>
    <?php }?>
    
	<?php echo getFlash();?>
    
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<h4 class="panel-title">Message details</h4>
        </div>
        
        <div class="panel-body">
        	<form class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
            	<div class="col-md-9">
                    <div class="row mb10">
                        <input name="loginEmail" id="loginEmail" type="text" class="form-control" value="<?php echo h($dtl['loginEmail']);?>" maxlength="100" />
                        <label class="error"><?php echo error_msg($errors,'loginEmail');?></label>
                    </div>
                    
                    <div class="row mb10">
                        <input name="subject" id="subject" type="text" class="form-control" value="<?php echo h($dtl['subject']);?>" placeholder="Subject" maxlength="200" />
                        <label class="error"><?php echo error_msg($errors,'subject');?></label>
                    </div>
                    
                    <div class="row mb10">
                        <textarea name="message" id="message" class="form-control" rows="5" spellcheck="false" maxlength="1000"><?php echo $dtl['message'];?></textarea>
                        <label class="error"><?php echo error_msg($errors,'message');?></label>
                        <div align="right" id="taLengthCountBx">1000</div>
                    </div>
                    
                    <div class="row mb10">
                        <button type="submit" class="btn btn-primary">Send</button>&nbsp;
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
$("#message").on("keypress keyup paste", function(){
	clength=$(this).val().length;
	$("#taLengthCountBx").text(1000-clength);
});
</script>