<?php 
	$treatOpt=treatmentOptionsArr();
?>

<style type="text/css">
.place 	{padding: 0px; width: 512px; height: 465px; position: relative; border: 1px solid #ccc; margin: 0px auto; color:#333}
	
.inn 	{padding: 0px; margin: 0px; background: url(<?php echo URL;?>assets/img/venn.png) no-repeat; width: 447px; height: 340px; margin: 10px auto;
	position: relative; color: #000;}
	
.t1 	{position: absolute; top: 115px; left: 50px;}
.t12	{position: absolute; top: 85px; left: 106px;}
.t13	{position: absolute; top: 225px; left: 120px;}
.t14	{position: absolute; top: 285px; left: 215px;}

.t2 	{position: absolute; top: 30px; left: 135px;}
.t23 	{position: absolute; top: 75px; left: 215px;}
.t24 	{position: absolute; top: 225px; left: 305px;}


.t3 	{position: absolute; top: 30px; left: 295px;}
.t34	{position: absolute; top: 85px; left: 325px;}

.t4		{position: absolute; top: 115px; left: 375px;}

.vleg			{margin:0 5px; font-size:13px}
.tbx			{height:16px; width:18px; display:inline-block; border-radius:2px}
.t1bx			{background:#fe0000}
.t2bx			{background:#019934}
.t3bx			{background:#99cd00}
.t4bx			{background:#0000fe}

.rblueCircle	{background:#0000fe; width:300px; height:300px; border-radius:50%; text-align:center; line-height:300px; color:#fff; margin:20px auto}
</style>

<div class="pageheader">
	<h2><i class="fa fa-home"></i> Dashboard</h2>
</div>

<div class="contentpanel">
	<div class="panel panel-default">
    	<div class="panel-body" style="min-height:800px">
        	<div id="dashAcco">
            	<h3>Treatment Response Status</h3>
                <div class="resAnaBx">
                    <a href="<?php echo DOCT_URL."patients?resType=P";?>" class="boxResPic boxPoorRes posRel">
                        <div>Poor Response</div>
                        <div class="b1BtmText"><?php echo $noOfPoorRes;?> Patient(s)</div>
                    </a>
                    
                    <a href="<?php echo DOCT_URL."patients?resType=A";?>" class="boxResPic boxAvgRes posRel">
                        <div>Average Response</div>
                        <div class="b1BtmText"><?php echo $noOfAvgRes;?> Patient(s)</div>
                    </a>
                    
                    <a href="<?php echo DOCT_URL."patients?resType=G";?>" class="boxResPic boxGoodRes posRel">
                        <div>Good Response</div>
                        <div class="b1BtmText"><?php echo $noOfGoodRes;?> Patient(s)</div>
                    </a>
                    <div class="clear">&nbsp;</div>
                </div>
                
                <h3>Invitation and Response Tracking</h3>
                <div class="resAnaBx1">
                	<a href="<?php echo DOCT_URL."patients?invAna=TI";?>" class="posRel">
                        <div>
                            <img src="<?php echo URL."assets/img/patient-send-invite1.png"?>" align="left" />
                            <h2><?php echo $analysInfo['noOfInv'];?></h2>
                            <div class="b1BtmText">Total Invitations sent</div>
                        </div>
                    </a>
                    
                    <a href="<?php echo DOCT_URL."patients?invAna=IM";?>" class="posRel">
                        <div>
                            <img src="<?php echo URL."assets/img/patient-send-invite.png"?>" align="left" />
                            <h2><?php echo $analysInfo['noOfInvCurMonth'];?></h2>
                            <div class="b1BtmText">Total Invitations sent this month</div>
                        </div>
                    </a>
                    
                    <a href="<?php echo DOCT_URL."patients?invAna=RD";?>" class="posRel">
                        <div>
                            <img src="<?php echo URL."assets/img/response-pending.png"?>" align="left" />
                            <h2><?php echo $analysInfo['noOfResDue'];?></h2>
                            <div class="b1BtmText">Response(s) due but not submitted on time</div>
                        </div>
                    </a>
                    
                    <a href="<?php echo DOCT_URL."patients?invAna=RT";?>" class="posRel">
                        <div>
                            <img src="<?php echo URL."assets/img/response-today.png"?>" align="left" />
                            <h2><?php echo $analysInfo['resToday'];?></h2>
                            <div class="b1BtmText">Response(s) received today</div>
                        </div>
                    </a>
                    
                    <a href="<?php echo DOCT_URL."patients/invitePatients";?>" class="posRel">
                        <div>
                            <img src="<?php echo URL."assets/img/share.png"?>" align="left" />
                            <h2><?php echo $analysInfo['inviSend'];?></h2>
                            <div class="b1BtmText">Sharing requests send</div>
                        </div>
                    </a>
                    
                    <a href="<?php echo DOCT_URL."patients?invAna=IA";?>" class="posRel">
                        <div>
                            <img src="<?php echo URL."assets/img/shared.png"?>" align="left" />
                            <h2><?php echo $analysInfo['inviAppr'];?></h2>
                            <div class="b1BtmText">Sharing requests approved</div>
                        </div>
                    </a>
                </div>
                
                <h3>My Treatment Trends</h3>
                <div>
                	<?php
					?>
                    <div style="padding:0 10px">
                        <div class="row" style="width:870px; margin:0 auto">
                            <div style="float:left; width:520px">
                                <!-- Venn Chart -->
                                <div class="place">
                                    <div class="inn">
                                        <div class="t1" title="Interventional Pain Procedure"><?php echo $NPatients[1]?></div>
                                        <div class="t12" title="Interventional Pain Procedure, Medicines for Managing Chronic Pain">
											<?php echo $NPatients[12]?>
                                        </div>
                                        <div class="t13" title="Interventional Pain Procedure, Physiotherapy"><?php echo $NPatients[13]?></div>
                                        <div class="t14" title="Interventional Pain Procedure, Psychological therapy for chronic pain">
                                        	<?php echo $NPatients[14]?>
                                        </div>
                                        
                                        <div class="t2" title="Medicines for Managing Chronic Pain"><?php echo $NPatients[2]?></div>
                                        <div class="t23" title="Medicines for Managing Chronic Pain, Physiotherapy"><?php echo $NPatients[23]?></div>
                                        <div class="t24" title="Medicines for Managing Chronic Pain, Psychological therapy for chronic pain">
                                        	<?php echo $NPatients[24]?>
                                        </div>
                                        
                                        <div class="t3" title="Physiotherapy"><?php echo $NPatients[3]?></div>
                                        <div class="t34" title="Physiotherapy, Psychological therapy for chronic pain"><?php echo $NPatients[34]?></div>
                                        <div class="t4" title="Psychological therapy for chronic pain"><?php echo $NPatients[4]?></div>
                                    </div>
                                    
                                    <!-- Legends -->
                                    <div class="vleg">
                                		<div class="mb5"> <span class="tbx t1bx"></span> Interventional Pain Procedure </div>
                                        <div class="mb5"> <span class="tbx t2bx"></span> Medicines for Managing Chronic Pain </div>
                                        <div class="mb5"> <span class="tbx t3bx"></span> Physiotherapy </div>
                                        <div class="mb5"> <span class="tbx t4bx"></span> Psychological therapy for chronic pain </div>
                                    </div>
                                </div>
                                <!-- /Venn Chart -->
                            </div>
                            
                            <div style="float:right; width:330px; height:465px; border: 1px solid #ccc">
                            	<div class="mb10" align="center"><h4>Patients on opioids</h4></div>
                                <div align="center"><img src="<?php echo URL;?>assets/img/hazard-tri1.png" width="170px" /></div>
                                <p>&nbsp;</p>
                                <div class="resAnaBx">
                                    <div class="boxResPic boxPoorRes posRel" style="float:none; margin:0 auto">
                                    	<p>&nbsp;</p>
                                        <?php echo $NPatients[5].' ('.$NPatients['opiodPer'].'%)'?><br />Patient(s)
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <h3>CSQ-8</h3>
                <div>
                    <?php
				$fromD=showDate($firstLastDates['firstRes']);
				$toD=showDate($firstLastDates['lastRes']);
			?>
			<form action="javascript:void(0)" id="chartReportForm" onsubmit="chartReport()">
				<div class="form-group smallForm" style="width:550px; margin:0 auto">
                	<div class="row">
                        <label class="col-sm-2 control-label text-left">From Date</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control hasCal" id="fromDate" name="fromDate" value="<?php echo $fromD;?>">
                        </div>
                        
                        <label class="col-sm-2 control-label text-left">To Date</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control hasCal" id="toDate" name="toDate" value="<?php echo $toD;?>">
                        </div>
                        
                        <div class="col-md-2">
                            <button type="submit" id="repSubBtn" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    <div class="mb5"></div>
                    
                    
                    </div>
			</form>
                     <div id="chartBox" class="chartBox" style="min-height:500px;">
                <!-- Chart -->
            </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
/** Chart Report **/
function chartReport(){
	ajax(SITE_URL+"common/csqChartReport/<?php echo USER_ID ;?>", "chartBox", "chartReportForm", "", "POST");
}
chartReport();
</script>
<script type="text/javascript">
$( "#dashAcco" ).accordion({heightStyle: "content", active:0});
</script>