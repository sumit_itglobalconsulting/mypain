<div class="pageheader">
	<h2><i class="fa fa-cog"></i> Reset Password</h2>
</div>

<div class="contentpanel">
	<?php echo getFlash();?>
    
    <div class="panel panel-default">
    	<div class="panel-heading">
        </div>
        
        <div class="panel-body panel-body-nopadding">
        	<form class="form-horizontal form-bordered" method="post">
            	 <div class="form-group">
                    <label class="col-sm-3 control-label myreq">New Password</label>
                    <div class="col-sm-6">
                        <input name="password" id="password" type="password" class="form-control" maxlength="50" />
                        <label class="error"><?php echo error_msg($errors,'password');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Re-Enter Password</label>
                    <div class="col-sm-6">
                    	<input name="repassword" id="repassword" type="password" class="form-control" maxlength="50" />
                        <label class="error"><?php echo error_msg($errors,'repassword');?></label>
                    </div>
                </div>
                
                 <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6 col-sm-offset-3">
                			<button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                    		<button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>