<?php 
	if(!$dtl)
		$dtl=loggedUserData();
	$durationTypes=durationTypeArr();
?>

<div class="pageheader">
	<h2><i class="fa fa-user"></i> My Profile</h2>
</div>

<div class="contentpanel">
	<?php if($errors){?>
    	<div class="alert alert-danger">Please fill the fields marked with red color</div>
    <?php }?>
    
	<?php echo getFlash();?>
    
	<div class="panel panel-default">
    	<div class="panel-heading">
        	<h4 class="panel-title">Profile details</h4>
        </div>
        
        <div class="panel-body panel-body-nopadding">
        	<form class="form-horizontal form-bordered" method="post" onsubmit="return submitForm()" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">First Name</label>
                    <div class="col-sm-6">
                        <input name="firstName" id="firstName" type="text" class="form-control" value="<?php echo h($dtl['firstName']);?>" 
                        maxlength="100" />
                        <label class="error"><?php echo error_msg($errors,'firstName');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Login Email</label>
                    <div class="col-sm-6">
                    	<input name="loginEmail" id="loginEmail" type="text" class="form-control" value="<?php echo h($dtl['loginEmail']);?>" 
                        maxlength="100" />
                        <label class="error" id="loginEmailErr"><?php echo error_msg($errors,'loginEmail');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Contact Number (1)</label>
                    <div class="col-sm-6">
                    	<div>
                            <div class="pull-left" style="width:80px; padding-right:10px">
                                <?php 
									echo form_dropdown('stdCd1', stdCodesArr(), $dtl['stdCd1'], 
									'class="form-control chosen-select" data-placeholder="STD Code"');
								?>
                            </div>
                            
                            <div class="pull-left">
                                <input type="text" class="form-control" name="phone1" id="phone1" value="<?php echo h($dtl['phone1']);?>" 
                                maxlength="10" valid="int" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <label class="error" id="phone1Err"><?php echo error_msg($errors,'phone1');?></label>
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label">Profile Image (100 x 100)</label>
                    <div class="col-sm-6">
                    	<div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="input-append">
                                <div class="uneditable-input">
                                    <i class="glyphicon glyphicon-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileupload-new">Select file</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" name="image" accept="image/*" />
                                </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                        
                        <label class="error"><?php echo error_msg($errors,'image');?></label>
                    </div>
                </div>
                
                
                <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6 col-sm-offset-3">
                			<button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                    		<button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>

<script type="text/javascript">
$(document).ready(function(){
	jQuery(".chosen-select").chosen({'width':'100%', 'white-space':'nowrap', disable_search: true});
});

function submitForm() {
	var error=false;
	if(!checkValidPhone($("#phone1"), $("#phone1Err")))
		error=true;
	
	if(!checkValidEmail($("#loginEmail")))
		error=true;
	
	return !error;
}

function checkValidPhone(obj, errBxOb) {
	phone=$.trim(obj.val());
	errBxOb.text('');
	obj.removeClass('redBdr');
	if(phone){
		if(!isvalidPhone(phone)){
			obj.addClass('redBdr');
			errBxOb.text("Please enter a valid phone number");
			return false;
		}
	}
	return true;
}

$("#loginEmail").blur(function(){
	checkValidEmail($(this));
});

$("#phone1").blur(function(){
	checkValidPhone($(this), $("#"+$(this).attr("id")+"Err"));
});
</script>