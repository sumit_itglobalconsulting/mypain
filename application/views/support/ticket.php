<div class="pageheader">
	<h2><i class="fa fa-question-circle"></i> Create Ticket</h2>
</div>

<div class="contentpanel">
	<?php echo getFlash();?>
    
    <div class="panel panel-default">
    	<div class="panel-heading">
        </div>
        
        <div class="panel-body panel-body-nopadding">
        	<form class="form-horizontal form-bordered" method="post">
            	<input name="id" id="id" type="hidden" class="form-control" maxlength="255" value="<?php echo $dtl['id'];?>" />
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Name</label>
                    <div class="col-sm-6">
                    	<input name="name" id="name" type="text" class="form-control" maxlength="75" value="<?php echo $dtl['name'];?>" />
                        <label class="error"><?php echo error_msg($errors,'name');?></label>
                    </div>
                </div>
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Email Address</label>
                    <div class="col-sm-6">
                        <input name="email" id="email" type="text" class="form-control" maxlength="75" value="<?php echo $dtl['email'];?>" />
                    	<label class="error"><?php echo error_msg($errors,'email');?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label myreq">Select Issue Category</label>
                    <div class="col-sm-2">                        
                        <?php echo form_dropdown("categoryId", array(''=>'--Select--')+$issueHelpCategory, set_value('categoryId', $dtl['categoryId']), 'class="form-control required" id="categoryId"');?>
                        <span class="error"><?php echo error_msg($errors,'categoryId');?></span>                     
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Enter Query</label>
                    <div class="col-sm-6">
                    	<textarea name="query" id="query" rows="10" class="form-control" ><?php echo $dtl['query'];?></textarea>
                        <label class="error"><?php echo error_msg($errors,'query');?></label>
                    </div>
                </div>
                
                 <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6 col-sm-offset-3">
                			<a href="<?php echo URL."faq/lists"?>" class="btn btn-default">Cancel</a>&nbsp;
                            <button type="submit" class="btn btn-primary">Submit</button>
                    		
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>