<div class="pageheader">
	<h2><i class="fa fa-question-circle"></i> Check Ticket Status</h2>
</div>

<div class="contentpanel">
	<?php echo getFlash();?>
    
    <div class="panel panel-default">
    	<div class="panel-heading">
        </div>
        
        <div class="panel-body panel-body-nopadding">
        <label class="error" style="padding-left: 20px;"><?php echo error_msg($errors,'notfound');?></label>
        	<form class="form-horizontal form-bordered" method="get">
            	<input name="id" id="id" type="hidden" class="form-control" maxlength="255" value="<?php echo $dtl['id'];?>" />
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Ticket Number</label>
                    <div class="col-sm-6">
                    	<input name="ticketNo" id="ticketNo" type="text" class="form-control" maxlength="75" value="<?php echo $dtl['ticketNo'];?>" />
                        <label class="error"><?php echo error_msg($errors,'ticketNo');?></label>
                    </div>
                </div>
                <div class="form-group">
                	<label class="col-sm-3 control-label myreq">Email Address</label>
                    <div class="col-sm-6">
                        <input name="email" id="email" type="text" class="form-control" maxlength="75" value="<?php echo $dtl['email'];?>" />
                    	<label class="error"><?php echo error_msg($errors,'email');?></label>
                    </div>
                </div>
                
                 <!-- Submit Btn -->
                <div class="panel-footer">
                	<div class="row">
                    	<div class="col-sm-6 col-sm-offset-3">
                			<a href="<?php echo URL."support/lists"?>" class="btn btn-default">Cancel</a>&nbsp;
                            <button type="submit" class="btn btn-primary">Submit</button>
                    		
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php set_error_class($errors, 'redBdr');?>