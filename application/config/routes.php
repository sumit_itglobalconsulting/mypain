<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

$route['default_controller']		= "user/login";
$route['404_override'] 			= "common/error404";

/** Super Admin **/
$route['superadmin/myaccount']		= "superadmin/user/profile";
$route['superadmin/changePassword']	= "superadmin/user/changePassword";

/** Admin (Hospital) **/
$route['admin/myaccount']			= "admin/user/profile";
$route['admin/changePassword']		= "admin/user/changePassword";

/** Clinician (Doctor) **/
$route['clinician/myaccount']		= "clinician/user/profile";
$route['clinician/changePassword']	= "clinician/user/changePassword";

/** Patient **/
$route['patient/myaccount']			= "patient/user/profile";
$route['patient/changePassword']	= "patient/user/changePassword";

/** Report **/
$route['patient/user/reports']					="common/reports";
$route['clinician/patient/reports/(:any)']		="common/reports/$1";
$route['admin/patient/reports/(:any)/(:any)']	="common/reports/$1/$2";



/** Cms **/
$route['page/(:any)']		="page/index/$1";
