<?php
class Support extends CI_Controller {
    var $logUserId;
	function __construct() {
		parent::__construct();
        $this->load->model("Faq_model", "faq");
        $this->load->model("Help_category_model", "help");
        $this->load->model("Ticket_model", "ticket");
        $this->load->model("Common_model", "common");
       
	}
	
	function index() {
	   redirect(URL."support/lists");
	}
    
    function lists($p=1) {
	    $loggedDtl=loggedUserData();
        $userType = $loggedDt? $loggedDt['type'] : '';
        if($userType){
            if($userType == 'T'){
                $data=$this->faq->lists($p, '20');
            } else {
                $data=$this->faq->lists($p, '20',$userType);
            }
        } else {
            if($_GET && $_GET['ticket'] && $_GET['email']) {
                $data=$this->faq->lists($p, '20','', decode($_GET['email']),decode($_GET['ticket']),true);
                
                $data['email'] = decode($_GET['email']);
            } else {
                $data=$this->faq->lists($p, '20');
            }
        }
        
        $data['category'] = ($_GET)? $_GET['category']: '';
        $data['search'] = ($_GET)? $_GET['search']: '';
        $data['userType'] = $userType;
     // echo '<pre>'; print_r($data);exit;
        $data['page_title']='List FAQ | My Pain';
		$this->load->getView("support/list",$data);
	}
    
    
    function ticketValidateForm($data) {
		$errors=array();
        $errors['categoryId']	=!$data['categoryId']?'Please select category':'';
		$errors['name']	=!$data['name']?'Please emter name':'';
        $errors['email']	=!$data['email']?'Please enter email':'';
        if($data['email']){
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
              $errors['email'] = "Invalid email format"; 
            }
        }
        
        $errors['query']	=!$data['query']?'Please enter query':'';
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
    
    function ticketSave() {
		$data=arrayTrim($_POST);
		$err=$this->ticketValidateForm($data);
        
		if(!$err) {
		    $id=$this->ticket->save($data,'mp_tickets');
			if($id) {
			    $ticketRow = $this->ticket->ticketDetails($id);
                $msg=$this->load->view("email_temp/ticket_email", array('dtl'=>$ticketRow), true);
              
                sendMail($ticketRow['email'],'','','Ticket Updates',$msg);
               
                $ticketUserList = $this->common->listTicketUser();
                if(count($ticketUserList)){
                    foreach($ticketUserList as $value){
                        sendMail($value['loginEmail'],'','','New Ticket Created','New ticket has been created by "'.$ticketRow['name'].'"');
                    }
                } 
                 
				setFlash('<div class="msg1" hide="20000">Saved successfully</div>');
				redirect(URL."support/lists");
			}
		}
		return $err;
	}
    
    function ticket() {
	   if($_POST)
			$data['errors']=$this->ticketSave();
	
		$data['dtl']=$_POST;	
        
        $issueHelpCategory = $this->help->lists('','',true);
		$data['issueHelpCategory'] = multiArrToKeyValue($issueHelpCategory, 'id', 'name');
        
        if(!$_POST){
            $loggedUserData = loggedUserData();
            $data['dtl']['name'] = ($loggedUserData)? $loggedUserData['firstName'].' '.$loggedUserData['lastName'] : '';
            $data['dtl']['email'] = ($loggedUserData)? $loggedUserData['loginEmail'] : '';
        }
        
		$data['page_title']='Create Query | My Pain';
        
		$this->load->getView("support/ticket",$data);
	}
    
    function checkTicket() {
	   if($_GET) {
          $data=arrayTrim($_GET);
		  $data['errors']=$this->ticketStatus($data);
	   
		  $data['dtl']=$_GET;	
        } 
		$data['page_title']='Create Query | My Pain';
        
		$this->load->getView("support/checkTicket",$data);
	}
    
    function saveReply(){
        if($_POST){
            $ticketId =  $_POST['ticketId']; 
            $name =  $_POST['name'];
            $email =  $_POST['email'];
            $reply =  $_POST['message'];
            
            
            $data['ticketId'] = $ticketId;  
            $data['name'] = $name;  
            $data['email'] = $email;  
            $data['reply'] = $reply;   
            $data['created']=$data['updated']=currentDate();
			$id=$this->pdb->insert('mp_ticket_reply', $data);
            if($id){
                $ticketRow = $this->ticket->ticketDetails($ticketId);
                $data['ticketNo'] = $ticketNo;
              /*  $msg=$this->load->view("email_temp/ticket_email", array('dtl'=>$ticketRow), true);
                  
                sendMail($ticketRow['email'],'','','Ticket Updates',$msg);
              */  
                die(true);
            }
        } 
        else {
            die(false);
        } 
        
    }
    
    function ticketStatus($data) {
		$errors=array();
        $errors['ticketNo']	=!$data['ticketNo']?'Please enter ticketNo':'';
        $errors['email']	=!$data['email']?'Please enter email':'';
        if($data['email']){
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
              $errors['email'] = "Invalid email format"; 
            }
        }
        
        if(isArrayEmpty($errors) && $ticketRow = $this->ticket->ticketCheck($data['ticketNo'], $data['email'])){
            redirect(URL.'support/lists?ticket='.encode($data['ticketNo']).'&email='.encode($data['email']));
        } else {
            $errors['notfound'] = 'Invalid Details';
        }
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
}
