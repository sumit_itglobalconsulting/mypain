<?php
class Report extends ParentSuperAdminController {
	function __construct() {
		parent::__construct();
		$this->load->model('superadmin/Report_model','report');
	}
	
	function index() {
		$data['page_title']="Report | My Pain";
		$data['forms']=$this->report->listForms();
		$data['sedates']=$this->report->firstLastDates();
		
		$this->load->getView("superadmin/report/report", $data);
	}
	
	function getReport() {
		$inf=arrayTrim($_POST);
		$data['noOfUsers']=$this->report->getNoOfFormUsers($inf['formId']);
		$data['invitations']=$this->report->formInvAnalysis($inf);
		
		$data['noOfPoorRes']=$this->report->resWisePatients('P');
		$data['noOfAvgRes']=$this->report->resWisePatients('A');
		$data['noOfGoodRes']=$this->report->resWisePatients('G');
		
		$data['goalAnalys']=$this->report->goalAnalys($inf['formId']);
		$this->load->view("superadmin/report/report-area", $data);
	}
}
