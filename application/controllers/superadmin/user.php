<?php
class User extends ParentSuperAdminController {
	function __construct() {
		parent::__construct();
		$this->load->model('superadmin/User_model','user');
	}
	
	/** **/
	function profile() {
		if($_POST)	{
			$data=arrayTrim($_POST);
			$dtl=$this->loggedData();
			$data['id']=$dtl['id'];
			$err=$this->validateProfile($data);
			if(!$err){
				if($_FILES['image']['name']){
					$data['image']=$this->uploadUserImage($data);
				}
				
				$status=$this->user->updateProfile($data);
				if($status){
					$userDtl=$this->common->userDetail($data['id']);
					setSession(USR_SESSION_NAME, $userDtl);
					
					setFlash('<div class="alert alert-success" hide="20000">Saved successfully.</div>');
					redirect(SADM_URL."myaccount");
				}
			}
			$data['errors']=$err;
			$data['dtl']=$_POST;
			
			$data['dtl']['phone1']=$_POST['stdCode1'].$_POST['phone1'];
			$data['dtl']['phone2']=$_POST['stdCode2'].$_POST['phone2'];
		}
		
		$data['page_title']='Profile';
		$this->load->getView('superadmin/profile',$data);
	}
	
	function validateProfile($data) {
		$errors=array();
		$errors['firstName']	=!$data['firstName']?'Please enter first name':'';
		$errors['lastName']		=!$data['lastName']?'Please enter last name':'';
		$errors['loginEmail']	=!$data['loginEmail']?"Please enter login email":'';
		$errors['contactEmail']	=!$data['contactEmail']?"Please enter contact email":'';
		$errors['supportEmail']	=!$data['supportEmail']?"Please enter support email":'';
		
		if($data['loginEmail']){
			if(!isEmail($data['loginEmail']))
				$errors['loginEmail']="Invalid Email-ID";
			else if($this->common->isEmailExists($data['loginEmail'], $data['id']))
				$errors['loginEmail']="This Email-ID already exists";
		}
		
		$errors['phone1']		=!$data['phone1']?"Please enter contact number":'';
		$errors['address']		=!$data['address']?"Please enter address":'';
		$errors['city']			=!$data['city']?"Please enter city":'';
		$errors['zipcode']		=!$data['zipcode']?"Please enter zipcode":'';
		$errors['state']		=!$data['state']?"Please enter state":'';
		$errors['country']		=!$data['country']?"Please enter country":'';
		
		if($filename=$_FILES['image']['name']){
			if(!checkImageExt($filename))
				$errors['image']='Invalid image (Only .jpg, .jpeg, .png and .gif file allowed)';
		}
			
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	
	function changePassword() {
		if($_POST){
			$msg=$this->validateChangePassword($_POST);
			if(!$msg['err']) {
				$d=$this->loggedData();
				$_POST['id']=$d['id'];
				$status=$this->user->changePassword($_POST);
				if($status){
					$dtl=$this->common->userDetail($d['id']);
					setSession(USR_SESSION_NAME, $dtl);
					
					$dtl['newpass']=$_POST['password'];
					$msg=$this->load->view("email_temp/change_password", $dtl, true);
					sendMail($dtl['loginEmail'], "", "", "Password Change", $msg);
					
					setFlash('<div class="alert alert-success" hide="20000">Password changed successfully.</div>');
					redirect(SADM_URL."changePassword");
				}
			}
			else
				$data['errors']=$msg['errors'];
		}
		
		$data['page_title']='Change Password';
		$this->load->getView('superadmin/change_password',$data);
	}
	
	function validateChangePassword($data) {
		$err=false;
		if(!trim($data['password'])) {
			$inf['errors']['password']='Please enter password!';
			$err=true;
		}
		
		if(!trim($data['repassword'])) {
			$inf['errors']['repassword']='Please re-enter password!';
			$err=true;
		}
		
		if(trim($data['password']) && trim($data['repassword']) && $data['password']!=$data['repassword']){
			$inf['errors']['repassword']='Password mismatch!';
			$err=true;
		}
		
		$inf['err']=$err;
		return $inf;
	}
	
	function dashboard() {
		$data['users']=$this->user->countUsers();
		$data['page_title']="Dashboard";
		$this->load->getView("superadmin/user/dashboard", $data);
	}
	
	/*****************Start Nitin Code*******************/
	
	function countUserGroupBy()
	{
		$data['users']=$this->user->countUserGroupBy();
		echo json_encode($data['users']);
	}
	
	function registeredUser($plan="", $p=1)
	{
		$data=$this->user->getUserByPlan($plan, $p, 30);
		$data['page_title']="View Registered User";
		$data['plan']=$plan;
		if($plan)
			$data['planName']=$this->pdb->singleVal("mp_plans", "id='$plan'", "planName");
		else
			$data['planName']="All";
			
		$this->load->getView("superadmin/user/registeredUser", $data);
	}
	
	function editUser($userId)
	{
		if($userId){
			$userId=decode($userId);
			if($_POST)	{
				$data=arrayTrim($_POST);
				$data['id'] = $userId;
				$err=$this->validateReg($data);
				if(!$err){
					$data['username']=$data['loginEmail'];
					if($_FILES['image']['name'])
						$data['image']=$this->uploadUserImage($data);
					
					$status=$this->user->updateUser($data);
					if($status){
						setFlash('<div class="alert alert-success" hide="20000">Saved successfully.</div>');
						redirect(SADM_URL."user/editUser/".encode($userId));
					}
				}else{
					$data['errors'] = $err;
				}
			}
		
			$data['dtl'] = $this->common->userDetail($userId);
			//pr($data); die();
		}
		$this->load->getView("superadmin/user/editUser", $data);
	}
	
	
	function validateReg($data) {
		$errors=array();
		$errors['branchName']		=!$data['branchName']?"Please enter branch name":'';
		$errors['firstName']		=!$data['firstName']?"Please enter first name":'';
		$errors['lastName']			=!$data['lastName']?"Please enter last name":'';
		$errors['loginEmail']		=!$data['loginEmail']?"Please enter login email":'';
		$errors['contactEmail']		=!$data['contactEmail']?"Please enter administrator's email":'';
		
		if($data['loginEmail']){
			if(!isEmail($data['loginEmail']))
				$errors['loginEmail']="Invalid Email-ID";
			else if($this->common->isEmailExists($data['loginEmail'], $data['id']))
				$errors['loginEmail']="This Email-ID already exists";
		}
		
		$errors['phone1']		=!$data['phone1']?"Please enter contact number":'';
		$errors['address']		=!$data['address']?"Please enter address":'';
		$errors['city']			=!$data['city']?"Please enter city":'';
		$errors['zipcode']		=!$data['zipcode']?"Please enter zipcode":'';
		$errors['state']		=!$data['state']?"Please enter state":'';
		$errors['country']		=!$data['country']?"Please enter country":'';
		
		if($filename=$_FILES['image']['name']){
			if(!checkImageExt($filename))
				$errors['image']='Invalid image (Only .jpg, .jpeg, .png and .gif file allowed)';
		}
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	function uploadUserImage($data) {
		$this->load->library("Image");
		$dir="assets/uploads/user_images/";
		$old=$this->pdb->singleVal("mp_users", "id='{$data['id']}'", "image");
		delFile($dir.$old);
		$filename=renameFileIfExist($dir, $_FILES['image']['name']);
		$this->image->resize($_FILES['image']['tmp_name'], $dir.$filename, 120);
		return $filename;
	}
	
	/*****************End Nitin Code*******************/
	
	
	/** Misc **/
	function misc(){
		if(isset($_POST)){
			$data=arrayTrim($_POST);
			$this->pdb->update("mp_misc", $data, "id=1");
			setFlash('<div class="alert alert-success" hide="10000">Saved successfully</div>');
		}
		
		$data['dtl']=$this->common->misc();
		$this->load->getView("superadmin/user/misc", $data);
	}
}
