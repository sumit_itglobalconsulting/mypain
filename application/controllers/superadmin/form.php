<?php
class Form extends ParentSuperAdminController {
	function __construct() {
		parent::__construct();
		$this->load->model("superadmin/Form_model", "form");
	}
	
	function saveQ(){
		$q=file_get_contents("qq.txt");
		$q=explode("#", $q);
		unset($q[0]);
		
		$qs=array();
		$qs=arrayTrim($q);
		
		foreach($qs as $i=>$a){
			$inf['formId']=19;
			$inf['type']=2;
			$inf['scaleId']=36;
			
			$b=explode("~", $a);
			$inf['ques']=trim($b[0]);
			
			$c=explode("|", $b[1]);
			
			//$id=$this->pdb->insert("mp_form_question", $inf);
			foreach($c as $opt){
				$info['quesId']=$id;
				$info['optionName']=trim($opt);
				$info['score']=0;
				$info['caption']='';
				//$this->pdb->insert("mp_form_ques_options", $info);
			}
		}
			
		die;
	}
	
	/** Form **/
	function index() {
		$this->lists();
	}
	
	function lists() {
		$data['list']=$this->form->listForms();
		$data['cats']=$this->form->cats();
		$data['page_title']='Forms | My Pain';
		$this->load->getView("superadmin/form/list", $data);
	}
	
	function save() {
		$data=arrayTrim($_POST);
		$err=$this->validateForm($data);
		if(!$err) {
			if($_FILES['image']['name'])
				$data['image']=$this->uploadFormImage($data);
			
			if($_FILES['pdf']['name']){
				$data['pdf']=$_FILES['pdf']['name'];
				move_uploaded_file($_FILES['pdf']['tmp_name'], "assets/uploads/form_pdf/".$data['pdf']);
			}
			
			if($_FILES['toolkit']['name']){
				$data['toolkit']=time().$_FILES['toolkit']['name'];
				move_uploaded_file($_FILES['toolkit']['tmp_name'], "assets/uploads/form_pdf/".$data['toolkit']);
			}
			
			if($_FILES['supportingStatement']['name']){
				$data['supportingStatement']=time().$_FILES['supportingStatement']['name'];
				move_uploaded_file($_FILES['supportingStatement']['tmp_name'], "assets/uploads/form_pdf/".$data['supportingStatement']);
			}
			
			if($_FILES['userGuide']['name']){
				$data['userGuide']=time().$_FILES['userGuide']['name'];
				move_uploaded_file($_FILES['userGuide']['tmp_name'], "assets/uploads/form_pdf/".$data['userGuide']);
			}
			
			$id=$this->form->save($data);
			if($id) {
				setFlash('<div class="alert alert-success" hide="20000">Saved successfully</div>');
				redirect(SADM_URL."form/edit/".encode($id));
			}
		}
		return $err;
	}
	
	function validateForm($data) {
		$errors=array();
		$errors['formName']	=!$data['formName']?'Please enter form name':'';
		
		if(!$data['id']){
			$errors['image']	=!$_FILES['image']['name']?'Please upload form image':'';
			if($filename=$_FILES['image']['name']){
				if(!checkImageExt($filename))
					$errors['image']='Invalid image (Only .jpg, .jpeg, .png and .gif file allowed)';
			}
		}
		
		if($_FILES['pdf']['name']){
			if(strtolower(getExt($_FILES['pdf']['name']))!='pdf'){
				$errors['pdf']="Only .pdf file is allowed!";
			}
		}
		
		if($_FILES['userGuide']['name']){
			if(strtolower(getExt($_FILES['userGuide']['name']))!='pdf'){
				$errors['pdf']="Only .pdf file is allowed!";
			}
		}
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	function uploadFormImage($data) {
		$this->load->library("Image");
		$dir="assets/uploads/form_images/";
		$old=$this->pdb->singleVal("mp_forms", "id='{$data['id']}'", "image");
		delFile($dir.$old);
		$filename=renameFileIfExist($dir, $_FILES['image']['name']);
		$this->image->resize($_FILES['image']['tmp_name'], $dir.$filename, 180);
		return $filename;
	}
	
	function add() {
		if($_POST)
			$data['errors']=$this->save();
		
		$data['dtl']=$_POST;	
		$data['page_title']='Add Form | My Pain';
		$this->load->getView("superadmin/form/add", $data);
	}
	
	function edit($id=0) {
		if(!$id)
			show_404();
			
		$id=decode($id);
		
		if($_POST)
			$data['errors']=$this->save();
		
		if(!$data['dtl']=$this->form->detail($id))
			show_404();
		
		$data['page_title']='Edit Form | My Pain';
		$this->load->getView("superadmin/form/add", $data);
	}
	
	
	/** Form Questions **/
	function questions($formId=0) {
		if(!$formId)
			show_404();
			
		$formId=decode($formId);
		if(!$data['formDtl']=$this->form->detail($formId))
			show_404();
			
		$data['list']=$this->form->listQues($formId);
		
		$data['page_title']='Form Questions | My Pain';
		$this->load->getView("superadmin/form/ques_list", $data);
	}
	
	function saveQues($formId) {
		$data=arrayTrim($_POST);
		
		$data['formId']=$formId;
		$err=$this->validateQues($data);
		if(!$err) {
			$id=$this->form->saveQues($data);
			if($id) {
				setFlash('<div class="message1" hide="20000" center="true">Saved successfully</div>');
				redirect(SADM_URL."form/editQues/".encode($formId)."/".encode($id));
			}
		}
		return $err;
	}
	
	function validateQues($data) {
		$errors=array();
		$errors['ques']	=!$data['ques']?'Please enter question':'';
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	function addQues($formId=0) {
		if(!$formId)
			show_404();
		
		$formId=decode($formId);
		if(!$data['formDtl']=$this->form->detail($formId))
			show_404();
			
		if($_POST)
			$data['errors']=$this->saveQues($formId);
		
		$data['quesGroup']=$this->form->listQuesGroup();
		
		$data['page_title']='Add Question | My Pain';
		$this->load->getView("superadmin/form/ques_add", $data);
	}
	
	function editQues($formId=0, $id=0) {
		if(!$id || !$formId)
			show_404();
			
		$id=decode($id);
		$formId=decode($formId);
		
		if($_POST)
			$data['errors']=$this->saveQues($formId);
		
		if(!$data['dtl']=$this->form->quesDetail($id))
			show_404();
			
		if(!$data['formDtl']=$this->form->detail($formId))
			show_404();
		
		$data['quesGroup']=$this->form->listQuesGroup();
		
		$data['page_title']='Edit Form | My Pain';
		$this->load->getView("superadmin/form/ques_add", $data);
	}
	
	function quesOptions($quesId=0, $id=0, $action='Edit') {
		if($quesId){
			$quesId=decode($quesId);
			if($_POST){
				$_POST['quesId']=$quesId;
				$this->form->saveQuesOption(arrayTrim($_POST));
			}
			
			if($id){
				$id=decode($id);
				if($action=='Edit')
					$data['dtl']=$this->pdb->singleRow("mp_form_ques_options", "id=$id");
				else if($action=='Delete')
					$data['dtl']=$this->pdb->delete("mp_form_ques_options", "id=$id");
			}
			
			$data['quesId']=$quesId;
			$data['quesDtl']=$this->form->quesDetail($quesId);
			$data['list']=$this->form->listQuesOptions($quesId);
		}
		
		$this->load->view("superadmin/form/ques_options", $data);
	}
	
	function deleteQues($formId, $id) {
		$id=decode($id);
		$this->pdb->delete("mp_form_question", "id=$id");
		$this->pdb->delete("mp_form_ques_options", "quesId=$id");
		
		redirect(SADM_URL."form/questions/".$formId);
	}
	
	/** Plans and Pricing **/
	function plans() {
		$data['list']=$this->form->listPlans();
		$data['page_title']='Plans &amp; Pricing | My Pain';
		$this->load->getView("superadmin/plan/list", $data);
	}
	
	function savePlan() {
		$data=arrayTrim($_POST);
		$err=$this->validatePlan($data);
		if(!$err) {
			$id=$this->form->savePlan($data);
			if($id) {
				setFlash('<div class="alert alert-success" hide="20000">Saved successfully</div>');
				redirect(SADM_URL."form/editPlan/".encode($id));
			}
		}
		return $err;
	}
	
	function validatePlan($data) {
		$errors=array();
		$errors['planName']	=!$data['planName']?'Please enter plan name':'';
		if(isset($data['price']))
			$errors['price']	=!$data['price']?'Please enter plan cost':'';
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	function addPlan() {
		if($_POST)
			$data['errors']=$this->savePlan();
		
		$data['dtl']=$_POST;
		$data['page_title']='Add Plan | My Pain';
		$this->load->getView("superadmin/plan/add", $data);
	}
	
	function editPlan($id=0) {
		if(!$id)
			show_404();
			
		$id=decode($id);
		
		if($_POST)
			$data['errors']=$this->savePlan();
		
		if(!$data['dtl']=$this->form->planDetail($id))
			show_404();
		
		$data['page_title']='Edit Plan | My Pain';
		$this->load->getView("superadmin/plan/add", $data);
	}
	
	/** Forms Categories **/
	function cats() {
		$data['list']=$this->form->cats();
		$data['page_title']='Form Categories | My Pain';
		$this->load->getView("superadmin/form/cats", $data);
	}
	
	function addCat($id=0) {
		if($id){
			$data['dtl']=$this->pdb->singleRow("mp_form_cats", "id='$id'");
		}
		
		if($inf=arrayTrim($_POST)){
			if($inf['id']){
				$this->pdb->update("mp_form_cats", $inf, "id='{$inf['id']}'");
			}
			else{
				$this->pdb->insert("mp_form_cats", $inf);
			}
			
			setFlash('<div class="alert alert-success">Saved successfully</div>');
		}
		
		$this->load->view("superadmin/form/cat_add", $data);
	}
	
	function deleteCat($id=0) {
		if($id){
			if($this->pdb->delete("mp_form_cats", "id=:id", array(':id'=>$id))){
				setFlash('<div class="alert alert-success">Deleted successfully</div>');
			}
		}
		redirect(SADM_URL."form/cats");
	}
	
	function setCat($formId=0) {
		if($_POST){
			$this->pdb->delete("mp_form_categories", "formId='{$_POST['formId']}'");
			if($_POST['cat']){
				foreach($_POST['cat'] as $catId=>$v){
					$inf['formId']=$_POST['formId'];
					$inf['catId']=$catId;
					$this->pdb->insert("mp_form_categories", $inf);
				}
			}
		}
		
		$data['cats']=$this->form->cats();
		$data['formDtl']=$this->form->detail($formId);
		$this->load->view("superadmin/form/set_cat", $data);
	}
}
