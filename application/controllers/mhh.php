<?php
class Mhh extends CI_Controller {
    //var $logUserId;
	function __construct() {
		parent::__construct();
        /*$this->load->model("Faq_model", "faq");
        $this->load->model("Help_category_model", "help");
        $this->load->model("Ticket_model", "ticket");
        $this->load->model("Common_model", "common");
        
        
        if(loggedUserData()){
			$loggedDtl=loggedUserData();
            $this->logUserId = $loggedDtl['id'];
        }*/
	}
	
	function test(){
		echo 'Sat';
	}
	
	function index() {
	   redirect(URL."faq/lists");
	}
    
    function lists($p=1) {
	    $loggedDtl=loggedUserData();
        $userType = $loggedDtl['type'];
        if($userType == 'T'){
            $data=$this->faq->lists($p, '20');
        } else {
            $data=$this->faq->lists($p, '20',$userType);
        }
        
        $data['category'] = ($_GET)? $_GET['category']: '';
        $data['search'] = ($_GET)? $_GET['search']: '';
        $data['userType'] = $userType;
     
        $data['page_title']='List FAQ | My Pain';
		$this->load->getView("faq/list",$data);
	}
    
    function save() {
		$data=arrayTrim($_POST);
		$err=$this->validateForm($data);
        
		if(!$err) {
		    $data['userId'] = $this->logUserId;  	
            if(!isset($data['visible'])){
                $data['visible'] = '1';
            }
			$id=$this->faq->save($data,'mp_faq');
			if($id) {
				setFlash('<div class="msg1" hide="20000">Saved successfully</div>');
				redirect(URL."faq/lists");
			}
		}
		return $err;
	}
    
    function add() {
	   if($_POST)
			$data['errors']=$this->save();
	
		$data['dtl']=$_POST;	
		$data['page_title']='Add FAQ | My Pain';
        
		$this->load->getView("faq/add",$data);
	}
    
    function edit($id) {
	   if(!$id)
			show_404();
        
        $id=decode($id);
        
       if($_POST)
			$data['errors']=$this->save();
	    
        $row = $this->faq->faqDetails($id);
        
		$data['dtl']= ($_POST)? $_POST : $row;	
		$data['page_title']='Add FAQ | My Pain';
        
		$this->load->getView("faq/add",$data);
	}
    
    
    function validateForm($data) {
		$errors=array();
		$errors['category']	=!$data['category']?'Please select category':'';
        $errors['question']	=!$data['question']?'Please enter question':'';
        $errors['answer']	=!$data['answer']?'Please enter answer':'';
        $errors['keyword']	=!$data['keyword']?'Please enter keyword':'';
		
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
    
    
    
    function hide($id) {
	    if(!$id)
			show_404();
        
        $id=decode($id);
        
        if($row = $this->faq->faqDetails($id)){
            $data['id'] = $id;
            $data['visible'] = ($row['visible'])? '0' : '1';
            $re=$this->faq->save($data,'mp_faq');
            if($re){
                setFlash('<div class="alert alert-success" hide="20000">FAQ updated successfully</div>');
            } 
        }    
        redirect(URL."faq/lists");
	}
    
    function delete($id) {
	    if(!$id)
			show_404();
        
        $id=decode($id);
        
        if($row = $this->faq->faqDetails($id)){
            $re = $this->faq->deleteFaq($id);    
            if($re){
                setFlash('<div class="alert alert-success" hide="20000">FAQ removed successfully</div>');
            }        
        }    
        redirect(URL."faq/lists");
	}
    
    
    
    
    
    
    function saveHelpCategory() {
		$data=arrayTrim($_POST);
        
        $err=array();
        if(!$data['name']){
		  $err['name']	= 'Please select category';
        }
        
		if(!count($err)) {
		    $id=$this->help->save($data,'mp_help_category');
			if($id) {
				setFlash('<div class="msg1" hide="20000">Saved successfully</div>');
				redirect(URL."faq/listsHelp");
			}
		}
		return $err;
	}
    
    
    function listsHelp($p=1) {
	    
        $data=$this->help->lists($p, '20');
        
        $data['page_title']='List FAQ | My Pain';
		$this->load->getView("faq/helpList",$data);
	}
    
    function addHelpCategory() {
	   if($_POST)
			$data['errors']=$this->saveHelpCategory();
	
		$data['dtl']=$_POST;	
		$data['page_title']='Add Help Category | My Pain';
        
		$this->load->getView("faq/addHelpCategory",$data);
	}
    
    function editHelpCategory($id) {
	   if(!$id)
			show_404();
        
        $id=decode($id);
        
       if($_POST)
			$data['errors']=$this->saveHelpCategory();
	    
        $row = $this->help->helpCategoryDetails($id);
        
		$data['dtl']= ($_POST)? $_POST : $row;	
		$data['page_title']='Add  Help Category | My Pain';
        
		$this->load->getView("faq/addHelpCategory",$data);
	}
    
    function deleteHelpCategory($id) {
	    if(!$id)
			show_404();
        
        $id=decode($id);
        
        if($row = $this->help->helpCategoryDetails($id)){
            $re = $this->help->deleteHelpCategory($id);    
            if($re){
                setFlash('<div class="alert alert-success" hide="20000">Removed successfully</div>');
            }        
        }    
        redirect(URL."faq/listsHelp");
	}
    
    function ticketValidateForm($data) {
		$errors=array();
        $errors['categoryId']	=!$data['categoryId']?'Please select category':'';
		$errors['name']	=!$data['name']?'Please enter name':'';
        $errors['email']	=!$data['email']?'Please enter email':'';
        if($data['email']){
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
              $errors['email'] = "Invalid email format"; 
            }
        }
        
        $errors['query']	=!$data['query']?'Please enter query':'';
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
    
    function ticketSave() {
		$data=arrayTrim($_POST);
		$err=$this->ticketValidateForm($data);
        
		if(!$err) {
		    $id=$this->ticket->save($data,'mp_tickets');
			if($id) {
			    $ticketRow = $this->ticket->ticketDetails($id);
                $msg=$this->load->view("email_temp/ticket_email", array('dtl'=>$ticketRow), true);
                sendMail($email,'','','Ticket Updates',$msg);
                
                $ticketUserList = $this->common->listTicketUser();
                if(count($ticketUserList)){
                    foreach($ticketUserList as $value){
                        sendMail($value['loginEmail'],'','','New Ticket Created','New ticket has been created by "'.$ticketRow['name'].'"');
                    }
                } 
				setFlash('<div class="msg1" hide="20000">Saved successfully</div>');
				redirect(URL."faq/lists");
			}
		}
		return $err;
	}
    
    function ticket() {
	   if($_POST)
			$data['errors']=$this->ticketSave();
	
		$data['dtl']=$_POST;	
        
        $issueHelpCategory = $this->help->lists('','',true);
		$data['issueHelpCategory'] = multiArrToKeyValue($issueHelpCategory, 'id', 'name');
        
        if(!$_POST){
            $loggedUserData = loggedUserData();
            $data['dtl']['name'] = ($loggedUserData)? $loggedUserData['firstName'].' '.$loggedUserData['lastName'] : '';
            $data['dtl']['email'] = ($loggedUserData)? $loggedUserData['loginEmail'] : '';
        }
        
		$data['page_title']='Create Query | My Pain';
        
		$this->load->getView("faq/ticket",$data);
	}
    
    function saveReply(){
        if($_POST){
            $ticketId =  $_POST['ticketId']; 
            $name =  $_POST['name'];
            $email =  $_POST['email'];
            $reply =  $_POST['message'];
            
            
            $data['ticketId'] = $ticketId;  
            $data['name'] = $name;  
            $data['email'] = $email;  
            $data['reply'] = $reply;   
            $data['created']=$data['updated']=currentDate();
			$id=$this->pdb->insert('mp_ticket_reply', $data);
            if($id){
                $ticketRow = $this->ticket->ticketDetails($ticketId);
                
                if(loggedUserData()['loginEmail'] != $ticketRow['email']){
                    $data['ticketNo'] = $ticketNo;
    				$msg=$this->load->view("email_temp/ticket_email", array('dtl'=>$ticketRow), true);
               
                    sendMail($ticketRow['email'],'','','Ticket Updates',$msg);
                }
                die(true);
            }
        } 
        else {
            die(false);
        } 
        
    }
}
