<?php

/** For Patient * */
class User extends ParentPatientController {

    function __construct() {
        parent::__construct();
        $this->load->model('patient/User_model', 'user');
    }

    /**     * */
    function profile() {
        if ($_POST) {
            $data = arrayTrim($_POST);
            $dtl = $this->loggedData();
            $data['id'] = $dtl['id'];
            $err = $this->validateProfile($data);
            if (!$err) {
                $data['username'] = $data['loginEmail'];
                if ($_FILES['image']['name'])
                    $data['image'] = $this->uploadUserImage($data);

                $status = $this->user->updateProfile($data);
                if ($status) {
                    $userDtl = $this->common->userDetail($data['id']);
                    unset($userDtl['bodymap']);
                    setSession(USR_SESSION_NAME, $userDtl);

                    setFlash('<div class="alert alert-success" hide="20000">Saved successfully.</div>');
                    redirect(PATIENT_URL . "myaccount");
                }
            }
            $data['errors'] = $err;
            $data['dtl'] = $_POST;
            $data['dtl']['phone1'] = $_POST['stdCode1'] . $_POST['phone1'];
        }

        $data['page_title'] = 'My Account | My Pain';
        $this->load->getView('patient/profile', $data);
    }

    function validateProfile($data) {
        $errors = array();
        $errors['phone1'] = !$data['phone1'] ? "Please enter contact number" : '';

        if ($filename = $_FILES['image']['name']) {
            if (!checkImageExt($filename))
                $errors['image'] = 'Invalid image (Only .jpg, .jpeg, .png and .gif file allowed)';
        }

        if (!isArrayEmpty($errors))
            return $errors;
    }

    function uploadUserImage($data) {
        $this->load->library("Image");
        $dir = "assets/uploads/user_images/";
        $old = $this->pdb->singleVal("mp_users", "id='{$data['id']}'", "image");
        delFile($dir . $old);
        $filename = renameFileIfExist($dir, $_FILES['image']['name']);
        $this->image->resize($_FILES['image']['tmp_name'], $dir . $filename, 100);
        return $filename;
    }

    function changePassword() {
        if ($_POST) {
            $msg = $this->validateChangePassword($_POST);
            if (!$msg['err']) {
                $d = $this->loggedData();
                $_POST['id'] = $d['id'];
                $status = $this->user->changePassword($_POST);
                if ($status) {
                    $dtl = $this->common->userDetail($d['id']);
                    setSession(USR_SESSION_NAME, $dtl);

                    $dtl['newpass'] = $_POST['password'];
                    $msg = $this->load->view("email_temp/change_password", $dtl, true);
                    sendMail($dtl['loginEmail'], "", "", "Password Change", $msg);

                    setFlash('<div class="alert alert-success" hide="20000">Password changed successfully.</div>');
                    redirect(PATIENT_URL . "changePassword");
                }
            } else
                $data['errors'] = $msg['errors'];
        }

        $data['page_title'] = 'Change Password | My Pain';
        $this->load->getView('patient/change_password', $data);
    }

    function validateChangePassword($data) {
        $err = false;
        if (!trim($data['password'])) {
            $inf['errors']['password'] = 'Please enter password!';
            $err = true;
        }

        if (!trim($data['repassword'])) {
            $inf['errors']['repassword'] = 'Please re-enter password!';
            $err = true;
        }

        if (trim($data['password']) && trim($data['repassword']) && $data['password'] != $data['repassword']) {
            $inf['errors']['repassword'] = 'Password mismatch!';
            $err = true;
        }

        $inf['err'] = $err;
        return $inf;
    }

    /**     * */
    function dashboard() {
        $data['dtl'] = $this->common->userDetail(USER_ID);

        $data['overallRes'] = $this->common->getOverallScore(USER_ID);
        $data['feels'] = $this->common->getFeelScore(USER_ID);

        $data['page_title'] = 'Dashboard | My Pain';
        $this->load->getView('patient/user/dashboard', $data);
    }

    function myForms() {
        $data['cats'] = $this->pdb->select("mp_form_cats");
        $data['list'] = $this->common->patientForms(USER_ID);
        $data['page_title'] = 'Treatment Response Forms | My Pain';
        $this->load->getView('patient/form/my_forms', $data);
    }

    function saveBodyMap() {
        $data = arrayTrim($_POST);
        $this->pdb->update("mp_users", $data, "id=" . USER_ID);
        echo '1';
    }
    
    function myInvitations(){
       $data['dueInvitation'] = doctorInvitationDue(USER_ID);
       $this->load->getView('patient/user/my-invite', $data);
    }
    
    function doctorRequest($doctorId=0, $userId=0, $accept=0){
        if(!$doctorId || !$userId || !$accept)
            show_404 ();
        
        $doctorId = decode($doctorId); $userId = decode($userId); $accept = decode($accept);
        $cond = "doctorId='$doctorId' AND userId= '$userId' AND invitation='1'";
        $this->pdb->update("mp_patients", array("invitation"=>$accept, "invitationAcceptDate"=>  currentDate()), $cond);
        if($accept==2)
            setFlash ("<div class='alert alert-success'><p>Request accepted successfully</p></div>");
        else
            setFlash("<div class='alert alert-success'><p>Request cancelled successfully</p></div>");
        redirect(PATIENT_URL."user/myInvitations/");
    }

}
