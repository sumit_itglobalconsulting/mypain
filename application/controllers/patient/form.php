<?php

/** For Patient * */
class Form extends ParentPatientController {

    function __construct() {
        parent::__construct();
        $this->load->model('patient/Form_model', 'form');
    }

    function test(){
        
             setFlash('<div class="alert alert-success"><h4>Thank you!</h4><h4>Your have submitted your response successfully.</h4></div>');
             
              redirect(PATIENT_URL . "form/response/" . encode(19) . "/" . encode(1). "/0/0/1/");
    }
    function euroQol() {
            $formId = 19;
            $res = array("Mobility"=>"I_AM_CONFINED_TO_BED","Selfcare"=>"I_HAVE_NO_PROBLEMS_WITH_SELFCARE","UsualActivities"=>"I_HAVE_NO_PROBLEMS_WITH_PERFORMI","Paindiscomfort"=>"I_HAVE_EXTREME_PAIN_OR_DISCOMFOR","AnxietyDepression"=>"I_AM_EXTREMELY_ANXIOUS_OR_DEPRES", "VAS"=>50, "meta_stPartnerSubjectID"=>"43,18");
         
         //$res = str_replace('_', ' ', $res);pr($res);
         $euroQues = array(309=>"Mobility", 310=>"Selfcare", 311=>"UsualActivities", 312=>"Paindiscomfort", 313=>"AnxietyDepression");
         $data = array();
         $quesOpt = array();
         for($i=309;$i<=313;$i++){
             $optN = $res[$euroQues[$i]];
             $cond='';$cond="optionName like'$optN%'";
             $quesOpt[$i] = $this->pdb->singleVal("mp_form_ques_options", $cond, 'id');
         }
         $optN = $res['VAS'];
         $cond="optionNameEuro = '$optN'";
         $quesOpt[314] = $this->pdb->singleVal("mp_form_ques_options", $cond, 'id');
         $data['formId'] = $formId;
         $formDtl = $this->common->patientFormDetail($formId);
         $data['dueDate'] = $formDtl['nextResDueDate'];
         $patientData = explode('~', $res['meta_stPartnerSubjectID']);
         $data['userId'] =   $patientData[0];
         $data['doctorId'] =  $patientData[1];
         $doctorId = $data['doctorId'];
         $data['firstResDate'] = '';
         $data['lastResDate'] = '';
         $data['resDt'] = 0;
         $data['quesOpt'] = $quesOpt;
         if ($data) { 
            $_SESSION['responsed'] = 1; 
            $optIds = implode(",", $data['quesOpt']);
            $caps = $this->pdb->query("select caption from mp_form_ques_options where id IN ($optIds)");
            $str = "";
            for ($i = 0; $i <= 4; $i++)
                $str.=$caps[$i]['caption'];

            $data['combination'] = $str;
            $this->form->response($data);

            
             setFlash('<div class="alert alert-success"><h4>Thank you!</h4><h4>Your have submitted your response successfully.</h4></div>');
             
              redirect(PATIENT_URL . "form/response/" . encode($formId) . "/" . encode($doctorId). "/0/0/1/");
        }
         
         
         
                
    }
    
    function submitEuroqol($doctorId=0, $userId=0){
        $this->load->model('User_api');
        $dtl = $this->User_api->detail($userId);
        $dtl['doctorId'] = $doctorId;
        $data['dtl'] = $dtl;
        $this->load->view("patient/form/res_euro", $data);
    }

    /**     * */
    function response($formId, $doctorId = 0, $dt = 0, $userId = 0, $notEuro=0) {
        if (!$formId)
            show_404();
        if (!$doctorId)
            show_404();

        $formId = decode($formId);
        if ($doctorId)
            $doctorId = decode($doctorId);
        if ($userId)
            $userId = decode($userId);
        else
            $userId = USER_ID;

        if($formId==19 AND $notEuro==0){
            redirect(PATIENT_URL.'form/submitEuroqol/'.$doctorId.'/'.$userId);
        }
        $data['formDtl'] = $this->common->patientFormDetail($formId);

        if ($_POST) { 
            $_SESSION['responsed'] = 1; 

            if ($_POST['formId'] == 19) {
                $optIds = implode(",", $_POST['quesOpt']);
                $caps = $this->pdb->query("select caption from mp_form_ques_options where id IN ($optIds)");
                $str = "";
                for ($i = 0; $i <= 4; $i++)
                    $str.=$caps[$i]['caption'];

                $_POST['combination'] = $str;
            }
            
            $this->form->response($_POST);

            if ($_POST['resDT'])
                setFlash('<div class="alert alert-success"><h4>Thank you!</h4><h4>Your have updated your response successfully.</h4></div>');
            else
                setFlash('<div class="alert alert-success"><h4>Thank you!</h4><h4>Your have submitted your response successfully.</h4></div>');

            if ($_POST['resDT'])
                redirect(PATIENT_URL . "form/response/" . encode($formId) . "/" . encode($doctorId) . "?resDate=" . urlencode($_POST['resDT']));
            else
                redirect(PATIENT_URL . "form/response/" . encode($formId) . "/" . encode($doctorId));
        }

        $resDate = urldecode($_GET['resDate']);
        $data['resDtl'] = $this->common->responseDetail($formId, $resDate, $doctorId, $userId);


        $data['resDates'] = $this->common->responseDates($formId, $doctorId, $userId);

        $data['ques'] = $this->common->ques($formId);

        $data['doctorId'] = $doctorId;
        $data['patientDtl'] = $this->common->userDetail($userId);

        $forms = $this->common->patientForms($userId);
        $dueForms = array();
        if ($forms) {
            foreach ($forms as $f) {
                $resDtl = isFormSubmited($userId, $f['formId']);
                if (isTodayOrBefore($f['nextResDueDate'])) {
                    if ($f['resFreq'] != 'O')
                        $dueForms[] = $f;
                    else if (!$resDtl)
                        $dueForms[] = $f;
                }
            }
            $data['dueForms'] = $dueForms;
        }

        $data['formId'] = $formId;
        $data['doctorId'] = $doctorId;
        if($formId == 8)
            $data['doctorName'] = $this->common->doctorName($doctorId);

        $data['page_title'] = "Response {$data['formDtl']['formName']} | My Pain"; 
        
        $this->load->getView("patient/form/response", $data);
    }

    function updateRes($formId = 0, $doctorId = 0, $dt = 0) {
        if ($formId && $doctorId && $dt)
            $this->response($formId, $doctorId, $dt);
    }

}



