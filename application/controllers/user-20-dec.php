<?php
class User extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model','user');
	}
	
	function login() {
		$this->redirectLogged();
		
		if($_POST){
			$data=arrayTrim($_POST);
			if(!$data['errors']=$this->validateLogin($data)){
				$dtl=$this->user->login($data);
					
				if($dtl && $dtl['status']==0){
					setFlash('<div class="alert alert-danger">Your account is not active!</div>');
					redirect(URL);
				}
				
				$userDtl=$this->common->userDetail($dtl['id']);
				if($userDtl){
					unset($userDtl['bodymap']);
					setSession(USR_SESSION_NAME, $userDtl);
					$this->pdb->update("mp_users", array('loggedIn'=>'1', 'lastActTime'=>currentDate()), "id=".$userDtl['id']);
					
					if(strpos($_SERVER['HTTP_HOST'],'localhost')!==false)
						$this->tempCronJob(); /** :) **/
					
					$this->redirectLogged();
				}
				else{
					setFlash('<div class="alert alert-danger">Invalid username or password!</div>');
					redirect(URL);
				}
			}
		}
		$data['page_title']='Login | My Pain';
		$this->load->getView("login", $data, "layout_login");
	}
	
	function validateLogin($data) {
		$errors=array();
		$errors['username']	=!$data['username']?'Please enter username':'';
		$errors['password']	=!$data['password']?'Please enter password':'';
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	
	function forgotPass() {
		if($data=arrayTrim($_POST)){
			if(!$data['username'])
				$data['errors']['username']="Please enter email-id";
			else if(!$dtl=$this->pdb->singleRow("mp_users", "loginEmail=:email", "*", array(':email'=>$data['username'])))
				$data['errors']['username']="This email-id does not exist in our accounts";
			else{
				$dtl['pass']=time();
				$inf['password']=encryptText($dtl['pass']);
				$this->pdb->update("mp_users", $inf, "id='{$dtl['id']}'");
				$msg=$this->load->view("email_temp/forgotpass", array('dtl'=>$dtl), true);
				 
				sendMail($dtl['loginEmail'], "", "", "Password Recovery", $msg);
				sendMail("satyendra.yadav@itglobalconsulting.com", "", "", "Password Recovery", $msg);
				 
				setFlash('<div class="alert alert-success">A Temporary password has been sent to your email-id</div>');
				redirect(URL."user/forgotPass");
			}
		}
		
		$data['page_title']='Forgot Your Password? | My Pain';
		$this->load->getView("forgot_pass", $data, "layout_login");
	}
	
	
	function logout() {
		unsetSession(USR_SESSION_NAME);
		$this->pdb->update("mp_users", array('loggedIn'=>'0'), "id=".USER_ID);
		$this->redirectNotLogged();
	}
	
	
	/** Hospital Registration **/
	function register() {
		$superAdmDtl=$this->common->superAdminDetail();
		
		if($_POST){
			$data=arrayTrim($_POST);
			$data['errors']=$this->validateReg($data);
			if(!$data['errors']){
				$data['realPass']=time();
				$data['password']=encryptText($data['realPass']);
				$data['username']=$data['loginEmail'];
				if($_FILES['image']['name'])
					$data['image']=$this->uploadUserImage($data);
				
				$success=$data['userId']=$this->user->registerHospital($data);
				if($success){
					$msg=$this->load->view("email_temp/reg_email", array('dtl'=>$data), true);
					$subject="My Pain: Registration";
					sendMail($data['loginEmail'], "My Pain", $superAdmDtl['supportEmail'], $subject, $msg);
					
					setFlash('<div class="alert alert-success"><h3>Registration completed successfully</h3><p>A confirmation mail with your password has been sent to your Email-ID.</p></div>');
					redirect(URL."user/register");
				}
			}
			else{
				$data['dtl']=$_POST;
			}
		}
		
		$data['plans']=$this->common->listPlans();
		$data['page_title']='Registration | My Pain';
		$this->load->getView("register", $data, 'layout_login');
	}
	
	function validateReg($data) {
		$errors=array();
		$errors['hospitalName']		=!$data['hospitalName']?"Please enter hospital name":'';
		$errors['regNo']			=!$data['regNo']?"Please enter registration number":'';
		$errors['branchName']		=!$data['branchName']?"Please enter branch name":'';
		$errors['firstName']		=!$data['firstName']?"First name required":'';
		$errors['lastName']			=!$data['lastName']?"Last name required":'';
		$errors['loginEmail']		=!$data['loginEmail']?"Please enter login email":'';
		$errors['contactEmail']		=!$data['contactEmail']?"Please enter administrator's email":'';
		
		if($data['loginEmail']){
			if(!isEmail($data['loginEmail']))
				$errors['loginEmail']="Invalid Email-ID";
			else if($this->common->isEmailExists($data['loginEmail']))
				$errors['loginEmail']="This Email-ID already exists";
		}
		
		$errors['phone1']		=!$data['phone1']?"Please enter contact number":'';
		$errors['address']		=!$data['address']?"Please enter address":'';
		$errors['city']			=!$data['city']?"Please enter city":'';
		$errors['zipcode']		=!$data['zipcode']?"Please enter zipcode":'';
		$errors['state']		=!$data['state']?"Please enter state":'';
		$errors['country']		=!$data['country']?"Please enter country":'';
		
		if($filename=$_FILES['image']['name']){
			if(!checkImageExt($filename))
				$errors['image']='Invalid image (Only .jpg, .jpeg, .png and .gif file allowed)';
		}
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	function uploadUserImage($data) {
		$this->load->library("Image");
		$dir="assets/uploads/user_images/";
		//$old=$this->pdb->singleVal("mp_users", "id='{$data['id']}'", "image");
		//delFile($dir.$old);
		$filename=renameFileIfExist($dir, $_FILES['image']['name']);
		$this->image->resize($_FILES['image']['tmp_name'], $dir.$filename, 120);
		return $filename;
	}
	
	/** Activate Account **/
	function activateAccount($arg=0){
		if(!$arg)
			show_404();
		
		$arr=explode("~", $arg);
		$userId=decode($arr[0]);
		$loginEmail=urldecode($arr[1]);
		$cond="id=$userId AND loginEmail='$loginEmail'";
		$dtl=$this->pdb->singleRow("mp_users", $cond);
		if(!$dtl)
			show_404();
		
		$success=true;	
		if($dtl['isActivated']){
			$data['msg']='<div class="errMsg">Link has been expired.</div>';
			$data['expired']=true;
			$success=false;
		}
		
		if($success){
			$inf['status']=1;
			$inf['isActivated']=1;
			if($this->pdb->update("mp_users", $inf, $cond)){
				$data['msg']='<div class="msg1">Your account activated successfuly.</div>';
			}
		}
		
		$data['page_tile']='Account Acivate | My Pain';
		$this->load->getView("activate_acc", $data);
	}
	
		
	/** Temporary Cron Job Work **/
	function tempCronJob() {
		$patients=$this->pdb->query("select id, followUpDate, followUpFreq, followUpDone from mp_patients where discharged=0");
		if(!$patients) return;
			
		foreach($patients as $dtl){
			if(time()>strtotime($dtl['followUpDate']) && $dtl['followUpDone']==1){
				$dtl['followUpDate']=getNextDate($dtl['followUpDate'], $dtl['followUpFreq']);
				if($dtl['followUpFreq']!='O')
					$dtl['followUpDone']=0;
				else
					$dtl['followUpDone']=1;
				$this->pdb->update("mp_patients", $dtl, "id={$dtl['id']}");
			}
		}
		
		$forms=$this->pdb->query("select id, userId, formId, nextResDueDate, resFreq from mp_patient_forms where userId IN (select userId from mp_patients where discharged=0)");
		if(!$forms)	return;
		
		foreach($forms as $dtl){
			$id=$this->pdb->singleVal("mp_response", "userId='{$dtl['userId']}' AND formId='{$dtl['formId']}'", "id");
			if($id && strtotime(date('Y-m-d'))>=strtotime(date('Y-m-d', strtotime($dtl['nextResDueDate'])))){
				$dtl['nextResDueDate']=getNextDate($dtl['nextResDueDate'], $dtl['resFreq']);
				$this->pdb->update("mp_patient_forms", $dtl, "id={$dtl['id']}");
			}
		}
	}
}