<?php
class Hospital extends ParentAdminController {
	function __construct() {
		parent::__construct();
		$this->load->model('admin/Hospital_model','hos');
		$this->load->model('clinician/Patient_model','patient');
	}
	
	/** Forms **/
	
	function allForms() {
		$data['cats']=$this->pdb->select("mp_form_cats");
		$data['list']=$this->common->listAllForms();
		$data['planDtl']=$this->common->getUserLastPlanDtl(USER_ID);
		
		$data['addedForms']=$this->hos->hospitalForms();//echo '<pre>';print_r($data);exit;
		$data['page_title']='All Treatment Response Forms | My Pain';
		$this->load->getView("admin/form/all_forms", $data);
	}
	
	function addForm($formId=0) {
		if(!$formId)
                    return;
		$formId=decode($formId);
		
		$planDtl=$this->common->getUserLastPlanDtl(USER_ID);
		
		if(!$this->pdb->singleVal("mp_forms", "id=$formId", "id"))
			return;
                /**Added on 14-jan-2015**/
//		$totalAdded=$this->pdb->singleVal("mp_hospital_forms", "userId=".USER_ID, "COUNT(id)");
//		if($totalAdded>=$planDtl['noOfFormsAllowed'])
//                    return;
		/** End **/	
                
                /**Added on 14-jan-2015**/
                $allowFormIds = explode(',',$planDtl['formId']);
                if(!in_array($formId, $allowFormIds))
                        return;
                /** End **/	
		$inf['formId']=$formId;
		$inf['userId']=USER_ID;
		$inf['created']=currentDate(); 
		if(!$this->pdb->singleVal("mp_hospital_forms", "formId=$formId AND userId=".USER_ID, "id")){
			$this->pdb->insert("mp_hospital_forms", $inf);
		}
	}
	
	function myForms() {
		$data['cats']=$this->pdb->select("mp_form_cats");
		$data['list']=$this->hos->hospitalForms();
		$data['page_title']='My Treatment Response Forms | My Pain';
		$this->load->getView("admin/form/my_forms", $data);
	}
	
	
	/** Departments **/
	
	function departments() {
		$data['list']=$this->hos->listDepts();
		$data['page_title']='Departments | My Pain';
		$this->load->getView("admin/department/list", $data);
	}
	
	function saveDept() {
		$data=arrayTrim($_POST);
		$err=$this->validateDept($data);
		if(!$err) {
			if($_FILES['image']['name'])
				$data['image']=$this->uploadDeptImage($data);
			$id=$this->hos->saveDept($data);
			if($id) {
				setFlash('<div class="alert alert-success" hide="20000">Saved successfully</div>');
				redirect(ADM_URL."hospital/editDepartment/".encode($id));
			}
		}
		return $err;
	}
	
	function validateDept($data) {
		$errors=array();
		$errors['name']			=!$data['name']?'Please enter department name':'';
		$errors['deptHead']		=!$data['deptHead']?'Please enter department head name':'';
		$errors['phone']		=!$data['phone']?'Please enter contact number':'';
		$errors['email']		=!$data['email']?'Please enter Email-ID':'';
		
		if($data['email']){
			if(!isEmail($data['email']))
				$errors['email']="Please enter a valid Email-ID";
				
			if($this->pdb->singleVal("mp_departments", "email='{$data['email']}' AND id!='{$data['id']}'", "id"))
				$errors['email']="This Email-ID already exists";
		}
		
		if(!$data['id']){
			if($filename=$_FILES['image']['name']){
				if(!checkImageExt($filename))
					$errors['image']='Invalid image (Only .jpg, .jpeg, .png and .gif file allowed)';
			}
		}
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	function uploadDeptImage($data) {
		$this->load->library("Image");
		$dir="assets/uploads/department_images/";
		$old=$this->pdb->singleVal("mp_departments", "id='{$data['id']}'", "image");
		delFile($dir.$old);
		$filename=renameFileIfExist($dir, $_FILES['image']['name']);
		$this->image->resize($_FILES['image']['tmp_name'], $dir.$filename, 100);
		return $filename;
	}
	
	function addDepartment() {
		if($_POST)
			$data['errors']=$this->saveDept();
		
		$data['dtl']=$_POST;	
		$data['hospitalForms']=$this->hos->hospitalForms();
		$data['page_title']='Add Form | My Pain';
		$this->load->getView("admin/department/add", $data);
	}
	
	function editDepartment($id=0) {
		if(!$id)
			show_404();
			
		$id=decode($id);
		
		if(!$data['dtl']=$this->hos->deptDetail($id))
			show_404();
			
		if($_POST) {
			$_POST['id']=$id;
			$data['errors']=$this->saveDept();
			$_POST['image']=$data['dtl']['image'];
			$data['dtl']=$_POST;
		}
		
		$data['hospitalForms']=$this->hos->hospitalForms();
		$data['deptForms']=$this->hos->deptForms($id);
		$data['page_title']='Edit Department | My Pain';
		$this->load->getView("admin/department/add", $data);
	}
	
	
	/** Clinicians / Doctors **/
	
	function clinicians($p=1) {
		$data=$this->hos->listClinicians($p, PAGE_SIZE);
		$data['page_title']='Clinicians | My Pain';
		$this->load->getView("admin/clinician/list", $data);
	}
	
	function saveClinician() {
		$data=arrayTrim($_POST);
		$err=$this->validateClinician($data);
		if(!$err) {
			if($_FILES['image']['name'])
				$data['image']=$this->uploadClinicianImage($data);
			
			if(!$data['id']){
				$data['username']=$data['loginEmail'];
				$data['realPass']=time();
				$data['password']=encryptText($data['realPass']);
			}
			
			$id=$this->hos->saveClinician($data);
			if($id) {
				if(!$data['id']){
					$data['userId']=$id;
					$msg=$this->load->view("email_temp/add_clinician_email", array('dtl'=>$data), true);
					$subject="My Pain: Registration";
					sendMail($data['loginEmail'], "My Pain", $superAdmDtl['supportEmail'], $subject, $msg);
				}
				
				setFlash('<div class="alert alert-success" hide="20000">Saved successfully</div>');
				redirect(ADM_URL."hospital/editClinician/".encode($id));
			}
		}
		return $err;
	}
	
	function validateClinician($data) {
		$errors=array();
		$errors['firstName']	=!$data['firstName']?'Please enter name':'';
		$errors['deptId']		=!$data['deptId']?'Please select a department':'';
		$errors['phone1']		=!$data['phone1']?'Please enter contact number':'';
		$errors['loginEmail']	=!$data['loginEmail']?'Please enter Email-ID':'';
		
		if($data['loginEmail']){
			if(!isEmail($data['loginEmail']))
				$errors['email']="Please enter a valid Email-ID";
				
			if($this->common->isEmailExists($data['loginEmail'], $data['id']))
				$errors['loginEmail']="This Email-ID already exists";
		}
		
		if(!$data['id']){
			if($filename=$_FILES['image']['name']){
				if(!checkImageExt($filename))
					$errors['image']='Invalid image (Only .jpg, .jpeg, .png and .gif file allowed)';
			}
		}
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	function uploadClinicianImage($data) {
		$this->load->library("Image");
		$dir="assets/uploads/user_images/";
		$old=$this->pdb->singleVal("mp_users", "id='{$data['id']}'", "image");
		if($old)
			delFile($dir.$old);
		$filename=renameFileIfExist($dir, $_FILES['image']['name']);
		$this->image->resize($_FILES['image']['tmp_name'], $dir.$filename, 100);
		return $filename;
	}
	
	function addClinician() {
		if($_POST)
			$data['errors']=$this->saveClinician();
		
		$data['dtl']=$_POST;
		$data['dtl']['phone1']=$_POST['stdCode1'].$_POST['phone1'];
		
		$data['depts']=$this->hos->listDepts();
		$data['deptForms']=$this->hos->deptForms($data['dtl']['deptId']);
		$data['page_title']='Add Clinician | My Pain';
		$this->load->getView("admin/clinician/add", $data);
	}
	
	function editClinician($id=0) {
		if(!$id)
			show_404();
			
		$id=decode($id);
		
		if(!$data['dtl']=$this->common->userDetail($id))
			show_404();
			
		if($_POST) {
			$_POST['id']=$id;
			$data['errors']=$this->saveClinician();
			$_POST['image']=$data['dtl']['image'];
			$data['dtl']=$_POST;
			$data['dtl']['phone1']=$_POST['stdCode1'].$_POST['phone1'];
		}
		
		$data['depts']=$this->hos->listDepts();
		$data['deptForms']=$this->hos->deptForms($data['dtl']['deptId']);
		$data['clinicianForms']=$this->hos->clinicianForms($data['dtl']['userId']);
		$data['page_title']='Edit Clinician | My Pain';
		$this->load->getView("admin/clinician/add", $data);
	}
	
	function deptFormAjax($deptId=0) {
		$forms=$this->hos->deptForms($deptId);
		echo json_encode($forms);
	}
	
	function deleteClinician($id=0) {
		if(!$id)
			show_404();
		$id=decode($id);
		if(!$data['dtl']=$this->common->userDetail($id))
			show_404();
		if($data['dtl']['parentId']!=USER_ID)
			show_404();
			
		$status=$this->hos->deleteClinician($data['dtl']);
		if($status['success'])
			setFlash("<div class='msg1' hide='20000'>Deleted successfully</div>");
		else
			setFlash("<div class='errMsg' hide='20000'>{$status['msg']}</div>");
			
		redirect(ADM_URL."hospital/clinicians");
	}
	
	function resetClinicianPassword($id=0) {
		if(!$id)
			show_404();
		$id=decode($id);
		if(!$data['dtl']=$this->common->userDetail($id))
			show_404();
		if($data['dtl']['parentId']!=USER_ID)
			show_404();
		
		if($_POST){
			$postData=arrayTrim($_POST);
			$data['errors']=$this->validateClinicianResetPassword($postData);
			if(!$data['errors']){
				$data['realPass']=$postData['password'];
				$data['userId']=$id;
				$data['firstName']=$data['dtl']['firstName'];
				$data['loginEmail']=$data['dtl']['loginEmail'];
				
				$inf['password']=encryptText($postData['password']);
				
				
				if($this->pdb->update("mp_users", $inf, "id=:userId", array(':userId'=>$id))){
					$msg=$this->load->view("email_temp/clinician_reset_pass_email", array('dtl'=>$data), true);
					$subject="My Pain: Password Reset";
					sendMail($data['loginEmail'], "My Pain", $superAdmDtl['supportEmail'], $subject, $msg);
					
					setFlash("<div class='alert alert-success' hide='20000'>Password reset successfully</div>");
				}
				
				redirect(ADM_URL."hospital/resetClinicianPassword/".encode($id));
			}
		}
		
		$this->load->getView("admin/clinician/change_password", $data);
	}
	
	function validateClinicianResetPassword($data) {
		$errors=array();
		$errors['password']			=!$data['password']?'Please enter password':'';
		$errors['repassword']		=!$data['repassword']?'Please re-enter password':'';
		
		if($data['password'] && $data['repassword'] && $data['password']!=$data['repassword']){
			$errors['repassword']='Password mismatch!';
		}
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	/** Patients **/ 
	function patients($p=1) {
		$inf=addSlash(arrayTrim($_REQUEST));
		$docts=$this->hos->allClinicians();
		
		$doctorId=$inf['doctorId'];
		if(!$doctorId)
			$doctorId=$docts[0]['id'];
			
		$docts=multiArrToKeyValue($docts, 'id', 'name');
			
		$data=$this->patient->listPatients($p, PAGE_SIZE, 0, $doctorId);
		$data['doctors']=$docts;
		$data['doctorId']=$doctorId;
		
		$data['page_title']='Patients | My Pain';
		$this->load->getView("admin/patient/list", $data);
	}
	
	/** Form Response of patient **/
	function response($formId, $userId=0, $doctorId=0) {
		if(!$formId) show_404();
		$formId=decode($formId);
		$userId=decode($userId);
		$doctorId=decode($doctorId);
		
		if(!$data['formDtl']=$this->common->patientFormDetail($formId, $userId))
			show_404();
			
		$resDate=urldecode($_GET['resDate']);
		$data['resDtl']=$this->common->responseDetail($formId, $resDate, $doctorId, $userId);
		$data['resDates']=$this->common->responseDates($formId, $doctorId, $userId);
			
		$data['ques']=$this->common->ques($formId);
		$data['quesGroup']=$this->common->listQuesGroup();
		
		$data['patientDtl']=$this->common->userDetail($userId);
		$data['patientId']=$userId;
		
		$data['page_title']="Response {$data['formDtl']['formName']} | My Pain";
		$this->load->getView("patient/form/response", $data);
	}
}
