<?php

class Patients extends ParentClinicianController {

    function __construct() {
        parent::__construct();
        $this->load->model('clinician/Patient_model', 'patient');
    }

    function index() {
        $this->lists();
    }

    function lists($p = 1) {
        $data = $this->patient->listPatients($p, PAGE_SIZE);

        $data['doctorId'] = USER_ID;
        $data['page_title'] = 'Patients | My Pain';
        $this->load->getView("clinician/patient/list", $data);
    }
    
    function invitePatients($p = 1){
        $data = $this->patient->invitePatients($p, PAGE_SIZE);

        $data['doctorId'] = USER_ID;
        $data['page_title'] = 'Invite Patients | My Pain';
        $this->load->getView("clinician/patient/list-invite", $data);
    }
    
    function approvePatients($p = 1){
        $data = $this->patient->approvePatients($p, PAGE_SIZE);

        $data['doctorId'] = USER_ID;
        $data['page_title'] = 'Approve Patients | My Pain';
        $this->load->getView("clinician/patient/list-approve", $data);
    }

    function archivedPatients($p = 1) {
        $data = $this->patient->listPatients($p, PAGE_SIZE, false, USER_ID, true);

        $data['doctorId'] = USER_ID;
        $data['page_title'] = 'Patients | My Pain';
        $this->load->getView("clinician/patient/list-archived", $data);
    }

    function dischargedPatients($p = 1) {
        $data = $this->patient->listPatients($p, PAGE_SIZE, false, USER_ID, false, true);

        $data['doctorId'] = USER_ID;
        $data['page_title'] = 'Patients | My Pain';
        $this->load->getView("clinician/patient/list-discharged", $data);
    }

    function followUps($p = 1) {
        $data = $this->patient->listPatients($p, PAGE_SIZE, true);
        $data['page_title'] = 'Follow Ups | My Pain';
        $this->load->getView("clinician/patient/follow_up_list", $data);
    }

    function savePatient() {
        $data = arrayTrim($_POST);

        $err = $this->validatePatient($data);
        if (!$err) {
            $data['dob'] = timeStamp($data['dob']);

            if ($data['followUpDate'])
                $data['followUpDate'] = timeStamp($data['followUpDate']);

            if (!$data['id']) {
                $data['realPass'] = time();
                $data['username'] = $data['loginEmail'];
                $data['password'] = encryptText($data['realPass']);
            }
            //pr($data);exit;
            $id = $this->patient->savePatient($data);
            if ($id) {
                if (!$data['id']) {
                    $data['userId'] = $id;
                    $msg = $this->load->view("email_temp/add_patient_email", array('dtl' => $data), true);
                    $subject = "My Pain: Registration";
                    sendMail($data['loginEmail'], "My Pain", $superAdmDtl['supportEmail'], $subject, $msg);
                }

                setFlash('<div class="alert alert-success" hide="20000">Saved successfully</div>');
                $this->session->set_flashdata('flash_edit', 1);
                redirect(DOCT_URL . "patients/edit/" . encode($id));
            }
        }
        return $err;
    }

    function validatePatient($data) {
        $errors = array();
        $errors['hospitalId'] = !$data['hospitalId'] ? 'Please enter Hospital #' : '';
        $errors['firstName'] = !$data['firstName'] ? 'Please enter first name' : '';
        $errors['lastName'] = !$data['lastName'] ? 'Please enter last name' : '';
        $errors['phone1'] = !$data['phone1'] ? 'Please enter contact number' : '';
        $errors['zipcode'] = !$data['zipcode'] ? 'Please enter postcode' : '';
        $errors['loginEmail'] = !$data['loginEmail'] ? 'Please enter Email-ID' : '';
        $errors['dob'] = !$data['dob'] ? 'Please enter dob' : '';

        if ($data['needFollowUp'] == 'Y') {
            $errors['followUpDate'] = !$data['followUpDate'] ? 'Please enter follow up date' : '';
            $errors['followUpFreq'] = !$data['followUpFreq'] ? 'Please enter follow up frequency' : '';
        }

        if ($data['hospitalId']) {
            if ($this->pdb->singleVal("mp_patients", "hospitalId='{$data['hospitalId']}' AND userId!='{$data['id']}'", "id"))
                $errors['hospitalId'] = "This Hospital # already exists";
        }

        if ($data['loginEmail']) {
            if (!isEmail($data['loginEmail']))
                $errors['loginEmail'] = "Please enter a valid Email-ID";

            if ($this->common->isEmailExists($data['loginEmail'], $data['id']))
                $errors['loginEmail'] = "This Email-ID already exists";
        }

        if ($data['dob']) {
            if (strtotime($data['dob']) >= time()) {
                $errors['dob'] = "Invalid date of birth";
            }
        }

        if ($data['followUpDate']) {
            if (strtotime($data['followUpDate']) <= time()) {
                //$errors['followUpDate']="Follow up date must be greater than today";
            }
        }

        if (!isArrayEmpty($errors))
            return $errors;
    }

    function add() {
        if ($_POST) {
            $data['errors'] = $this->savePatient();

            $data['dtl'] = $_POST;
            $data['dtl']['phone1'] = $_POST['stdCode1'] . $_POST['phone1'];
            $data['dtl']['phone2'] = $_POST['stdCode2'] . $_POST['phone2'];

            $data['assignedForms'] = array();

            foreach ($_POST['assignForm'] as $formId => $v) {
                if ($v && $formId) {
                    $info['formId'] = $formId;
                    $info['nextResDueDate'] = timeStamp($_POST['nextResDueDate'][$formId]);
                    $info['resFreq'] = $_POST['resFreq'][$formId];
                    $info['symptom'] = $_POST['symptom'][$formId] ? $_POST['symptom'][$formId] : '';
                    $info['weightage'] = $_POST['weightage'][$formId];
                    $info['goodRes'] = $_POST['goodRes'][$formId] ? $_POST['goodRes'][$formId] : '';
                    $info['avgRes'] = $_POST['avgRes'][$formId] ? $_POST['avgRes'][$formId] : '';

                    array_push($data['assignedForms'], $info);
                }
            }
        }

        $data['cats'] = $this->pdb->select("mp_form_cats");
        $data['doctorForms'] = $this->common->doctorForms(USER_ID);
        $data['page_title'] = 'Add Patient | My Pain';
        $this->load->getView("clinician/patient/add", $data);
    }

    function edit($id = 0, $return = 0, $share=0) {
        
        if($share)
            $share = decode($share);

        if (!$id)
            show_404();

        $id = decode($id);

        if (!$data['dtl'] = $this->common->userDetail($id, USER_ID)) {
            show_404();
        }

        if ($_POST) {
            $_POST['id'] = $id;
            $data['errors'] = $this->savePatient();



            $data['dtl'] = $_POST;
            $data['dtl']['fullName'] = $_POST['firstName'] . ' ' . $_POST['lastName'];

            $data['dtl']['phone1'] = $_POST['stdCode1'] . $_POST['phone1'];
            $data['dtl']['phone2'] = $_POST['stdCode2'] . $_POST['phone2'];
        }

        $data['patientId'] = $id;

        $data['cats'] = $this->pdb->select("mp_form_cats");
        $data['doctorForms'] = $this->common->doctorForms(USER_ID); 
        

        $data['isDischarged'] = $this->common->isDischarged($id, USER_ID);

        if ($data['isDischarged'])
            $data['assignedForms'] = $this->patient->assignedForms($id, USER_ID);
        else
            $data['assignedForms'] = $this->patient->assignedForms($id);



        /** Report section * */
        $data['patientForms'] = $this->common->patientForms($id, USER_ID);
        $data['feels'] = $this->common->getFeelScore($id);
        $data['score'] = $this->common->getOverallScore($id);
        $data['firstLastDates'] = $this->common->firstLastResDates($id);
        /** Report section end * */
        $data['otherDocts'] = $this->patient->otherDoctorsOfPatient($data['dtl']['id']);
        
        $data['share'] = $share;

        //$data['dtl']['edit']=$save;
        $data['page_title'] = 'Edit Patient | My Pain'; 
        $this->load->getView("clinician/patient/add", $data);
    }

    function checkPatientExists() {
        $data['dtl'] = $this->patient->checkPatientExists($_POST);
        if (!$data['dtl']) {
            $data['dtl']['hospitalId'] = $_POST['hospitalId'];
            $data['dtl']['loginEmail'] = $_POST['loginEmail'];
            unset($data['dtl']['id']);

            if ($this->common->isEmailExists($_POST['loginEmail'])) {
                $data['errors']['loginEmail'] = "This Email-ID already exists";
            }
        }

        $data['cats'] = $this->pdb->select("mp_form_cats");
        $data['doctorForms'] = $this->common->doctorForms(USER_ID);

        if ($data['dtl']['id']) {
            $data['assignedForms'] = $this->patient->assignedForms($data['dtl']['id']);

            $data['patientExist'] = true;
            $data['otherDocts'] = $this->patient->otherDoctorsOfPatient($data['dtl']['id']);
        }

        $data['checkFlag'] = true;
        $this->load->view("clinician/patient/add", $data);
    }

    /** Form Response of patient * */
    function response($formId, $userId = 0) {
        if (!$formId)
            show_404();
        $formId = decode($formId);
        $userId = decode($userId);

        if (!$data['formDtl'] = $this->common->patientFormDetail($formId, $userId))
            show_404();

        $resDate = urldecode($_GET['resDate']);
        $data['resDtl'] = $this->common->responseDetail($formId, $resDate, USER_ID, $userId);
        $data['resDates'] = $this->common->responseDates($formId, USER_ID, $userId);

        $data['ques'] = $this->common->ques($formId);

        $data['patientDtl'] = $this->common->userDetail($userId);
        $data['patientId'] = $userId;

        $data['page_title'] = "Response {$data['formDtl']['formName']} | My Pain";
        $this->load->getView("patient/form/response", $data);
    }

    /** Archive / Discharge* */
    function makeArchive($patientId = 0) {
        $patientId = decode($patientId);
        $pdtl = $this->common->userDetail($patientId);
        $ddtl = $this->common->userDetail(USER_ID);

        $success = $this->patient->makeArchive($patientId, 1);
        if ($success) {
            /** Email Report * */
            $data['overallRes'] = $this->common->getOverallScore($pdtl['id'], $ddtl['id']);
            $data['feels'] = $this->common->getFeelScore($pdtl['id']);
            $data['reportForms'] = $this->common->patientForms($pdtl['id'], $ddtl['id']);

            $data['patientName'] = $pdtl['fullName'];
            $data['doctorName'] = $ddtl['fullName'];
            $m = $this->load->view("email_temp/archive_email_patient", $data, true);
            sendMail($pdtl['loginEmail'], "My-Pain", "info@mypain.com", "You are archived by {$data['doctorName']}", $m);
            $m = $this->load->view("email_temp/archive_email_doctor", $data, true);
            sendMail($ddtl['loginEmail'], "My-Pain", "info@mypain.com", "You have archived {$data['patientName']}", $m);
            /** Email Report End * */
            setFlash('<div class="alert alert-success" hide="20000"><b>' . $pdtl['fullName'] . '</b> Archived successfully</div>');
        }

        redirect(DOCT_URL . "patients");
    }

    function makeUnArchive($patientId = 0) {
        $patientId = decode($patientId);
        $pdtl = $this->common->userDetail($patientId);
        $ddtl = $this->common->userDetail(USER_ID);

        $success = $this->patient->makeArchive($patientId, 0);
        if ($success) {
            setFlash('<div class="alert alert-success" hide="20000"><b>' . $pdtl['fullName'] . '</b> Unarchived successfully</div>');
        }

        redirect(DOCT_URL . "patients");
    }

    function makeDischarge($patientId = 0) {
        $patientId = decode($patientId);
        $pdtl = $this->common->userDetail($patientId);
        $ddtl = $this->common->userDetail(USER_ID);

        $success = $this->patient->makeDischarge($patientId, 1);
        //$success = true;
        if ($success) {
            /** Email Report * */
            $data['overallRes'] = $this->common->getOverallScore($pdtl['id'], $ddtl['id']);
            $data['feels'] = $this->common->getFeelScore($pdtl['id']);
            $data['reportForms'] = $this->common->patientForms($pdtl['id'], $ddtl['id']);
            $data['patientName'] = $pdtl['fullName'];
            $data['doctorName'] = $ddtl['fullName'];
            $data['patientId'] = $pdtl['id'];
            echo $m = $this->load->view("email_temp/discharge_email_patient", $data, true);
            sendMail($pdtl['loginEmail'], "My-Pain", "info@mypain.com", "You are discharged by {$data['doctorName']}", $m);
            $m = $this->load->view("email_temp/discharge_email_doctor", $data, true);
            sendMail($ddtl['loginEmail'], "My-Pain", "info@mypain.com", "You have discharged {$data['patientName']}", $m);
            /** Email Report End * */
            setFlash('<div class="alert alert-success" hide="20000"><b>' . $pdtl['fullName'] . '</b> Discharged successfully</div>');
        }

        redirect(DOCT_URL . "patients");
    }

    function makeUnDischarge($patientId = 0) {
        $patientId = decode($patientId);
        $pdtl = $this->common->userDetail($patientId);
        $ddtl = $this->common->userDetail(USER_ID);

        $success = $this->patient->makeDischarge($patientId, 0);
        if ($success) {
            setFlash('<div class="alert alert-success" hide="20000"><b>' . $pdtl['fullName'] . '</b> Re-admitted successfully</div>');
        }

        redirect(DOCT_URL . "patients");
    }

    /**     * */
    function followUpChecked($u = 0, $d = 0, $st = 0) {
        if (!$u && !$d)
            return;
        $inf['followUpDone'] = $st;

        $cond = "userId=$u AND doctorId=$d";

        $dtl = $this->pdb->singleRow("mp_patients", $cond);
        $dates = $dtl['followedDates'];
        if ($st == 1) {
            $dates = explode(",", $dates);
            $dates[] = currentDate();
            foreach ($dates as $i => $dd) {
                if (!$dd)
                    unset($dates[$i]);
            }
            $inf['followedDates'] = implode(",", $dates);
        }
        else {
            $inf['followedDates'] = substr($dates, 0, -19);
        }

        $this->pdb->update("mp_patients", $inf, $cond);
        if ($st)
            echo 'Y';
        else
            echo 'N';
    }

    /** Patient Deletion * */
    function delete($id = 0) {
        if (!$id)
            show_404();
        $id = addslashes(decode($id));

        if (!$this->common->userDetail($id)) {
            show_404();
        }

        if ($this->patient->delete($id)) {
            setFlash('<div class="alert alert-success">Deleted successfully</div>');
        }

        redirect(DOCT_URL . "patients/dischargedPatients");
    }

    /** Check if Patient already Exist * */
    function isPatientExist() {
        if (!empty($_POST['email'])) {
            if ($id=$this->common->isEmailExists($_POST['email'])) {
               $str['text'] = "Patient already exists with this email.";
               $str['id'] = $id;
               echo json_encode($str);
            } else {
                echo '';
            }
        }
    }
    
    /** Invitation send by dr. to patient **/
    function sendInvitation($userId=0){  
        $data['doctorDtl']=getSession(USR_SESSION_NAME);
        $info['userId'] = $userId;
        $info['doctorId'] = $data['doctorDtl']['id'];
        $doctorId = $info['doctorId'];
        
        $cond = "userId='$userId' AND doctorId='$doctorId' AND invitation <> 4";
        
        if($id = $this->pdb->singleVal('mp_patients', $cond, 'id')){
             setFlash('<div class="alert alert-danger"><p>Patient already Invited or in your list.</p></div>');
             redirect(DOCT_URL."patients/add");
            
        }
        
        $data['patientDtl']=$this->common->userDetail($userId); 
       
        $msg=$this->load->view("email_temp/invi_email", $data, true);
        $subject="My Pain: Invitation";
        $email = $data['patientDtl']['loginEmail'];
        //$email = "sumit@itglobalconsulting.com";
        sendMail($email, "My Pain", $superAdmDtl['supportEmail'], $subject, $msg);
        
        $pname = $data['patientDtl']['firstName'].' '.$data['patientDtl']['lastName'];
        $dname = $data['doctorDtl']['firstName'].' '.$data['doctorDtl']['lastName'];
        $msg="Dear $pname, Dr $dname would like to request your permission for accessing your health record on My Pain Impact";
        
        $mob = $data['patientDtl']['stdCd1'].$data['patientDtl']['phone1']; 
        if($mob){
            $mob=str_replace("+", "", $mob);
            $this->sendMsg($mob, $msg);
	}
        
        $info['invitation'] = 1;
        $info['invitationDate'] = currentDate();
        $this->pdb->delete("mp_patients", "userId='$userId' AND doctorId='$doctorId' AND invitation = '4'");
        $this->pdb->insert("mp_patients", $info);
        setFlash('<div class="alert alert-success"><h3>Invitation send successfully</h3><p>A notification will appear when patient accept your invitation.</p></div>');
        
        
        redirect(DOCT_URL."patients/add");
    }
    
    function sendMsg($mob, $msg) {
        $optional_headers = 'Content-type:application/x-www-form-urlencoded';

        $url = 'http://www.bulksms.co.uk:5567/eapi/submission/send_sms/2/2.0';
        $data = 'username=MypainSMS&password=Apptology2014&message='.urlencode($msg).'&msisdn='.urlencode($mob);

        $params = array('http'      => array(
                'method'       => 'POST',
                'content'      => $data,
                ));
        if ($optional_headers !== null) {
                $params['http']['header'] = $optional_headers;
        }

        $ctx = stream_context_create($params);


        $response = @file_get_contents($url, false, $ctx);
        if ($response === false) {
                print "Problem reading data from $url, No status returned\n";
        }

        return $response;
    }
    
    

}
