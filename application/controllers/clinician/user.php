<?php
/** For Doctor **/
class User extends ParentClinicianController {
	function __construct() {
		parent::__construct();
		$this->load->model('clinician/User_model','user');
	}
	
	/** **/
	function profile() {
		if($_POST)	{
			$data=arrayTrim($_POST);
			$dtl=$this->loggedData();
			$data['id']=$dtl['id'];
			$err=$this->validateProfile($data);
			if(!$err){
				$data['username']=$data['loginEmail'];
				if($_FILES['image']['name'])
					$data['image']=$this->uploadUserImage($data);
				
				$status=$this->user->updateProfile($data);
				if($status){
					$userDtl=$this->common->userDetail($data['id']);
					setSession(USR_SESSION_NAME, $userDtl);
					
					setFlash('<div class="alert alert-success" hide="20000">Saved successfully.</div>');
					redirect(DOCT_URL."myaccount");
				}
			}
			$data['errors']=$err;
			$data['dtl']=$_POST;
			$data['dtl']['phone1']=$_POST['stdCode1'].$_POST['phone1'];
		}
		
		$data['page_title']='My Account | My Pain';
		$this->load->getView('clinician/profile', $data);
	}
	
	function validateProfile($data) {
		$errors=array();
		$errors['firstName']		=!$data['firstName']?"Please enter name":'';
		
		if($data['loginEmail']){
			if(!isEmail($data['loginEmail']))
				$errors['loginEmail']="Invalid Email-ID";
			else if($this->common->isEmailExists($data['loginEmail'], USER_ID))
				$errors['loginEmail']="This Email-ID already exists";
		}
		
		$errors['phone1']		=!$data['phone1']?"Please enter contact number":'';
		
		if($filename=$_FILES['image']['name']){
			if(!checkImageExt($filename))
				$errors['image']='Invalid image (Only .jpg, .jpeg, .png and .gif file allowed)';
		}
			
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	function uploadUserImage($data) {
		$this->load->library("Image");
		$dir="assets/uploads/user_images/";
		$old=$this->pdb->singleVal("mp_users", "id='{$data['id']}'", "image");
		delFile($dir.$old);
		$filename=renameFileIfExist($dir, $_FILES['image']['name']);
		$this->image->resize($_FILES['image']['tmp_name'], $dir.$filename, 100);
		return $filename;
	}
	
	function changePassword() {
		if($_POST){
			$msg=$this->validateChangePassword($_POST);
			if(!$msg['err']) {
				$d=$this->loggedData();
				$_POST['id']=$d['id'];
				$status=$this->user->changePassword($_POST);
				if($status){
					$dtl=$this->common->userDetail($d['id']); 
					setSession(USR_SESSION_NAME, $dtl);
					
					$dtl['newpass']=$_POST['password'];
					$msg=$this->load->view("email_temp/change_password", $dtl, true);
					sendMail($dtl['loginEmail'], "", "", "Password Change", $msg);
					
					setFlash('<div class="alert alert-success" hide="20000">Password changed successfully.</div>');
					redirect(DOCT_URL."changePassword");
				}
			}
			else
				$data['errors']=$msg['errors'];
		}
		
		$data['page_title']='Change Password | My Pain';
		$this->load->getView('clinician/change_password',$data);
	}
	
	function validateChangePassword($data) {
		$err=false;
		if(!trim($data['password'])) {
			$inf['errors']['password']='Please enter password!';
			$err=true;
		}
		
		if(!trim($data['repassword'])) {
			$inf['errors']['repassword']='Please re-enter password!';
			$err=true;
		}
		
		if(trim($data['password']) && trim($data['repassword']) && $data['password']!=$data['repassword']){
			$inf['errors']['repassword']='Password mismatch!';
			$err=true;
		}
		
		$inf['err']=$err;
		return $inf;
	}
	
	/** **/
	function myForms() {
		$ddtl=loggedUserData();
		$data['cats']=$this->pdb->select("mp_form_cats");
		$data['list']=$this->common->doctorForms(USER_ID);
		$data['hosForms']=$this->common->hospitalForms($ddtl['parentId']);
		$data['allForms']=$this->common->listAllForms();
		$data['page_title']='Treatment Response Forms | My Pain';
		$this->load->getView('clinician/form/my_forms', $data);
	}
	
	function contactMyPainSupport() {
		if($_POST){
			$data=arrayTrim($_POST);
			$data['errors']=$this->validateSupport($data);
			if(!$data['errors']){
				$doctDtl=loggedUserData();
				$superAdmDtl=$this->common->superAdminDetail();
				$data['name']=$doctDtl['fullName'];
				$msg=$this->load->view("email_temp/contact_mypain_support_email", array('dtl'=>$data), true);
				$subject=$data['subject']?$data['subject']:'My Pain Support';
				sendMail($superAdmDtl['supportEmail'], $doctDtl['fullName'], $data['loginEmail'], $subject, $msg);
				
				setFlash('<div class="msg1" hide="20000">Your message has been sent successfully.</div>');
				redirect(DOCT_URL."user/contactMyPainSupport");
			}
		}
		
		$data['page_title']='Contact Support | My Pain';
		$this->load->getView('clinician/support_mail', $data);
	}
	
	function validateSupport($data) {
		$errors=array();
		$errors['loginEmail']		=!$data['loginEmail']?"Please enter Email-ID":'';
		if($data['loginEmail'] && !isEmail($data['loginEmail']))
			$errors['loginEmail']="Please enter a valid Email-ID";
		
		$errors['subject']			=!$data['subject']?"Please enter subject":'';
		$errors['message']			=!$data['message']?"Please enter message":'';
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	
	/** Dashboard **/
	function dashboard() {
		$data['noOfPoorRes']=$this->user->resWisePatients('P');
		$data['noOfAvgRes']=$this->user->resWisePatients('A');
		$data['noOfGoodRes']=$this->user->resWisePatients('G');
		
		$data['NPatients']=$this->user->noOfPatientTreatOptWise();
		$data['analysInfo']=$this->user->analysisInfo();
                $data['firstLastDates']=$this->common->csqFirstLastResDates(USER_ID);
                
		
		$data['page_title']='Dashboard | My Pain';
		$this->load->getView('clinician/user/dashboard', $data);
	}
}
