<?php

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('common_helper');
        $this->load->helper('func_helper');
        $this->load->helper('api_helper');
        $this->load->helper('mpform_helper');
        $this->load->model('User_api', 'user');
    }

    function userLogin() {
        $this->user->login();
    }
    
    function userLogout() {
        $this->user->logout();
    }

    function userDetail() {
        $this->user->userDetail();
    }

    function updateProfile() {
        $this->user->updateProfile();
    }

    function changePassword() {
        $this->user->changePassword();
    }

    function forgotPass() {
        $this->user->forgotPass();
    }

    function userGuide() {
        $this->user->userGuide();
    }

    function disclaimer() {
        $this->user->disclaimer();
    }

    function responseForms() {
        $this->user->responseForms();
    }

    function formQuestions() {
        $this->user->formQuestions();
    }

    function submitForm() {
        $this->user->submitForm();
    }

    function faq() {
        $this->user->faq();
    }

    function myDoctors() {
        $this->user->myDoctors();
    }

    function test() {
        $a = array('userId' => 16, 'formId' => 1, 'doctorId' => 14, 'nextResDueDate' => '2014-11-10',
            'response' => array(array('questionId' => 3, 'optionId' => array(6, 7, 9)), array('questionId' => 4, 'optionId' => array(3, 5))));
        //pr($a);
        //pr(json_encode($a));
        pr(json_decode('[{"optionId":["369"],"questionId":"63"},{"optionId":["371"],"questionId":"64"},{"optionId":["373"],"questionId":"65"},{"optionId":["375"],"questionId":"66"},{"optionId":["378"],"questionId":"67"},{"optionId":["380"],"questionId":"68"},{"optionId":["382"],"questionId":"69"},{"optionId":["384"],"questionId":"70"},{"optionId":["390"],"questionId":"71"}]', true));
    }

}
