<?php
class Message extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Message_model', 'msg');
	}
	
	function index() {
		show_404();
	}
	
	function inbox($p=1) {
		$data=$this->msg->recievedMsg($p, PAGE_SIZE);
		
		if(USER_TYPE=='P')
			$data['users']=$this->common->doctorsOfPatient();
		else
			$data['users']=$this->common->listPatients();
			
		$data['page_title']='Inbox | My Pain';
		$this->load->getView("common/message/recieved", $data);
	}
	
	function sent($p=1) {
		$data=$this->msg->sentMsg($p, PAGE_SIZE);
		
		if(USER_TYPE=='P')
			$data['users']=$this->common->doctorsOfPatient();
		else
			$data['users']=$this->common->listPatients();
		
		$data['page_title']='Sent Messages | My Pain';
		$this->load->getView("common/message/sent", $data);
	}
	
	function sendMsg($flg=0,$urlfunc=0,$isNew=0) { 
		$data=arrayTrim($_POST);
		if(!$data)
			return;
                $data['isNew']=1;
                if($urlfunc==='inbox'){
                    $data['isNew']=0;
                 }
                 else{
                    $data['isNew']=1;
                 }
                if(!empty($isNew))
                    $data['isNew']=$isNew;
		$data['fromId']=USER_ID;
		$data['message']=html_entity_decode($data['message']);
		$data['created']=currentDate();
		if($this->msg->sendMsg($data) && !$flg)
			echo 1;
	}
	
	function read($id=0, $urlfunc='inbox', $type=0) { 
		if(!$id)
                    show_404();
		$id=decode($id);
		
		$data['dtl']=$this->msg->detail($id);
		if(!$data['dtl'])
			show_404();
			
                //pr($data['dtl']);pr($_POST);return;
		if($_POST['sendMsg']){ 
                    if($type==='sent'){
                        if($data['dtl'][0]['toId']!=$_POST['toId'])
                            $isNew=1;
                    }else if($type==='rcv'){
                        if($data['dtl'][0]['fromId']!=$_POST['toId'])
                            $isNew=1;
                    }
                    $last_insert_id=$this->sendMsg(1,'inbox',$isNew);

                    //$this->pdb->update("mp_messages", $inf, "id=:id", array(':id'=>$last_insert_id));
                    setFlash('<div class="alert alert-success">Message sent successfully.</div>');
                    redirect(URL."message/read/".encode($id)."/$urlfunc/$type");
		}
		
		
			
		if(USER_TYPE=='P')
			$data['users']=$this->common->doctorsOfPatient();
		else
			$data['users']=$this->common->listPatients();
			
		$data['urlfunc']=$urlfunc;
		$data['page_title']=$data['dtl']['subject'].' | My Pain'; //echo USER_ID;pr($data);exit;
		$this->load->getView("common/message/read", $data);
	}
	
	function delete($id=0, $urlfunc='inbox') {
		if(!$id)
			show_404();
		$id=decode($id);
		
		$inf=$_GET;
		$this->pdb->update("mp_messages", $inf, "id=:id", array(':id'=>$id));
		setFlash('<div class="alert alert-success">Message deleted successfully.</div>');
		redirect(URL."message/".$urlfunc);
	}
        /* Mark message important */
        function star($id=0, $urlfunc='inbox') {
		if(!$id)
			show_404();
		echo $id=decode($id);
		
		$inf=$_GET;
                echo $q="update mp_messages set `star`= NOT `star` where id=$id";
		$this->pdb->query($q);
		//setFlash('<div class="alert alert-success">Message deleted successfully.</div>');
		//redirect(URL."message/".$urlfunc);
	}
        
        function test(){
            $this->msg->sendMsg();
        }
        
}
