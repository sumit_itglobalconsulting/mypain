<?php
class Common extends MY_Controller {
	function __construct() {
		parent::__construct();
	}
	
	function error404() {
		$this->load->view('layouts/error404');
	}
	
	function formPreview($formId) 
        {
            if(!$formId) show_404();

            $formId=decode($formId);

            if(!$data['formDtl']=$this->common->formDetail($formId)){
                    //show_404();
            }

            $data['ques']=$this->common->ques($formId);
            $data['patientDtl']=$this->common->userDetail(USER_ID);

            $data['titleH']='Preview';
            $data['page_title']="Response {$data['formDtl']['formName']} | My Pain";
            $this->load->getView("patient/form/response", $data);
	}
	
	
	/** Report **/ 
	function reports($userId='', $doctorId=0, $return=false, $share=0) {
                if($share)
                    $share = decode($share);
		if($doctorId)
			$doctorId=decode($doctorId);
			
		if($userId){
			$userId=decode($userId);
			$userId=addslashes($userId);
		}
		else
			$userId=USER_ID;
			
		if(USER_TYPE=='D'){
			$doctorId=USER_ID;
		}
                
                $totDoctor = $this->common->getTotalDoctor($userId);
                if($totDoctor > 1)
                     $doctorId = 0;
                
                if($share == 1)
                    $doctorId = 0;
                
                
                    
			
		if(USER_TYPE=='D'){
			$data['dtl']=$this->common->userDetail($userId, $doctorId);
		}
		else
			$data['dtl']=$this->common->userDetail($userId); 
                
                
		
		$data['overallRes']=$this->common->getOverallScore($userId, $doctorId, $share);
		$data['feels']=$this->common->getFeelScore($userId);
		
		$data['patientForms']=$this->common->patientForms($userId, $doctorId, true, true);
		
		$data['firstLastDates']=$this->common->firstLastResDates($userId);
		
		/** PGHI **/
		$data['pghi']=false;
		if($pghi=$this->common->isFormSubmited($userId, 7)){
			$data['pghiDoctorId']=$pghi['doctorId'];
			if(!$doctorId)
				$data['pghi']=true;
			else if($pghi['doctorId']==$doctorId)
				$data['pghi']=true;
		}
		
		if($return)
			return $data;
		
		
		$data['patientId']=$userId;
		$data['doctorId']=$doctorId;
		
		$data['page_title']='Reports | My Pain'; //echo '<pre>';print_r($data);die;
		$this->load->getView('report/patient-report', $data);
                //$this->load->view('report/patient-report', $data);
	}
        
        /** Report for start back **/
        function star_back_risk($formId,$doctorId,$userId,$trend=false){
           $score=$this->common->star_back_risk($formId,$doctorId,$userId,$trend);
           if(!$trend){
                if($score['ts'] <=3){
                    $risk="Low risk";
                    $class="boxGoodRes";
                    $tooltip="Recommendations
- 30-min assessment
- Physical exam
- Subjective history
- Self management
- Advice sheet
- Local exs venues
- 15-min DVD";
                }    
                elseif($score['ts'] >=4){
                    if($score['sc'] <=3){
                        $risk="Medium risk";
                        $class="boxAvgRes";
                        $tooltip="Recommendations
- 45 min assessment & up to 6 x 30-min Tx sessions
- Promote self-mgt
- Advice info (written)
- Exs to increase function
- Manual therapy
- RTW advice
- Pain medication compliance";
                    }            
                    else {
                        $risk="High risk";
                        $class="boxPoorRes";
                        $tooltip="Recommendations
- 60 min assessment & up to 6 x 45-min Tx sessions
- Physical Tx as per medium risk group
- CBT approach to reduce disability and pain, improve psychological functioning and enable the patient to manage ongoing and/or future episodes.";
                    }        
                }
                $result['risk']=$risk;
                $result['class']=$class;
                $result['tooltip']=$tooltip;
                return $result;
           }else{
               return $score;
           }    
           
        }
        
        /** Trending report for gppaq **/
        function gppaq_trend($formId,$doctorId,$userId){
            $score=$this->common->gppaq_trend($formId,$doctorId,$userId);
            return $score;
        }
        /** End of gppaq**/
	
	/** Chart Report **/
	function chartReport($userId=USER_ID, $doctorId=0) {
                //print_r($_POST);exit;
               
		$data=$_POST;
//                $data =  array('fromDate' => '29 Aug 2014',
//                            'toDate' => '29 Dec 2014',
//                            'doctorId' => array
//                                (
//                                    6 => 18,
//                                    3 => 18,
//                                    4  => 18,
//                                    8 => 18,
//                                    9 => 18,
//                                    1 => 18,
//                                    2 => 18,
//                                ),
//
//                            'formId' => Array
//                                (
//                                    0 => 8
//                                )
//                        );
		if(!$data['formId']){
			echo '<div class="red" align="center"><br>Please select at least one form</div>';
			return;
		}
			
		$data=arrayUrlDecode($data);
		
		$from	=strtotime($data['fromDate']);
		$to	=strtotime($data['toDate']);
		
		if($to<$from){
			echo '<div class="red" align="center"><br>To Date must be greater than From Date</div>';
			return;
		}
		$dif=floor(($to-$from)/3600/24/30)+1;
		if($dif>12){
			echo '<div class="red" align="center"><br>Range of dates must be equal to or smaller than 1 Year</div>';
			return;
		}
		
		$reports=$this->common->chartReports($data['formId'], $from, $to, $userId, $doctorId); 
		
		if(!$reports){
			echo '<div class="red" align="center"><br>No Data Found</div>';
			return;
		}
		
		$data['reports']=$reports;
		$this->load->view("report/patient-chart-report", $data);
	}
	
	function getPateintCalendar($userId=USER_ID, $return=false) {
		$pdocts=$this->common->doctorsOfPatient($userId);
		$pforms=$this->common->patientForms($userId);
		
		$calArr=array();
		foreach($pdocts as $pd){
			$calArr[]=array('title'=>$pd['fullName'], 'start'=>$pd['followUpDate'], 'className'=>'EColor1');
			
			if($pd['followedDates']){
				$dates=explode(",", $pd['followedDates']);
				foreach($dates as $dd){
					$dd=date('Y-m-d', strtotime($dd));
					$calArr[]=array('title'=>"Follow up done by ".$pd['fullName'], 'start'=>$dd, 'className'=>'EColor3');
				}
			}
		}
		
		foreach($pforms as $pd){
			$show=true;
			$css='EColor2';
			$submitted=$this->common->isFormSubmited(USER_ID, $pd['formId']);
			
			if($submitted && $pd['resFreq']=='O'){
				$show=false;
			}
			else if(strtotime($pd['nextResDueDate'])<strtotime(date('Y-m-d'))){
				$css='EColor4';
			}
			
			if($show){
				$calArr[]=array('title'=>formName($pd['formId']), 'start'=>$pd['nextResDueDate'], 'className'=>$css, 'tip'=>$pd['formTitle']);
			}
		}
		
		if($return)
			return $calArr;
			
		echo json_encode($calArr);
	}
	
	
	/** Export report to PDF **/
	function exportReport($userId='', $doctorId=0) {
            $data=$this->reports($userId, $doctorId, true);
            if($_POST['exType']=='P'){
                    $filename="mypain-patient-report-on-".date('Y-m-d-H-i-s');

                    $content=$_POST['pdf'];
                    $content=preg_replace('@<script[^>]*?>.*?</script>@si', '', $content);
                    file_put_contents("assets/temp/$filename.html", $content);
                    $client = pdfcrowd();
                    $pdf = $client->convertFile(FCPATH."assets/temp/$filename.html");
                    //$pdf = $client->convertUri(URL."assets/temp/$filename.html");

                    header("Content-Type: application/pdf");
                    header("Cache-Control: no-cache");
                    header("Accept-Ranges: none");
                    header("Content-Disposition: attachment; filename=\"$filename.pdf\"");
                    echo $pdf;
                    return;
            }

            if($_POST['exType']=='E'){
                    $this->exportReportToExcel($data);
                    return;
            }

            $data['page_title']='Export Report | My Pain';
            $this->load->getView('report/patient-report-export', $data, "export");
	}
	
	function exportReportToExcel($data) {
		if($_POST['resImg']){
			$data['resImg']="resimg".USER_ID.".png";
			delFile("assets/temp/".$data['resImg']);
			
			$img=trim(str_replace("data:image/png;base64,", "", $_POST['resImg']));
			$img = base64_decode($img);
			$result = imagecreatefromstring($img);
			imagepng($result, "assets/temp/".$data['resImg']);
			$this->load->library("image");
			$this->image->resize("assets/temp/".$data['resImg'], "assets/temp/".$data['resImg'], 600);
		}
		
		$this->load->view("report/excel-export", $data);
		return;
	}
	
	function exportPghiForm($userId=0, $doctorId=0) {
		$formId=7;
		$userId=decode($userId);
		$doctorId=decode($doctorId);
		
		$data['resDtl']=$this->common->responseDetail($formId, '', $doctorId, $userId, '', true);
		//$data['resDates']=$this->common->responseDates($formId, $doctorId, $userId);
		//$data['ques']=$this->common->ques($formId);
		
		$data['dtl']=$this->common->userDetail($userId);
		
		$this->load->getView("report/pghi-export", $data, 'export');
		return;
	}
	
	/** Notification Settings **/
	function notificationSetting() {
		if($data=$_POST){
			if(!$data['notificationEmail'])
				$data['notificationEmail']=0;
				
			if(!$data['notificationMsg'])
				$data['notificationMsg']=0;
				
			if($this->pdb->update("mp_users", $data, "id=".USER_ID)){
				setFlash('<div class="alert alert-success">Saved successfully</div>');
			}
			redirect(URL."common/notificationSetting");
		}
		
		$data['dtl']=$this->common->userDetail(USER_ID);
		$data['page_title']="Notification Settings | My Pain";
		$this->load->getView("common/notification", $data);
	}
	
	
	/** Tutorial / Disclaimer**/
	function tutorial() {
		$data['page_title']="Tutorial | My Pain";
		switch(USER_TYPE){
			case 'P':
				$vew="patient";
			break;
			
			case 'D':
				$vew="doctor";
			break;
			
			case 'H':
				$vew="hospital";
			break;
		}
		
		$this->load->getView("common/tutorial/".$vew, $data);
	}
	
	function privacyPolicyDisclaimer() {
		$data['dtl']=$this->common->misc();
		$data['page_title']="Privacy Policy & Disclaimer | My Pain";
		$this->load->getView("common/tutorial/privacy_policy_dis", $data);
	}
	
	/** Ics Calendar Urls **/
	function patientCal($userId=0){
		//$userId=decode($userId);
		$data['cal']=$this->getPateintCalendar($userId, true);
		
		$this->load->view("common/patient-cal", $data);
	}
	
	
	
	/** Weemo Chat Notification **/
	function gotoWeemo($requestFrom, $doctorId, $patientId) {
		$inf['requestFrom']=$requestFrom;
		$inf['doctorId']=$doctorId;
		$inf['patientId']=$patientId;
		$inf['created']=currentDate();
		
		if(USER_TYPE=='P'){
			$caller="MP".$patientId;
			$callee="MP".$doctorId;
		}
		else{
			$caller="MP".$doctorId;
			$callee="MP".$patientId;
		}
		
		$this->pdb->insert("mp_chat_notification", $inf);
		redirect(URL."common/weemoScreen/".encode($doctorId)."/".encode($patientId)."/C/".$caller."/".$callee);
	}
	
	function weemoScreen($doctorId, $patientId, $caller='', $callerId='', $calleeId='') {
		$data['doctorId']=decode($doctorId);
		$data['patientId']=decode($patientId);
		
		$data['callerId']=$callerId;
		$data['calleeId']=$calleeId;
		
		$callerUserId=str_replace('MP', '', $callerId);
		//$callerUserId=$callerId;
		$calleeUserId=str_replace('MP', '', $calleeId);
		//$calleeUserId=$calleeId;
		
		$data['caller']=$this->pdb->singleVal("mp_users", "id='$callerUserId'", "firstName");
		$data['callee']=$this->pdb->singleVal("mp_users", "id='$calleeUserId'", "firstName");
		
		$data['page_title']="Video Confrenshing: ".$data['caller']." & ".$data['callee']." | My Pain Impact";
		if($caller)
			$this->load->getView("common/weemoCaller", $data, 'export');
		else
			$this->load->getView("common/weemoCallee", $data, 'export');
	}
	
	function getChatNoti() {
		$time=date('Y-m-d H:i:s', strtotime("-30 minutes", time()));
		$sec30=date('Y-m-d H:i:s', strtotime("-30 seconds", time()));
		$cond="A.created>='$time' AND A.connected=0";
		if(USER_TYPE=='P'){
			$cond.=" AND ( (A.requestFrom='D' AND A.status='A') OR (A.requestFrom='P' AND A.status='C' AND A.created>'$sec30'))";
		}
		else{
			$cond.=" AND ( (A.requestFrom='P' AND A.status='A') OR (A.requestFrom='D' AND A.status='C' AND A.created>'$sec30'))";
		}
		
		$q="select DISTINCT A.id, A.status, A.patientId, A.doctorId, IF(A.requestFrom='P', P.firstName, D.firstName) userName, 
			IF(A.requestFrom='P', D.firstName, P.firstName) toUserName from mp_chat_notification A 
			JOIN mp_users P ON (A.patientId=P.id) JOIN mp_users D ON (A.doctorId=D.id) where $cond order by A.id DESC LIMIT 0,10";
			
		$data['list']=$this->pdb->query($q);
		$this->load->view("common/chat-noti", $data);
	}
	
	function cancelChat($id) {
		$this->pdb->update("mp_chat_notification", array('status'=>'C', 'created'=>currentDate()), "id=$id");
	}
	
	/** Set Online Status **/
	function setOnlineStatus($status) {
		$this->pdb->update("mp_users", array('onlineStatus'=>$status), "id=".USER_ID);
		$userDtl=$this->common->userDetail(USER_ID);
		unset($userDtl['bodymap']);
		setSession(USR_SESSION_NAME, $userDtl);
	}
        function getOnlineStatus(){
            if(USER_TYPE=='P' || USER_TYPE=='D'){
                if(USER_TYPE=='P'){
                        $usersList=$this->common->doctorsOfPatient(USER_ID, true);
                        $title="Online Doctors";
                        $iconclass='fa fa-user-md';
                        $link=URL."common/gotoWeemo/P/";
                }
                else{
                        $usersList=$this->common->listPatients(USER_ID, true);
                        $title="Online Patients";
                        $iconclass='fa fa-user';
                        $link=URL."common/gotoWeemo/D/";
                }
                $noOfOnline=count($usersList);
                if($noOfOnline){
                     $status = '<div class="btn-group">';
                        $status .= '<button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">'; 
                            $status .= '<i class="'.$iconclass.'"></i>'; 
                            $status .= '<span class="badge">'.$noOfOnline.'</span>'; 
                        $status .= '</button>'; 
                        $status .= '<div class="dropdown-menu dropdown-menu-head pull-right">';
                            $status .= '<h5 class="title">'.$title.'</h5>';
                            $status .= '<div style="max-height:400px; overflow:auto">';
                                $status .= '<ul class="dropdown-list gen-list">';
                                    $status .= '<li class="new">';
                                                foreach($usersList as $u){
                                                    $status .= '<span>';
							$status .= '<span class="name">';
                                                            $status .= '<div class="pull-right">';
                                                            switch($u['onlineStatus']){
                                                                case 'A':
                                                                                                                                                                                                            $status .= '<i class="fa fa-circle text-success"></i> Available';
																	break;
																	
                                                                                                                                                                                                            case 'B':
                                                                                                                                                                                                                    $status .= '<i class="fa fa-circle text-danger"></i> Busy';
                                                                                                                                                                                                            break;

                                                                                                                                                                                                            case 'N':
                                                                                                                                                                                                                    $status .= '<i class="fa fa-circle text-default"></i> Not Available';
                                                                                                                                                                                                            break;
                                                            }
                                                            $status .= '</div>';
                                                        
							    $status .= '<a href="'.$link.$u['doctorId']."/".$u['patientId'].'" target="_blank">'.$u['fullName'].'</a>';
								$status .= '</span>';
                                                            $status .= '</span>';
                                                }
                                 echo   $status .= '</li>
									</ul>
								</div>
							</div>
						</div></li>';
                                   
                }
            }                                     
        }
        
        function countUnreadMsg() {
            $countUnreadMsg = countUnreadMsg();
            echo '<div class="btn-group" title="Messages">';
						echo '<button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown" page-link="'.URL.'message/inbox">';
							echo '<i class="glyphicon glyphicon-envelope"></i>';
                                                        if($countUnreadMsg>0)
                                                            echo '<span class="badge">'.$countUnreadMsg.'</span>';
						echo '</button>';
					echo '</div>';
        }
        
        /** Chart Report **/
	function csqChartReport($doctorId=0) {
                //print_r($_POST);exit;
               
		$data=$_POST;
//                $data =  array('fromDate' => '29 Aug 2014',
//                            'toDate' => '29 Dec 2014',
//                            'doctorId' => array
//                                (
//                                    6 => 18,
//                                    3 => 18,
//                                    4  => 18,
//                                    8 => 18,
//                                    9 => 18,
//                                    1 => 18,
//                                    2 => 18,
//                                ),
//
//                            'formId' => Array
//                                (
//                                    0 => 8
//                                )
//                        );
		
			
		$data=arrayUrlDecode($data);
		
		$from	=strtotime($data['fromDate']);
		$to	=strtotime($data['toDate']);
		
		if($to<$from){
			echo '<div class="red" align="center"><br>To Date must be greater than From Date</div>';
			return;
		}
		$dif=floor(($to-$from)/3600/24/30)+1;
		if($dif>12){
			echo '<div class="red" align="center"><br>Range of dates must be equal to or smaller than 1 Year</div>';
			return;
		}
		
		$reports=$this->common->csqChartReports(array(8), $from, $to, $userId, $doctorId); 
		
		if(!$reports){
			echo '<div class="red" align="center"><br>No Data Found</div>';
			return;
		}
		
		$data['reports']=$reports;
		$this->load->view("report/patient-chart-report", $data);
	}
        
}
?>