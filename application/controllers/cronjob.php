<?php
class Cronjob extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model("comm");
	}
	
	function index() {
		if($_GET['test']==1){
			$this->responseDue();
		}
		
		$h=date('H'); $m=date('i')*1;
		
		if($h=='08' && $m<12){
			$this->setFollowUpAndNextResponseDate();
		
		
			$this->responseDue();
			$this->followUpDueDoctors();
		}
	}
	
	function setFollowUpAndNextResponseDate() {
		$patients=$this->pdb->query("select id, followUpDate, followUpFreq, followUpDone from mp_patients where discharged=0");
		if(!$patients) return;
			
		foreach($patients as $dtl){
			if(time()>strtotime($dtl['followUpDate']) && $dtl['followUpDone']==1){
				$dtl['followUpDate']=getNextDate($dtl['followUpDate'], $dtl['followUpFreq']);
				if($dtl['followUpFreq']!='O')
					$dtl['followUpDone']=0;
				else
					$dtl['followUpDone']=1;
				$this->pdb->update("mp_patients", $dtl, "id={$dtl['id']}");
			}
		}
		
		$forms=$this->pdb->query("select id, userId, formId, nextResDueDate, resFreq from mp_patient_forms where userId IN 
		(select userId from mp_patients where discharged=0)");
		if(!$forms)	return;
		
		foreach($forms as $dtl){
			$id=$this->pdb->singleVal("mp_response", "userId='{$dtl['userId']}' AND formId='{$dtl['formId']}'", "id");
			if($id && strtotime(date('Y-m-d'))>=strtotime(date('Y-m-d', strtotime($dtl['nextResDueDate'])))){
				$dtl['nextResDueDate']=getNextDate($dtl['nextResDueDate'], $dtl['resFreq']);
				$this->pdb->update("mp_patient_forms", $dtl, "id={$dtl['id']}");
			}
		}
	}
	
	
	function responseDue() {
		$rs=$this->comm->resDuePatients();
		
		if(!$rs)
			return;
		
		//pr($rs);
		
		foreach($rs as $u){
			if($u['notificationEmail']){
				$msg=$this->load->view("cronemail/res-due-patients", $u, true);
				$emails=array($u['loginEmail'], 'satyendra.yadav@itglobalconsulting.com');
				sendMail($emails, "", "", "My Pain Impact: Response Due", $msg);
			}
			
			$msg="Dear ".$u['name'].", Response submission is due for these forms: ".$u['forms'];
			
			if($u['deviceToken']){
				pushNotification($u['deviceToken'], $msg);
			}
			
			if($u['notificationMsg'] && $u['mob']){
				$mob=str_replace("+", "", $u['mob']);
				$this->sendMsg($mob, $msg);
			}
		}
	}
	
	function followUpDueDoctors() {
		$rs=$this->comm->followUpDueDoctors();
		if(!$rs)
			return;
		
		foreach($rs as $u){
			if(!$u['notificationEmail']) continue;
			
			$msg=$this->load->view("cronemail/followup-due-doctors", $u, true);
			$emails=array($u['doctorEmail'], 'satyendra.yadav@itglobalconsulting.com');
			sendMail($emails, "", "", "My Pain Impact: Follow Up Due", $msg);
		}
	}
	
	function test() {
		$url = 'http://www.bulksms.co.uk:5567/eapi/submission/send_sms/2/2.0';
		$msisdn = '918447017434';
		
		//$response = $this->sendMsg("918447017434", "Hello My Pain");
		$u=$this->pdb->singleRow("mp_users", "id=16");
		echo pushNotification($u['deviceToken'], "Test notification");
	}
	
	function sendMsg($mob, $msg) {
		$optional_headers = 'Content-type:application/x-www-form-urlencoded';
		
		$url = 'http://www.bulksms.co.uk:5567/eapi/submission/send_sms/2/2.0';
		$data = 'username=MypainSMS&password=Apptology2014&message='.urlencode($msg).'&msisdn='.urlencode($mob);
		
		$params = array('http'      => array(
			'method'       => 'POST',
			'content'      => $data,
			));
		if ($optional_headers !== null) {
			$params['http']['header'] = $optional_headers;
		}
	
		$ctx = stream_context_create($params);


		$response = @file_get_contents($url, false, $ctx);
		if ($response === false) {
			print "Problem reading data from $url, No status returned\n";
		}
	
		return $response;
	}
}
