<?php
class User extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User_model','user');
	}
	
	function login() {
		$this->redirectLogged();
		
		if($_POST){
			$data=arrayTrim($_POST);
                        /** Last Modified Date: 20 DEC 2014 7:09 PM 
                         *  Modified By: Php Developer 2 
                         */
                        if($this->user->confirmIPAddress($_SERVER['REMOTE_ADDR'])){
                            setFlash('<div class="alert alert-danger">You have tried more than 3 time with wrong username/password, your access is blocked for 4 hours!</div>');
                            redirect(URL);
                        }
                        /**
                         * End Modified
                         */
			if(!$data['errors']=$this->validateLogin($data)){
				$dtl=$this->user->login($data);
				
				if($dtl && $dtl['status']==0){
					setFlash('<div class="alert alert-danger">Your account is not active!</div>');
					redirect(URL);
				}
				
				$userDtl=$this->common->userDetail($dtl['id']);
                                //pr($userDtl);exit;	
                                $this->checkExpire($userDtl);
                                   
                                   
                            
				if($userDtl){
					unset($userDtl['bodymap']);
                                        
					setSession(USR_SESSION_NAME, $userDtl);
					$this->pdb->update("mp_users", array('loggedIn'=>'1', 'lastActTime'=>currentDate()), "id=".$userDtl['id']);
					
					if(strpos($_SERVER['HTTP_HOST'],'localhost')!==false)
						$this->tempCronJob(); /** :) **/
					$this->redirectLogged();
				}
				else{
                                        /** Last Modified Date: 20 DEC 2014 7:09 PM 
                                        *  Modified By: Php Developer 2 
                                        */
                                        $left=3-$this->user->addLoginAttempt($_SERVER['REMOTE_ADDR']);
					setFlash("<div class='alert alert-danger'>Invalid username or password!, You have only $left more attempt left.</div>");
					redirect(URL);
                                        /*
                                         * End Modified
                                         */
				}
			}
		}
		$data['page_title']='Login | My Pain';
		$this->load->getView("login", $data, "layout_login");
	}
	
	function validateLogin($data) {
		$errors=array();
		$errors['username']	=!$data['username']?'Please enter username':'';
		$errors['password']	=!$data['password']?'Please enter password':'';
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	
	function forgotPass() {
		if($data=arrayTrim($_POST)){
			if(!$data['username'])
				$data['errors']['username']="Please enter email-id";
			else if(!$dtl=$this->pdb->singleRow("mp_users", "loginEmail=:email", "*", array(':email'=>$data['username'])))
				$data['errors']['username']="This email-id does not exist in our accounts";
			else{
				$dtl['pass']=time();
				$inf['password']=encryptText($dtl['pass']);
				$this->pdb->update("mp_users", $inf, "id='{$dtl['id']}'");
				$msg=$this->load->view("email_temp/forgotpass", array('dtl'=>$dtl), true);
				 
				sendMail($dtl['loginEmail'], "", "", "Password Recovery", $msg);
				sendMail("satyendra.yadav@itglobalconsulting.com", "", "", "Password Recovery", $msg);
				 
				setFlash('<div class="alert alert-success">A Temporary password has been sent to your email-id</div>');
				redirect(URL."user/forgotPass");
			}
		}
		
		$data['page_title']='Forgot Your Password? | My Pain';
		$this->load->getView("forgot_pass", $data, "layout_login");
	}
	
	
	function logout() {
            unsetSession(USR_SESSION_NAME);
            $this->pdb->update("mp_users", array('loggedIn'=>'0'), "id=".USER_ID);
            $this->redirectNotLogged();
	}
	
	
	/** Hospital Registration **/
	function register() { 
		$superAdmDtl=$this->common->superAdminDetail();
		
		if($_POST){
			$data=arrayTrim($_POST);
			$data['errors']=$this->validateReg($data);
			if(!$data['errors']){
				$data['realPass']=time();
				$data['password']=encryptText($data['realPass']);
				$data['username']=$data['loginEmail'];
				if($_FILES['image']['name'])
					$data['image']=$this->uploadUserImage($data);
				
				$success=$data['userId']=$this->user->registerHospital($data);
				if($success){
                                    $msg=$this->load->view("email_temp/reg_email", array('dtl'=>$data), true);
                                    $subject="My Pain: Registration";
                                    sendMail($data['loginEmail'], "My Pain", $superAdmDtl['supportEmail'], $subject, $msg);

                                    setFlash('<div class="alert alert-success"><h3>Registration completed successfully</h3><p>A confirmation mail with your password has been sent to your Email-ID.</p></div>');
                                    redirect(URL."user/register");
				}
			}
			else{
				$data['dtl']=$_POST;
			}
		}
		
		//$data['plans']=$this->common->listPlans();//echo '<pre>';print_r($data['plans']);
		$data['page_title']='Registration | My Pain';
		$this->load->getView("register", $data, 'layout_login');
	}
	
	function validateReg($data) {
		$errors=array();
		$errors['hospitalName']		=!$data['hospitalName']?"Please enter hospital name":'';
		$errors['regNo']		=!$data['regNo']?"Please enter registration number":'';
		$errors['branchName']		=!$data['branchName']?"Please enter branch name":'';
		$errors['firstName']		=!$data['firstName']?"First name required":'';
		$errors['lastName']		=!$data['lastName']?"Last name required":'';
		$errors['loginEmail']		=!$data['loginEmail']?"Please enter login email":'';
		
		
                if(empty($data['email'])){
                    if($data['loginEmail']){
                            if(!isEmail($data['loginEmail']))
                                    $errors['loginEmail']="Invalid Email-ID";
                            else if($this->common->isEmailExists($data['loginEmail']))
                                    $errors['loginEmail']="This Email-ID already exists";
                    }
                }    
		
		$errors['phone1']		=!$data['phone1']?"Please enter contact number":'';
		$errors['address']		=!$data['address']?"Please enter address":'';
		$errors['city']			=!$data['city']?"Please enter city":'';
		$errors['zipcode']		=!$data['zipcode']?"Please enter zipcode":'';
		$errors['state']		=!$data['state']?"Please enter state":'';
		$errors['country']		=!$data['country']?"Please enter country":'';
		
		if($filename=$_FILES['image']['name']){
			if(!checkImageExt($filename))
				$errors['image']='Invalid image (Only .jpg, .jpeg, .png and .gif file allowed)';
		}
		
		if(!isArrayEmpty($errors))
			return $errors;
	}
	
	function uploadUserImage($data) {
		$this->load->library("Image");
		$dir="assets/uploads/user_images/";
		//$old=$this->pdb->singleVal("mp_users", "id='{$data['id']}'", "image");
		//delFile($dir.$old);
		$filename=renameFileIfExist($dir, $_FILES['image']['name']);
		$this->image->resize($_FILES['image']['tmp_name'], $dir.$filename, 120);
		return $filename;
	}
	
	/** Activate Account **/
	function activateAccount($arg=0){
		if(!$arg)
			show_404();
		
		$arr=explode("~", $arg);
		$userId=decode($arr[0]);
		$loginEmail=urldecode($arr[1]);
                $payment=decode($arr[2]);
		$cond="id= '$userId' AND loginEmail='$loginEmail'";
		$dtl=$this->pdb->singleRow("mp_users", $cond);
               
		if(!$dtl)
			show_404();
		
		$success=true;	
		if($dtl['isActivated']){
			$data['msg']='<div class="errMsg">Link has been expired.</div>';
			$data['expired']=true;
			$success=false;
		}
		
		if($success){
			$inf['status']=1;
			$inf['isActivated']=1;
			if($this->pdb->update("mp_users", $inf, $cond)){
				$data['msg']='<div class="msg1">Your account activated successfuly.</div>';
			}
		}
		
		$data['page_tile']='Account Acivate | My Pain';
                
                if(!empty($payment) && !empty($success)){
                    //$this->autoRegister(encode($userId));
                    redirect(URL.'user/autoRegister/'.encode($userId));
                }    
                else
                    $this->load->getView("activate_acc", $data);
                 
	}
	
		
	/** Temporary Cron Job Work **/
	function tempCronJob() {
		$patients=$this->pdb->query("select id, followUpDate, followUpFreq, followUpDone from mp_patients where discharged=0");
		if(!$patients) return;
			
		foreach($patients as $dtl){
			if(time()>strtotime($dtl['followUpDate']) && $dtl['followUpDone']==1){
				$dtl['followUpDate']=getNextDate($dtl['followUpDate'], $dtl['followUpFreq']);
				if($dtl['followUpFreq']!='O')
					$dtl['followUpDone']=0;
				else
					$dtl['followUpDone']=1;
				$this->pdb->update("mp_patients", $dtl, "id={$dtl['id']}");
			}
		}
		
		$forms=$this->pdb->query("select id, userId, formId, nextResDueDate, resFreq from mp_patient_forms where userId IN (select userId from mp_patients where discharged=0)");
		if(!$forms)	return;
		
		foreach($forms as $dtl){
			$id=$this->pdb->singleVal("mp_response", "userId='{$dtl['userId']}' AND formId='{$dtl['formId']}'", "id");
			if($id && strtotime(date('Y-m-d'))>=strtotime(date('Y-m-d', strtotime($dtl['nextResDueDate'])))){
				$dtl['nextResDueDate']=getNextDate($dtl['nextResDueDate'], $dtl['resFreq']);
				$this->pdb->update("mp_patient_forms", $dtl, "id={$dtl['id']}");
			}
		}
	}
        
        /** Choose Plans **/
        function choosePlans(){
            $planId = $this->uri->segment(3);
            if(!empty($planId) AND base64_decode($planId)==1){
                redirect(URL.'user/register');
            }
            elseif(!empty($planId) AND base64_decode($planId)>1){
                //$this->customerDetail($planid);
                redirect(URL.'user/customerDetail/'.$planId);
            }
            elseif(empty($planId)){
                $data['plans']=$this->common->listPlans();
                //pr($data['plans']);
                $this->load->view("common/choose-plans",$data);
            }
        }
        /**End of Choose Plans **/
        
        /** 
         * Customer Payment
         */
        function customerDetail($planId=0){
            if(!empty($_POST)){
                $data=arrayTrim($_POST);
                    $data['errors']=$this->validateCusDet($data);
                    if(empty($data['errors'])){
                        $this->load->library('sagepay');
                        $data['planId']=  base64_decode($planId);
                        $planDetail=getPlanDetail($data['planId']);
                        $data['ac']=$planDetail['price']*$data['noOfPatient'];
                        $data['vendorTxCode']=  $this->sagepay->getVendorTxCode();
                        $obj = $this->setPayment($data);
                        $this->addCustomer($data);
                        $info['crypt']=$obj->getCrypt();
                        $this->load->getView("payment/sage_payment", $info,"layout_login");
                    }
                    else{
                            $data['dtl']=$_POST;
                    }
            }  
            $data['$planId']=base64_decode($planId);
            $data['price']=$this->common->licenceCost($data['$planId']);
            
            if(empty($_POST) || !empty($data['errors']))
            $this->load->getView("payment/customer_detail",$data,"layout_login");
        }
        
        function setPayment($data){
            //$this->load->library('sagepay');
            $this->sagepay->setCurrency('GBP');
            $this->sagepay->setAmount($data['ac']+$data['ac']*(VAT/100));
            $this->sagepay->setDescription($data['description']);
            $this->sagepay->setCustomerName($data['firstName']." ".$data['lastName']);
            $this->sagepay->setCustomerEMail($data['loginEmail']);
            $this->sagepay->setBillingSurname($data['lastName']);
            $this->sagepay->setBillingFirstnames($data['firstName']);
            $this->sagepay->setBillingCity($data['city']);
            $this->sagepay->setBillingPostCode($data['zipcode']);
            $this->sagepay->setBillingAddress1($data['address']);
            $this->sagepay->setBillingCountry($data['country']);
            $this->sagepay->setDeliverySameAsBilling();
            $this->sagepay->setSuccessURL(URL.'user/successPayment');
            $this->sagepay->setFailureURL(URL.'user/failurePayment');

            $this->sagepay->setApply3DSecure(1);
           
           
            return $this->sagepay;
        }
        function addCustomer($data){
            if(empty($data))
                return;
            
            $data['realPass']=time();
            $data['password']=encryptText($data['realPass']);
            $data['username']=$data['loginEmail'];
            $this->user->registerCustomer($data);
        }
        
        function validateCusDet($data){ 
            $errors=array();
            $errors['firstName']	= empty($data['firstName'])?"First name required":'';
            $errors['lastName']         = empty($data['lastName'])?"Last name required":'';
            $errors['loginEmail']	= empty($data['loginEmail'])?"Email Address Required":'';

            if(!empty($data['loginEmail'])){
                if(!isEmail($data['loginEmail']))
                    $errors['loginEmail']="Invalid Email-ID";
                //else if($this->common->isEmailExists($data['loginEmail']))
                    //$errors['loginEmail']="This Email-ID already exists";
            }

            //$errors['phone1']		=!$data['phone1']?"Please enter contact number":'';
            $errors['address']		=empty($data['address'])?"Please enter address":'';
            $errors['city']             =empty($data['city'])?"Please enter city":'';
            $errors['zipcode']		=empty($data['zipcode'])?"Please enter zipcode":'';
            $errors['state']		=empty($data['state'])?"Please enter state":'';
            $errors['country']		=empty($data['country'])?"Please enter country":'';
            $errors['description']	=empty($data['description'])?"Please enter description":'';

            if(!isArrayEmpty($errors))
                    return $errors;
        }
        
        
        function successPayment(){
            $this->load->library('sagepay');
            if ($_REQUEST['crypt']) {
                $data = $this->sagepay->decode($_REQUEST['crypt']);
                $userDtl=$this->user->paymentHosp($data);
                $userDtl['payment']=1;
                $superAdmDtl=$this->common->superAdminDetail(); 
                $msg=$this->load->view("email_temp/reg_email", array('dtl'=>$userDtl), true);
                $subject="My Pain: Registration";
                sendMail($userDtl['loginEmail'], "My Pain", $superAdmDtl['supportEmail'], $subject, $msg);

                setFlash('<div class="alert alert-success"><h3>Payment completed successfully</h3><p>A confirmation mail with your password has been sent to your Email-ID.</p></div>');
                redirect(URL."user/login");
            }
            
        }
        
        function paymentHosp($data){
            $this->user->paymentHosp($data);
        }
        
        function failurePayment(){
            $this->load->library('sagepay');
            
            $response_array=array();
            $response_array = $this->sagepay->decode($_REQUEST['crypt']);
            if ($response_array['Status'] == "NOTAUTHED")
            {
                    $failure_reason = "You payment was declined by the bank.  This could be due to insufficient funds, or incorrect card details.";
            }
            else if ($response_array['Status'] == "ABORT")
            {
                    $failure_reason =  "You choose to cancel your order on the payment pages.  If you wish to change your order and resubmit it you can do so here. If you have questions or concerns about ordering online, please contact us at <a href='mailto:enquiries@my-pain.co.uk'>enquiries@my-pain.co.uk</a>.";
            }
            else if ($response_array['Status'] == "REJECTED")
            {
                    $failure_reason = "Your order did not meet our minimum fraud screening requirements. If you have questions about our fraud screening rules, or wish to contact us to discuss this, please contact us at <a href='mailto:enquiries@my-pain.co.uk'>enquiries@my-pain.co.uk</a>.";
            }
            else if ($response_array['Status'] == "INVALID" or $response_array['Status'] == "MALFORMED")
            {
                    $failure_reason = "We could not process your order because we have been unable to register your transaction with our payment Gateway. You can place the order over the telephone instead by contact us at <a href='mailto:enquiries@my-pain.co.uk'>enquiries@my-pain.co.uk</a>.";
            }
            else if ($response_array['Status'] == "ERROR")
            {
                    $failure_reason = "We could not process your order because our Payment Gateway service was experiencing difficulties. You can place the order over the telephone instead by contact us at <a href='mailto:enquiries@my-pain.co.uk'>enquiries@my-pain.co.uk</a>.";
            }
            else
            {
                    $failure_reason =  "The transaction process failed. Please contact us with the date and time of your order and we will investigate.";
            }
            
            $response_array['failure_reason'] = $failure_reason;
            
            
            $this->load->getView("payment/payment_failure",$response_array);

        }
         
        function autoRegister($userId) { 
            $this->load->helper('form');
            $userId=decode($userId);
            if(empty($_POST)) {
                $data['dtl']=$this->user->autoRegister($userId);
                if(empty($userId) || empty($data['dtl']))
                    show_404 ();
                else{
                    $id=$this->user->checkHospAlreadyRegi($userId);
                    if(!empty($id)){
                        $userDtl=$this->common->userDetail($userId);
                        setSession(USR_SESSION_NAME, $userDtl);
                        if(strpos($_SERVER['HTTP_HOST'],'localhost')!==false)
                            $this->tempCronJob(); 
			$this->redirectLogged();
                    }
                        
                }
            }
            else{
                $data=arrayTrim($_POST);
                $data['errors']=$this->validateReg($data);
                if( !empty($data['errors']) ) {
                    $data['dtl']=$_POST;
                }else{
                    if($_FILES['image']['name'])
			$data['image']=$this->uploadUserImage($data);
                    $data['userId']=$userId;
                    $this->user->updateAutoRegister($data);
                    $userDtl=$this->common->userDetail($userId);
                    setSession(USR_SESSION_NAME, $userDtl);
                    if(strpos($_SERVER['HTTP_HOST'],'localhost')!==false)
			$this->tempCronJob(); /** :) **/
			$this->redirectLogged();
                }
            }
            $data['dtl']['page_title']='Registration | My Pain';
            $this->load->getView("auto_register", $data, 'layout_login');
           
        }
        
        function hospitalExpireCheck($diff,$plntype){
            if($diff>0){        //plan remaining;
                 if($plntype=="F"){
                     setFlash('<div class="alert alert-danger">Your trail period will expired in '.$diff.' days.</div>');
                 }else{
                     if($diff <=30) {
                      setFlash('<div class="alert alert-danger">Your plan will expired in '.$diff.' days.</div>');
                     }
                         
                 }
             }elseif($diff == 0){ //plan today expired
                 if($plntype=="F"){
                    setFlash('<div class="alert alert-danger">Your trail period will expire today.</div>');
                 }else{
                    setFlash('<div class="alert alert-danger">Your trail period will expire today.</div>');
                 }
             }else{ // plan already expired
                 if($plntype=="F"){
                     setFlash('<div class="alert alert-danger">Your trail period expired, please use below register now link to purchase any plan</div>');
                     redirect(URL);
                 }else{
                     
                 }
             }
        }
        
        function doctorExpireCheck($diff,$plntype){
            if($diff>0){        //plan remaining;
                 if($plntype=="F"){
                     setFlash('<div class="alert alert-danger">Your trail period will expired in '.$diff.' days.</div>');
                 }else{
                    if($diff <=30) {
                         setFlash('<div class="alert alert-danger">Your plan will expired in '.$diff.' days.</div>');
                    }
                 }
             }elseif($diff == 0){ //plan today expired
                 if($plntype=="F"){
                     setFlash('<div class="alert alert-danger">Your trail period will expire today.</div>');
                 }else{
                     setFlash('<div class="alert alert-danger">Your plan will expire today.</div>');
                 }
             }else{ // plan already expired
                 if($plntype=="F"){
                     setFlash('<div class="alert alert-danger">Your trail period expired.</div>');
                     redirect(URL);
                 }else{
                     if($diff > -30){
                         setFlash('<div class="alert alert-danger">Your plan already expired, you can only use the system '.(30+$diff).' days.</div>');
                     }
                 }
             }
        }
        
        function patientExpireCheck($diff,$plntype){
            if($diff>0){        //plan remaining;
                 if($plntype=="F"){
                     
                 }else{
                     
                 }
             }elseif($diff == 0){ //plan today expired
                 if($plntype=="F"){
                    
                 }else{
                     
                 }
             }else{ // plan already expired
                 if($plntype=="F"){
                     setFlash('<div class="alert alert-danger">Your trail period expired.</div>');
                     redirect(URL);
                 }else{
                     if($diff > -30){
                         setFlash('<div class="alert alert-danger">Your hospital plan already expired, you can only use the system '.(30+$diff).' days.</div>');
                     }
                 }
             }
        }
        
        function checkExpire($userDtl){ 
             $planDtl=$this->user->checkExpire($userDtl); 
             $diff=strtotime($planDtl['expiryDate']) - strtotime(currentDate());
             $diff=$diff/(60*60*24);
             $diff=  intval($diff);
             
             switch($userDtl['type']){
                    case 'H':
                            return $this->hospitalExpireCheck($diff,$planDtl['type']);
                    break;

                    case 'D':
                            return $this->doctorExpireCheck($diff,$planDtl['type']);
                    break;

                    case 'P':
                            return  $this->patientExpireCheck($diff,$planDtl['type']);
                    break;

                    
                    }
          
             
        }
        
         function showCustomerDetail(){
            $email = $this->input->post('email');
            if(!empty($email))
                $rs=$this->user->showCustomerDetail($email);
                $rs=!empty($rs)?(json_encode($rs)):$rs;
                echo $rs;
        }
        
        function docotorInvi($doctorId,$patientId){
            $doctorId=decode($doctorId);
            $patientId = decode($patientId);
            if(!$doctorId OR !$patientId){
                show_404();
                
            }
            $cond = "userId='$patientId' AND doctorId='$doctorId'";
            $dtl=$this->pdb->singleVal("my_patients", $cond, 'id');
            $success=true;	
            if($dtl){
                $data['msg']='<div class="errMsg">Link has been expired.</div>';
                $data['expired']=true;
                $success=false;
            }
		
            if($success){
                    $inf['invitation']=1;
                    $inf['isActivated']=1;
                    if($this->pdb->update("mp_users", $inf, $cond)){
                            $data['msg']='<div class="msg1">Your account activated successfuly.</div>';
                    }
            }
		
		$data['page_tile']='Account Acivate | My Pain';
                
                if(!empty($payment) && !empty($success)){
                    $this->autoRegister(encode($userId));
                }    
                else
                    $this->load->getView("activate_acc", $data);    
        }
        //f29192721a8be9ad8bb0dcf52f21678ae5efd13a22262fddf67eca66ed760c5e
       
}

?>