<?php
class MY_Controller extends CI_Controller {
	function __construct() {
		parent::__construct();
		
		$allowedArr=array('login', 'register', 'activateAccount', 'forgotPass', 'patientCal','choosePlans','customerDetail' ,'successPayment', 'failurePayment', 'autoRegister', 'showCustomerDetail', 'doctorInvi');
		
		if(!in_array($this->uri->rsegments[2], $allowedArr)){ 
			$this->redirectNotLogged();
                }        
		
		$userDtl=getSession(USR_SESSION_NAME);
		define('USER_TYPE', $userDtl['type']);
		define('USER_ID', $userDtl['id']);
		define('PARENT_ID', $userDtl['parentId']);
		
		$class=$this->uri->rsegments[1];
		$func=$this->uri->rsegments[2];
		if($func!='response' && $class.$func!='patientsedit' && $class.$func!='patientsadd'){
			unset($_SESSION['pformSubmitted']);
		}
			
		$this->load->model('Common_model','common');
		$this->pdb->update("mp_users", array('lastActTime'=>currentDate()), "id=".USER_ID);
	}
	
	function isLogged() {
		if($rs=getSession(USR_SESSION_NAME)){
			return $rs;
		}
		else
			return false;
	}
	
	function loggedData() {
		return getSession(USR_SESSION_NAME);
	}
	
	function redirectLogged() { 
		if($dtl=$this->isLogged()){
			switch($dtl['type']){
				case 'S': 
					redirect(SADM_URL.'user/dashboard');
				break;
				
				case 'H':
					redirect(ADM_URL.'user/dashboard');
				break;
				
				case 'D':
					redirect(DOCT_URL.'user/dashboard');
				break;
				
				case 'P':
					redirect(PATIENT_URL.'user/dashboard');
				break;
				
				case 'T':
					redirect(URL.'faq/lists');
				break;
			}
		}
	}
	
	function redirectNotLogged() {
		if(!$this->isLogged())
			redirect(URL);
	}
      
        function sendExpireMail($plan, $diff ,$userDtl){
              $to = $userDtl['loginEmail'].",".ADMIN_EMAIL;
              if($plan=='trial'){
                  $subject = "Mypain trial";
                  $msg = "";
              }
              sendMail($data['loginEmail'], "My Pain", $superAdmDtl['supportEmail'], $subject, $msg);
        }
}
