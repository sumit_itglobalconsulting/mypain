<?php
class ParentSuperAdminController extends MY_Controller {
	function __construct() {
		parent::__construct();
		
		if(defined('USER_TYPE') && USER_TYPE!='S')
			show_404();
	}
}
