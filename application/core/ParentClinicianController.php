<?php
class ParentClinicianController extends MY_Controller {
	function __construct() {
		parent::__construct();
		
		if(defined('USER_TYPE') && USER_TYPE!='D')
			show_404();
	}
}
