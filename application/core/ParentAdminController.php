<?php
class ParentAdminController extends MY_Controller {
	function __construct() {
		parent::__construct();
		
		if(defined('USER_TYPE') && USER_TYPE!='H')
			show_404();
	}
}
