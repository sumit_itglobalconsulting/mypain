<?php
class ParentPatientController extends MY_Controller {
	function __construct() {
		parent::__construct();
		
		if(defined('USER_TYPE') && USER_TYPE!='P')
			show_404();
	}
}
