<?php
$url=base_url();
/*if(strpos($_SERVER['HTTP_HOST'], 'localhost')===false)
	$url=str_replace("http://", "https://", $url);*/
	
define('URL',					$url);
define('IMG_URL',				$url.'assets/images/');

define('SADM_URL',				URL.'superadmin/');
define('ADM_URL',				URL.'admin/');
define('DOCT_URL',				URL.'clinician/');
define('PATIENT_URL',			URL.'patient/');

define('REQ_URI', 			$_SERVER['REQUEST_URI']);

define('USR_SESSION_NAME',		'MPUser');

define('APP_VERSION', 			'1.54');
define('PAGE_SIZE',			20);

define('BLANK_DATE',			'0000-00-00 00:00:00');

define('ADMIN_EMAIL',			'info@mypain.com');
define('FREE_TRAIL_MAIL1',               10);
define('FREE_TRAIL_MAIL2',               5);
define('VAT',                           20);
?>