<?php

function euroVals($index = '') {
    $arr = array(11111 => 1.000, 11112 => 0.848, 11113 => 0.414, 11121 => 0.796, 11122 => 0.725, 11123 => 0.291, 11131 => 0.264, 11132 => 0.193, 11133 => 0.028, 11211 => 0.883, 11222 => 0.689, 11223 => 0.255, 11231 => 0.228, 11232 => 0.157, 11233 => -0.008, 11311 => 0.556, 11312 => 0.485, 11212 => 0.812, 11213 => 0.378, 11221 => 0.760, 11313 => 0.320, 11321 => 0.433, 11322 => 0.362, 11323 => 0.197, 11331 => 0.170, 11332 => 0.099, 11333 => -0.066, 12111 => 0.815, 12112 => 0.744, 12113 => 0.310, 12121 => 0.692, 12122 => 0.621, 12123 => 0.187, 12131 => 0.160, 12132 => 0.089, 12133 => -0.076, 12211 => 0.779, 12212 => 0.708, 12213 => 0.274, 12221 => 0.656, 12222 => 0.585, 12223 => 0.151, 12231 => 0.124, 12232 => 0.053, 12233 => -0.112, 12311 => 0.452, 12312 => 0.381, 12313 => 0.216, 12321 => 0.329, 12322 => 0.258, 12323 => 0.093, 12331 => 0.066, 12332 => -0.005, 12333 => -0.170, 13111 => 0.436, 13112 => 0.365, 13113 => 0.200, 13121 => 0.313, 13122 => 0.242, 13123 => 0.077, 13131 => 0.050, 13132 => -0.021, 13133 => -0.186, 13211 => 0.400, 13212 => 0.329, 13213 => 0.164, 13221 => 0.277, 13222 => 0.206, 13223 => 0.041, 13231 => 0.014, 13232 => -0.057, 13233 => -0.222, 13311 => 0.342, 13312 => 0.271, 13313 => 0.106, 13321 => 0.219, 13322 => 0.148, 13323 => -0.017, 13331 => -0.044, 13332 => -0.115, 13333 => -0.280, 21111 => 0.850, 21112 => 0.779, 21113 => 0.345, 21121 => 0.727, 21122 => 0.656, 21123 => 0.222, 21131 => 0.195, 21132 => 0.124, 21133 => -0.041, 21211 => 0.814, 21212 => 0.743, 21213 => 0.309, 21221 => 0.691, 21222 => 0.620, 21223 => 0.186, 21231 => 0.159, 21232 => 0.088, 21233 => -0.077, 21311 => 0.487, 21312 => 0.416, 21313 => 0.251, 21321 => 0.364, 21322 => 0.293, 21323 => 0.128, 21331 => 0.101, 21332 => 0.030, 21333 => -0.135, 22111 => 0.746, 22112 => 0.675, 22113 => 0.241, 22121 => 0.623, 22122 => 0.552, 22123 => 0.118, 22131 => 0.091, 22132 => 0.020, 22133 => -0.145, 22211 => 0.710, 22212 => 0.639, 22213 => 0.205, 22221 => 0.587, 22222 => 0.516, 22223 => 0.082, 22231 => 0.055, 22232 => -0.016, 22233 => -0.181, 22311 => 0.383, 22312 => 0.312, 22313 => 0.147, 22321 => 0.260, 22322 => 0.189, 22323 => 0.024, 22331 => -0.003, 22332 => -0.074, 22333 => -0.239, 23111 => 0.367, 23112 => 0.296, 23113 => 0.131, 23121 => 0.244, 23122 => 0.173, 23123 => 0.008, 23131 => -0.019, 23132 => -0.090, 23133 => -0.255, 23211 => 0.331, 23212 => 0.260, 23213 => 0.095, 23221 => 0.208, 23222 => 0.137, 23223 => -0.028, 23231 => -0.055, 23232 => -0.126, 23233 => -0.291, 23311 => 0.273, 23312 => 0.202, 23313 => 0.037, 23321 => 0.150, 23322 => 0.079, 23323 => -0.086, 23331 => -0.113, 23332 => -0.184, 23333 => -0.349, 31111 => 0.336, 31112 => 0.265, 31113 => 0.100, 31121 => 0.213, 31122 => 0.142, 31123 => -0.023, 31131 => -0.050, 31132 => -0.121, 31133 => -0.286, 31211 => 0.300, 31212 => 0.229, 31213 => 0.064, 31221 => 0.177, 31222 => 0.106, 31223 => -0.059, 31231 => -0.086, 31232 => -0.157, 31233 => -0.322, 31311 => 0.242, 31312 => 0.171, 31313 => 0.006, 31321 => 0.119, 31322 => 0.048, 31323 => -0.117, 31331 => -0.144, 31332 => -0.215, 31333 => -0.380, 32111 => 0.232, 32112 => 0.161, 32113 => -0.004, 32121 => 0.109, 32122 => 0.038, 32123 => -0.127, 32131 => -0.154, 32132 => -0.225, 32133 => -0.390, 32211 => 0.196, 32212 => 0.125, 32213 => -0.040, 32221 => 0.073, 32222 => 0.002, 32223 => -0.163, 32231 => -0.190, 32232 => -0.261, 32233 => -0.426, 32311 => 0.138, 32312 => 0.067, 32313 => -0.098, 32321 => 0.015, 32322 => -0.056, 32323 => -0.221, 32331 => -0.248, 32332 => -0.319, 32333 => -0.484, 33111 => 0.122, 33112 => 0.051, 33113 => -0.114, 33121 => -0.001, 33122 => -0.072, 33123 => -0.237, 33131 => -0.264, 33132 => -0.335, 33133 => -0.500, 33211 => 0.086, 33212 => 0.015, 33213 => -0.150, 33221 => -0.037, 33222 => -0.108, 33223 => -0.273, 33231 => -0.300, 33232 => -0.371, 33233 => -0.536, 33311 => 0.028, 33312 => -0.043, 33313 => -0.208, 33321 => -0.095, 33322 => -0.166, 33323 => -0.331, 33331 => -0.358, 33332 => -0.429, 33333 => -0.594);

    if ($index)
        return $arr[$index];

    return $arr;
}

function eqVasGoal($age = 0) {
    $goals = array('18-29' => 87.0, '30-39' => 86.2, '40-49' => 85.1, '50-59' => 81.3, '60-69' => 79.8, '70-79' => 75.3, '80' => 72.5);
    if ($age >= 18 && $age <= 29)
        $index = '18-29';
    else if ($age >= 30 && $age <= 39)
        $index = '30-39';
    else if ($age >= 40 && $age <= 49)
        $index = '40-49';
    else if ($age >= 50 && $age <= 59)
        $index = '50-59';
    else if ($age >= 60 && $age <= 69)
        $index = '60-69';
    else if ($age >= 70 && $age <= 79)
        $index = '70-79';
    else if ($age >= 80)
        $index = '80';
    else
        $index = '18-29';

    return $goals[$index];
}

function getOptionDetail($arg) {
    $dtl = array();
    $a = explode('~', $arg);
    if (!$a)
        return $dtl;
    foreach ($a as $i => $b) {
        $c = explode('|', $b);
        $dtl[$i]['optionId'] = $c[0];
        $dtl[$i]['option'] = $c[1];
        $dtl[$i]['score'] = $c[2];
        $dtl[$i]['caption'] = $c[3];
    }
    return $dtl;
}

function radioOptions($options, $qdtl, $patientDtl, $formDtl) {
    foreach ($options as $i => $opt) {
        $i = $i + 1;
        $cap = $opt['caption'] ? ". " . $opt['caption'] : "";
        echo "<div class='leftFlow1'>";
        echo "<input type='radio' name='ques[{$qdtl['id']}]' value='{$opt['optionId']}' id='qr{$opt['optionId']}' />&nbsp;";
        echo "<label for='qr{$opt['optionId']}'>{$opt['option']}{$cap}</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        echo "</div>";
        echo $i % 4 == 0 ? "<div class='clr'>&nbsp;</div>" : "";
    }
    echo "<div class='clr'>&nbsp;</div>";
}

function checkOptions($options, $qdtl, $patientDtl, $formDtl) {
    foreach ($options as $i => $opt) {
        $i = $i + 1;
        echo "<div class='leftFlow1'>";
        echo "<input type='checkbox' name='ques[{$qdtl['id']}][]' value='{$opt['optionId']}' id='qr{$opt['optionId']}' />&nbsp;";
        echo "<label for='qr{$opt['optionId']}'>{$opt['option']}</label>";
        echo "</div>";
        echo $i % 4 == 0 ? "<div class='clr'>&nbsp;</div>" : "";
    }
    echo "<div class='clr'>&nbsp;</div>";
}

function textInfoOptions($options, $qdtl, $patientDtl, $formDtl) {
    echo "<textarea name='ques[{$qdtl['id']}][]' value='{$opt['optionId']}' class='ansTa' spellcheck='false' placeholder='Write here...'></textarea>";
}

function textMsgInfoOptions($options, $qdtl) {
    echo $qdtl['infoText'];
}

function bodyOptions($options, $qdtl, $patientDtl, $formDtl) {
    $class = $patientDtl['gender'] == 'F' ? 'bodyMapFemaleBg' : 'bodyMapMaleBg';
    echo "<div class='bodyMap $class' style='border:none'>";
    echo $patientDtl['bodymap'];
    echo "</div>";
}

function slideOptions($options, $qdtl, $patientDtl, $formDtl, $apposite = false) {
    $totalOpt = count($options);
    $min = $options[0]['option'];
    $max = $options[$totalOpt - 1]['option'];

    $optionId = $qdtl['optionId'] ? $qdtl['optionId'] : $options[0]['optionId'];

    $v = $qdtl['optionName'] ? $qdtl['optionName'] : $min;
    if (strpos(REQ_URI, "formPreview") !== false)
        $v = $max;
    ?>
    <div class='posRel pad5'>
        <?php foreach ($options as $i => $opt) {
            if ($opt['caption']) {
                $opt['caption'] = str_replace('{SYMPTOM}', $formDtl['symptom'], $opt['caption']); ?>
                <div class="optCap optCap<?php echo $i; ?>"><table><tr><td><?php echo $opt['caption']; ?></td></tr></table></div>
                        <?php }
                    } ?>

        <div class="optN1">
            <div class="optNBx">
                <ul class="Q<?php echo $qdtl['id']; ?>">
                            <?php foreach ($options as $i => $opt) {
                                $i = $i + 1; ?>
                        <li class="<?php echo $i == $totalOpt - 1 ? 'lastOpt' : ''; ?> <?php echo 'L' . $i; ?>">
                            <input type="hidden" class="optId<?php echo $opt['option']; ?>" value="<?php echo $opt['optionId']; ?>" />
                            <div class="pipe"><span>|</span></div>
                            <div class="optName">
        <?php
        if ($qdtl['id'] == 9)
            echo ($opt['option'] * 10) . '%';
        else
            echo $opt['option'];
        ?>
                            </div>
                        </li>
    <?php } ?>
                </ul>
            </div>
        </div>

        <div class='quesSlide <?php if ($qdtl['id'] == 9) echo 'med'; ?>'  min='<?php echo $min; ?>' max='<?php echo $max; ?>' v='<?php echo $v; ?>' frm="1">
            <input type="hidden" class="quesOpt" name="quesOpt[<?php echo $qdtl['id']; ?>]" value="<?php echo $optionId; ?>" />
        </div>
    </div>
    <?php
}

function csqSlideOptions($options, $qdtl, $patientDtl, $formDtl, $apposite = false) {
    $totalOpt = count($options);
    $min = $options[0]['option'];
    $max = $options[$totalOpt - 1]['option'];

    $optionId = $qdtl['optionId'] ? $qdtl['optionId'] : $options[0]['optionId'];

    $v = $qdtl['optionName'] ? $qdtl['optionName'] : $min;
    if (strpos(REQ_URI, "formPreview") !== false)
        $v = $max;
    ?>
    <div class='posRel pad5'>
        <?php foreach ($options as $i => $opt) {
            if ($opt['caption']) {
                $opt['caption'] = str_replace('{SYMPTOM}', $formDtl['symptom'], $opt['caption']); ?>
                <div class="optCap optCap<?php echo $i; ?>"><table><tr><td><?php echo $opt['caption']; ?></td></tr></table></div>
                        <?php }
                    } ?>

        <div class="optN1">
            <div class="optNBx">
                <ul class="Q<?php echo $qdtl['id']; ?>">
                            <?php foreach ($options as $i => $opt) {
                                $i = $i + 1; ?>
                        <li class="<?php echo $i == $totalOpt - 1 ? 'lastOpt' : ''; ?> <?php echo 'L' . $i; ?>">
                            <input type="hidden" class="optId<?php echo $opt['option']; ?>" value="<?php echo $opt['optionId']; ?>" />
                            <div class="pipe"><span>|</span></div>
                            <div class="optName">
        <?php
        if ($qdtl['id'] == 9)
            echo ($opt['option'] * 10) . '%';
        else
            echo $opt['option'];
        ?>
                            </div>
                        </li>
    <?php } ?>
                </ul>
            </div>
        </div>

        <div class='quesSlide <?php if ($qdtl['id'] == 9) echo 'med'; ?>'  min='1' max='4' v='<?php echo $v; ?>' frm="1">
            <input type="hidden" class="quesOpt" name="quesOpt[<?php echo $qdtl['id']; ?>]" value="<?php echo $optionId; ?>" />
        </div>
    </div>
    <?php
}


function slideOptions2ndForm($options, $qdtl, $patientDtl, $formDtl) {
    $totalOpt = count($options);
    $min = $options[0]['option'];
    $max = $options[$totalOpt - 1]['option'];

    //$optionId=$qdtl['optionId']?$qdtl['optionId']:$options[0]['optionId'];
    $optionId = $qdtl['optionId'] ? $qdtl['optionId'] : 196;

    $v = $qdtl['optionName'] ? $qdtl['optionName'] : 0;
    if (strpos(REQ_URI, "formPreview") !== false)
        $v = $max;
    ?>
    <div class='posRel pad5'>
                    <?php foreach ($options as $i => $opt) {
                        if ($opt['caption']) {
                            $opt['caption'] = str_replace('{SYMPTOM}', $formDtl['symptom'], $opt['caption']); ?>
                <div class="optCapS2 optCap2<?php echo $i; ?>"><table><tr><td><?php echo $opt['caption']; ?></td></tr></table></div>
        <?php }
    } ?>

        <div class="optN2">
            <div class="optNBx2">
                <ul>
    <?php foreach ($options as $i => $opt) {
        $i = $i + 1; ?>
                        <input type="hidden" class="optId<?php echo $opt['option']; ?>" value="<?php echo $opt['optionId']; ?>" />
                        <li class="<?php echo $i == $totalOpt - 1 ? 'lastOpt' : ''; ?> <?php echo 'L' . $i; ?>">
                            <div class="pipe"><span>|</span></div>
                            <div><?php echo $opt['option']; ?></div>
                        </li>
    <?php } ?>
                </ul>
            </div>
        </div>

        <div class='quesSlide2' min='<?php echo $min; ?>' max='<?php echo $max; ?>' v='<?php echo $v; ?>' frm="2">
            <input type="hidden" class="quesOpt" name="quesOpt[<?php echo $qdtl['id']; ?>]" value="<?php echo $optionId; ?>" />
        </div>
    </div>
    <?php
}
?>