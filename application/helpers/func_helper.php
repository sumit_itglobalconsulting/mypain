<?php

function pdfcrowd() {
    require 'assets/pdfc/pdfcrowd.php';
    return new Pdfcrowd("mypainltd", "5a8a5c0e862260bab5b9a9ad9ff9b0e4");
}

function monthsArr($first = '') {
    $m = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sept', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
    return $m;
}

function daysArr() {
    return array('mon' => 'Monday', 'tue' => 'Tuesday', 'wed' => 'Wednesday', 'thu' => 'Thursday', 'fri' => 'Friday', 'sat' => 'Saturday', 'sun' => 'Sunday');
}

function embedPdf($url, $h = "800px") {
    $pdf = "<object data='$url' type='application/pdf' width='100%' height='$h'>";
    $pdf.="<p>It appears you don't have a PDF plugin for this browser. No biggie... you can click <a href='$url'></a> to download the PDF file.";
    $pdf.="</object>";

    return $pdf;
}

function checkImageExt($filename) {
    $validExt = array('jpg', 'jpeg', 'png', 'gif');
    $ext = getExt($filename);
    if (in_array(strtolower($ext), $validExt))
        return true;
    else
        return false;
}

function currentDate() {
    return date('Y-m-d H:i:s');
}

function questionTypesArr() {
    return array(1 => 'Radio', 2 => 'Radio (2nd Type)', 3 => 'Check Box', 4 => 'Mark on body map', 5 => 'Text Answer', 6 => 'File');
}

function planTypeArr() {
    return array('P' => 'Paid', 'F' => 'Free');
}

function currencyArr() {
    return array('GBP' => 'GBP (&pound;)', 'USD' => 'USD ($)');
}

function reportTrackingTypeArr() {
    return array('A' => 'All', 'D' => 'Daily', 'W' => 'Weekly', 'M' => 'Monthly', 'Y' => 'Yearly');
}

function loginUserOptArr() {
    return array('M' => 'Multiple', 'S' => 'Single');
}

function loginUserLimitOptArr() {
    return array(5 => 'Up to 5', 10 => 'Up to 10', 50 => 'Up to 50', 100 => 'Up to 100');
}

function durationTypeArr() {
    return array('Y' => 'Year', 'M' => 'Month', 'D' => 'Day');
}

function dataUsageArr() {
    return array(102400000 => 'Unlimited', 102400 => '100 MB', 512000 => '500 MB', 1024000 => '1 GB');
}

function treatmentOptionsArr() {
    return array(1 => 'Interventional Pain Procedure', 2 => 'Medicines for Managing Chronic Pain', 3 => 'Physiotherapy',
        4 => 'Psychological Therapy for Chronic Pain');
}

function invAnaArr() {
    return array('TI' => 'Total Invitations sent', 'IM' => 'Invitations this month', 'RD' => 'Responses due', 'RT' => 'Response recieved today',
        'RW' => 'Response recieved this week', 'RM' => 'Response recieved this month');
}

function followUpTypesArr() {
    return array(1 => 'Internet', 2 => 'Telephone', 3 => 'Face to Face', 4 => 'Video Conferencing');
}

function followUpFrequencyArr() {
    return array('O' => 'Once Only', 'W' => 'Every Week', '2W' => 'Every 2 Weeks', '3W' => 'Every 3 Weeks', 'M' => 'Every Month', '2M' => 'Every 2 Months'
        , '3M' => 'Every 3 Months', '4M' => 'Every 4 Months', '5M' => 'Every 5 Months', '6M' => 'Every 6 Months');
}

function responseFrequencyArr() {
    return array('O' => 'Once Only', 'W' => 'Every Week', '2W' => 'Every 2 Weeks', '3W' => 'Every 3 Weeks', 'M' => 'Every Month');
}

function stdCodesArr() {
    return array('+44' => '+44', '+91' => '+91');
}

function weightageArr() {
    $arr = array();
    for ($i = 1; $i <= 100; $i++) {
        $arr[$i] = $i . ' %';
    }
    return $arr;
}

function msgCats() {
    return array(1 => 'Change in Condition', 2 => 'Medication Advice', 3 => 'Appointment Query', 4 => 'Waiting List Query', 5 => 'Technical Problem', 6 => 'Other');
}

function loggedUserData() {
    return getSession(USR_SESSION_NAME);
}

function getNextDate($d, $type) {
    $freq = array('O' => '+0 days', 'W' => '+7 days', '2W' => '+14 days', '3W' => '+21 days', 'M' => '+1 months', '2M' => '+2 months', '3M' => '+3 months',
        '4M' => '+4 months', '5M' => '+5 months');

    for ($i = 1; $i <= 52; $i++) {
        $d = timeStamp(strtotime($freq[$type], strtotime($d)));
        if (strtotime($d) > time())
            break;
    }

    return $d;
}

/** Form's Related Functions * */
function isTodayOrBefore($ddate) {
    if (!$ddate)
        return false;

    if (strtotime($ddate) <= time())
        return true;
    return false;
}

function isFormSubmited($userId = USER_ID, $formId = 0) {
    $ci = &get_instance();
    return $ci->common->isFormSubmited($userId, $formId);
}

/** Calculated Staticaly * */
function formTotalScore() {
    $ci = &get_instance();
    $sc = $ci->pdb->select("mp_forms", "1", "id, totalScore");
    $sc = multiArrToKeyValue($sc, "id", "totalScore");
    return $sc;
}

function formRealTotalScore() {
    $ci = &get_instance();
    $sc = $ci->pdb->select("mp_forms", "1", "id, realTotalScore");
    $sc = multiArrToKeyValue($sc, "id", "realTotalScore");
    return $sc;
}

function formColor($formId) {
    $f = array(1 => '#3366cc', 2 => '#dc3912', 3 => '#ff9900', 4 => '#109618', 5 => '#990099', 6 => '#0099c6');
    return $f[$formId];
}

function formName($formId) {
    $n = array(1 => 'BPI', 2 => 'PGIC', 3 => 'DAPOS', 4 => 'PHQ-9', 5 => 'PSEQ', 6 => 'Start Back', 7 => 'PGHE', 8 => 'CSQ-8', 9 => 'GPPAQ', 10 => 'HURT',
        11 => 'CRSFS', 12 => 'FSFI', 13 => 'PCS', 14 => 'PDI', 15 => 'PFDI', 16 => 'NDI', 17 => 'RMDQ', 18 => 'PFIQ', 19 => 'EuroQol');
    return $n[$formId];
}

function overallScoreNdFeels($userId) {
    $ci = &get_instance();
    $data['score'] = $ci->common->getOverallScore($userId);
    $data['feels'] = $ci->common->getFeelScore($userId);
    return $data;
}

function getFeels($userId) {
    $ci = &get_instance();
    $feels = $ci->common->getFeelScore($userId);
    return $feels;
}

/** * */
function countUnreadMsg() {
    $ci = &get_instance();
    $ci->load->model('Message_model', 'msg');
    return $ci->msg->countUnreadMsg();
}

/** * */
function tmpSendMail($to, $fromname, $fromemail, $subject, $message) {
    if (!$fromemail)
        $fromemail = ADMIN_EMAIL;

    if (!$fromname)
        $fromname = "My Pain Impact";

    $data['to'] = $to;
    $data['fromname'] = $fromname;
    $data['fromemail'] = $fromemail;
    $data['subject'] = $subject;
    $data['message'] = $message;
    getCurl("http://wackybrain.co/work/sat/mail.php", $data);
    return;
}

/** Scale Report * */
function scalesReport($formId = 0, $doctorId = 0, $userId = 0) {
    $ci = &get_instance();
    return $ci->common->scalesReport($formId, $doctorId, $userId);
}

function formScales($formId = 0, $userId = 0, $doctId = 0) {
    $ci = &get_instance();
    return $ci->common->formScales($formId, $userId, $doctId);
}

/** * */
function patientDueForms($userId = 0) {
    $ci = &get_instance();
    $ci->load->model("comm");
    return $ci->comm->patientDueForms($userId);
}

function doctorInvitationDue($userId = 0){
    $ci = &get_instance();
    $ci->load->model("comm");
    return $ci->comm->doctorInvitationDue($userId);
}

function doctorInvitationApprove($userId = 0){
    $ci = &get_instance();
    $ci->load->model("comm");
    return $ci->comm->doctorInvitationApprove($userId);
}

function followUpDue($doctorId = 0) {
    $ci = &get_instance();
    $ci->load->model("comm");
    return $ci->comm->followUpDue($doctorId);
}

function resDue($doctorId = 0) {
    $ci = &get_instance();
    $ci->load->model("comm");
    return $ci->comm->resDue($doctorId);
}

function userCategoryList() {
    return array('A' => 'All', 'H' => 'Hospital', 'D' => 'Clinician', 'P' => 'Patient');
}

function dateFormat($date) {
    return date('d M Y H:i A', strtotime($date));
}

/* * */

function sendJson($payload = array(), $status = 'F', $msg = '', $code = '', $extraParameter = array()) {
    if (!$payload)
        $payload = array();

    echo json_encode(array('Payload' => $payload, 'Status' => $status, 'Message' => $msg, 'Code' => $code));
    die;
}

function getFormAvail($planId = 0) {
    $ci = &get_instance();
    return $ci->common->getFormAvail($planId);
}

?>