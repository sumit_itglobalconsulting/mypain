<?php
function setFlash($msg) {
	$ci =&get_instance();
	$ci->session->set_flashdata('tmp_flash',$msg);
}

function getFlash() {
	$ci =&get_instance();
	$msg=$ci->session->flashdata('tmp_flash');
	return $msg;
}

function encryptText($txt) {
	return hash_hmac('sha256', $txt, 'app[007]');
}

function error_msg($errors, $arg) {
	if(isset($errors[$arg]))
		echo $errors[$arg];
}

function set_error_class($errors, $class) {
	if(is_array($errors)){
		echo "<script>";
		$ids="";
		foreach($errors as $k=>$v){
			if($v) $ids.="#$k,";
		}
		if($ids){
			$ids=substr($ids, 0, -1);
			echo "jQuery('$ids').addClass('$class');";
		}
		echo "</script>";
	}
}

function replaceSessionSlash($arg){
	if(is_array($arg)) {
		foreach($arg as &$v){
			if(!is_array($v))
				$v=str_replace('\\', '{{slash}}', $v);
		}
	}
	else{
		$arg=str_replace('\\', '{{slash}}', $arg);
	}
	return $arg;
}

function undoSessionSlash($arg){
	if(is_array($arg)) {
		foreach($arg as &$v){
			if(!is_array($v))
				$v=str_replace('{{slash}}', '\\' , $v);
		}
	}
	else{
		$arg=str_replace('{{slash}}', '\\', $arg);
	}
	return $arg;
}

function arrayTrim($arr){
	foreach($arr as &$v){
		if(!is_array($v))
			$v=trim($v);
	}
	return $arr;
}

function isArrayEmpty($arr) {
	if(is_array($arr)){
		if(!$arr)
			return true;
		else{
			foreach($arr as $v){
				if(trim($v)){
					return false;
				}
			}
		}
	}
	return true;
}

function arrayUrlEncode($arr){
	foreach($arr as &$v){
		if(!is_array($v))
			$v=urlencode($v);
	}
	return $arr;
}

function arrayUrlDecode($arr){
	foreach($arr as &$v){
		if(!is_array($v))
			$v=urldecode($v);
	}
	return $arr;
}

function encode($arg) {
	return base64_encode($arg);
}

function decode($arg) {
	return base64_decode($arg);
}

function pageNotFound() {
	$CI =&get_instance();
	$CI->load->view('layouts/error404');
	echo $CI->output->get_output();
	exit;
}
	
/** Misc **/
function setCheckChecked($val, $compare) {
	if($val==$compare)
		echo 'checked="checked"';
}

function multiArrToKeyValue($arr, $key, $val, $decodeKey=false) {
	$ar=array();
	if($arr && is_array($arr)){
            foreach($arr as $d){
                    if($d[$key]){
                            if($decodeKey)
                                    $d[$key]=encode($d[$key]);

                            $ar[$d[$key]]=$d[$val];
                    }
                    else
                            $ar[]=$d[$val];
            }
	}
	return $ar;
}

function pr($data) {
	echo '<pre>';print_r($data);echo '</pre>';
}

function shortString($string='',$len=0) {
	$string=strip_tags($string);
	$tmp=substr($string,0,$len);
	if(strlen($string)<=$len) {
		return $string;
	}
	return $tmp.((strlen($string)<=$len)?'':'...');
}

function getExt($file_name='') {
	return substr($file_name,strrpos($file_name,'.')+1,strlen($file_name)-(strrpos($file_name,'.')+1));
}

function appendFilename($fn='',$appendTxt='') {
	$ext=getExt($fn);
	$n=str_replace(".".$ext,"",$fn);
	return $n.$appendTxt.".".$ext;
}

function isDateBlank($date){
	$date=trim($date);
	if(!$date || $date=="" || $date=="0000-00-00 00:00:00" || $date=="0000-00-00")
		return true;
}

function showDate($timestamp=false,$long=false,$sufix=false) {
	if($timestamp) {
		if(!is_numeric($timestamp) && isDateBlank($timestamp))
			return;
			
		if(!is_numeric($timestamp)) 
			$timestamp=strtotime($timestamp);
		
		if($sufix)
			$df='dS M Y';
		else
			$df='d M Y';
			
		if($long)
			return date($df.' - h:i A',$timestamp);
		else
			return date($df,$timestamp);	
	}	
}

function h($data) {
	return htmlspecialchars($data);
}

function addSlash($data) {
	if(is_array($data))	{
		$inf=array();
		foreach($data as $field=>$val) {
			if(!is_array($val))
				$inf[$field]=addslashes($val);
			else
				$inf[$field]=$val;
		}
	}
	else
		$inf=addslashes($data);
	
	return $inf;
}

function stripSlash($data) {
	if(is_array($data))	{
		$inf=array();
		foreach($data as $field=>$val) {
			if(!is_array($val))
				$inf[$field]=stripslashes($val);
			else
				$inf[$field]=$val;
		}
	}
	else
		$inf=stripslashes($data);
	
	return $inf;
}

function countAr($arr) {
	if($arr)
		return count($arr);
	else
		return 0;
}

function gotoPage($url) {
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		echo '<script type="text/javascript">location.href="'.$url.'"</script>';
		exit();
	}
	
	if(headers_sent())
		echo '<script type="text/javascript">location.href="'.$url.'"</script>';
	else
		header('Location: '.$url);
		
	exit();
}

function isAjax() {
	return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function getLatLong($address) {
	$address = str_replace(" ","+",$address);
	$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$response = curl_exec($ch);
	curl_close($ch);
	$response_a = json_decode($response);
	$lat = $response_a->results[0]->geometry->location->lat;
	$long = $response_a->results[0]->geometry->location->lng;
	
	return array("Lat"=>$lat,"Long"=>$long);
}


function js_array($array,$f) {
  $temp=array();
  foreach ($array as $value)  {
	if($f)
  		$temp[] = '"'.addcslashes($value[$f], "\0..\37\"\\").'"';
	else
		$temp[] = '"'.addcslashes($value, "\0..\37\"\\").'"';
  }
  
  return '['.implode(',', $temp).']';
}


function replaceForUrl($arg,$repW=0) {
	if(!$repW) $repW='-';
	
	$arg=trim($arg);
	
	$arg=str_replace(' - ','-',$arg);
	$arg=str_replace('- ','-',$arg);
	$arg=str_replace(' -','-',$arg);
	$arg=str_replace(' ? ','',$arg);
	$arg=str_replace('? ','',$arg);
	$arg=str_replace(' ?','',$arg);
	$arg=str_replace('?','',$arg);
	
	$arg=str_replace(' + ',$repW,$arg);
	$arg=str_replace('+ ',$repW,$arg);
	$arg=str_replace(' +',$repW,$arg);
	$arg=str_replace('+',$repW,$arg);
	
	$arg=str_replace(' & ',$repW,$arg);
	$arg=str_replace('& ',$repW,$arg);
	$arg=str_replace(' &',$repW,$arg);
	$arg=str_replace('&',$repW,$arg);
	
	$arg=str_replace(' : ',$repW,$arg);
	$arg=str_replace(': ',$repW,$arg);
	$arg=str_replace(' :',$repW,$arg);
	$arg=str_replace(':',$repW,$arg);
	
	$arg=str_replace('/',$repW,str_replace('\\',$repW,str_replace(' ',$repW,$arg)));
	
	return $arg;
}

function replaceSpChar($arg,$repW=0) {
	if(!$repW) $repW='-';
	$arg=preg_replace('/[^a-zA-Z0-9_\-]/',$repW,$arg);
	return $arg;
}

function encodeHtml($arg) {
	$arg=str_replace('<','&lt;',$arg);
	return str_replace('>','&gt;',$arg);
}

function encodeScript($arg) {
	$arg=str_replace('<script>','&lt;script&gt;',$arg);
	return str_replace('</script>','&lt;/script&gt;',$arg);
}

function setCss($id,$class,$errMsg) {
	if($errMsg)	{
		echo '<script>jQuery("#'.$id.'").addClass("'.$class.'")</script>';
	}
}

function removeCss($id,$class) {
	echo '<script>jQuery("#'.$id.'").removeClass("'.$class.'")</script>';
}

function numbersArr($from,$to,$leadzero=0,$sufix=0) {
	$ar=array();
	$sf="";
	if($sufix) $sf=$sufix;
	for($i=$from; $i<=$to; $i++) {
		if($leadzero && $i<10)
			$n="0".$i;
		else
			$n=$i;
		$ar[$i]=$n.$sf;
	}
	return $ar;
}

function delFile($file) {
	if(file_exists($file)) {
		unlink($file);
	}
}

function renameFileIfExist($path,$file) {
	if(file_exists($path.$file)) {
		$file=appendFilename($file,'-'.time());
	}
	return $file;
}


function xml2array($xml) { 
    $get = file_get_contents($xml);
	$array = simplexml_load_string($get);
	return $array; 
}

function isEmail($email) {
	if(filter_var($email, FILTER_VALIDATE_EMAIL))
		return true;
	else
		return false;
}


function timeStamp($T) {
	if(!$T)
		return '';
		
	if(!is_numeric($T) && isDateBlank($T))
		return '';
		
	if(!is_numeric($T)) 
		$T=strtotime($T);
	return date('Y-m-d H:i:s', $T);
}

function as_($arg) {
		return addslashes($arg);
	}
	
function ss_($arg) {
	return stripslashes($arg);
}

/** Paging **/
function pagingLinks($data,$url,$activeClass='',$sep='') {
	if(!$data)
		return;
		
	$start=$data['start'];
	$total_pages=$data['total_pages'];
	$cur_page=$data['cur_page'];
	
	$pages=array();
	if($total_pages>1) {
		$qs="";
		if($_GET) {
			foreach($_GET as $k=>$v) {
				if(trim($v))
					$qs.=$k."=".$v."&";
			}
		}
		
		if(!empty($qs))
			$qs='/?'.substr($qs,0,-1);
		
		for($i=1;$i<=$total_pages;$i++)	{
			if($cur_page==$i)	
				$link = '<a href="'.$url.'/'.$i.$qs.'" class="'.$activeClass.'">'.$i.'</a>';
			else
				$link = '<a href="'.$url.'/'.$i.$qs.'">'.$i.'</a>';
			
			echo $link;
				
			if($sep && $i<$total_pages)
				echo ''.$sep.'';
		}
	}
}

function pagingLinksLI($data,$url,$activeClass='',$sep='') {
	if(!$data)
		return;
		
	$start=$data['start'];
	$total_pages=$data['total_pages'];
	$cur_page=$data['cur_page'];
	
	$pages=array();
	if($total_pages>1) {
		$qs="";
		if($_GET) {
			foreach($_GET as $k=>$v) {
				if(trim($v))
					$qs.=$k."=".$v."&";
			}
		}
		
		if(!empty($qs))
			$qs='/?'.substr($qs,0,-1);
		
		for($i=1;$i<=$total_pages;$i++)	{
			if($cur_page==$i)	
				$link = '<li class="'.$activeClass.'"><a href="'.$url.'/'.$i.$qs.'">'.$i.'</a></li>';
			else
				$link = '<li><a href="'.$url.'/'.$i.$qs.'">'.$i.'</a></li>';
			
			echo $link;
				
			if($sep && $i<$total_pages)
				echo ''.$sep.'';
		}
	}
}

function pagingNumbers($data,$url) {
	$start=$data['start'];
	$total_pages=$data['total_pages'];
	$cur_page=$data['cur_page'];
	$pages=array();
	if($total_pages>1) {
		$qs="";
		if($_GET) {
			foreach($_GET as $k=>$v) {
				if(trim($v))
					$qs.=$k."=".$v."&";
			}
		}
		
		if(!empty($qs))
			$qs='/?'.substr($qs,0,-1);
		
		for($i=1;$i<=$total_pages;$i++)	{
			$pages[$i]=$url.'/'.$i.$qs;
		}
	}
	
	return $pages;
}

/** **/
function deleteFilesFromFolder($dir) {
	error_reporting(0);
    foreach(scandir($dir) as $file) {
        if ('.' === $file || '..' === $file) continue;
        if (is_dir("$dir/$file")) deleteFilesFromFolder("$dir/$file");
        else unlink("$dir/$file");
    }
	//rmdir($dir); //it will delete folders too.
}

function getCurl($url, $data=null) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, true); 
	
	
	//curl_setopt($ch, CURLOPT_PORT , 80); 
	//curl_setopt($ch, CURLOPT_SSLVERSION,3);
	//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	$output = curl_exec($ch);
	
	//$info = curl_getinfo($ch);
	curl_close($ch);
	
	return $output;
}

/** Arrays Functions **/
function arrayFilter($array, $index, $value){ 
	$newarray=array();
	if(is_array($array) && count($array)>0)	{ 
		foreach(array_keys($array) as $key){ 
			$temp[$key] = $array[$key][$index]; 
			
			if ($temp[$key] == $value){ 
				$newarray[$key] = $array[$key]; 
			} 
		} 
	} 
	return $newarray; 
}

/** Push Notification for IOS **/
function pushNotification($deviceToken, $message) {
	error_reporting(0);
	$deviceToken=trim($deviceToken);
	if(!$deviceToken)
		return;
		
	//$passphrase = 'push';
	$passphrase = '';
	//echo $deviceToken.'<br>'.$message;
	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', 'assets/pem/devmypain.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

	$fp = stream_socket_client(
	'ssl://gateway.sandbox.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	
	$body['aps'] = array(
	'alert' => $message,
	'sound' => 'default'
	);

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));
	//echo $deviceToken;
	if (!$result){//echo 'fail'; die;
		fclose($fp);
		return 'Message not delivered' . PHP_EOL;
	}
	else{//echo 'success'; die;
		fclose($fp);
		return 'Message successfully delivered' . PHP_EOL;
	}

	//echo json_encode($result;);
	fclose($fp);
}

/** **/
function sendMail($to, $fromname, $fromemail, $subject, $message) {
	if(!$fromname)
		$fromname="My Pain Impact";
		
	$fromemail="info@mypainimpact.com";
	
	$CI =& get_instance();
        $CI->load->library('email');
	$mail=$CI->email;
	
	$config['charset'] = 'utf-8';
	$config['wordwrap'] = TRUE;
	$config['mailtype'] = 'html';
	$mail->initialize($config);

	$mail->from($fromemail, $fromname);
	$mail->to($to);
	$mail->reply_to('no-reply@mypainimpact.com', 'My Pain');
	
	$mail->subject($subject);
	$mail->message($message);
	
	return $mail->send();
}

function setSession($sname, $val) {
	$_SESSION[$sname]=$val;
}

function getSession($sname) { 
	return $_SESSION[$sname];
}

function unsetSession($sname) {
	unset($_SESSION[$sname]);
}

function destroySession($sname) {
	session_destroy();
}

function allowPatAdd(){ 
    $ci =&get_instance();
    $ci->load->model('Common_model','common');
    return $ci->common->allowPatAdd();
}
function noOfAddedPat(){
    $ci = &get_instance();
    $ci->load->model('Common_model','common');
    return $ci->common->noOfAddedPat();
}
function getPlanDetail($planId){
    $ci = &get_instance();
    $ci->load->model('Common_model','common');
    return $ci->common->getPlanDetail($planId);
}
function showCountry(){
    return array(''=>'Please Select','GB'=>'United Kingdom','US'=>'United States');
}
function getMsgOtherReply($id=0){
    if(empty($id))
        return ;
    else{
        $ci = &get_instance();
        $ci->load->model('Common_model','common');
        return $ci->common->getMsgOtherReply($id);
    }
}

function multiArrToKeyValueAssignedForms($arr, $key, $val, $decodeKey=false) {
	$ar=array();
	if($arr && is_array($arr)){
            foreach($arr as $d){
                    if($d[$key]){
                            if($decodeKey)
                                    $d[$key]=encode($d[$key]);

                            $ar[$d[$key]]=$d[$val]."_".$d['doctorId'];
                    }
                    else
                            $ar[]=$d[$val];
            }
	}
	return $ar;
}
?>