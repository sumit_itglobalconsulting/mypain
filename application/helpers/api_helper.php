<?php
	function formInfo($formId=0){
		$text=array(
			1=>'BPI &copy; Dr. Charles Cleeland (1991)<br><h3>Pain intensity scale</h3> Q[1-5] <h3>Pain interference scale</h3>Select the response that describes how, <strong>during the past 24 hours</strong>, pain has interfered with Q[6-12]',
			
			2=>'<h3>Chief Complaint: SYMPTOM</h3>Since beginning treatment at this clinic, how would you describe the change (if any) in ACTIVITY LIMITATIONS, SYMPTOMS, EMOTIONS, and OVERALL QUALITY OF LIFE, related to your painful condition?<br><br>Please select the response below, that matches your degree of change since beginning care at this clinic for the above stated chief complaint.', 
			
			3=>'&copy; Royal Holloway, University of London, 2004 <br>We would like to know how you have been feeling in the <strong>last few weeks.</strong> Please select an option for each statement indicating how often you feel that way.',
			
                        4=>'Developed by Drs. Robert L. Spitzer, Janet B.W. Williams, Kurt Kroenke and colleagues, with an educational grant from Pfizer Inc. No permission required to reproduce, translate, display or distribute <br>Over the <strong>last 2 weeks,</strong> how often have you been bothered by any of the following problems?',
			
			5=>'&copy; M.K.Nicholas (1989) <br>Please rate how confident you are that you can do the following things at present, despite the pain. To indicate your answer select one of the numbers on the scale under each item, where 0 = not at all confident and 6 = completely confident.<br><br>
Remember, this questionnaire is not asking whether or not you have been doing these things, but rather how confident you are that you can do them at present, despite the pain.',

			6=>'&copy; Keele University Funded by Arthritis Research UK <br>Thinking about the <strong>last 2 weeks</strong> select your response to the following questions',
			
			7=>'&copy; Dr Rahul Seewal <br>',
			
			8=>'CSQ Scales &copy; 2012 Clifford Attkisson, Ph.D. All Rights Reserved<br> Please help us improve our program by answering some questions about the services you have received. We are interested in your honest opinions, whether they are positive or negative. Please answer all of the questions. We also welcome your comments and suggestions. Thank you very much; we really appreciate your help.',
			
			9=>'',
			
			10=>'www.l-t-b.org © Lifting The Burden<br> Lifting The Burden <strong>Please answer these questions carefully.</strong><br>Please tick ONE option in each row.',
			
			11=>'The CRSFS is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License. (http://creativecommons.org/licenses/by-nc-nd/3.0/) <br>Please selct the frequency with which you have felt the following symptoms for the last 2 weeks',
			
			12=>'© 2000 From Rosen R, et al. The Female Sexual Function Index (FSFI): A Multidimensional Self-Report Instrument for the Assessment of Female Sexual Function. Journal of Sex and Marital Therapy. 2000;26(2):191-208. Reprinted by permission of the copyright holder. <br>These questions ask about your sexual feelings and responses.<br><br>During the <strong>past 4 weeks</strong>. Please answer the following questions as honestly and clearly as possible. Your responses will be kept completely confidential.<br><br>In answering these questions the following definitions apply:<br><br>
				1. Sexual activity can include caressing, foreplay, masturbation and vaginal intercourse.<br>
				2. Sexual intercourse is defined as penile penetration (entry) of the vagina.<br>
				3. Sexual stimulation includes situations like foreplay with a partner, self-stimulation (masturbation), or sexual fantasy.',
				
			13=>'© Dr Michael JL Sullivan 1995 <br>Everyone experiences painful situations at some point in their lives. Such experiences include headache, tooth pain, joint or muscle pain.<br>
				People are often exposed to situations that may cause pain,  such as illness, injury, dental procedure or surgery.<br>
				We are interested in the types of thoughts and feelings that you have when you are in pain. Listed below are thirteen statements describing different thoughts and 
    feelings that may be asociated with pain.<br><br>Using the following scale, please indicate the degree to which you have these thoughts and feelings when you are 
	experiencing pain.',
			
			14=>'Adapted from ￼Tait, Chibnall & Krause. Pain (40) 1990; 171-182<br>Pain Disability Index (PDI) is used to assess the degree to which chronic pain interferes with various daily activities.<br><br>For each of the 7 categories of life activity Listed, please select the number on the scale which describes the level of disability you typically experience. A score of 0 means no disability at all, and a score of 10 signifies that all of the activities in which you would normally be involved have been totally 
    disrupted or prevented by your pain.',
	
			15=>'Barber M, Walters M et al (2005) "Short forms of two condition specific quality of life questionnaires for womenwith pelvic disorders (PFDI - 20 and PFIQ - 7). American Journal of Obstetrics and Gyenecology; 193:103-113 <br>Please answer all questions. These questions will ask you if you have certain bowel, bladder, or pelvic symptoms and if you do, how much do they bother you?
Answer these by selecting the appropriate number. While answering, please consider your symptoms over the <strong>last 3 months</strong><h3>Pelvic Organ Prolapse Distress Inventory 6 (POPDI - 6)</h3> Q[1-6] <h3>Colorectal-Anal Distress Inventory 8 (CRAD-8)</h3> Q[7-14] <h3>Urinary Distress Inventory 6 (UDI-6)</h3> Q[15-20]',

			16=>'© Vernon, H. & Moir, S. (1991). The neck Disability Index: A study of reliability and validity. Journal of Manipulative and Physiological therapeutics. 14, 409-415. <br>This questionnaire has been designed to give us information as to how your neck pain has affected your ability to manage in everyday life.<br><br>
				<strong>Please answer every section and select only one response in each section that most closely describes your problem.</strong>',
				
			17=>'&copy; Roland MO, Morris RW. A study of the natural history of back pain. Part 1: Development of a reliable and sensitive measure of disability in low back pain. Spine 1983; 8: 141-144<br><br>When your back hurts, you may find it difficult to do some of the things you normally do.<br><br>This list contains sentences that people have used to 
				describe themselves when they have back pain. When you read them, you may find that some stand out because they describe you today, please select that.<br>
				As you read the list, think of yourself today. If a sentence does not describe you, then leave the space blank and go on to the next one.  Remember, 
    only select the sentence if you are sure it describes you today.',
	
			18=>'© Barber , M. D., Walters, M. D., & Bump, R. C. (2005). Short forms of two condition-specific quality-of-life questionnaires for women with pelvic floor disorders (PFDI-20 and PFIQ-7) . American Journal of Obstetrics & Gynecology , 193, 103-13. <br>Some women find that bladder, bowel or vaginal symptoms affect their activities, relatioships and feelings. For each question, select the response that best 
        describes how much your activities, relatioships or feeling s have been affected by your bladder, bowel or vaginal symptoms or conditions over that 
        <strong>last 3 months</strong>.',
		
			19=>'<strong>Please select the statements that best describe your own health state today:</strong>'
		);
		
		return $text[$formId];
	}
	
	
    function formatQuesArr($ques, $formId){
		$mixQues=array(73, 77, 80, 81, 85, 88);
		
        foreach($ques as $k=>&$q){
            $a=explode("~", $q['optionDtl']);
            if($q['type']==1)
                $q['type']='Radio';
            else if($q['type']==2)
                $q['type']='Slider';
			else if($q['type']==3)
				$q['type']='Checkbox';
			else if($q['type']==5)
                $q['type']='Text';
			else if($q['type']==6)
                $q['type']='Image';
			
			if($formId==19)
				$q['type']='Radio';
			if($q['questionId']==314)
				$q['type']='Slider';
			
			if($formId==17){
				$q['type']='Checkbox';
				$q['check']='Single';
			}
			else if($q['type']==3)
				$q['check']='Multiple';
				
			if($formId==11 || $formId==12 || $formId==15 || $formId==16)
				$q['type']='Radio';
				
			if(in_array($q['questionId'], $mixQues))
				$q['type']='Mix';
			
			if($formId==15)
				$q['question']="Do you ".$q['question'];
				
			if($formId==18)
				$q['question']="Usually affect your ".$q['question'];
				
			if($q['questionId']>=109 && $q['questionId']<=113)
				$q['question']="During the last week, how many hours did you spend on ".$q['question']; 
				
            
            $q['options']=array();
			
			$capsForm11=array('Never/ Once', 'Several days', 'More than half of the days', 'Almost everyday');
            foreach($a as $i=>$b){
                $c=explode("|", $b);
                $q['options'][$i]['optionId']=$c[0];
                if($q['questionId']==9)
                    $q['options'][$i]['optionName']=($c[1]*10).'%';
                else
                   $q['options'][$i]['optionName']=$c[1];
                
                $q['options'][$i]['caption']=$c[3];
				
				if($formId==7)
					$q['options'][$i]['caption']=$q['options'][$i]['optionName'];
					
				if($formId==11)
					$q['options'][$i]['optionName']=$capsForm11[$i];
					
				if($formId==12 || $formId==15 || $formId==16){
					$q['options'][$i]['optionName']=$q['options'][$i]['caption'];
					$q['options'][$i]['caption']='';
				}
				
				if($formId==17)
					$q['options'][$i]['optionName']='Yes';
            }
            
            unset($q['optionDtl']);
        }
        return $ques;
    }
?>